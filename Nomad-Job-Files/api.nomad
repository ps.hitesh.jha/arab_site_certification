job "mpay-api" {
    datacenters = ["dc1"]

 

 update {
    # max_parallel      = 1

 

    # The "canary" parameter specifies that changes to the job that would result
    # in destructive updates should create the specified number of canaries
    # without stopping any previous allocations. Once the operator determines the
    # canaries are healthy, they can be promoted which unblocks a rolling update
    # of the remaining allocations at a rate of "max_parallel".
    #
    # Further, setting "canary" equal to the count of the task group allows
    # blue/green deployments. When the job is updated, a full set of the new
    # version is deployed and upon promotion the old version is stopped.
    # canary            = 1

 

    #health_check Specifies the mechanism in which allocations health is determined. 
    #"checks" - Specifies that the allocation should be considered healthy 
    #when all of its tasks are running and their associated checks are healthy, 
    #and unhealthy if any of the tasks fail or not all checks become healthy.
    # health_check      = "checks"
    # min_healthy_time  = "10s"
    
    # The "healthy_deadline" parameter specifies the deadline in which the
    # allocation must be marked as healthy after which the allocation is
    # automatically transitioned to unhealthy.Transitioning to unhealthy will
    # fail the deployment and potentially roll back the job if "auto_revert" is
    # set to true.
    healthy_deadline  = "5m"
    auto_revert = false

 

    # The "progress_deadline" parameter specifies the deadline in which an
    # allocation must be marked as healthy. The deadline begins when the first
    # allocation for the deployment is created and is reset whenever an allocation
    # as part of the deployment transitions to a healthy state. If no allocation
    # transitions to the healthy state before the progress deadline, the
    # deployment is marked as failed.
    progress_deadline = "10m"
  }

 


    group "mpay-api" {
        count = 1
        
        task "mpay-api" {
            driver = "raw_exec"

 

             env {
                db_type="MSSQL"
                db_dialect="org.hibernate.dialect.SQLServer2008Dialect"
                db_driver="net.sourceforge.jtds.jdbc.Driver"
                db_url="jdbc:jtds:sqlserver://qa00-mpaydb01:53414;ssl=request"
                db_validation_query="SELECT 1"
                db_check_query="SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES"
                db_user="mpayjfw"
                db_password="QaAB@123456789"
                db_schema="mpayjfw"
                SPRING_URL="http://127.0.0.1:8081/processMessage"
                SPRING_BASE_URL="http://127.0.0.1:8081"
                SPRING_REVERS_URL="http://127.0.0.1:8081/processMessage/reverse"
                MINIO_URL="http://127.0.0.1:9000"
                MINIO_ACCESS_KEY="minioadmin"
                MINIO_SECRET_KEY="minioadmin"
                env_broker_url="tcp://127.0.0.1:61616"
                env_broker_url_mpc="ssl://mpc.qcb.gov.qa:5555"
                JAVA_HOME = "C:/mpay/mpay-setup/jdk-11.0.10"
				app_spring_url="http://127.0.0.1:8081"
				EKYC_BASE_URL="http://127.0.0.1:7085/api"
				tenant_name="ARAB"
             }

 

            config {    
                command = "C:/mpay/mpay-setup/jdk-11.0.10/bin/java.exe"
                args= ["-Dserver.port=8081","-Djavax.servlet.request.encoding=UTF-8","-Dfile.encoding=UTF-8","-jar","C:/mpay/artifacts/v160/arab-apis-v1.6.0.jar"]
            }

 

            resources {
                #cpu = 100
                #memory = 6000
                network {
                    port "mpay-api" {
                        static = 8081
                    }
                }
            }

 

             service {
                tags = ["mpay-api"]
                port = "mpay-api"

 

            #  check {
            #    name     = "healthcheck-http"
            #    type     = "http"
            #    port     = "mpay-api"
            #    path     = "HeartBeat/check"
            #    interval = "60s"
            #    timeout  = "10s"
            #    check_restart {
            #      limit = 3
            #      grace = "60s"
            #      ignore_warnings = false
            #    }
            #  }
              
            #  check {
            #    name     = "healthcheck-tcp"
            #    type     = "tcp"
            #    port     = "mpay-api"
            #    interval = "60s"
            #    timeout  = "10s"
            #    check_restart {
            #      limit = 3
            #      grace = "60s"
            #      ignore_warnings = false
            #    }
            #  }              
            }
        }
    }
}