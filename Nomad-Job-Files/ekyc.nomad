job "mpay-ekyc" {
    datacenters = ["dc1"]

 

 update {
    # max_parallel      = 1

 

    # The "canary" parameter specifies that changes to the job that would result
    # in destructive updates should create the specified number of canaries
    # without stopping any previous allocations. Once the operator determines the
    # canaries are healthy, they can be promoted which unblocks a rolling update
    # of the remaining allocations at a rate of "max_parallel".
    #
    # Further, setting "canary" equal to the count of the task group allows
    # blue/green deployments. When the job is updated, a full set of the new
    # version is deployed and upon promotion the old version is stopped.
    # canary            = 1

 

    #health_check Specifies the mechanism in which allocations health is determined. 
    #"checks" - Specifies that the allocation should be considered healthy 
    #when all of its tasks are running and their associated checks are healthy, 
    #and unhealthy if any of the tasks fail or not all checks become healthy.
    # health_check      = "checks"
    # min_healthy_time  = "10s"
    
    # The "healthy_deadline" parameter specifies the deadline in which the
    # allocation must be marked as healthy after which the allocation is
    # automatically transitioned to unhealthy.Transitioning to unhealthy will
    # fail the deployment and potentially roll back the job if "auto_revert" is
    # set to true.
    healthy_deadline  = "5m"
    auto_revert = false

 

    # The "progress_deadline" parameter specifies the deadline in which an
    # allocation must be marked as healthy. The deadline begins when the first
    # allocation for the deployment is created and is reset whenever an allocation
    # as part of the deployment transitions to a healthy state. If no allocation
    # transitions to the healthy state before the progress deadline, the
    # deployment is marked as failed.
    progress_deadline = "10m"
  }

 


    group "mpay-ekyc" {
        count = 1
        
        task "mpay-ekyc" {
            driver = "raw_exec"

 

             env {
                jdbc_url="jdbc:sqlserver://qa00-mpaydb01:53414;ssl=request"
                jdbc_username="ekycmp"
                jdbc_password="QaAB@123456789"
                JAVA_HOME="C:/Temp/mPay/jdk-11.0.101"
             }

 

            config {    
                command = "C:/Temp/mPay/jdk-11.0.10/bin/java.exe"
                args = ["-jar","C:/mpay/ekyc/quarkus-run.jar"]
            }

 

            resources {
                #cpu = 100
                #memory = 6000
                network {
                    port "mpay-ekyc" {
                        static = 7085
                    }
                }
            }

 

             service {
                tags = ["mpay-ekyc"]
                port = "mpay-ekyc"

 

            #  check {
            #    name     = "healthcheck-http"
            #    type     = "http"
            #    port     = "mpay-api"
            #    path     = "HeartBeat/check"
            #    interval = "60s"
            #    timeout  = "10s"
            #    check_restart {
            #      limit = 3
            #      grace = "60s"
            #      ignore_warnings = false
            #    }
            #  }
              
            #  check {
            #    name     = "healthcheck-tcp"
            #    type     = "tcp"
            #    port     = "mpay-api"
            #    interval = "60s"
            #    timeout  = "10s"
            #    check_restart {
            #      limit = 3
            #      grace = "60s"
            #      ignore_warnings = false
            #    }
            #  }              
            }
        }
    }
}