USE [master]
GO
/****** Object:  Database [mpayjfw]    Script Date: 9/6/2021 12:23:29 PM ******/
CREATE DATABASE [mpayjfw]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'mpayjfw', FILENAME = N'E:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\mpayjfw.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'mpayjfw_log', FILENAME = N'E:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\mpayjfw_log.ldf' , SIZE = 401408KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [mpayjfw] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [mpayjfw].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [mpayjfw] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [mpayjfw] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [mpayjfw] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [mpayjfw] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [mpayjfw] SET ARITHABORT OFF 
GO
ALTER DATABASE [mpayjfw] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [mpayjfw] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [mpayjfw] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [mpayjfw] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [mpayjfw] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [mpayjfw] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [mpayjfw] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [mpayjfw] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [mpayjfw] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [mpayjfw] SET  DISABLE_BROKER 
GO
ALTER DATABASE [mpayjfw] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [mpayjfw] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [mpayjfw] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [mpayjfw] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [mpayjfw] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [mpayjfw] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [mpayjfw] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [mpayjfw] SET RECOVERY FULL 
GO
ALTER DATABASE [mpayjfw] SET  MULTI_USER 
GO
ALTER DATABASE [mpayjfw] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [mpayjfw] SET DB_CHAINING OFF 
GO
ALTER DATABASE [mpayjfw] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [mpayjfw] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [mpayjfw] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [mpayjfw] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [mpayjfw] SET QUERY_STORE = OFF
GO
USE [mpayjfw]
GO
/****** Object:  User [mpayjfw]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE USER [mpayjfw] FOR LOGIN [mpayjfw] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [mpayjfw]
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[ACCOUNT_NUMBER_SEQ]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[ACCOUNT_NUMBER_SEQ] 
 AS [bigint]
 START WITH 200000000
 INCREMENT BY 1
 MINVALUE 200000000
 MAXVALUE 9999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[CITY_CODE_SEQ]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[CITY_CODE_SEQ] 
 AS [bigint]
 START WITH 100000000
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[DEVICE_ID_SEQ]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[DEVICE_ID_SEQ] 
 AS [bigint]
 START WITH 100000000
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[HIBERNATE_SEQUENCE]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[HIBERNATE_SEQUENCE] 
 AS [bigint]
 START WITH 100001
 INCREMENT BY 1000000
 MINVALUE 100000
 MAXVALUE 2147483647
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[MPAY_CLIENTSLIMITS_SEQ]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[MPAY_CLIENTSLIMITS_SEQ] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[MPAY_MESSAGEID_SEQ]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[MPAY_MESSAGEID_SEQ] 
 AS [bigint]
 START WITH 100000000
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[MPAY_MonthlyLimits_SEQ]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[MPAY_MonthlyLimits_SEQ] 
 AS [bigint]
 START WITH 100000000
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[MPAY_MPAYMESSAGES_SEQ]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[MPAY_MPAYMESSAGES_SEQ] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[REFERENCE_NUMBER_SEQ]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[REFERENCE_NUMBER_SEQ] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[SEQ_MPAY_ACCOUNTS]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[SEQ_MPAY_ACCOUNTS] 
 AS [bigint]
 START WITH 200000000
 INCREMENT BY 1
 MINVALUE 200000000
 MAXVALUE 9999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[SEQ_MPAY_BANKINTEGMESSAGES]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[SEQ_MPAY_BANKINTEGMESSAGES] 
 AS [bigint]
 START WITH 50001
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[SEQ_MPAY_CUSTOMERMOBILES]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[SEQ_MPAY_CUSTOMERMOBILES] 
 AS [bigint]
 START WITH 50001
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[SEQ_MPAY_CUSTOMERS]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[SEQ_MPAY_CUSTOMERS] 
 AS [bigint]
 START WITH 50001
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[SEQ_MPAY_JVDETAILS]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[SEQ_MPAY_JVDETAILS] 
 AS [bigint]
 START WITH 50001
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[SEQ_MPAY_MOBILEACCOUNTS]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[SEQ_MPAY_MOBILEACCOUNTS] 
 AS [bigint]
 START WITH 50001
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[SEQ_MPAY_NOTIFICATION]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[SEQ_MPAY_NOTIFICATION] 
 AS [bigint]
 START WITH 50001
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[SEQ_MPAY_TRANSACTION]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[SEQ_MPAY_TRANSACTION] 
 AS [bigint]
 START WITH 50001
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999
 CACHE 
GO
USE [mpayjfw]
GO
/****** Object:  Sequence [dbo].[SERVICE_ID_SEQ]    Script Date: 9/6/2021 12:23:30 PM ******/
CREATE SEQUENCE [dbo].[SERVICE_ID_SEQ] 
 AS [bigint]
 START WITH 1000000000
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999
 CACHE 
GO
/****** Object:  Table [dbo].[mpay_processingstatuses]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_processingstatuses](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_processingstatuses_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_8f9rg2fpacb2rlg54yt9w2vsk] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_9rs0q156qb8y523bquddivlec] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corpoarteservices]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corpoarteservices](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[activationcode] [nvarchar](255) NULL,
	[activationcodevalidy] [datetime] NULL,
	[alias] [nvarchar](25) NULL,
	[approvalnote] [nvarchar](255) NULL,
	[approveddata] [nvarchar](4000) NULL,
	[bankedunbanked] [nvarchar](20) NOT NULL,
	[branch] [nvarchar](50) NULL,
	[chargeamountcharacters] [nvarchar](200) NULL,
	[chargeamountnumber] [numeric](19, 2) NULL,
	[chargeduration] [nvarchar](50) NULL,
	[description] [nvarchar](128) NULL,
	[externalacc] [nvarchar](100) NULL,
	[extradata] [nvarchar](1000) NULL,
	[iban] [nvarchar](100) NULL,
	[isactive] [bit] NOT NULL,
	[isadmin] [nvarchar](50) NOT NULL,
	[isblocked] [bit] NOT NULL,
	[isdormant] [bit] NULL,
	[isregistered] [bit] NULL,
	[mas] [bigint] NOT NULL,
	[maxnumberofdevices] [bigint] NOT NULL,
	[mpclearalias] [nvarchar](16) NULL,
	[name] [nvarchar](128) NOT NULL,
	[newalias] [nvarchar](25) NULL,
	[newdescription] [nvarchar](128) NULL,
	[newname] [nvarchar](128) NULL,
	[notificationreceiver] [nvarchar](1000) NULL,
	[pin] [nvarchar](255) NULL,
	[pinlastchanged] [datetime] NULL,
	[rejectionnote] [nvarchar](255) NULL,
	[retrycount] [bigint] NOT NULL,
	[sessionid] [nvarchar](1000) NULL,
	[virtualiban] [nvarchar](100) NULL,
	[walletaccount] [nvarchar](50) NULL,
	[wallettype] [nvarchar](50) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[adminserviceid] [bigint] NULL,
	[bankid] [bigint] NOT NULL,
	[cityid] [bigint] NOT NULL,
	[notificationchannelid] [bigint] NOT NULL,
	[paymenttypeid] [bigint] NOT NULL,
	[refcorporateid] [bigint] NOT NULL,
	[refmerchantcategorycodeid] [bigint] NOT NULL,
	[refprofileid] [bigint] NOT NULL,
	[servicecategoryid] [bigint] NULL,
	[servicetypeid] [bigint] NULL,
	[transactionsizeid] [bigint] NULL,
 CONSTRAINT [mpay_corpoarteservices_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_jvdetails]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_jvdetails](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[amount] [numeric](21, 8) NOT NULL,
	[balanceafter] [numeric](21, 8) NULL,
	[balancebefore] [numeric](21, 8) NULL,
	[debitcreditflag] [nvarchar](1) NOT NULL,
	[description] [nvarchar](255) NULL,
	[isposted] [bit] NOT NULL,
	[serial] [bigint] NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
	[jvtypeid] [bigint] NOT NULL,
	[refaccountid] [bigint] NOT NULL,
	[reftrsansactionid] [bigint] NULL,
 CONSTRAINT [mpay_jvdetails_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_serviceaccounts]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_serviceaccounts](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[approvalnote] [nvarchar](255) NULL,
	[approveddata] [nvarchar](4000) NULL,
	[bankedunbanked] [nvarchar](20) NOT NULL,
	[branch] [nvarchar](50) NULL,
	[chargeamountcharacters] [nvarchar](200) NULL,
	[chargeamountnumber] [numeric](19, 2) NULL,
	[chargeduration] [nvarchar](50) NULL,
	[externalacc] [nvarchar](100) NULL,
	[extradata] [nvarchar](1000) NULL,
	[iban] [nvarchar](100) NULL,
	[isactive] [bit] NULL,
	[isdefault] [bit] NOT NULL,
	[isregistered] [bit] NULL,
	[isswitchdefault] [bit] NOT NULL,
	[lasttransactiondate] [datetime] NOT NULL,
	[mas] [bigint] NOT NULL,
	[newexternalacc] [nvarchar](100) NULL,
	[rejectionnote] [nvarchar](255) NULL,
	[walletaccount] [nvarchar](50) NULL,
	[wallettype] [nvarchar](50) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[bankid] [bigint] NOT NULL,
	[newprofileid] [bigint] NULL,
	[refaccountid] [bigint] NULL,
	[refprofileid] [bigint] NOT NULL,
	[serviceid] [bigint] NOT NULL,
	[transactionsizeid] [bigint] NULL,
 CONSTRAINT [mpay_serviceaccounts_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corporates]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corporates](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[address] [nvarchar](500) NULL,
	[alias] [nvarchar](16) NULL,
	[approvalnote] [nvarchar](255) NULL,
	[approveddata] [nvarchar](4000) NULL,
	[bankedunbanked] [nvarchar](20) NULL,
	[branch] [nvarchar](50) NULL,
	[buildingnum] [nvarchar](20) NULL,
	[chamberid] [nvarchar](50) NULL,
	[chargeamountcharacters] [nvarchar](200) NULL,
	[chargeamountnumber] [numeric](19, 2) NULL,
	[chargeduration] [nvarchar](50) NULL,
	[clientref] [nvarchar](50) NOT NULL,
	[consigneearabicname] [nvarchar](255) NULL,
	[consigneeenglishname] [nvarchar](255) NULL,
	[cosigneeemail] [nvarchar](128) NULL,
	[dateofbirth] [date] NULL,
	[description] [nvarchar](255) NOT NULL,
	[email] [nvarchar](128) NULL,
	[expectedactivities] [nvarchar](200) NULL,
	[externalacc] [nvarchar](100) NULL,
	[gender] [nvarchar](50) NULL,
	[hint] [nvarchar](255) NULL,
	[iban] [nvarchar](100) NULL,
	[identificationcard] [nvarchar](50) NULL,
	[identificationcardissuancedate] [date] NULL,
	[identificationreference] [nvarchar](50) NULL,
	[isactive] [bit] NOT NULL,
	[isregistered] [bit] NULL,
	[location] [nvarchar](100) NULL,
	[mas] [nvarchar](10) NULL,
	[maxnumberofdevices] [bigint] NOT NULL,
	[mobilenumber] [nvarchar](50) NULL,
	[name] [nvarchar](128) NOT NULL,
	[nationalid] [nvarchar](50) NULL,
	[note] [nvarchar](255) NULL,
	[otherlegalentity] [nvarchar](100) NULL,
	[othernationality] [nvarchar](100) NULL,
	[passportid] [nvarchar](50) NULL,
	[passportissuancedate] [date] NULL,
	[phoneone] [nvarchar](20) NULL,
	[phonetwo] [nvarchar](20) NULL,
	[placeofbirth] [nvarchar](100) NULL,
	[pobox] [nvarchar](20) NULL,
	[refmessageid] [nvarchar](100) NULL,
	[registrationauthority] [nvarchar](100) NULL,
	[registrationdate] [date] NOT NULL,
	[registrationid] [nvarchar](50) NOT NULL,
	[registrationlocation] [nvarchar](100) NULL,
	[rejectionnote] [nvarchar](255) NULL,
	[sectortype] [nvarchar](50) NULL,
	[servicedescription] [nvarchar](128) NULL,
	[servicename] [nvarchar](128) NULL,
	[servicenotificationreceiver] [nvarchar](128) NULL,
	[streetname] [nvarchar](255) NULL,
	[walletaccount] [nvarchar](50) NULL,
	[wallettype] [nvarchar](50) NULL,
	[zipcode] [nvarchar](20) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[bankid] [bigint] NULL,
	[cityid] [bigint] NOT NULL,
	[clienttypeid] [bigint] NOT NULL,
	[idtypeid] [bigint] NOT NULL,
	[instid] [bigint] NULL,
	[legalentityid] [bigint] NULL,
	[nationalityid] [bigint] NULL,
	[paymenttypeid] [bigint] NULL,
	[preflangid] [bigint] NOT NULL,
	[refaccountid] [bigint] NULL,
	[refchargeaccountid] [bigint] NULL,
	[refcountryid] [bigint] NOT NULL,
	[reflastmsglogid] [bigint] NULL,
	[refmerchantcategorycodeid] [bigint] NULL,
	[refprofileid] [bigint] NULL,
	[servicecategoryid] [bigint] NULL,
	[servicenotificationchannelid] [bigint] NULL,
	[servicetypeid] [bigint] NULL,
	[transactionsizeid] [bigint] NULL,
 CONSTRAINT [mpay_corporates_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customers]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customers](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[address] [nvarchar](1000) NULL,
	[alias] [nvarchar](25) NULL,
	[approvalnote] [nvarchar](255) NULL,
	[approveddata] [nvarchar](4000) NULL,
	[arabicfullname] [nvarchar](1000) NULL,
	[availablebalance] [nvarchar](250) NULL,
	[bankbranch] [nvarchar](50) NULL,
	[bankedunbanked] [nvarchar](20) NULL,
	[beneficiaryemail] [nvarchar](128) NULL,
	[beneficiaryid] [nvarchar](84) NULL,
	[beneficiaryidexpiry] [date] NULL,
	[beneficiaryname] [nvarchar](100) NULL,
	[beneficiarynationalid] [bigint] NULL,
	[buildingnum] [nvarchar](20) NULL,
	[chargeamountcharacters] [nvarchar](200) NULL,
	[chargeamountnumber] [numeric](19, 2) NULL,
	[chargeduration] [nvarchar](50) NULL,
	[clientref] [nvarchar](50) NOT NULL,
	[dob] [date] NOT NULL,
	[eighthtextfield] [nvarchar](100) NULL,
	[eleventhtextfield] [nvarchar](100) NULL,
	[email] [nvarchar](128) NULL,
	[englishfullname] [nvarchar](200) NULL,
	[externalacc] [nvarchar](100) NULL,
	[fifteenthtextfield] [nvarchar](100) NULL,
	[fifthtextfield] [nvarchar](100) NULL,
	[firstcheckbox] [bit] NULL,
	[firstdaterfield] [date] NULL,
	[firstname] [nvarchar](60) NOT NULL,
	[firstnumberfield] [nvarchar](100) NULL,
	[firsttextfield] [nvarchar](100) NULL,
	[forthtextfield] [nvarchar](100) NULL,
	[fourteenthtextfield] [nvarchar](100) NULL,
	[fullname] [nvarchar](200) NULL,
	[gender] [nvarchar](20) NULL,
	[guardian] [nvarchar](100) NULL,
	[guardianemail] [nvarchar](128) NULL,
	[hint] [nvarchar](255) NULL,
	[iban] [nvarchar](100) NULL,
	[idcardissuancedate] [date] NULL,
	[idexpirydate] [date] NULL,
	[idnum] [nvarchar](84) NOT NULL,
	[identificationcard] [nvarchar](100) NULL,
	[identificationreference] [nvarchar](50) NULL,
	[isactive] [bit] NOT NULL,
	[isbeneficiary] [nvarchar](50) NULL,
	[isblacklisted] [bit] NULL,
	[isregistered] [bit] NULL,
	[isspring] [bit] NULL,
	[jobtitle] [nvarchar](100) NULL,
	[kyccompleted] [bit] NULL,
	[kycid] [nvarchar](20) NULL,
	[lastname] [nvarchar](60) NOT NULL,
	[mas] [bigint] NULL,
	[maxnumberofdevices] [bigint] NOT NULL,
	[middlename] [nvarchar](60) NULL,
	[mobilenumber] [nvarchar](20) NULL,
	[monthlyincome] [nvarchar](50) NULL,
	[nfcserial] [nvarchar](255) NULL,
	[ninthtextfield] [nvarchar](100) NULL,
	[note] [nvarchar](255) NULL,
	[notificationshowtype] [nvarchar](50) NULL,
	[occupation] [nvarchar](50) NULL,
	[otherbank] [nvarchar](100) NULL,
	[otherincomeresource] [nvarchar](200) NULL,
	[othernumber] [nvarchar](100) NULL,
	[passportid] [nvarchar](100) NULL,
	[passportissuancedate] [date] NULL,
	[permitexpiry] [date] NULL,
	[permitid] [bigint] NULL,
	[permitnumber] [bigint] NULL,
	[phoneone] [nvarchar](20) NULL,
	[phonethree] [nvarchar](20) NULL,
	[phonetwo] [nvarchar](20) NULL,
	[pobox] [nvarchar](20) NULL,
	[profession] [nvarchar](200) NULL,
	[pspuses1] [nvarchar](200) NULL,
	[pspusesother] [nvarchar](100) NULL,
	[reasonforresidence] [nvarchar](300) NULL,
	[refmessageid] [nvarchar](100) NULL,
	[rejectionnote] [nvarchar](255) NULL,
	[retrycount] [bigint] NULL,
	[riskprofile] [nvarchar](100) NULL,
	[secondcheckbox] [bit] NULL,
	[seconddaterfield] [date] NULL,
	[secondnumberfield] [nvarchar](100) NULL,
	[secondtextfield] [nvarchar](100) NULL,
	[seventhtextfield] [nvarchar](100) NULL,
	[sixthtextfield] [nvarchar](100) NULL,
	[streetname] [nvarchar](255) NULL,
	[tenthtextfield] [nvarchar](100) NULL,
	[thirdtextfield] [nvarchar](100) NULL,
	[thirteenthtextfield] [nvarchar](100) NULL,
	[twelfthtextfield] [nvarchar](100) NULL,
	[walletaccount] [nvarchar](50) NULL,
	[walletlimit] [nvarchar](250) NULL,
	[wallettype] [nvarchar](20) NULL,
	[worknature] [nvarchar](100) NULL,
	[workplace] [nvarchar](100) NULL,
	[zipcode] [nvarchar](20) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[areaid] [bigint] NULL,
	[bankid] [bigint] NULL,
	[beneficiaryidtypeid] [bigint] NULL,
	[bulkregistrationid] [bigint] NULL,
	[cityid] [bigint] NOT NULL,
	[clienttypeid] [bigint] NOT NULL,
	[idtypeid] [bigint] NOT NULL,
	[instid] [bigint] NULL,
	[nationalityid] [bigint] NOT NULL,
	[preflangid] [bigint] NOT NULL,
	[reflastmsglogid] [bigint] NULL,
	[refprofileid] [bigint] NULL,
	[transactionsizeid] [bigint] NULL,
	[crs] [bit] NULL,
	[fatca] [bit] NULL,
	[isekyc] [bit] NULL,
	[ekycstatus] [bigint] NULL,
	[ekycref] [nvarchar](255) NULL,
 CONSTRAINT [mpay_customers_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_accounts]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_accounts](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[acccount] [bigint] NULL,
	[acclevel] [nvarchar](1) NULL,
	[accname] [nvarchar](255) NULL,
	[accnumber] [nvarchar](35) NOT NULL,
	[accountupdatedate] [datetime] NULL,
	[balance] [numeric](21, 8) NOT NULL,
	[externalacc] [nvarchar](100) NULL,
	[isactive] [bit] NULL,
	[isbanked] [bit] NOT NULL,
	[isdormant] [bit] NULL,
	[level1] [nvarchar](1) NULL,
	[level2] [nvarchar](2) NULL,
	[level3] [nvarchar](2) NULL,
	[level4] [nvarchar](5) NULL,
	[level5] [nvarchar](25) NULL,
	[minbalance] [numeric](21, 8) NOT NULL,
	[nature] [nvarchar](1) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[accparentid] [bigint] NULL,
	[accounttypeid] [bigint] NOT NULL,
	[balancetypeid] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
 CONSTRAINT [mpay_accounts_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_s5q3la58flh6fgkkiqwj1l48v] UNIQUE NONCLUSTERED 
(
	[accnumber] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_banks]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_banks](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[biccode] [nvarchar](20) NULL,
	[isactive] [bit] NULL,
	[processor] [nvarchar](1000) NULL,
	[settlementparticipant] [nvarchar](128) NOT NULL,
	[tellercode] [nvarchar](20) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refchargeaccountid] [bigint] NULL,
 CONSTRAINT [mpay_banks_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customermobiles]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customermobiles](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[isadminsuspension] [bit] NULL,
	[activationcode] [nvarchar](255) NULL,
	[activationcodevalidy] [datetime] NULL,
	[alias] [nvarchar](25) NULL,
	[approvalnote] [nvarchar](255) NULL,
	[approveddata] [nvarchar](4000) NULL,
	[balancethreshold] [float] NULL,
	[bankbranch] [nvarchar](50) NULL,
	[bankedunbanked] [nvarchar](20) NOT NULL,
	[chargeamountcharacters] [nvarchar](200) NULL,
	[chargeamountnumber] [numeric](19, 2) NULL,
	[chargeduration] [nvarchar](50) NULL,
	[dailybalancetime] [nvarchar](2) NULL,
	[email] [nvarchar](50) NULL,
	[externalacc] [nvarchar](100) NULL,
	[extradata] [nvarchar](1000) NULL,
	[hint] [nvarchar](255) NULL,
	[iban] [nvarchar](100) NULL,
	[isactive] [bit] NOT NULL,
	[isblocked] [bit] NOT NULL,
	[isdormant] [bit] NULL,
	[isregistered] [bit] NULL,
	[mas] [bigint] NOT NULL,
	[maxnumberofdevices] [bigint] NOT NULL,
	[mobilenumber] [nvarchar](20) NOT NULL,
	[newalias] [nvarchar](25) NULL,
	[newmobilenumber] [nvarchar](20) NULL,
	[nfcserial] [nvarchar](255) NULL,
	[notificationshowtype] [nvarchar](50) NOT NULL,
	[pin] [nvarchar](255) NULL,
	[pinlastchanged] [datetime] NULL,
	[promocode] [nvarchar](100) NULL,
	[rejectionnote] [nvarchar](255) NULL,
	[retrycount] [bigint] NOT NULL,
	[sessionid] [nvarchar](1000) NULL,
	[walletaccount] [nvarchar](50) NULL,
	[wallettype] [nvarchar](20) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[bankid] [bigint] NOT NULL,
	[channeltypeid] [bigint] NULL,
	[refcustomerid] [bigint] NOT NULL,
	[refprofileid] [bigint] NOT NULL,
	[referralid] [bigint] NULL,
	[transactionsizeid] [bigint] NULL,
 CONSTRAINT [mpay_customermobiles_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_CURRENCIES]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_CURRENCIES](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[STRING_ISO_CODE] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[ACTIVE] [bit] NULL,
	[NUMERIC_ISO_CODE] [bigint] NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_CURR__3214EC27723AF2F9] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UK3gb7wuhfnkb64q5plmh04fo40] UNIQUE NONCLUSTERED 
(
	[STRING_ISO_CODE] ASC,
	[Z_TENANT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UKobw3o1gdxu7n1ipgde1afir4r] UNIQUE NONCLUSTERED 
(
	[NUMERIC_ISO_CODE] ASC,
	[Z_TENANT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mobileaccounts]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mobileaccounts](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[approvalnote] [nvarchar](255) NULL,
	[approveddata] [nvarchar](4000) NULL,
	[bankbranch] [nvarchar](50) NULL,
	[bankedunbanked] [nvarchar](20) NOT NULL,
	[chargeamountcharacters] [nvarchar](200) NULL,
	[chargeamountnumber] [numeric](19, 2) NULL,
	[chargeduration] [nvarchar](50) NULL,
	[externalacc] [nvarchar](100) NULL,
	[extradata] [nvarchar](1000) NULL,
	[iban] [nvarchar](100) NULL,
	[isactive] [bit] NOT NULL,
	[isdefault] [bit] NOT NULL,
	[isregistered] [bit] NULL,
	[isswitchdefault] [bit] NOT NULL,
	[lasttransactiondate] [datetime] NOT NULL,
	[mas] [bigint] NOT NULL,
	[newexternalacc] [nvarchar](100) NULL,
	[rejectionnote] [nvarchar](255) NULL,
	[walletaccount] [nvarchar](50) NULL,
	[wallettype] [nvarchar](20) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[bankid] [bigint] NOT NULL,
	[mobileid] [bigint] NOT NULL,
	[newprofileid] [bigint] NULL,
	[refaccountid] [bigint] NULL,
	[refprofileid] [bigint] NOT NULL,
	[transactionsizeid] [bigint] NULL,
 CONSTRAINT [mpay_mobileaccounts_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactions]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactions](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[channelid] [nvarchar](1000) NULL,
	[comments] [nvarchar](255) NULL,
	[customerbankaccountnumber] [nvarchar](255) NULL,
	[externalfees] [numeric](21, 8) NOT NULL,
	[externalreference] [varchar](19) NULL,
	[isreceiverbanked] [bit] NULL,
	[isrefunded] [bit] NULL,
	[isreversed] [bit] NOT NULL,
	[issenderbanked] [bit] NULL,
	[isspring] [bit] NULL,
	[mpclearmessageid] [nvarchar](1000) NULL,
	[mpclearrequestid] [nvarchar](1000) NULL,
	[originalamount] [numeric](21, 8) NOT NULL,
	[reasondesc] [nvarchar](4000) NULL,
	[receivercharge] [numeric](21, 8) NOT NULL,
	[receivercommission] [numeric](21, 8) NULL,
	[receiverinfo] [nvarchar](1000) NULL,
	[receivername] [nvarchar](255) NULL,
	[receivertax] [numeric](21, 8) NOT NULL,
	[reference] [nvarchar](1000) NULL,
	[requestedid] [nvarchar](1000) NULL,
	[retrycount] [bigint] NULL,
	[reversable] [bit] NULL,
	[reversalreason] [nvarchar](1000) NULL,
	[reversedby] [nvarchar](1000) NULL,
	[sendercharge] [numeric](21, 8) NOT NULL,
	[sendercommission] [numeric](21, 8) NULL,
	[senderinfo] [nvarchar](1000) NULL,
	[sendername] [nvarchar](255) NULL,
	[sendertax] [numeric](21, 8) NOT NULL,
	[sessionid] [nvarchar](1000) NULL,
	[shopid] [nvarchar](1000) NULL,
	[totalamount] [numeric](21, 8) NOT NULL,
	[totalcharge] [numeric](21, 8) NOT NULL,
	[transdate] [datetime] NOT NULL,
	[walletid] [nvarchar](1000) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
	[directionid] [bigint] NULL,
	[inwardmessageid] [bigint] NULL,
	[originaltransactionid] [bigint] NULL,
	[processingstatusid] [bigint] NOT NULL,
	[reasonid] [bigint] NOT NULL,
	[receiverbankid] [bigint] NULL,
	[receivermobileid] [bigint] NULL,
	[receivermobileaccountid] [bigint] NULL,
	[receiverserviceid] [bigint] NULL,
	[receiverserviceaccountid] [bigint] NULL,
	[refmessageid] [bigint] NULL,
	[refoperationid] [bigint] NULL,
	[reftypeid] [bigint] NOT NULL,
	[senderbankid] [bigint] NULL,
	[sendermobileid] [bigint] NULL,
	[sendermobileaccountid] [bigint] NULL,
	[senderserviceid] [bigint] NULL,
	[senderserviceaccountid] [bigint] NULL,
 CONSTRAINT [mpay_transactions_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_ocno3jlnewq61bastttcagino] UNIQUE NONCLUSTERED 
(
	[reference] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MPAY_CUSTOMER_TRANS_REP]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MPAY_CUSTOMER_TRANS_REP] AS SELECT mpaytransaction.id id,
            (SELECT getdate()) EXTRACTSTAMP,
            mpaytransaction.reference transactionreference,
            mpaytransaction.z_creation_date trndate,
            mpaytransaction.z_creation_date trntime,
            (SELECT name
            FROM mpay_processingstatuses ps
            WHERE ps.id = mpaytransaction.processingstatusid)
            trnstatus,
            mpaytransaction.shopId trnsactionsource,
            CASE mpaytransaction.reftypeid
            WHEN 1 THEN 'CR'
            WHEN 2 THEN 'DR'
            END
            trntype,
            (SELECT description
            FROM jfw_currencies
            WHERE id IN (SELECT currencyid FROM mpay_transactions))
            trnccy,
            mpaytransaction.originalamount TRNAMOUNT,
            isnull (senderservice.iban, sendermobile.iban) senderiban,
            isnull (sendercorporate.registrationid, sendercustomer.idnum)
            sendernin,
            isnull (senderservice.name, sendercustomer.fullname) sendername,
            CASE
            WHEN isnull (senderservice.externalacc, sendermobile.externalacc) = '#####' THEN ''
            ELSE isnull (senderservice.externalacc, sendermobile.externalacc)
            END senderbankaccountnumber,

            isnull (senderserviceaccount.accnumber, sendermobileaccount.accnumber)
            senderaccountnumber,
            isnull (sendercorporate.mobilenumber, sendermobile.mobilenumber)
            sendermobilenumber,
            mpaytransaction.sendercharge senderchargesamount,
            isnull (senderservicejv.amount, sendermobilejv.amount) senderamount,
            isnull (senderservicejv.balancebefore, sendermobilejv.balancebefore)
            senderbalancebefore,
            isnull (senderservicejv.balanceafter, sendermobilejv.balanceafter)
            senderbalanceafter,
            CASE
            WHEN mpaytransaction.sendermobileid IS NOT NULL
            THEN
            (SELECT name
            FROM mpay_banks bank
            WHERE sendermobile.bankid = bank.id)
            ELSE
            (SELECT name
            FROM mpay_banks bank
            WHERE senderservice.bankid = bank.id)
            END
            senderbank,
            mpaytransaction.sendercharge sendervatamount,
            isnull (receiverservice.iban, receivermobile.iban) receiveriban,
            isnull (receivercorporate.registrationid, receivercustomer.idnum)
            receivernin,
            isnull (receiverservice.name, receivercustomer.fullname) receivername,
            CASE
            WHEN isnull (receiverservice.externalacc, receivermobile.externalacc) = '#####' THEN ''
            ELSE isnull (receiverservice.externalacc, receivermobile.externalacc)
            END receiverbankaccountnumber,
            isnull (receiverserviceaccount.accnumber,
            receivermobileaccount.accnumber)
            receiveraccountnumber,
            isnull (receivercorporate.mobilenumber, receivermobile.mobilenumber)
            receivermobilenumber,
            mpaytransaction.receivercharge receiverchargesamount,
            isnull (receiverservicejv.amount, receivermobilejv.amount)
            receivernetamount,
            isnull (receiverservicejv.balancebefore,
            receivermobilejv.balancebefore)
            RECEIVERBALANCEBEFORE,
            isnull (receiverservicejv.balanceafter, receivermobilejv.balanceafter)
            receiverbalanceafter,
            CASE
            WHEN mpaytransaction.receivermobileid IS NOT NULL
            THEN
            (SELECT name
            FROM mpay_banks bank
            WHERE receivermobile.bankid = bank.id)
            ELSE
            (SELECT name
            FROM mpay_banks bank
            WHERE receiverservice.bankid = bank.id)
            END
            receiverbank,
            mpaytransaction.receivercharge receivervatamount,
            isnull (sendercorporate.name, receivercorporate.name) merchantname,
            mpaytransaction.z_archive_on,
            mpaytransaction.z_assigned_user,
            mpaytransaction.z_archive_queued,
            mpaytransaction.z_archive_status,
            mpaytransaction.z_assigned_group,
            mpaytransaction.z_created_by,
            mpaytransaction.z_creation_date,
            mpaytransaction.z_deleted_by,
            mpaytransaction.z_deleted_flag,
            mpaytransaction.z_deleted_on,
            mpaytransaction.z_editable,
            mpaytransaction.z_locked_by,
            mpaytransaction.z_locked_until,
            mpaytransaction.z_org_id,
            mpaytransaction.z_tenant_id,
            mpaytransaction.z_updated_by,
            mpaytransaction.z_updating_date,
            mpaytransaction.z_workflow_id,
            mpaytransaction.z_ws_token,
            mpaytransaction.z_draft_status,
            mpaytransaction.z_draft_id,
            mpaytransaction.z_status_id
            FROM mpay_transactions mpaytransaction
            LEFT JOIN mpay_customermobiles sendermobile
            ON (sendermobile.id = mpaytransaction.sendermobileid)
            LEFT JOIN mpay_customermobiles receivermobile
            ON (receivermobile.id = mpaytransaction.receivermobileid)
            LEFT JOIN mpay_corpoarteservices senderservice
            ON (senderservice.id = mpaytransaction.senderserviceid)
            LEFT JOIN mpay_corpoarteservices receiverservice
            ON (receiverservice.id = mpaytransaction.receiverserviceid)
            LEFT JOIN mpay_accounts sendermobileaccount
            ON sendermobileaccount.id IN (SELECT refaccountid
            FROM mpay_mobileaccounts
            WHERE mobileid = sendermobile.id)
            LEFT JOIN mpay_accounts receivermobileaccount
            ON receivermobileaccount.id IN
            (SELECT refaccountid
            FROM mpay_mobileaccounts
            WHERE mobileid = receivermobile.id)
            LEFT JOIN mpay_accounts senderserviceaccount
            ON senderserviceaccount.id IN
            (SELECT refaccountid
            FROM mpay_serviceaccounts
            WHERE serviceid = senderservice.id)
            LEFT JOIN mpay_accounts receiverserviceaccount
            ON receiverserviceaccount.id IN
            (SELECT refaccountid
            FROM mpay_serviceaccounts
            WHERE serviceid = receiverservice.id)
            LEFT JOIN mpay_customers sendercustomer
            ON (sendercustomer.id = sendermobile.refcustomerid)
            LEFT JOIN mpay_customers receivercustomer
            ON (receivercustomer.id = receivermobile.refcustomerid)
            LEFT JOIN mpay_corporates sendercorporate
            ON (sendercorporate.id = senderservice.refcorporateid)
            LEFT JOIN mpay_corporates receivercorporate
            ON (receivercorporate.id = receiverservice.refcorporateid)
            LEFT JOIN mpay_jvdetails sendermobilejv
            ON sendermobilejv.reftrsansactionid = mpaytransaction.id
            AND sendermobilejv.refaccountid = sendermobileaccount.id
            LEFT JOIN mpay_jvdetails receivermobilejv
            ON receivermobilejv.reftrsansactionid = mpaytransaction.id
            AND receivermobilejv.refaccountid = receivermobileaccount.id
            LEFT JOIN mpay_jvdetails senderservicejv
            ON senderservicejv.reftrsansactionid = mpaytransaction.id
            AND senderservicejv.refaccountid = senderserviceaccount.id
            LEFT JOIN mpay_jvdetails receiverservicejv
            ON receiverservicejv.reftrsansactionid = mpaytransaction.id
            AND receiverservicejv.refaccountid =
            receiverserviceaccount.id
GO
/****** Object:  Table [dbo].[mpay_idtypes]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_idtypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[iscustomer] [bit] NULL,
	[issystem] [bit] NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_idtypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_o1nl7d1kinijcx1wev5oct59w] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_sfiwsq0u0et21jnxvvb6f2dj3] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_WF_STATUS]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_WF_STATUS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[code] [varchar](255) NOT NULL,
	[description] [varchar](255) NULL,
	[active] [bit] NULL,
	[label] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_WF_S__3213E83F138ACDEA] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UKnns6a5ws3720yfvbtwukju8kb] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[Z_TENANT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_messagetypes]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_messagetypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[categorycode] [nvarchar](10) NULL,
	[isfinancial] [bit] NULL,
	[isotpenabled] [bit] NOT NULL,
	[ispinlessenabled] [bit] NOT NULL,
	[issystem] [bit] NULL,
	[mpclearcode] [nvarchar](100) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_messagetypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_cities]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_cities](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_cities_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_hha240pqmkxcwgce8ws75ln47] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_languages]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_languages](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_languages_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_7b5h09qu3m3buedmi4puik81y] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_f24ap5v264uievd4cykw2nfku] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_COUNTRIES]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_COUNTRIES](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[COUNTRY_ISOCODE] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[COUNTRY_NAME] [varchar](255) NULL,
	[ACTIVE_FLAG] [bit] NULL,
	[BIC_CODE_FLAG] [bit] NULL,
	[IBAN_CD_FLAG] [bit] NULL,
	[NUMERIC_ISO_CODE] [bigint] NULL,
	[SORT_CD_FLAG] [bit] NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[DEFAULT_CURRENCY] [bigint] NULL,
 CONSTRAINT [PK__JFW_COUN__3213E83F2D892052] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UK39ik1grfkl56i83cywc8gogbc] UNIQUE NONCLUSTERED 
(
	[COUNTRY_ISOCODE] ASC,
	[Z_TENANT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UKownmg59y7j1lb9t2l395137oi] UNIQUE NONCLUSTERED 
(
	[NUMERIC_ISO_CODE] ASC,
	[Z_TENANT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MPAY_Corporate_Detail_Rep]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MPAY_Corporate_Detail_Rep] AS SELECT (SELECT getdate()) extratstamp,
            c.id id,
            c.name shortname,
            c.description fullname,
            c.registrationdate,
            (SELECT name
            FROM mpay_idtypes TYPE
            WHERE TYPE.id = c.idtypeid)
            idtype,
            c.registrationid,
            (SELECT s.description
            FROM jfw_wf_status s
            WHERE s.id = c.z_status_id)
            status,
            CASE WHEN c.isregistered = 1 THEN 'Yes' ELSE 'No' END registered,
            CASE WHEN c.isactive = 1 THEN 'Yes' ELSE 'No' END active,
            (SELECT name
            FROM mpay_languages l
            WHERE c.preflangid = l.id)
            notificationslanguage,
            c.pobox,
            c.zipcode,
            c.streetname street,
            (SELECT name
            FROM mpay_cities city
            WHERE city.id = c.cityid)
            city,
            (SELECT country.country_name
            FROM jfw_countries country
            WHERE c.refcountryid = country.id)
            country,
            c.phoneone PHONE1,
            c.mobilenumber mobileno,
            CASE
            WHEN c.externalacc = '#####' THEN ''
            ELSE c.externalacc
            END BANKACCOUNTNUMBER,
            c.iban,
            (SELECT name
            FROM mpay_messagetypes mt
            WHERE mt.id = c.paymenttypeid)
            paymenttype,
            (SELECT COUNT (*)
            FROM mpay_corpoarteservices
            WHERE refcorporateid = c.id)
            noofservices,
            c.z_created_by addedby,
            c.z_deleted_by deletedby,
            c.approvalnote,
            c.rejectionnote,
            c.z_creation_date createddate,
            c.z_created_by createduser,
            c.z_updating_date modifieddate,
            c.z_updated_by modifieduser,
            c.z_deleted_on deleteddate,
            c.z_deleted_by deleteduser,
            c.z_archive_on,
            c.z_assigned_user,
            c.z_archive_queued,
            c.z_archive_status,
            c.z_assigned_group,
            c.z_created_by,
            c.z_creation_date,
            c.z_deleted_by,
            c.z_deleted_flag,
            c.z_deleted_on,
            c.z_editable,
            c.z_locked_by,
            c.z_locked_until,
            c.z_org_id,
            c.z_tenant_id,
            c.z_updated_by,
            c.z_updating_date,
            c.z_workflow_id,
            c.z_ws_token,
            c.z_draft_status,
            c.z_draft_id,
            c.z_status_id
            FROM mpay_corporates c
            WHERE c.clienttypeid = 9
GO
/****** Object:  View [dbo].[MPAY_P2P_TRANS_REP]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[MPAY_P2P_TRANS_REP] AS SELECT
            (
            SELECT
            getdate()
            ) EXTRACTDATE,
            mpaytransaction.z_creation_date   TRNDATE,
            mpaytransaction.z_creation_date   trntime,
            (
            SELECT
            name
            FROM
            mpay_processingstatuses ps
            WHERE
            ps.id = mpaytransaction.processingstatusid
            ) trnstatus,
            mpaytransaction.reference         TRNREFERNCENO,
            mpaytransaction.originalamount    trnamount,
            mpaytransaction.reasondesc        trndescription,
            sendercustomer.iban               senderiban,
            sendercustomer.idnum              sendernin,
            sendercustomer.fullname           sendername,
            CASE
            WHEN sendercustomer.externalacc = '#####' THEN ''
            ELSE sendercustomer.externalacc
            END senderbankaccountnumber,
            senderaccount.accnumber           senderaccountnumber,
            sendercustomer.mobilenumber       sendermobilenumber,
            mpaytransaction.sendercharge      senderchargesamount,
            senderjv.amount                   senderamount,
            senderjv.balancebefore            senderbalancebefore,
            senderjv.balanceafter             senderbalanceafter,
            (
            SELECT
            name
            FROM
            mpay_banks b
            WHERE
            b.id = sendercustomer.bankid
            ) senderbank,
            mpaytransaction.totalcharge       sendervatamount,
            receivercustomer.iban             RECEIVERIBAN,
            receivercustomer.idnum            RECEIVERNIN,
            receivercustomer.fullname         RECEIVERNAME,
            CASE
            WHEN receivercustomer.externalacc = '#####' THEN ''
            ELSE receivercustomer.externalacc
            END RECEIVERBANKACCOUNTNUMBER,
            receiveraccount.accnumber         RECEIVERaccountnumber,
            receivercustomer.mobilenumber     RECEIVERMOBILENUMBER,
            mpaytransaction.sendercharge      RECEIVERCHARGESAMOUNT,
            receiverjv.amount                 RECEIVERNETAMOUNT,
            receiverjv.balancebefore          RECEIVERBALANCEBEFORE,
            receiverjv.balanceafter           RECEIVERbalanceafter,
            (
            SELECT
            name
            FROM
            mpay_banks b
            WHERE
            b.id = sendercustomer.bankid
            ) RECEIVERbank,
            mpaytransaction.totalcharge       RECEIVERVATAMOUNT,
            mpaytransaction.z_archive_on,
            mpaytransaction.z_assigned_user,
            mpaytransaction.z_archive_queued,
            mpaytransaction.z_archive_status,
            mpaytransaction.z_assigned_group,
            mpaytransaction.z_created_by,
            mpaytransaction.z_creation_date,
            mpaytransaction.z_deleted_by,
            mpaytransaction.z_deleted_flag,
            mpaytransaction.z_deleted_on,
            mpaytransaction.z_editable,
            mpaytransaction.z_locked_by,
            mpaytransaction.z_locked_until,
            mpaytransaction.z_org_id,
            mpaytransaction.z_tenant_id,
            mpaytransaction.z_updated_by,
            mpaytransaction.z_updating_date,
            mpaytransaction.z_workflow_id,
            mpaytransaction.z_ws_token,
            mpaytransaction.z_draft_status,
            mpaytransaction.z_draft_id,
            mpaytransaction.z_status_id,
            mpaytransaction.id id

            FROM
            mpay_transactions mpaytransaction
            LEFT JOIN mpay_customermobiles sendermobile ON ( sendermobile.id = mpaytransaction.sendermobileid )
            LEFT JOIN mpay_customermobiles recivermobile ON ( recivermobile.id = mpaytransaction.receivermobileid )
            LEFT JOIN mpay_accounts senderaccount ON senderaccount.id IN (
            SELECT
            refaccountid
            FROM
            mpay_mobileaccounts
            WHERE
            mobileid = sendermobile.id
            )
            LEFT JOIN mpay_accounts receiveraccount ON receiveraccount.id IN (
            SELECT
            refaccountid
            FROM
            mpay_mobileaccounts
            WHERE
            mobileid = recivermobile.id
            )
            LEFT JOIN mpay_customers sendercustomer ON ( sendercustomer.id = sendermobile.refcustomerid )
            LEFT JOIN mpay_customers receivercustomer ON ( receivercustomer.id = recivermobile.refcustomerid )
            LEFT JOIN mpay_jvdetails senderjv ON senderjv.reftrsansactionid = mpaytransaction.id
            AND senderjv.refaccountid = senderaccount.id
            LEFT JOIN mpay_jvdetails receiverjv ON receiverjv.reftrsansactionid = mpaytransaction.id
            AND receiverjv.refaccountid = receiveraccount.id
            WHERE
            refoperationid = 1
GO
/****** Object:  View [dbo].[mobile_trn_info]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[mobile_trn_info] AS SELECT DISTINCT m.id  mobileid,
            FIRST_VALUE(t.z_creation_date) OVER (PARTITION BY m.id ORDER BY t.z_creation_date DESC) mobilelasttrn,
            m.mobilenumber
            FROM mpay_customermobiles m,
            mpay_transactions t
            WHERE m.refcustomerid IN (SELECT id FROM mpay_customers)
            AND t.sendermobileid = m.id
            OR t.receivermobileid = m.id
GO
/****** Object:  View [dbo].[mobiledormant]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[mobiledormant] AS select mobile.id, mobile.mobilenumber, accounts.accountupdatedate, mobileaccount.REFACCOUNTID
            from mpay_customermobiles mobile,mpay_mobileaccounts mobileaccount,mpay_accounts accounts
            where mobile.id = mobileaccount.mobileid and mobile.Z_STATUS_ID = 55178985 and mobileaccount.refaccountid = accounts.id
            and (mobile.ISDORMANT is null or mobile.ISDORMANT = 0) and mobile.ISACTIVE = 1 and mobile.ISREGISTERED = 1
GO
/****** Object:  Table [dbo].[mpay_notificationchannels]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationchannels](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_notificationchannels_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_8pjqobp1uk4kjab2m6wpa8eh8] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_95wef9egqruond2tovns8x4k1] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customerdevices]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customerdevices](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[isonline] [bit] NOT NULL,
	[retrycount] [bigint] NOT NULL,
	[signinlocation] [nvarchar](1000) NULL,
	[activedevice] [bit] NULL,
	[approveddata] [nvarchar](4000) NULL,
	[deviceid] [nvarchar](255) NULL,
	[devicename] [nvarchar](100) NULL,
	[extradata] [nvarchar](1000) NULL,
	[extradata1] [nvarchar](1000) NULL,
	[extradata2] [nvarchar](1000) NULL,
	[extradata3] [nvarchar](1000) NULL,
	[isblocked] [bit] NOT NULL,
	[isdefault] [bit] NOT NULL,
	[isstolen] [bit] NULL,
	[lastlogintime] [datetime] NULL,
	[password] [nvarchar](255) NULL,
	[passwordlastchanged] [datetime] NULL,
	[publickey] [nvarchar](4000) NULL,
	[refreshtoken] [nvarchar](100) NULL,
	[sessionid] [nvarchar](1000) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refcustomermobileid] [bigint] NOT NULL,
 CONSTRAINT [mpay_customerdevices_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[mpay_customer_rep]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[mpay_customer_rep] AS SELECT @@rowcount                         ID,
            (SELECT getdate())     extractstamp,
            c.fullname                     customername,
            CASE
            WHEN c.CLIENTTYPEID IS NULL THEN 'UNBANKED'
            ELSE 'BANKED'
            END                        customertype,
            c.z_status_id,
            (
            SELECT description
            FROM jfw_wf_status
            WHERE id = c.z_status_id
            )                              status,
            CASE
            WHEN c.isactive = 1 THEN 'Yes'
            ELSE 'No'
            END                        active,
            c.idnum                        nin,
            (
            SELECT country_name
            FROM jfw_countries
            WHERE id = c.nationalityid
            )                              nationality,
            c.idexpirydate,
            (
            SELECT name
            FROM mpay_idtypes
            WHERE mpay_idtypes.id = c.idtypeid
            )                              idtype,
            c.dob,
            m.mobilenumber                 customermobile,
            c.iban,
            mpayaccount.accnumber          accountnumber,

            CASE
            WHEN c.externalacc = '#####' THEN ''
            ELSE c.externalacc
            END                        bankaccnumber,

            mpayaccount.balance            balances,
            CASE
            WHEN c.preflangid = 1 THEN 'en'
            ELSE 'Ar'
            END                        languageindicator,
            (
            SELECT name
            FROM mpay_notificationchannels
            WHERE id = c.notificationshowtype
            )                              notificationchannel,
            cd.deviceid                    deviceid,
            cd.devicename                  devicename,
            (
            SELECT description
            FROM jfw_wf_status
            WHERE id = cd.z_status_id
            )                              devicestatus,
            CASE
            WHEN cd.activedevice = 1 THEN 'Yes'
            ELSE 'No'
            END                        deviceactive,
            CASE
            WHEN cd.isstolen = 1 THEN 'Yes'
            ELSE 'No'
            END                        devicestolen,
            cd.retrycount                  deviceretry,
            c.maxnumberofdevices           maximumnumberofdevices,
            cd.lastlogintime               lastlogondate,
            c.address                      customeralladdressdetails,
            CASE
            WHEN c.gender = 1 THEN 'Male '
            ELSE 'Female'
            END                        gender,
            c.email                        emailaddress,
            'Active'                       dormancyflag,
            ma.lasttransactiondate         lasttransactiondate,
            CASE
            WHEN mpaytransaction.reftypeid = 1 THEN 'CR'
            ELSE 'DR'
            END                        lasttransactiontype,

            mpaytransaction.originalamount lasttransactionamount,
            c.z_created_by                 createduser,
            c.z_creation_date              createddate,
            c.z_updated_by                 modifieduser,
            c.z_updating_date              modifieddate,
            c.z_deleted_on                 deleteddate,
            c.z_deleted_by                 deleteduser,
            c.id                           customerId,
            c.z_updating_date,
            c.z_archive_on,
            c.z_assigned_user,
            c.z_archive_queued,
            c.z_archive_status,
            c.z_assigned_group,
            c.z_created_by,
            c.z_creation_date,
            c.z_deleted_by,
            c.z_deleted_flag,
            c.z_deleted_on,
            c.z_editable,
            c.z_locked_by,
            c.z_locked_until,
            c.z_org_id,
            c.z_tenant_id,
            c.z_workflow_id,
            c.z_ws_token,
            c.z_draft_status,
            c.z_draft_id,
            c.z_updated_by
            FROM mpay_customers c
            JOIN mpay_customermobiles m ON m.refcustomerid = c.id
            JOIN mpay_mobileaccounts ma ON ma.mobileid = m.id
            JOIN mpay_accounts mpayaccount ON ma.refaccountid = mpayaccount.id
            LEFT JOIN mpay_transactions mpaytransaction ON mpaytransaction.receivermobileid = m.id
            OR mpaytransaction.sendermobileid = m.id
            LEFT JOIN mpay_customerdevices cd ON cd.refcustomermobileid = m.id
            where mpaytransaction.z_creation_date IN (
            SELECT mobilelasttrn
            FROM mobile_trn_info mobiletrn
            WHERE mobiletrn.mobileid = m.id
            )
GO
/****** Object:  Table [dbo].[mpay_jvtypes]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_jvtypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_jvtypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_kenph39kfwsq4vgc6axopnl2r] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_qb59jihr6fmo5dnebik1hnber] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_endpointoperations]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_endpointoperations](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[isactive] [bit] NOT NULL,
	[ischargereversalenabled] [bit] NOT NULL,
	[ismobilerequest] [bit] NULL,
	[ispersistable] [bit] NULL,
	[issystem] [bit] NOT NULL,
	[istaxreversalenabled] [bit] NOT NULL,
	[operation] [nvarchar](100) NOT NULL,
	[processor] [nvarchar](1000) NOT NULL,
	[requiresdevice] [bit] NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[messagetypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_endpointoperations_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_76rysxbny7qfy6shfdjgkjhp6] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_jhey4357tpblo52epb8orp2q6] UNIQUE NONCLUSTERED 
(
	[operation] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[mpay_jv_report]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[mpay_jv_report] AS SELECT tx.id "TRANSACTIONID",
            tx.reference "TRANSACTIONREFERENCE",
            ops.name "OPERATION",
            jv.z_creation_date "CREATEDON",
            jv.serial "SERIAL",
            jv.description,
            (SELECT DESCRIPTION  FROM MPAY_PROCESSINGSTATUSES  WHERE id = tx.PROCESSINGSTATUSID) "PROCESSINGSTATUS",
            CONCAT(jvt.id, ' -', jvt.name) "JVENTRYTYPE", CASE  WHEN jv.debitcreditflag = 2 THEN 'Credit'
            ELSE 'Debit'
            END "DEPITORCREDITINDICATOR",
            acc.accnumber "ACCOUNT",'QAR - Qatari Rial' "CURRENCY",jv.amount,jv.isposted "POSTED",jv.balancebefore "BALANCEBEFORE",jv.balanceafter "BALANCEAFTER",

            (SELECT mobilenumber FROM mpay_customermobiles WHERE id IN (SELECT mobileid  FROM mpay_mobileaccounts
            WHERE refaccountid IN  (SELECT id FROM mpay_accounts c WHERE c.accnumber=acc.accnumber))) "MOBILENUMBER",
            jv.id,
            jv.z_updating_date,
            jv.z_archive_on,
            jv.z_assigned_user,
            jv.z_archive_queued,
            jv.z_archive_status,
            jv.z_assigned_group,
            jv.z_created_by,
            jv.z_creation_date,
            jv.z_deleted_by,
            jv.z_deleted_flag,
            jv.z_deleted_on,
            jv.z_editable,
            jv.z_locked_by,
            jv.z_locked_until,
            jv.z_org_id,
            jv.z_tenant_id,
            jv.z_workflow_id,
            jv.z_ws_token,
            jv.z_draft_status,
            jv.z_draft_id,
            jv.z_updated_by,
            jv.Z_STATUS_ID,
            tx.MPCLEARMESSAGEID "MPCLEARTRANSACTIONID"
            FROM mpay_jvdetails jv,
            mpay_transactions tx,
            mpay_endpointoperations ops,
            mpay_jvtypes jvt,
            mpay_accounts acc

            WHERE jv.reftrsansactionid = tx.id
            AND tx.refoperationid = ops.id
            AND jvt.id = jv.jvtypeid
            AND jv.refaccountid = acc.id
GO
/****** Object:  View [dbo].[servicedormant]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[servicedormant] AS select services.id, services.name, accounts.accountupdatedate, serviceaccount.REFACCOUNTID
            from mpay_corpoarteservices services,mpay_serviceaccounts serviceaccount,mpay_accounts accounts
            where services.id = serviceaccount.serviceid and services.Z_STATUS_ID = 1660010746
            and serviceaccount.refaccountid = accounts.id and (services.ISDORMANT is null or services.ISDORMANT = 0)
            and services.ISACTIVE = 1 and services.ISREGISTERED = 1 and (accounts.LEVEL1 <> '1' or accounts.LEVEL1 is null)
GO
/****** Object:  View [dbo].[serviceinfo]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[serviceinfo] AS SELECT DISTINCT
            name   servicename,
            FIRST_VALUE(t.z_creation_date) OVER(
            PARTITION BY name
            ORDER BY
            t.z_creation_date DESC
            ) servicelasttrn
            FROM
            mpay_transactions t,
            mpay_corpoarteservices s
            WHERE
            s.id = t.senderserviceid
            OR s.id = t.receiverserviceid
GO
/****** Object:  View [dbo].[sessionReportDetails]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[sessionReportDetails] AS select top 100 percent
            (SELECT getdate()) CREATIONDATE,SESSIONID, sum(TOTALAMOUNT) TOTALAMOUNT ,SUM(totalcharge) TOTALCHARGE ,
            DIRECTIONID DIRECTION ,MESSAGETYPEID MESSAGETYPE, CURRENCYID CURRENCYID, operation.ID ID
            from MPAY_TRANSACTIONS transactions,
            mpay_endpointoperations operation ,
            mpay_messagetypes messagetype
            where transactions.PROCESSINGSTATUSID= 1
            and operation.id= transactions.REFOPERATIONID
            and OPERATION.MESSAGETYPEID= messagetype.ID
            group by transactions.REFOPERATIONID,
            DIRECTIONID ,
            SESSIONID,
            OPERATION.MESSAGETYPEID,
            CURRENCYID,
            operation.ID
            order by SESSIONID desc
GO
/****** Object:  Table [dbo].[mpay_transactiondirections]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactiondirections](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_transactiondirections_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_6di1c017x5v7nryuy20hhs6ht] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_p28leg000qttghh2a0k3dq19d] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[MPAY_ArabReport]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[MPAY_ArabReport] as
select
       tx.id,
    case when tx.sendermobileid is not null then (select idnum from mpay_customers c,mpay_customermobiles cm where cm.refcustomerid = c.id and cm.id = tx.sendermobileid)
         else (select cc.registrationid from mpay_corporates cc,mpay_corpoarteservices mc where cc.id = mc.refcorporateid  and mc.id = tx.senderserviceid) end
        "PAYER_ID",tx.senderinfo "PAYER_MSISDN",'' "PAYER_EMAIL",'' "PAYER_DEVICE_ID",'' "PAYER_INSTRUMENT_TYPE",'' "PAYER_CARD_NUMBER",
    case when tx.receivermobileid is not null then (select idnum from mpay_customers c,mpay_customermobiles cm where cm.refcustomerid = c.id and cm.id = tx.receivermobileid)
         else (select cc.registrationid from mpay_corporates cc,mpay_corpoarteservices mc where cc.id = mc.refcorporateid  and mc.id = tx.receiverserviceid) end
        "PAYEE_ID", tx.receiverinfo "PAYEE_MSISDN",'' "PAYEE_EMAIL", '' "PAYEE_INSTRUMENT_TYPE", '' "PAYEE_ACCOUNT_NO", mm.code  + ' - ' + mt."name" "TRANSACTION_TYPE",
'MPAY' "CHANNEL_CODE",TX.reference "REFERENCE_ID1", '' "REFERENCE_ID2",TX.originalamount "AMOUNT",
TX.totalcharge "SERVICE_CHARGE_AMOUNT",'' "SERVICE_TAX_AMOUNT",'' "CREDIT_AMOUNT",'' "REMARKS",'Accepted' "STATUS",
tx.transdate "CREATED_ON",'' "MODIFIED_ON", '' "TRANSACTION_MODE", '' "TRANSACTION_STAGE", '' "CLIENT_REFERENCE_ID",
tx.mpclearmessageid "MPCLEAR_Message_Id",tx.SESSIONID "Session_Id"
from mpay_transactions tx, mpay_endpointoperations  ops, mpay_messagetypes mm , mpay_transactiondirections mt
where
    tx.refoperationid  = ops.id
  and ops.messagetypeid  = mm.id
  and tx.processingstatusid = 1
  and tx.reasonid = 1
  and tx.directionid  = mt.id
GO
/****** Object:  Table [dbo].[CustomerRegistration]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerRegistration](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[customermnemonic] [nvarchar](60) NULL,
	[mobilenumber] [nvarchar](20) NULL,
	[errormessage] [nvarchar](255) NULL,
	[requestdate] [date] NULL,
	[status] [nvarchar](20) NULL,
	[language] [nvarchar](20) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DATABASECHANGELOG]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DATABASECHANGELOG](
	[ID] [nvarchar](255) NOT NULL,
	[AUTHOR] [nvarchar](255) NOT NULL,
	[FILENAME] [nvarchar](255) NOT NULL,
	[DATEEXECUTED] [datetime2](3) NOT NULL,
	[ORDEREXECUTED] [int] NOT NULL,
	[EXECTYPE] [nvarchar](10) NOT NULL,
	[MD5SUM] [nvarchar](35) NULL,
	[DESCRIPTION] [nvarchar](255) NULL,
	[COMMENTS] [nvarchar](255) NULL,
	[TAG] [nvarchar](255) NULL,
	[LIQUIBASE] [nvarchar](20) NULL,
	[CONTEXTS] [nvarchar](255) NULL,
	[LABELS] [nvarchar](255) NULL,
	[DEPLOYMENT_ID] [nvarchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DATABASECHANGELOGLOCK]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DATABASECHANGELOGLOCK](
	[ID] [int] NOT NULL,
	[LOCKED] [bit] NOT NULL,
	[LOCKGRANTED] [datetime2](3) NULL,
	[LOCKEDBY] [nvarchar](255) NULL,
 CONSTRAINT [PK_DATABASECHANGELOGLOCK] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_ACCOUNT_POLICIES]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_ACCOUNT_POLICIES](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[allowConslowerLtrs] [bit] NULL,
	[allowConsNumbers] [bit] NULL,
	[allowConsUpperLtrs] [bit] NULL,
	[allowRptChar] [bit] NULL,
	[allowSeqLtrs] [bit] NULL,
	[allowSeqNum] [bit] NULL,
	[allowSeqSym] [bit] NULL,
	[CHANGE_TEMP_ID] [bigint] NULL,
	[CHANNEL] [varchar](255) NULL,
	[chatSupported] [bit] NULL,
	[CREATE_TEMP_ID] [bigint] NULL,
	[DAYS_TO_DISABLE] [int] NULL,
	[description] [varchar](255) NULL,
	[logoutLeastRecently] [bit] NULL,
	[LOGOUT_OUTSIDE_WORKING_HOURS] [bit] NULL,
	[LOGOUT_WHEN_PASSWORD_RESET] [bit] NULL,
	[LOGOUT_WHEN_USER_DISABLED] [bit] NULL,
	[maxConcurrentSessions] [int] NULL,
	[maxOldPasswords] [int] NULL,
	[maxPasswordAge] [int] NULL,
	[maxPasswordLength] [int] NULL,
	[maxUnsuccessfulAttempts] [int] NULL,
	[maximumChainFailedAttemps] [int] NULL,
	[minPasswordAge] [int] NULL,
	[minPasswordLength] [int] NULL,
	[passwordStrength] [int] NULL,
	[NOTIFY_ON_CHANGE_PASSWORD] [bit] NULL,
	[ONE_TIME_PASS_AGE] [int] NULL,
	[passwordRegex] [varchar](255) NULL,
	[RESET_PSW_ID] [bigint] NULL,
	[RESET_TEMP_ID] [bigint] NULL,
	[idleSessionTimeout] [int] NULL,
	[subNet] [varchar](255) NULL,
	[userPasswordValidityDuration] [varchar](255) NULL,
	[usernameLength] [int] NULL,
	[usernameRegex] [varchar](255) NULL,
	[USER_REGEX_SAMPLE] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[AUTH_CHAIN_SCHM] [bigint] NULL,
	[AUTH_SCHEM_ID] [bigint] NULL,
	[JFW_CALENDAR_ID] [bigint] NULL,
	[otpScheme_ID] [bigint] NULL,
	[SECRET_QUESTION_POLICY_ID] [bigint] NULL,
	[WORKING_HOURS_SCHED_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_ACCO__3213E83F87879799] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UKjpijrqo1rjlj19hifnh93agb2] UNIQUE NONCLUSTERED 
(
	[description] ASC,
	[Z_TENANT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_ACTION_LEVEL]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_ACTION_LEVEL](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[ALLOW_CONSEQUENT_TRIGGER] [bit] NULL,
	[ENABLE_RECORDING] [bit] NULL,
	[LEVEL_CODE] [bigint] NULL,
	[LEVEL_DESCRIPTION] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_ACTI__3214EC27FD73AEB8] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UK_8984vn18qfa19kxk88f02j0pr] UNIQUE NONCLUSTERED 
(
	[LEVEL_CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_ATTACHMENT_ITEM]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_ATTACHMENT_ITEM](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[attFile] [varbinary](max) NULL,
	[attachment_source] [varchar](255) NULL,
	[attachment_token] [varchar](255) NULL,
	[comments] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[entityId] [varchar](255) NULL,
	[image_thumbnail] [varbinary](max) NULL,
	[image_type] [varchar](255) NULL,
	[name] [varchar](255) NULL,
	[original_micr] [varchar](255) NULL,
	[recordId] [varchar](255) NULL,
	[REF_VALUE] [varchar](255) NULL,
	[REV] [int] NULL,
	[attachment_size] [numeric](19, 2) NULL,
	[security_level] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_ATTA__3213E83F2B8CD2F8] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_ATTACHMNT_DICTIONARIES]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_ATTACHMNT_DICTIONARIES](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[entityName] [varchar](255) NULL,
	[fileMaxSize] [bigint] NULL,
	[fileSizeUnit] [varchar](255) NULL,
	[maxAttachments] [int] NULL,
	[minAttachments] [int] NULL,
	[parentPartByProperty] [varchar](255) NULL,
	[partByProperty] [varchar](255) NULL,
	[security_level] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_ATTA__3213E83F74DB9948] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_AUTH_CHAIN]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_AUTH_CHAIN](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[CHAIN_DESC] [varchar](255) NULL,
	[CHAIN_KEY] [varchar](255) NOT NULL,
	[CHAIN_NAME] [varchar](255) NULL,
	[CHAIN_ORDER] [int] NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[chainScheme_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_AUTH__3214EC27EA1DD40F] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_AUTH_CHAIN_SCHM]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_AUTH_CHAIN_SCHM](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_AUTH__3214EC27738D2601] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_AUTHENTICATION_TOKEN]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_AUTHENTICATION_TOKEN](
	[id] [varchar](255) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[token] [varchar](max) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[lastUpdateTime] [datetime2](7) NULL,
	[username] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_AUTH__3213E83F19CD4091] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_BROLES_GENERIC_AUTH]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_BROLES_GENERIC_AUTH](
	[BROLE_ID] [bigint] NOT NULL,
	[AUTHORITY_ID] [bigint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_BROLES_RPRTS_AUTH]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_BROLES_RPRTS_AUTH](
	[BROLE_ID] [bigint] NOT NULL,
	[AUTHORITY_ID] [bigint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_BROLES_VIEWS]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_BROLES_VIEWS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[CONDITION_SCHEME_ID] [bigint] NULL,
	[VIEW_ID] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[BUSINESS_ROLE_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_BROL__3214EC2798EE69C1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UKka5qdnd7bw1u3dnm78cf7jqk2] UNIQUE NONCLUSTERED 
(
	[VIEW_ID] ASC,
	[BUSINESS_ROLE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_BUSINESS_ROLES]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_BUSINESS_ROLES](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[NAME] [varchar](255) NULL,
	[SECURITY_LEVEL] [varchar](255) NULL,
	[shownToChilds] [bit] NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[ACTION_LEVEL] [bigint] NULL,
 CONSTRAINT [PK__JFW_BUSI__3214EC27382D14EB] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UKa0th4kippq1ov3iyilymhfljf] UNIQUE NONCLUSTERED 
(
	[NAME] ASC,
	[Z_ORG_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_BUSINESS_ROLES_ACTIONS]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_BUSINESS_ROLES_ACTIONS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[WORKFLOW_ACTION] [bigint] NULL,
	[CONDITION_SCHEME_ID] [bigint] NULL,
	[HAS_PRIVILEGE] [bit] NULL,
	[WORKFLOW_STEP] [bigint] NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[BUSINESS_ROLE_ID] [bigint] NULL,
	[BUSINESS_ROLE_VIEW_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_BUSI__3214EC2767FD669D] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_BUSINESS_ROLES_REPORT]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_BUSINESS_ROLES_REPORT](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[BR_NAME] [varchar](255) NULL,
	[BR_DESC] [varchar](255) NULL,
	[BRV_NAME] [varchar](255) NULL,
	[STEP_NAME] [varchar](255) NULL,
	[ACTION_NAME] [varchar](255) NULL,
	[HAS_PRIVI] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JBRReports__BRGLE83AHMDC0REH] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_CALENDAR]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_CALENDAR](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[CODE] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_CALE__3214EC27111C335B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_CHANGE_DICTIONARY]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_CHANGE_DICTIONARY](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[changeItemTable] [varchar](255) NULL,
	[changeSetTable] [varchar](255) NULL,
	[security_level] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_CHAN__3213E83F844B5649] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_CHANGE_GROUP]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_CHANGE_GROUP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[actionName] [varchar](255) NULL,
	[auditChangeType] [int] NULL,
	[AUDIT_CHANGE_TYPE_DES] [varchar](255) NULL,
	[BRANCH] [varchar](255) NULL,
	[changeField] [varchar](255) NULL,
	[changeFieldDescription] [varchar](255) NULL,
	[changeFieldName] [varchar](255) NULL,
	[change_history_token] [varchar](255) NULL,
	[draft] [bit] NULL,
	[entityId] [varchar](255) NULL,
	[recordId] [varchar](255) NULL,
	[REF_VALUE] [varchar](255) NULL,
	[security_level] [varchar](255) NULL,
	[USER_GROUP] [varchar](255) NULL,
	[USERID] [varchar](255) NULL,
	[USERNAME] [varchar](255) NULL,
	[viewDescription] [varchar](255) NULL,
	[viewName] [varchar](255) NULL,
	[workstation] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_CHAN__3213E83F623C77E6] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_CHANGE_ITEM]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_CHANGE_ITEM](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[field] [varchar](255) NULL,
	[fieldDescription] [varchar](255) NULL,
	[ip_Address] [varchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [varchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [PK__JFW_CHAN__3213E83FF278E20B] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_COMMENT_ITEM]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_COMMENT_ITEM](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[commentToken] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[entityId] [varchar](255) NULL,
	[recordId] [varchar](255) NULL,
	[REF_VALUE] [varchar](255) NULL,
	[security_level] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_COMM__3213E83F0F973E3D] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_CONDITION]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_CONDITION](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[provider] [varchar](255) NULL,
	[security_level] [varchar](255) NULL,
	[type] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_COND__3213E83F0E5EE238] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_CONFIRM_MSG]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_CONFIRM_MSG](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[cond_bean] [varchar](255) NULL,
	[desc_id] [varchar](255) NOT NULL,
	[msg_id] [varchar](255) NOT NULL,
 CONSTRAINT [PK__JFW_CONF__3213E83FCD5258D2] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_COUNTRY_TRANSLATION]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_COUNTRY_TRANSLATION](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[languageCode] [varchar](255) NULL,
	[translation] [varchar](255) NOT NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[country_id] [bigint] NULL,
 CONSTRAINT [PK__JFW_COUN__3213E83FDD175E47] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_CUSTOM_AUTHORITIES]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_CUSTOM_AUTHORITIES](
	[AUTHORITY_NAME] [varchar](255) NOT NULL,
 CONSTRAINT [PK__JFW_CUST__DA956DC21F4B85FA] PRIMARY KEY CLUSTERED 
(
	[AUTHORITY_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_CUSTOM_REPORTS]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_CUSTOM_REPORTS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[NAME] [varchar](255) NULL,
	[REPORT_FILTER_HTML] [varbinary](max) NULL,
	[REPORT_JASPER] [varbinary](max) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[AUTHORITY_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_CUST__3214EC272493F38B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_DRAFTS]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_DRAFTS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[DRAFT_DATA] [varchar](max) NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_DRAF__3214EC27DC910E53] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_EMAIL_PROVIDER_CONFIG]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_EMAIL_PROVIDER_CONFIG](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[PARAMS] [varchar](max) NULL,
	[PROVIDER_IDENTIFIER] [varchar](255) NULL,
	[TYPE] [int] NULL,
 CONSTRAINT [PK__JFW_EMAI__3214EC27C0A1A011] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_EXCEPTIONAL_WORKING_DAYS]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_EXCEPTIONAL_WORKING_DAYS](
	[EXPWD_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[IS_ACTIVE] [bit] NULL,
	[IS_CALC_FLAG] [bit] NULL,
	[EXPWD_DESC] [varchar](255) NULL,
	[EXPWD_FRM] [date] NULL,
	[EXPWD_TO] [date] NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[CALENDAR_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_EXCE__57D48FC0C7EBFC41] PRIMARY KEY CLUSTERED 
(
	[EXPWD_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_EXCPTN_HNDLR]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_EXCPTN_HNDLR](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[EXCEPTION_HANDLER] [varchar](255) NULL,
	[VIEW_NAME] [varchar](255) NULL,
	[EXCPTN_HNDLR_SCHME_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_EXCP__3214EC27D717D46A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_EXCPTN_HNDLR_SCHEME]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_EXCPTN_HNDLR_SCHEME](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[FALL_BACK_HANDLER] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_EXCP__3214EC273FAFD127] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_FLD_USR_LGN_LOG]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_FLD_USR_LGN_LOG](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[LST_FLD_LGN_DATE] [datetime2](7) NULL,
	[LST_FLD_LGN_LCN] [varchar](255) NULL,
	[NUM_OF_FLD_ATTEMPS] [int] NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[USER_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_FLD___3213E83FE975382D] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_GENERIC_AUTHORITIES]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_GENERIC_AUTHORITIES](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[groupLabel] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[name] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[userType_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_GENE__3213E83F836D11FD] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_GENERIC_CONFIGURATION]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_GENERIC_CONFIGURATION](
	[config_key] [varchar](255) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[config_value] [varchar](2000) NULL,
	[NUMERIC_ID] [bigint] NULL,
	[security_level] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_GENE__BDF6033C3D0F4AE4] PRIMARY KEY CLUSTERED 
(
	[config_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_GENERIC_LKUP_CAT]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_GENERIC_LKUP_CAT](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[code] [varchar](255) NOT NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](255) NOT NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_GENE__3213E83F996FB46E] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UKenlmgu0hapfwdnnxhxf84mbt1] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[Z_TENANT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_GENERIC_LKUP_CAT_TRANS]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_GENERIC_LKUP_CAT_TRANS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[languageCode] [varchar](255) NULL,
	[translation] [varchar](255) NOT NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[genericLookupCategory_id] [bigint] NULL,
 CONSTRAINT [PK__JFW_GENE__3213E83F7590B07B] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_GENERIC_LKUP_TRANS]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_GENERIC_LKUP_TRANS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[languageCode] [varchar](255) NULL,
	[translation] [varchar](255) NOT NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[genericLookup_id] [bigint] NULL,
 CONSTRAINT [PK__JFW_GENE__3213E83F5201CA95] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_GENERIC_LOOKUP]    Script Date: 9/6/2021 12:23:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_GENERIC_LOOKUP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[code] [varchar](255) NOT NULL,
	[description] [varchar](255) NULL,
	[name] [varchar](255) NOT NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[genericLookupCategory_id] [bigint] NULL,
 CONSTRAINT [PK__JFW_GENE__3213E83F4334E884] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_GLOBAL_CONTROL_FIELDS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_GLOBAL_CONTROL_FIELDS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[assignedby] [varchar](255) NULL,
	[assignedto] [varchar](255) NULL,
	[createdby] [varchar](255) NULL,
	[createdon] [datetime2](7) NULL,
	[deleted] [smallint] NOT NULL,
	[modifiedby] [varchar](255) NULL,
	[modifiedon] [datetime2](7) NULL,
	[recordId] [numeric](19, 2) NULL,
	[status] [smallint] NOT NULL,
	[tableId] [varchar](255) NULL,
	[version] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_GLOB__3213E83FC7CF7301] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_GROUP_MEMBERS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_GROUP_MEMBERS](
	[GROUP_ID] [bigint] NOT NULL,
	[user_id] [bigint] NOT NULL,
 CONSTRAINT [PK__JFW_GROU__4A51DD326C911721] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC,
	[GROUP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_GROUPS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_GROUPS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[ExternalGroup] [bit] NULL,
	[groupName] [varchar](255) NULL,
	[security_level] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[rootOrg_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_GROU__3213E83F4405386F] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UKnor5e75316sm4yfmp77mfv003] UNIQUE NONCLUSTERED 
(
	[Z_ORG_ID] ASC,
	[groupName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_GROUPS_BUSINESS_ROLES]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_GROUPS_BUSINESS_ROLES](
	[BROLE_ID] [bigint] NOT NULL,
	[GROUP_ID] [bigint] NOT NULL,
 CONSTRAINT [PK__JFW_GROU__78C3BFA028AC5ECC] PRIMARY KEY CLUSTERED 
(
	[GROUP_ID] ASC,
	[BROLE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_HOLIDAYS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_HOLIDAYS](
	[HLDY_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CALC_FLAG] [bit] NULL,
	[HLDY_DESC] [varchar](255) NULL,
	[HLDY_FRM] [date] NULL,
	[HLDY_TO] [date] NULL,
	[RECURING_HOLIDAY] [bit] NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[CALENDAR_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_HOLI__069E67C0FBA1251B] PRIMARY KEY CLUSTERED 
(
	[HLDY_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_IMPORT_LOG]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_IMPORT_LOG](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[ERROR_MESSAGE] [varchar](255) NULL,
	[EXECUTED_HANDLER] [varchar](255) NULL,
	[IMPORT_LOG] [varbinary](max) NULL,
	[IMPORTED_DATA] [varbinary](max) NULL,
	[IMPORTER_ID] [bigint] NULL,
	[STATUS] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_IMPO__3214EC27D133600F] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_LOGIN_TOKEN]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_LOGIN_TOKEN](
	[ONE_TIME_USE_ID] [varchar](255) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[FULL_USERNAME] [varchar](255) NULL,
	[LOCALE] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_LOGI__FEFD21E90E93E750] PRIMARY KEY CLUSTERED 
(
	[ONE_TIME_USE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_MESSAGE_LOG]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_MESSAGE_LOG](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[flag] [bigint] NOT NULL,
	[name] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_MESS__3213E83F24E995A0] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UK13cd757ofthygp1okm639qona] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_MESSAGE_PRM]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_MESSAGE_PRM](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[PARAM_NAME] [varchar](255) NOT NULL,
	[PARAM_VALUE] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[MESSAGE_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_MESS__3214EC27DAFD9EEE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_MESSAGES]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_MESSAGES](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[MESSAGE_BODY] [varchar](4000) NULL,
	[MESSAGE_CATEGORY] [varchar](255) NULL,
	[ITEM_ID] [varchar](255) NULL,
	[MESSAGE_DATE] [datetime2](7) NULL,
	[MESSAGE_STR_DATE] [varchar](255) NULL,
	[MESSAGE_DAY] [date] NULL,
	[MESSAGE_GROUP] [varchar](255) NULL,
	[MESSAGE_KEY] [varchar](255) NOT NULL,
	[MESSAGE_LABELS] [varchar](255) NULL,
	[OPEN_VIEW_AS] [varchar](255) NULL,
	[MESSAGE_REFERENCE] [varchar](255) NULL,
	[MESSAGE_TITLE] [varchar](255) NULL,
	[TO_USERS_LIST] [varchar](4000) NULL,
	[MESSAGE_TYPE] [varchar](255) NULL,
	[VIEW_NAME] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[FROM_USER] [bigint] NULL,
	[TO_USER] [bigint] NULL,
 CONSTRAINT [PK__JFW_MESS__3214EC27DFCEA91A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_NOTIFICATION_IM]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_NOTIFICATION_IM](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[BODY] [varchar](max) NULL,
	[SOURCE_ADDRESS] [varchar](255) NULL,
	[IS_READ] [bit] NULL,
	[SUBJECT] [varchar](255) NULL,
	[user_id] [bigint] NULL,
 CONSTRAINT [PK__JFW_NOTI__3214EC270CF0E140] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_NOTIFY_RESULT_GROUP]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_NOTIFY_RESULT_GROUP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[VIEW_ID] [bigint] NULL,
	[NOTIF_SCHEME_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_NOTI__3213E83F241F32F4] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UKcy5n6ldgt5867frehnwawnvls] UNIQUE NONCLUSTERED 
(
	[VIEW_ID] ASC,
	[NOTIF_SCHEME_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_NOTIFY_SCHEME_GROUPS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_NOTIFY_SCHEME_GROUPS](
	[SCHEME_ID] [bigint] NOT NULL,
	[GROUP_ID] [bigint] NOT NULL,
 CONSTRAINT [PK__JFW_NOTI__C61322FEABC8F39D] PRIMARY KEY CLUSTERED 
(
	[SCHEME_ID] ASC,
	[GROUP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_NOTIFY_SCHEME_ROLES]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_NOTIFY_SCHEME_ROLES](
	[SCHEME_ID] [bigint] NOT NULL,
	[ROLE_ID] [bigint] NOT NULL,
 CONSTRAINT [PK__JFW_NOTI__005085E1C1E51F5A] PRIMARY KEY CLUSTERED 
(
	[SCHEME_ID] ASC,
	[ROLE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_NOTIFY_SCHEMES]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_NOTIFY_SCHEMES](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[BODY_TEMP] [varchar](max) NULL,
	[CHANNEL] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[EXCLUDE_EXECUTER] [bit] NULL,
	[NOTIFY_ORG] [bit] NULL,
	[RE_NOTIFY] [bit] NULL,
	[RE_NOTIFY_EVERY] [int] NULL,
	[SUBJECT_TEMP] [varchar](255) NULL,
	[WATCHERS_SCHEME] [bit] NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[EMAIL_PROVIDER_ID] [bigint] NULL,
	[ORG_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_NOTI__3213E83F35BDA514] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UKj87ve5r4nm7vx174k5okxo69f] UNIQUE NONCLUSTERED 
(
	[Z_ORG_ID] ASC,
	[DESCRIPTION] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_NOTIFY_WF_RESULT]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_NOTIFY_WF_RESULT](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[ENABLED] [bit] NULL,
	[result] [varbinary](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[resultGroup_id] [bigint] NULL,
 CONSTRAINT [PK__JFW_NOTI__3214EC2733AEBCF4] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_OLD_PASSWORDS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_OLD_PASSWORDS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[user_id] [bigint] NULL,
 CONSTRAINT [PK__JFW_OLD___3213E83F079A7511] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_ORG_COMM_CHANNELS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_ORG_COMM_CHANNELS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[EMAIL_ADDRESS] [varchar](255) NULL,
	[EMAIL_NAME] [varchar](255) NULL,
	[SMPP_HOST] [varchar](255) NULL,
	[SMPP_PASSWORD] [varchar](255) NULL,
	[SMPP_PORT] [int] NULL,
	[SMPP_SOURCE_ADDRESS] [varchar](255) NULL,
	[SMPP_SYSTEM_ID] [varchar](255) NULL,
	[SMPP_SYSTEM_TYPE] [varchar](255) NULL,
	[SMPP_TON] [varchar](255) NULL,
	[SMTP_AUTH_REQ] [bit] NULL,
	[SMTP_HOST] [varchar](255) NULL,
	[SMTP_PASSWORD] [varchar](255) NULL,
	[SMTP_PORT] [int] NULL,
	[SMTP_USERNAME] [varchar](255) NULL,
	[ORG_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_ORG___3214EC27DC0D3848] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_ORG_PREFERENCES]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_ORG_PREFERENCES](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[PREF_NAME] [varchar](255) NULL,
	[PREF_VALUE] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[ASSIGNED_ORG_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_ORG___3214EC272CF64235] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_ORG_TYPES]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_ORG_TYPES](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_ORG___3214EC27A094C6D8] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_ORG_WORKFLOW]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_ORG_WORKFLOW](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[WORKFLOW_NAME] [varchar](255) NULL,
	[WORKFLOW_ORG] [varchar](255) NULL,
	[ORG_WRKFLW_SCHME_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_ORG___3214EC271A3DC686] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_ORG_WORKFLOW_SCHM]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_ORG_WORKFLOW_SCHM](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[FALLBACK_WORKFLOW] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_ORG___3214EC27DC373668] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_ORGS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_ORGS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[ABBREVIATION] [varchar](255) NULL,
	[ORG_ADDRESS] [varchar](255) NULL,
	[ALLOW_DELEGATION] [bit] NULL,
	[CHECKER_ADMIN_EMAIL] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[DUTIES_SEPARATION_TYPE] [int] NULL,
	[ORG_EMAIL] [varchar](255) NULL,
	[EXTERNAL_ORG] [bit] NULL,
	[EXTRA_INFO] [varchar](255) NULL,
	[HAS_ADMIN] [bit] NULL,
	[MAKER_ADMIN_EMAIL] [varchar](255) NULL,
	[ORG_Name] [varchar](255) NULL,
	[ORG_PHONE_NUMBER] [varchar](255) NULL,
	[REALM_NAME] [varchar](255) NULL,
	[ORG_REPORT_NARRATIVE] [varchar](255) NULL,
	[SECURITY_LEVEL] [varchar](255) NULL,
	[ORG_SHORT_NAME] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[VIEWS_SCHME] [bigint] NULL,
	[ORG_TYPE_CD] [bigint] NULL,
	[PARENT_ID] [bigint] NULL,
	[TERM_CONDITION] [bigint] NULL,
 CONSTRAINT [PK__JFW_ORGS__3214EC27D3C2F4B4] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UK4d8vl78sdcccpkbhayrgg4ao9] UNIQUE NONCLUSTERED 
(
	[Z_TENANT_ID] ASC,
	[ORG_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UK6soi85waomtumjs1bnvntksw1] UNIQUE NONCLUSTERED 
(
	[ABBREVIATION] ASC,
	[Z_TENANT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UKp6n2c1txsvaidxolgt9bg8gdt] UNIQUE NONCLUSTERED 
(
	[Z_TENANT_ID] ASC,
	[ORG_SHORT_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_OS_CURRENTSTEP]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_OS_CURRENTSTEP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[action_id] [int] NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[caller] [varchar](255) NULL,
	[due_date] [datetime2](7) NULL,
	[finish_date] [datetime2](7) NULL,
	[owner] [varchar](255) NULL,
	[start_date] [datetime2](7) NULL,
	[status] [varchar](255) NULL,
	[step_id] [int] NULL,
	[entry_id] [bigint] NULL,
 CONSTRAINT [PK__JFW_OS_C__3213E83FEF46EED9] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_OS_HISTORYSTEP]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_OS_HISTORYSTEP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[action_id] [int] NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[caller] [varchar](255) NULL,
	[due_date] [datetime2](7) NULL,
	[finish_date] [datetime2](7) NULL,
	[owner] [varchar](255) NULL,
	[start_date] [datetime2](7) NULL,
	[status] [varchar](255) NULL,
	[step_id] [int] NULL,
	[entry_id] [bigint] NULL,
 CONSTRAINT [PK__JFW_OS_H__3213E83FE43FCACB] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_OS_PROPERTYSET]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_OS_PROPERTYSET](
	[entity_id] [bigint] NOT NULL,
	[entity_key] [varchar](255) NOT NULL,
	[entity_name] [varchar](255) NOT NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[boolean_val] [bit] NULL,
	[data_val] [varbinary](max) NULL,
	[date_val] [datetime2](7) NULL,
	[double_val] [float] NULL,
	[int_val] [int] NULL,
	[key_type] [int] NULL,
	[long_val] [bigint] NULL,
	[string_val] [varchar](max) NULL,
 CONSTRAINT [PK__JFW_OS_P__8A0E64F6711C7F8E] PRIMARY KEY CLUSTERED 
(
	[entity_id] ASC,
	[entity_key] ASC,
	[entity_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_OS_WFENTRY]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_OS_WFENTRY](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[name] [varchar](255) NULL,
	[state] [int] NULL,
	[version] [int] NULL,
 CONSTRAINT [PK__JFW_OS_W__3213E83F747746CB] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_OTP_SCHEME]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_OTP_SCHEME](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[codeLength] [varchar](255) NULL,
	[codeValidityDuration] [varchar](255) NULL,
	[gatewayImpl] [varchar](255) NULL,
	[name] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_OTP___3213E83F6895E736] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_RESOURCE_TYPES]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_RESOURCE_TYPES](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[NAME] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_RESO__3213E83FF32C8841] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_RPTS_AUTHORITIES]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_RPTS_AUTHORITIES](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[groupLabel] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[name] [varchar](255) NULL,
	[REPORT_DESCRIPTION] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[userType_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_RPTS__3213E83FA54E53D1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_SCHM_VIEWS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_SCHM_VIEWS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[VIEW_ID] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[VIEWS_SCHM_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_SCHM__3214EC27135CB556] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_SECRET_QUESTION_POLICY]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_SECRET_QUESTION_POLICY](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[POLICY_DESCRIPTION] [varchar](255) NULL,
	[MAX_ANSWER_LENGTH] [int] NULL,
	[MIN_ANSWER_LENGTH] [int] NULL,
	[NUM_OF_LGN_CORCT_ANSRS] [int] NULL,
	[NUM_OF_LOGIN_QUESTIONS] [int] NULL,
	[NUM_OF_PRF_COMP_QUES] [int] NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_SECR__3213E83F9DB423D3] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_SECRET_QUESTIONS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_SECRET_QUESTIONS](
	[QUESTION_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[QUESTION] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_SECR__30BE07AD41F18E39] PRIMARY KEY CLUSTERED 
(
	[QUESTION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_SECURITY_ADT_CHNG_GRP]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_SECURITY_ADT_CHNG_GRP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[actionName] [varchar](255) NULL,
	[auditChangeType] [int] NULL,
	[AUDIT_CHANGE_TYPE_DES] [varchar](255) NULL,
	[BRANCH] [varchar](255) NULL,
	[changeField] [varchar](255) NULL,
	[changeFieldDescription] [varchar](255) NULL,
	[changeFieldName] [varchar](255) NULL,
	[change_history_token] [varchar](255) NULL,
	[draft] [bit] NULL,
	[entityId] [varchar](255) NULL,
	[recordId] [varchar](255) NULL,
	[REF_VALUE] [varchar](255) NULL,
	[security_level] [varchar](255) NULL,
	[USER_GROUP] [varchar](255) NULL,
	[USERID] [varchar](255) NULL,
	[USERNAME] [varchar](255) NULL,
	[viewDescription] [varchar](255) NULL,
	[viewName] [varchar](255) NULL,
	[workstation] [varchar](255) NULL,
	[auditDescription] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_SECU__3213E83FFB83349D] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_SECURITY_ADT_CHNG_ITEM]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_SECURITY_ADT_CHNG_ITEM](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[field] [varchar](255) NULL,
	[fieldDescription] [varchar](255) NULL,
	[ip_Address] [varchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [varchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [PK__JFW_SECU__3213E83F68FAA389] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_SECURITY_ALERTS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_SECURITY_ALERTS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[BODY_TEMP] [varchar](max) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[FAILED_LOGIN_COUNT] [int] NULL,
	[NOTIFY_INVALID_USER] [bit] NULL,
	[SUBJECT_TEMP] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_SECU__3213E83F09EB7777] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_SECURITY_ALERTS_USER]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_SECURITY_ALERTS_USER](
	[SCHEME_ID] [bigint] NOT NULL,
	[USER_ID] [bigint] NOT NULL,
 CONSTRAINT [PK__JFW_SECU__DAC7267CB5373F75] PRIMARY KEY CLUSTERED 
(
	[SCHEME_ID] ASC,
	[USER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_SECURITY_CHANGE_GROUP]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_SECURITY_CHANGE_GROUP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[actionName] [varchar](255) NULL,
	[auditChangeType] [int] NULL,
	[AUDIT_CHANGE_TYPE_DES] [varchar](255) NULL,
	[BRANCH] [varchar](255) NULL,
	[changeField] [varchar](255) NULL,
	[changeFieldDescription] [varchar](255) NULL,
	[changeFieldName] [varchar](255) NULL,
	[change_history_token] [varchar](255) NULL,
	[draft] [bit] NULL,
	[entityId] [varchar](255) NULL,
	[recordId] [varchar](255) NULL,
	[REF_VALUE] [varchar](255) NULL,
	[security_level] [varchar](255) NULL,
	[USER_GROUP] [varchar](255) NULL,
	[USERID] [varchar](255) NULL,
	[USERNAME] [varchar](255) NULL,
	[viewDescription] [varchar](255) NULL,
	[viewName] [varchar](255) NULL,
	[workstation] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_SECU__3213E83FE96F02BE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_SECURITY_CHANGE_ITEM]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_SECURITY_CHANGE_ITEM](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[field] [varchar](255) NULL,
	[fieldDescription] [varchar](255) NULL,
	[ip_Address] [varchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [varchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [PK__JFW_SECU__3213E83F3893632F] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_SESSION_INFORMATION]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_SESSION_INFORMATION](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[applicationInstanceName] [varchar](255) NULL,
	[browser] [varchar](255) NULL,
	[hashData] [varchar](255) NULL,
	[hashToken] [varchar](255) NULL,
	[server] [varchar](255) NULL,
	[sessionId] [varchar](255) NULL,
	[userId] [bigint] NULL,
	[userName] [varchar](255) NULL,
	[userType] [varchar](255) NULL,
	[workStation] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_SESS__3213E83F8E475DAA] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_SUCC_USR_LGN_LOGS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_SUCC_USR_LGN_LOGS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[LST_SUCC_LGN_DATE] [datetime2](7) NULL,
	[LST_SUCC_LGN_LCN] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[USER_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_SUCC__3213E83F4F6129AA] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_SYS_NOTIFICATIONS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_SYS_NOTIFICATIONS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[BODY] [varchar](max) NULL,
	[ENTITY_ID] [varchar](255) NULL,
	[ENTITY_LAST_EXECUTED] [datetime2](7) NULL,
	[ENTITY_STEP_ID] [int] NULL,
	[ENTITY_TYPE] [varchar](255) NULL,
	[ENTITY_WF_ID] [bigint] NULL,
	[FAILURE_DESC] [varchar](255) NULL,
	[SUBJECT] [varchar](255) NULL,
	[WATCHER_NOTIFICATION] [bit] NULL,
	[workflowResult] [varbinary](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[EXECUTED_BY_ID] [bigint] NULL,
	[SCHEME_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_SYS___3213E83F433B48D6] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_TENANT]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_TENANT](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[checkerAdminEmail] [varchar](255) NULL,
	[makerAdminEmail] [varchar](255) NULL,
	[maxConcurrentUserLogin] [int] NULL,
	[name] [varchar](255) NULL,
	[STATUS_BAR_ID] [bigint] NULL,
	[superOrgEmail] [varchar](255) NULL,
	[superOrgId] [varchar](255) NULL,
	[TENANT_DESCRIPTION] [varchar](255) NULL,
	[TENANT_KEY] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[FAILED_LOGIN_SCHEME] [bigint] NULL,
	[VIEWS_SCHME] [bigint] NULL,
	[SUPER_ORG_VIEWS_SCHME] [bigint] NULL,
 CONSTRAINT [PK__JFW_TENA__3214EC27C280916D] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UK_kt82bn5ro7r3v8s9o59c2pqug] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_TERMS_CONDITIONS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_TERMS_CONDITIONS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[INHERIT_TO_CHILD] [bit] NULL,
	[TERMS_CONDITIONS] [varchar](max) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_TERM__3214EC274C9A26F7] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_USER_NOTIFICATIONS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_USER_NOTIFICATIONS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[BODY] [varchar](max) NULL,
	[CHANNEL] [varchar](255) NULL,
	[FAILURE_DESC] [varchar](255) NULL,
	[SUBJECT] [varchar](255) NULL,
	[TARGET_EMAIL] [varchar](255) NULL,
	[TARGET_NAME] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[SYS_NOTIF_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_USER__3213E83F68FC0976] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_USER_PREFS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_USER_PREFS](
	[user_id] [bigint] NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[is_newsletter_event_enabled] [bit] NULL,
	[is_on_action_event_enabled] [bit] NULL,
	[locale] [varchar](255) NULL,
	[multiTabs] [bit] NULL,
	[security_level] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_USER__B9BE370F8E8427AC] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_USER_PROFILE]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_USER_PROFILE](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[ARCHIVE_USER] [bit] NULL,
	[BY_PASS_LOCK] [bit] NULL,
	[clear_username] [varchar](255) NULL,
	[displayName] [varchar](255) NULL,
	[mail] [varchar](255) NULL,
	[enabled] [bit] NULL,
	[EXTERNAL_USER] [bit] NULL,
	[extraInfo1] [varchar](255) NULL,
	[extraInfo2] [varchar](255) NULL,
	[extraInfo3] [varchar](255) NULL,
	[extraInfo4] [varchar](255) NULL,
	[extraInfo5] [varchar](255) NULL,
	[failedCaptchaLoginAttempts] [int] NULL,
	[failedLoginAttempts] [int] NULL,
	[FALLBACK_DASH_NAME] [varchar](255) NULL,
	[FIRST_TIME_PASSWORD] [bit] NULL,
	[inetuserstatus] [varchar](255) NULL,
	[ipAddress] [varchar](255) NULL,
	[MOBILE_NUMBER] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[passwordDate] [date] NULL,
	[prefOrgId] [bigint] NULL,
	[removed] [bit] NULL,
	[security_level] [varchar](255) NULL,
	[SHOW_SERVIC_ACTION] [bit] NULL,
	[TERMS_ACCEPTED] [bit] NULL,
	[USER_KEY] [varchar](255) NULL,
	[username] [varchar](255) NULL,
	[webServiceOnly] [bit] NOT NULL,
	[SECRET_QUESTION_LINK] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_USER__3213E83F17592CB7] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_USER_SECRET_ANSWERS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_USER_SECRET_ANSWERS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[ANSWER] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[QUESTION_ID] [bigint] NULL,
	[USER_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_USER__3213E83F35EBBA37] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_USER_STATE_PREF]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_USER_STATE_PREF](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[formId] [bigint] NULL,
	[key1] [varchar](255) NULL,
	[key2] [varchar](255) NULL,
	[name] [varchar](255) NULL,
	[originalValue] [varchar](max) NULL,
	[scanLocId] [bigint] NULL,
	[stateType] [varchar](255) NULL,
	[userId] [bigint] NULL,
	[value] [varchar](max) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_USER__3213E83F7B24FC8F] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_USER_TYPES]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_USER_TYPES](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_USER__3214EC27EB9F8982] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_USERS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_USERS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[ARCHIVE_USER] [bit] NULL,
	[BY_PASS_LOCK] [bit] NULL,
	[clear_username] [varchar](255) NULL,
	[displayName] [varchar](255) NULL,
	[mail] [varchar](255) NULL,
	[enabled] [bit] NULL,
	[EXTERNAL_USER] [bit] NULL,
	[extraInfo1] [varchar](255) NULL,
	[extraInfo2] [varchar](255) NULL,
	[extraInfo3] [varchar](255) NULL,
	[extraInfo4] [varchar](255) NULL,
	[extraInfo5] [varchar](255) NULL,
	[failedCaptchaLoginAttempts] [int] NULL,
	[failedLoginAttempts] [int] NULL,
	[FALLBACK_DASH_NAME] [varchar](255) NULL,
	[FIRST_TIME_PASSWORD] [bit] NULL,
	[inetuserstatus] [varchar](255) NULL,
	[ipAddress] [varchar](255) NULL,
	[MOBILE_NUMBER] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[passwordDate] [date] NULL,
	[prefOrgId] [bigint] NULL,
	[removed] [bit] NULL,
	[security_level] [varchar](255) NULL,
	[SHOW_SERVIC_ACTION] [bit] NULL,
	[TERMS_ACCEPTED] [bit] NULL,
	[USER_KEY] [varchar](255) NULL,
	[username] [varchar](255) NULL,
	[webServiceOnly] [bit] NOT NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[accountPolicy_id] [bigint] NULL,
	[rootOrg_ID] [bigint] NULL,
	[USER_PROFILE_ID] [bigint] NULL,
	[USER_TYPE_CD] [bigint] NULL,
 CONSTRAINT [PK__JFW_USER__3213E83FD822D650] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UKdsesxqpu3cqv5b7a69xdklhv] UNIQUE NONCLUSTERED 
(
	[mail] ASC,
	[Z_TENANT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_USR_CUST_AUTH]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_USR_CUST_AUTH](
	[user_id] [bigint] NOT NULL,
	[AUTHORITY_NAME] [varchar](255) NOT NULL,
 CONSTRAINT [PK__JFW_USR___841761D3B028BFE7] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC,
	[AUTHORITY_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_VIEWS_SCHM]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_VIEWS_SCHM](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[VIEWS_SELECTION_PATTERN] [varchar](255) NULL,
 CONSTRAINT [PK__JFW_VIEW__3214EC278A6EB83F] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_WEEK_DAYS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_WEEK_DAYS](
	[WD_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[calc_flag] [bit] NULL,
	[WD_DESC] [varchar](255) NULL,
	[WD_FRI] [bit] NULL,
	[WD_MON] [bit] NULL,
	[WD_SAT] [bit] NULL,
	[WD_SUN] [bit] NULL,
	[WD_THU] [bit] NULL,
	[WD_TUE] [bit] NULL,
	[WD_WED] [bit] NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[CALENDAR_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_WEEK__CF98529ADC2BB3D1] PRIMARY KEY CLUSTERED 
(
	[WD_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JFW_WORKING_HOURS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JFW_WORKING_HOURS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[frLogonDay] [bit] NOT NULL,
	[frLogonTimeFrom] [datetime2](7) NULL,
	[frLogonTimeTo] [datetime2](7) NULL,
	[moLogonDay] [bit] NOT NULL,
	[moLogonTimeFrom] [datetime2](7) NULL,
	[moLogonTimeTo] [datetime2](7) NULL,
	[saLogonDay] [bit] NOT NULL,
	[saLogonTimeFrom] [datetime2](7) NULL,
	[saLogonTimeTo] [datetime2](7) NULL,
	[SECURITY_LEVEL] [varchar](255) NULL,
	[suLogonDay] [bit] NOT NULL,
	[suLogonTimeFrom] [datetime2](7) NULL,
	[suLogonTimeTo] [datetime2](7) NULL,
	[thLogonDay] [bit] NOT NULL,
	[thLogonTimeFrom] [datetime2](7) NULL,
	[thLogonTimeTo] [datetime2](7) NULL,
	[tuLogonDay] [bit] NOT NULL,
	[tuLogonTimeFrom] [datetime2](7) NULL,
	[tuLogonTimeTo] [datetime2](7) NULL,
	[weLogonDay] [bit] NOT NULL,
	[weLogonTimeFrom] [datetime2](7) NULL,
	[weLogonTimeTo] [datetime2](7) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
 CONSTRAINT [PK__JFW_WORK__3213E83F8EFDB5DD] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UKe5uuva6fofraw0vxp7slgyptl] UNIQUE NONCLUSTERED 
(
	[description] ASC,
	[Z_ORG_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JPTR_CHNG_HISTRY]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JPTR_CHNG_HISTRY](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime2](7) NULL,
	[Z_ARCHIVE_QUEUED] [datetime2](7) NULL,
	[Z_ARCHIVE_STATUS] [varchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [varchar](255) NULL,
	[Z_CREATION_DATE] [datetime2](7) NULL,
	[Z_DELETED_BY] [varchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime2](7) NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [varchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime2](7) NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [varchar](255) NULL,
	[Z_UPDATED_BY] [varchar](255) NULL,
	[Z_UPDATING_DATE] [datetime2](7) NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [varchar](255) NULL,
	[Z_DRAFT_STATUS] [varchar](255) NULL,
	[collectionName] [varchar](255) NULL,
	[entityId] [varchar](255) NULL,
	[snapshot] [varchar](max) NULL,
	[changes] [varchar](max) NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[actionName] [varchar](255) NULL,
 CONSTRAINT [PK__PPSPurpose__BRGLE83AHMDC090E] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[messageentity]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[messageentity](
	[id] [nvarchar](1000) NOT NULL,
	[contentpart1] [nvarchar](4000) NULL,
	[contentpart2] [nvarchar](4000) NULL,
	[contentpart3] [nvarchar](4000) NULL,
	[contentpart4] [nvarchar](4000) NULL,
	[creationdatetime] [datetime2](6) NULL,
	[type] [nvarchar](255) NULL,
	[destination] [nvarchar](255) NULL,
	[updatedatetime] [datetime2](6) NULL,
	[state] [nvarchar](255) NULL,
	[additionalinfo] [nvarchar](4000) NULL,
	[locktoken] [nvarchar](255) NULL,
	[lockdatetime] [datetime2](6) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[messagehistoryentity]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[messagehistoryentity](
	[id] [nvarchar](1000) NOT NULL,
	[contentpart1] [nvarchar](4000) NULL,
	[contentpart2] [nvarchar](4000) NULL,
	[contentpart3] [nvarchar](4000) NULL,
	[contentpart4] [nvarchar](4000) NULL,
	[creationdatetime] [datetime2](6) NULL,
	[type] [nvarchar](255) NULL,
	[destination] [nvarchar](255) NULL,
	[updatedatetime] [datetime2](6) NULL,
	[state] [nvarchar](255) NULL,
	[additionalinfo] [nvarchar](4000) NULL,
	[locktoken] [nvarchar](255) NULL,
	[lockdatetime] [datetime2](6) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_accountgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_accountgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_accountgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_accountitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_accountitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_accountitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_accounttypegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_accounttypegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_accounttypegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_accounttypeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_accounttypeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_accounttypeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_accounttypes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_accounttypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_accounttypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_qukrxgws665i58iwg994n21en] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_sgo4haxgafxmmtgyl1ompyth8] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_accounttypes_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_accounttypes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[accid] [bigint] NOT NULL,
 CONSTRAINT [mpay_accounttypes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_amlcasegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_amlcasegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_amlcasegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_amlcaseitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_amlcaseitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_amlcaseitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_amlcases]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_amlcases](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[address] [nvarchar](4000) NULL,
	[arabicname] [nvarchar](1000) NULL,
	[dateofbirth] [date] NULL,
	[englishname] [nvarchar](1000) NULL,
	[gender] [nvarchar](10) NULL,
	[idexpirydate] [nvarchar](100) NULL,
	[mobilenumber] [nvarchar](100) NULL,
	[nationalid] [nvarchar](255) NULL,
	[notes] [nvarchar](4000) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refcustomerid] [bigint] NULL,
 CONSTRAINT [mpay_amlcases_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_amlhitgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_amlhitgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_amlhitgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_amlhititm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_amlhititm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_amlhititm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_amlhits]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_amlhits](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[arabicname] [nvarchar](1000) NULL,
	[dateofbirth] [nvarchar](100) NULL,
	[documentserial] [nvarchar](1000) NULL,
	[englishaddress] [nvarchar](4000) NULL,
	[englishname] [nvarchar](1000) NULL,
	[entityreference] [nvarchar](1000) NULL,
	[ismatchflag] [nvarchar](5) NULL,
	[itemcategory] [nvarchar](1000) NULL,
	[itemsource] [nvarchar](1000) NULL,
	[itemsubcategory] [nvarchar](1000) NULL,
	[score] [nvarchar](3) NULL,
	[typeofalias] [nvarchar](100) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refcaseid] [bigint] NULL,
 CONSTRAINT [mpay_amlhits_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_appschecksums]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_appschecksums](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[checksum] [nvarchar](1000) NOT NULL,
	[code] [nvarchar](1000) NOT NULL,
	[isactive] [bit] NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_appschecksums_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_2i7bmu46ad6am5knarxd36r13] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_40hm6wu2ae81w7e5w858j8lsj] UNIQUE NONCLUSTERED 
(
	[checksum] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_atm_vouchergrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_atm_vouchergrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_atm_vouchergrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_atm_voucheritm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_atm_voucheritm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_atm_voucheritm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_atm_vouchers]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_atm_vouchers](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[amount] [numeric](21, 8) NOT NULL,
	[expirydate] [datetime] NOT NULL,
	[iscanceled] [bit] NOT NULL,
	[isused] [bit] NOT NULL,
	[requestdate] [datetime] NOT NULL,
	[value] [nvarchar](16) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
	[messagetypeid] [bigint] NULL,
	[refmobileid] [bigint] NOT NULL,
 CONSTRAINT [mpay_atm_vouchers_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_attachmenttypegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_attachmenttypegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_attachmenttypegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_attachmenttypeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_attachmenttypeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_attachmenttypeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_attachmenttypes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_attachmenttypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_attachmenttypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_jn9ss666tfb55oejyp949yusn] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_balancetypegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_balancetypegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_balancetypegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_balancetypeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_balancetypeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_balancetypeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_balancetypes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_balancetypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_balancetypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_aq2omk0clejh2cnbprxq9tiy7] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_t1bllykcauyxn0tjpiffk8ttf] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_balancetypes_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_balancetypes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[typeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_balancetypes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bankgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bankgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_bankgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bankintegmessageatt]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bankintegmessageatt](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[attfile] [varbinary](max) NULL,
	[attachment_source] [nvarchar](255) NULL,
	[attachment_token] [nvarchar](255) NULL,
	[comments] [nvarchar](255) NULL,
	[contenttype] [nvarchar](255) NULL,
	[entityid] [nvarchar](255) NULL,
	[image_thumbnail] [varbinary](max) NULL,
	[image_type] [nvarchar](255) NULL,
	[name] [nvarchar](255) NULL,
	[original_micr] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[rev] [int] NULL,
	[attachment_size] [numeric](19, 2) NULL,
 CONSTRAINT [mpay_bankintegmessageatt_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bankintegmessages]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bankintegmessages](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[amount] [numeric](21, 8) NULL,
	[isreversal] [bit] NOT NULL,
	[isreversed] [bit] NOT NULL,
	[messageintegtype] [nvarchar](20) NULL,
	[requestcontent] [varchar](max) NOT NULL,
	[requestdate] [datetime] NOT NULL,
	[requestid] [nvarchar](255) NOT NULL,
	[requesttoken] [nvarchar](4000) NULL,
	[responsecontent] [varchar](max) NULL,
	[responsedate] [datetime] NULL,
	[responseid] [nvarchar](255) NULL,
	[responsetoken] [nvarchar](4000) NULL,
	[statusdescription] [nvarchar](4000) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NULL,
	[refbankid] [bigint] NOT NULL,
	[refstatusid] [bigint] NULL,
	[reftransactionid] [bigint] NULL,
 CONSTRAINT [mpay_bankintegmessages_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bankintegreasongrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bankintegreasongrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_bankintegreasongrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bankintegreasonitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bankintegreasonitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_bankintegreasonitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bankintegreasons]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bankintegreasons](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[processorcode] [nvarchar](12) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_bankintegreasons_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_8r3uxkrfh9m7ml5u27j96fea] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[processorcode] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_bwpp5swcu234jgacie66edx4p] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bankintegreasons_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bankintegreasons_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[rsnid] [bigint] NOT NULL,
 CONSTRAINT [mpay_bankintegreasons_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bankintegsettings]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bankintegsettings](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[description] [nvarchar](1000) NULL,
	[key] [nvarchar](255) NOT NULL,
	[value] [nvarchar](1000) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refbankid] [bigint] NOT NULL,
 CONSTRAINT [mpay_bankintegsettings_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bankintegstatus_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bankintegstatus_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[sttsid] [bigint] NOT NULL,
 CONSTRAINT [mpay_bankintegstatus_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bankintegstatuses]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bankintegstatuses](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_bankintegstatuses_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_7hl1pqpxtj172t7h1ih75t6ew] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_asijxbfo721qaw1n5ngnamooa] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bankintegstatusgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bankintegstatusgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_bankintegstatusgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bankintegstatusitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bankintegstatusitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_bankintegstatusitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bankitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bankitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_bankitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_banksusers]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_banksusers](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[confirm] [nvarchar](1000) NULL,
	[isactive] [bit] NOT NULL,
	[password] [nvarchar](1000) NOT NULL,
	[username] [nvarchar](200) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[bankid] [bigint] NOT NULL,
 CONSTRAINT [mpay_banksusers_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bulkpayment]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bulkpayment](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[description] [nvarchar](255) NULL,
	[filename] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_bulkpayment_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bulkpaymentatt]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bulkpaymentatt](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[attfile] [varbinary](max) NULL,
	[attachment_source] [nvarchar](255) NULL,
	[attachment_token] [nvarchar](255) NULL,
	[comments] [nvarchar](255) NULL,
	[contenttype] [nvarchar](255) NULL,
	[entityid] [nvarchar](255) NULL,
	[image_thumbnail] [varbinary](max) NULL,
	[image_type] [nvarchar](255) NULL,
	[name] [nvarchar](255) NULL,
	[original_micr] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[rev] [int] NULL,
	[attachment_size] [numeric](19, 2) NULL,
 CONSTRAINT [mpay_bulkpaymentatt_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bulkregistration]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bulkregistration](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[description] [nvarchar](255) NULL,
	[filename] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_bulkregistration_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_bulkregistrationatt]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_bulkregistrationatt](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[attfile] [varbinary](max) NULL,
	[attachment_source] [nvarchar](255) NULL,
	[attachment_token] [nvarchar](255) NULL,
	[comments] [nvarchar](255) NULL,
	[contenttype] [nvarchar](255) NULL,
	[entityid] [nvarchar](255) NULL,
	[image_thumbnail] [varbinary](max) NULL,
	[image_type] [nvarchar](255) NULL,
	[name] [nvarchar](255) NULL,
	[original_micr] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[rev] [int] NULL,
	[attachment_size] [numeric](19, 2) NULL,
 CONSTRAINT [mpay_bulkregistrationatt_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_cashin]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_cashin](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[amount] [numeric](21, 8) NOT NULL,
	[bankbranch] [nvarchar](100) NULL,
	[chargeamount] [numeric](21, 8) NULL,
	[chequenumber] [nvarchar](256) NULL,
	[comments] [nvarchar](255) NULL,
	[customerbankaccountnumber] [nvarchar](250) NULL,
	[customertype] [nvarchar](19) NULL,
	[ischargeincluded] [bit] NULL,
	[reasondesc] [nvarchar](4000) NULL,
	[refmobileaccount] [nvarchar](100) NULL,
	[taxamount] [numeric](21, 8) NULL,
	[totalamount] [numeric](21, 8) NULL,
	[transamount] [numeric](21, 8) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[bankid] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
	[processingstatusid] [bigint] NULL,
	[reasonid] [bigint] NULL,
	[refcustmobid] [bigint] NOT NULL,
	[reflastmsglogid] [bigint] NULL,
	[refmessageid] [bigint] NULL,
 CONSTRAINT [mpay_cashin_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_cashout]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_cashout](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[amount] [numeric](21, 8) NOT NULL,
	[chargeamount] [numeric](21, 8) NULL,
	[comments] [nvarchar](255) NULL,
	[customerbankaccountnumber] [nvarchar](255) NULL,
	[customertype] [nvarchar](19) NULL,
	[ischargeincluded] [bit] NULL,
	[reasondesc] [nvarchar](4000) NULL,
	[refmobileaccount] [nvarchar](50) NULL,
	[taxamount] [numeric](21, 8) NULL,
	[totalamount] [numeric](21, 8) NULL,
	[transamount] [numeric](21, 8) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
	[processingstatusid] [bigint] NULL,
	[reasonid] [bigint] NULL,
	[refcustmobid] [bigint] NOT NULL,
	[reflastmsglogid] [bigint] NULL,
	[refmessageid] [bigint] NULL,
 CONSTRAINT [mpay_cashout_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_channeltype]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_channeltype](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[transporterbean] [nvarchar](255) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_channeltype_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_am48ss4w505nicle47i8j2re7] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_dj1lt8owa0lwsuw54te5xonsu] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_channeltype_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_channeltype_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[channeltypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_channeltype_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_channeltypegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_channeltypegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_channeltypegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_channeltypeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_channeltypeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_channeltypeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_chargesschemedetailgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_chargesschemedetailgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_chargesschemedetailgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_chargesschemedetailitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_chargesschemedetailitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_chargesschemedetailitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_chargesschemedetails]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_chargesschemedetails](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[chargempclearlpercent] [bigint] NOT NULL,
	[chargepsppercent] [bigint] NOT NULL,
	[description] [nvarchar](255) NULL,
	[isactive] [bit] NOT NULL,
	[pspscope] [nvarchar](20) NULL,
	[receivechargebearerpercent] [bigint] NOT NULL,
	[sendchargebearerpercent] [bigint] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[msgtypeid] [bigint] NOT NULL,
	[refchargesschemeid] [bigint] NULL,
 CONSTRAINT [mpay_chargesschemedetails_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_6oo2buocqcktg9ljvfybam91h] UNIQUE NONCLUSTERED 
(
	[refchargesschemeid] ASC,
	[msgtypeid] ASC,
	[pspscope] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_chargesschemegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_chargesschemegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_chargesschemegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_chargesschemeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_chargesschemeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_chargesschemeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_chargesschemes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_chargesschemes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[isactive] [bit] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
 CONSTRAINT [mpay_chargesschemes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_rrstxwottk39tjw8vkaq32esd] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_chargesslicegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_chargesslicegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_chargesslicegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_chargessliceitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_chargessliceitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_chargessliceitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_chargesslices]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_chargesslices](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[chargeamount] [numeric](21, 8) NULL,
	[chargepercent] [numeric](21, 8) NULL,
	[chargetype] [nvarchar](1) NOT NULL,
	[externalchargeamount] [numeric](21, 8) NULL,
	[externalchargepercent] [numeric](21, 8) NULL,
	[externalchargetype] [nvarchar](1) NOT NULL,
	[externalmaxamount] [numeric](21, 8) NULL,
	[externalminamount] [numeric](21, 8) NULL,
	[maxamount] [numeric](21, 8) NULL,
	[minamount] [numeric](21, 8) NULL,
	[txamountlimit] [numeric](21, 8) NULL,
	[txcountlimit] [bigint] NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refchargesdetailsid] [bigint] NULL,
 CONSTRAINT [mpay_chargesslices_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_citiesnls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_citiesnls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[cityid] [bigint] NOT NULL,
	[clangid] [bigint] NOT NULL,
 CONSTRAINT [mpay_citiesnls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_cityareas]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_cityareas](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[cityid] [bigint] NOT NULL,
 CONSTRAINT [mpay_cityareas_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_cityareas_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_cityareas_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[areaid] [bigint] NOT NULL,
	[arealanguageid] [bigint] NOT NULL,
 CONSTRAINT [mpay_cityareas_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_clientscommissiongrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_clientscommissiongrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_clientscommissiongrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_clientscommissionitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_clientscommissionitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_clientscommissionitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_clientscommissions]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_clientscommissions](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[totalamount] [numeric](19, 2) NOT NULL,
	[totalcount] [bigint] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[accountid] [bigint] NOT NULL,
	[messagetypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_clientscommissions_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_taq5b9b4wxxyxvj1eh2hb2ipk] UNIQUE NONCLUSTERED 
(
	[accountid] ASC,
	[messagetypeid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_clientslimitgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_clientslimitgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_clientslimitgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_clientslimititm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_clientslimititm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_clientslimititm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_clientslimits]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_clientslimits](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[dailyamount] [numeric](19, 2) NOT NULL,
	[dailycount] [bigint] NOT NULL,
	[dailydate] [datetime] NOT NULL,
	[month] [bigint] NOT NULL,
	[monthlyamount] [numeric](19, 2) NOT NULL,
	[monthlycount] [bigint] NOT NULL,
	[weeknumber] [bigint] NOT NULL,
	[weeklyamount] [numeric](19, 2) NOT NULL,
	[weeklycount] [bigint] NOT NULL,
	[year] [bigint] NOT NULL,
	[yearlyamount] [numeric](19, 2) NOT NULL,
	[yearlycount] [bigint] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[accountid] [bigint] NOT NULL,
	[messagetypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_clientslimits_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_2l7uh0sg0oybs2dfu9slgpdi9] UNIQUE NONCLUSTERED 
(
	[accountid] ASC,
	[messagetypeid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_clientsotpgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_clientsotpgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_clientsotpgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_clientsotpitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_clientsotpitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_clientsotpitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_clientsotps]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_clientsotps](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[maxamount] [numeric](21, 8) NOT NULL,
	[requesttime] [datetime] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
	[messagetypeid] [bigint] NOT NULL,
	[sendermobileid] [bigint] NULL,
	[senderserviceid] [bigint] NULL,
 CONSTRAINT [mpay_clientsotps_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_clienttype_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_clienttype_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[clienttypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_clienttype_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_clienttypes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_clienttypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[iscustomer] [bit] NULL,
	[issystem] [bit] NULL,
	[maxnumberofaccount] [bigint] NOT NULL,
	[maxnumberofowners] [bigint] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_clienttypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_lir7e6v6lra0cp0viachojnbk] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_nfu2tmb0hnshu4hgb4863vysf] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_commissiongrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_commissiongrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_commissiongrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_commissionitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_commissionitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_commissionitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_commissionparametergrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_commissionparametergrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_commissionparametergrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_commissionparameteritm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_commissionparameteritm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_commissionparameteritm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_commissionparameters]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_commissionparameters](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[value] [nvarchar](1000) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refcommissionid] [bigint] NULL,
 CONSTRAINT [mpay_commissionparameters_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_35289da8xklb2rnvirdeae0g] UNIQUE NONCLUSTERED 
(
	[refcommissionid] ASC,
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_commissions]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_commissions](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[description] [nvarchar](255) NULL,
	[isactive] [bit] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[msgtypeid] [bigint] NOT NULL,
	[refcommissionschemeid] [bigint] NULL,
 CONSTRAINT [mpay_commissions_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_jcdsuwlw5lfxuinjdelvx1dxn] UNIQUE NONCLUSTERED 
(
	[refcommissionschemeid] ASC,
	[msgtypeid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_commissionschemegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_commissionschemegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_commissionschemegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_commissionschemeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_commissionschemeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_commissionschemeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_commissionschemes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_commissionschemes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[isactive] [bit] NOT NULL,
	[processor] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_commissionschemes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_3eph18wosrfdjah3tgp0jufsm] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corpintegmessages]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corpintegmessages](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[narration] [nvarchar](4000) NULL,
	[refmessage] [nvarchar](40) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[actiontypeid] [bigint] NULL,
	[integmsgtypeid] [bigint] NOT NULL,
	[processrejectionid] [bigint] NULL,
	[refcorporateid] [bigint] NULL,
	[refmsglogid] [bigint] NOT NULL,
	[reforiginalmsgid] [bigint] NULL,
	[refserviceid] [bigint] NULL,
	[responseid] [bigint] NULL,
 CONSTRAINT [mpay_corpintegmessages_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corplegalentities]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corplegalentities](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_corplegalentities_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_4311qaoe95v9r2pq9t0qmth2a] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_hsrvoc0veogs5djmm28tg677i] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corplegalentities_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corplegalentities_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[legalentityid] [bigint] NOT NULL,
 CONSTRAINT [mpay_corplegalentities_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corporateatt]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corporateatt](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[attfile] [varbinary](max) NULL,
	[attachment_source] [nvarchar](255) NULL,
	[attachment_token] [nvarchar](255) NULL,
	[comments] [nvarchar](255) NULL,
	[contenttype] [nvarchar](255) NULL,
	[entityid] [nvarchar](255) NULL,
	[image_thumbnail] [varbinary](max) NULL,
	[image_type] [nvarchar](255) NULL,
	[name] [nvarchar](255) NULL,
	[original_micr] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[rev] [int] NULL,
	[attachment_size] [numeric](19, 2) NULL,
 CONSTRAINT [mpay_corporateatt_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corporatedevicegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corporatedevicegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_corporatedevicegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corporatedeviceitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corporatedeviceitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_corporatedeviceitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corporatedevices]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corporatedevices](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[isonline] [bit] NOT NULL,
	[retrycount] [bigint] NOT NULL,
	[signinlocation] [nvarchar](1000) NULL,
	[activedevice] [bit] NULL,
	[deviceid] [nvarchar](255) NULL,
	[devicename] [nvarchar](100) NOT NULL,
	[extradata] [nvarchar](1000) NULL,
	[isblocked] [bit] NOT NULL,
	[isdefault] [bit] NOT NULL,
	[ismanualgenerated] [bit] NOT NULL,
	[isstolen] [bit] NULL,
	[lastlogintime] [datetime] NULL,
	[password] [nvarchar](255) NULL,
	[passwordlastchanged] [datetime] NULL,
	[publickey] [nvarchar](4000) NULL,
	[refreshtoken] [nvarchar](100) NULL,
	[sessionid] [nvarchar](1000) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refcorporateserviceid] [bigint] NOT NULL,
 CONSTRAINT [mpay_corporatedevices_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corporatedevicetokens]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corporatedevicetokens](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[expirationdate] [datetime] NOT NULL,
	[token] [nvarchar](4000) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[corporatedeviceid] [bigint] NULL,
 CONSTRAINT [mpay_corporatedevicetokens_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_iftdns8l8q9u4ahbxt997gv2k] UNIQUE NONCLUSTERED 
(
	[corporatedeviceid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corporatedocumentgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corporatedocumentgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_corporatedocumentgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corporatedocumentitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corporatedocumentitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_corporatedocumentitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corporatedocuments]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corporatedocuments](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[documentname] [nvarchar](500) NOT NULL,
	[documentpath] [nvarchar](4000) NOT NULL,
	[documentsize] [bigint] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[corporateid] [bigint] NOT NULL,
	[documenttypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_corporatedocuments_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corporategrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corporategrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_corporategrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corporateitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corporateitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_corporateitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_corporatestenants]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_corporatestenants](
	[corporates_id] [bigint] NOT NULL,
	[tenants_id] [bigint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_custintegactiontypes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_custintegactiontypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_custintegactiontypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_7ynutbhg8jglilkmhcv17mhgh] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_98m3ti4jpj6eqtsajeea28b6c] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_custintegactiontypes_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_custintegactiontypes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[intgid] [bigint] NOT NULL,
 CONSTRAINT [mpay_custintegactiontypes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_custintegmessages]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_custintegmessages](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[narration] [nvarchar](4000) NULL,
	[refmessage] [nvarchar](40) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[actiontypeid] [bigint] NULL,
	[integmsgtypeid] [bigint] NOT NULL,
	[processrejectionid] [bigint] NULL,
	[refcustomerid] [bigint] NULL,
	[refmobileid] [bigint] NULL,
	[refmsglogid] [bigint] NOT NULL,
	[reforiginalmsgid] [bigint] NULL,
	[responseid] [bigint] NULL,
 CONSTRAINT [mpay_custintegmessages_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customeratt]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customeratt](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[attfile] [varbinary](max) NULL,
	[attachment_source] [nvarchar](255) NULL,
	[attachment_token] [nvarchar](255) NULL,
	[comments] [nvarchar](255) NULL,
	[contenttype] [nvarchar](255) NULL,
	[entityid] [nvarchar](255) NULL,
	[image_thumbnail] [varbinary](max) NULL,
	[image_type] [nvarchar](255) NULL,
	[name] [nvarchar](255) NULL,
	[original_micr] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[rev] [int] NULL,
	[attachment_size] [numeric](19, 2) NULL,
 CONSTRAINT [mpay_customeratt_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customercmt]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customercmt](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[commenttoken] [nvarchar](255) NULL,
	[description] [nvarchar](255) NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
 CONSTRAINT [mpay_customercmt_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customerdevicegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customerdevicegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_customerdevicegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customerdeviceitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customerdeviceitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_customerdeviceitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customerdevicetokens]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customerdevicetokens](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[expirationdate] [datetime] NOT NULL,
	[token] [nvarchar](4000) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[customerdeviceid] [bigint] NULL,
 CONSTRAINT [mpay_customerdevicetokens_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_sqtilm48lujjg3dup1e9xcx7e] UNIQUE NONCLUSTERED 
(
	[customerdeviceid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customerdocumentgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customerdocumentgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_customerdocumentgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customerdocumentitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customerdocumentitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_customerdocumentitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customerdocuments]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customerdocuments](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[documentname] [nvarchar](500) NOT NULL,
	[documentpath] [nvarchar](4000) NOT NULL,
	[documentsize] [bigint] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[customerid] [bigint] NOT NULL,
	[documenttypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_customerdocuments_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customergrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customergrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_customergrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customeritm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customeritm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_customeritm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customermobilegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customermobilegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_customermobilegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_customermobileitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_customermobileitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_customermobileitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MPAY_DOCUMENTGRP]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MPAY_DOCUMENTGRP](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime] NULL,
	[Z_ARCHIVE_QUEUED] [datetime] NULL,
	[Z_ARCHIVE_STATUS] [nvarchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [nvarchar](255) NULL,
	[Z_CREATION_DATE] [datetime] NULL,
	[Z_DELETED_BY] [nvarchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime] NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [nvarchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime] NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [nvarchar](255) NULL,
	[Z_UPDATED_BY] [nvarchar](255) NULL,
	[Z_UPDATING_DATE] [datetime] NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [nvarchar](255) NULL,
	[ACTIONNAME] [nvarchar](255) NULL,
	[AUDITCHANGETYPE] [bigint] NULL,
	[AUDIT_CHANGE_TYPE_DES] [nvarchar](255) NULL,
	[BRANCH] [nvarchar](255) NULL,
	[CHANGEFIELD] [nvarchar](255) NULL,
	[CHANGEFIELDDESCRIPTION] [nvarchar](255) NULL,
	[CHANGEFIELDNAME] [nvarchar](255) NULL,
	[CHANGE_HISTORY_TOKEN] [nvarchar](255) NULL,
	[DRAFT] [bit] NULL,
	[ENTITYID] [nvarchar](255) NULL,
	[RECORDID] [nvarchar](255) NULL,
	[REF_VALUE] [nvarchar](255) NULL,
	[SECURITY_LEVEL] [nvarchar](255) NULL,
	[USER_GROUP] [nvarchar](255) NULL,
	[USERID] [nvarchar](255) NULL,
	[USERNAME] [nvarchar](255) NULL,
	[VIEWDESCRIPTION] [nvarchar](255) NULL,
	[VIEWNAME] [nvarchar](255) NULL,
	[WORKSTATION] [nvarchar](255) NULL,
 CONSTRAINT [SYS_C001028398B55] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MPAY_DOCUMENTITM]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MPAY_DOCUMENTITM](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime] NULL,
	[Z_ARCHIVE_QUEUED] [datetime] NULL,
	[Z_ARCHIVE_STATUS] [nvarchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [nvarchar](255) NULL,
	[Z_CREATION_DATE] [datetime] NULL,
	[Z_DELETED_BY] [nvarchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime] NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [nvarchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime] NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [nvarchar](255) NULL,
	[Z_UPDATED_BY] [nvarchar](255) NULL,
	[Z_UPDATING_DATE] [datetime] NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [nvarchar](255) NULL,
	[FIELD] [nvarchar](255) NULL,
	[FIELDDESCRIPTION] [nvarchar](255) NULL,
	[IP_ADDRESS] [nvarchar](255) NULL,
	[NEWVALUE] [nvarchar](4000) NULL,
	[OLDVALUE] [nvarchar](4000) NULL,
	[SECURITY_LEVEL] [nvarchar](255) NULL,
	[GROUPID] [bigint] NULL,
 CONSTRAINT [SYS_C0010283965NN] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MPAY_DOCUMENTS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MPAY_DOCUMENTS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Z_ARCHIVE_ON] [datetime] NULL,
	[Z_ARCHIVE_QUEUED] [datetime] NULL,
	[Z_ARCHIVE_STATUS] [nvarchar](255) NULL,
	[Z_ASSIGNED_GROUP] [bigint] NULL,
	[Z_ASSIGNED_USER] [bigint] NULL,
	[Z_CREATED_BY] [nvarchar](255) NULL,
	[Z_CREATION_DATE] [datetime] NULL,
	[Z_DELETED_BY] [nvarchar](255) NULL,
	[Z_DELETED_FLAG] [bit] NULL,
	[Z_DELETED_ON] [datetime] NULL,
	[Z_EDITABLE] [bit] NULL,
	[Z_LOCKED_BY] [nvarchar](255) NULL,
	[Z_LOCKED_UNTIL] [datetime] NULL,
	[Z_ORG_ID] [bigint] NULL,
	[Z_TENANT_ID] [nvarchar](255) NULL,
	[Z_UPDATED_BY] [nvarchar](255) NULL,
	[Z_UPDATING_DATE] [datetime] NULL,
	[Z_WORKFLOW_ID] [bigint] NULL,
	[Z_WS_TOKEN] [nvarchar](255) NULL,
	[Z_DRAFT_STATUS] [nvarchar](255) NULL,
	[DOCUMENTNAME] [nvarchar](500) NOT NULL,
	[DOCUMENTPATH] [nvarchar](4000) NOT NULL,
	[DOCUMENTSIZE] [bigint] NOT NULL,
	[Z_DRAFT_ID] [bigint] NULL,
	[Z_STATUS_ID] [bigint] NULL,
	[RECORDID] [nvarchar](255) NOT NULL,
	[DOCUMENTTYPE] [nvarchar](255) NOT NULL,
	[viewname] [varchar](255) NULL,
 CONSTRAINT [SYS_C001028423] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MPAY_EKYCSTATUS]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MPAY_EKYCSTATUS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[status] [nvarchar](20) NULL,
 CONSTRAINT [mpay_EkycStatus_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_epaystatuscodegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_epaystatuscodegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_epaystatuscodegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_epaystatuscodeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_epaystatuscodeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_epaystatuscodeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_epaystatuscodes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_epaystatuscodes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[epaymessagecode] [nvarchar](12) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_epaystatuscodes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_6xhnn3n9srtogiu4wwbrm6i0f] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_n2odml1d0hyjpemhb03ifor9a] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_epaystatuscodes_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_epaystatuscodes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[sttsid] [bigint] NOT NULL,
 CONSTRAINT [mpay_epaystatuscodes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_externalintegmessageatt]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_externalintegmessageatt](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[attfile] [varbinary](max) NULL,
	[attachment_source] [nvarchar](255) NULL,
	[attachment_token] [nvarchar](255) NULL,
	[comments] [nvarchar](255) NULL,
	[contenttype] [nvarchar](255) NULL,
	[entityid] [nvarchar](255) NULL,
	[image_thumbnail] [varbinary](max) NULL,
	[image_type] [nvarchar](255) NULL,
	[name] [nvarchar](255) NULL,
	[original_micr] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[rev] [int] NULL,
	[attachment_size] [numeric](19, 2) NULL,
 CONSTRAINT [mpay_externalintegmessageatt_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_externalintegmessages]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_externalintegmessages](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[reasoncode] [nvarchar](19) NULL,
	[reasondescription] [nvarchar](1000) NULL,
	[reqfield1] [nvarchar](1000) NULL,
	[reqfield2] [nvarchar](1000) NULL,
	[reqfield3] [nvarchar](1000) NULL,
	[reqfield4] [nvarchar](1000) NULL,
	[reqfield5] [nvarchar](1000) NULL,
	[requestcontent] [varchar](max) NOT NULL,
	[requestdate] [datetime] NOT NULL,
	[requestid] [nvarchar](255) NOT NULL,
	[requesttoken] [nvarchar](4000) NULL,
	[responsecontent] [varchar](max) NULL,
	[responsedate] [datetime] NULL,
	[responseid] [nvarchar](255) NULL,
	[responsetoken] [nvarchar](4000) NULL,
	[rspfield1] [nvarchar](1000) NULL,
	[rspfield2] [nvarchar](1000) NULL,
	[rspfield3] [nvarchar](1000) NULL,
	[rspfield4] [nvarchar](1000) NULL,
	[rspfield5] [nvarchar](1000) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refmessageid] [bigint] NULL,
	[refserviceid] [bigint] NULL,
	[refstatusid] [bigint] NULL,
 CONSTRAINT [mpay_externalintegmessages_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_financialintegmsgs]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_financialintegmsgs](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[acct1] [nvarchar](148) NULL,
	[acct2] [nvarchar](148) NULL,
	[amount] [numeric](21, 8) NULL,
	[iscanceled] [bit] NOT NULL,
	[isprocessed] [bit] NULL,
	[maxreplytime] [datetime] NULL,
	[narration] [nvarchar](4000) NULL,
	[refmessage] [nvarchar](40) NOT NULL,
	[sessionid] [nvarchar](255) NULL,
	[swtprocessingtime] [datetime] NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NULL,
	[integmsgtypeid] [bigint] NOT NULL,
	[paymenttypeid] [bigint] NULL,
	[processrejectionid] [bigint] NULL,
	[refmsglogid] [bigint] NOT NULL,
	[reforiginalmsgid] [bigint] NULL,
	[responseid] [bigint] NULL,
 CONSTRAINT [mpay_financialintegmsgs_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_hear_beat]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_hear_beat](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[check_date] [datetime] NULL,
 CONSTRAINT [mpay_hear_beat_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_idtypes_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_idtypes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[idtypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_idtypes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_intg_corp_reg_instratt]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_intg_corp_reg_instratt](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[attfile] [varbinary](max) NULL,
	[attachment_source] [nvarchar](255) NULL,
	[attachment_token] [nvarchar](255) NULL,
	[comments] [nvarchar](255) NULL,
	[contenttype] [nvarchar](255) NULL,
	[entityid] [nvarchar](255) NULL,
	[image_thumbnail] [varbinary](max) NULL,
	[image_type] [nvarchar](255) NULL,
	[name] [nvarchar](255) NULL,
	[original_micr] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[rev] [int] NULL,
	[attachment_size] [numeric](19, 2) NULL,
 CONSTRAINT [mpay_intg_corp_reg_instratt_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_intg_corp_reg_instrgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_intg_corp_reg_instrgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_intg_corp_reg_instrgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_intg_corp_reg_instritm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_intg_corp_reg_instritm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_intg_corp_reg_instritm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_intg_corp_reg_instrs]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_intg_corp_reg_instrs](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[alias] [varchar](max) NULL,
	[bankshortname] [varchar](max) NULL,
	[buildingnum] [varchar](max) NULL,
	[city] [varchar](max) NULL,
	[clientref] [varchar](max) NULL,
	[clienttype] [varchar](max) NULL,
	[corporatename] [varchar](max) NULL,
	[description] [varchar](max) NULL,
	[email] [varchar](max) NULL,
	[externalacc] [varchar](max) NULL,
	[mobileaccselector] [varchar](max) NULL,
	[nationality] [varchar](max) NULL,
	[note] [varchar](max) NULL,
	[paymenttype] [varchar](max) NULL,
	[phoneone] [varchar](max) NULL,
	[phonetwo] [varchar](max) NULL,
	[pobox] [varchar](max) NULL,
	[preflang] [varchar](max) NULL,
	[reasondescription] [varchar](max) NULL,
	[reginstrcreationdt] [datetime] NOT NULL,
	[reginstrrecordid] [varchar](max) NOT NULL,
	[registrationdate] [varchar](max) NULL,
	[registrationid] [varchar](max) NULL,
	[servicedescription] [nvarchar](255) NULL,
	[servicename] [varchar](max) NULL,
	[streetname] [varchar](max) NULL,
	[zipcode] [varchar](max) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[reasonid] [bigint] NULL,
	[refregfileid] [bigint] NOT NULL,
	[reginstrprocessingsttsid] [bigint] NOT NULL,
	[servicecategoryid] [bigint] NULL,
	[servicetypeid] [bigint] NULL,
 CONSTRAINT [mpay_intg_corp_reg_instrs_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_intg_cust_reg_instratt]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_intg_cust_reg_instratt](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[attfile] [varbinary](max) NULL,
	[attachment_source] [nvarchar](255) NULL,
	[attachment_token] [nvarchar](255) NULL,
	[comments] [nvarchar](255) NULL,
	[contenttype] [nvarchar](255) NULL,
	[entityid] [nvarchar](255) NULL,
	[image_thumbnail] [varbinary](max) NULL,
	[image_type] [nvarchar](255) NULL,
	[name] [nvarchar](255) NULL,
	[original_micr] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[rev] [int] NULL,
	[attachment_size] [numeric](19, 2) NULL,
 CONSTRAINT [mpay_intg_cust_reg_instratt_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_intg_cust_reg_instrgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_intg_cust_reg_instrgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_intg_cust_reg_instrgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_intg_cust_reg_instritm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_intg_cust_reg_instritm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_intg_cust_reg_instritm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_intg_cust_reg_instrs]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_intg_cust_reg_instrs](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[alias] [varchar](max) NULL,
	[bankshortname] [varchar](max) NULL,
	[buildingnum] [varchar](max) NULL,
	[city] [varchar](max) NULL,
	[clientref] [varchar](max) NULL,
	[dob] [varchar](max) NULL,
	[email] [varchar](max) NULL,
	[externalacc] [varchar](max) NULL,
	[firstname] [varchar](max) NULL,
	[idnum] [varchar](max) NULL,
	[idtype] [varchar](max) NULL,
	[lastname] [varchar](max) NULL,
	[middlename] [varchar](max) NULL,
	[mobileaccselector] [varchar](max) NULL,
	[mobilenumber] [varchar](max) NULL,
	[nationality] [varchar](max) NULL,
	[nfcserial] [varchar](max) NULL,
	[note] [varchar](max) NULL,
	[notificationshowtype] [varchar](max) NULL,
	[operator] [varchar](max) NULL,
	[phoneone] [varchar](max) NULL,
	[phonetwo] [varchar](max) NULL,
	[pobox] [varchar](max) NULL,
	[preflang] [varchar](max) NULL,
	[reasondescription] [varchar](max) NULL,
	[reginstrcreationdt] [datetime] NOT NULL,
	[reginstrrecordid] [varchar](max) NOT NULL,
	[streetname] [varchar](max) NULL,
	[zipcode] [varchar](max) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[reasonid] [bigint] NULL,
	[refregfileid] [bigint] NOT NULL,
	[reginstrprocessingsttsid] [bigint] NOT NULL,
 CONSTRAINT [mpay_intg_cust_reg_instrs_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_intg_reg_filegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_intg_reg_filegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_intg_reg_filegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_intg_reg_fileitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_intg_reg_fileitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_intg_reg_fileitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_intg_reg_files]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_intg_reg_files](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[regfilename] [nvarchar](128) NOT NULL,
	[regfileparsingstts] [bit] NOT NULL,
	[regfileprocessedrecords] [bigint] NOT NULL,
	[regfileprocessingdt] [datetime] NOT NULL,
	[regfilereceivingdt] [datetime] NOT NULL,
	[regfilerecords] [bigint] NOT NULL,
	[regfiletype] [nvarchar](20) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[regfileprocessingsttsid] [bigint] NOT NULL,
 CONSTRAINT [mpay_intg_reg_files_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_jvtypegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_jvtypegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_jvtypegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_jvtypeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_jvtypeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_jvtypeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_jvtypes_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_jvtypes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[jvtid] [bigint] NOT NULL,
 CONSTRAINT [mpay_jvtypes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_languages_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_languages_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[langid] [bigint] NOT NULL,
 CONSTRAINT [mpay_languages_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_limitgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_limitgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_limitgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_limititm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_limititm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_limititm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_limits]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_limits](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[description] [nvarchar](255) NULL,
	[isactive] [bit] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refschemeid] [bigint] NULL,
	[reftypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_limits_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_limitsdetailgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_limitsdetailgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_limitsdetailgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_limitsdetailitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_limitsdetailitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_limitsdetailitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_limitsdetails]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_limitsdetails](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[isactive] [bit] NOT NULL,
	[txamountlimit] [numeric](21, 8) NOT NULL,
	[txcountlimit] [bigint] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[msgtypeid] [bigint] NOT NULL,
	[reflimitid] [bigint] NULL,
 CONSTRAINT [mpay_limitsdetails_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_6biu9mikjwqxhkuw4o01p40vy] UNIQUE NONCLUSTERED 
(
	[reflimitid] ASC,
	[msgtypeid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_limitsschemegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_limitsschemegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_limitsschemegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_limitsschemeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_limitsschemeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_limitsschemeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_limitsschemes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_limitsschemes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[isactive] [bit] NOT NULL,
	[pinlessamount] [numeric](21, 8) NOT NULL,
	[walletcap] [numeric](21, 8) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
 CONSTRAINT [mpay_limitsschemes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_cvxs5jkxkreph3du1ng8u9i3] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_limitstypegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_limitstypegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_limitstypegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_limitstypeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_limitstypeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_limitstypeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_limitstypes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_limitstypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_limitstypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_51gi0tqnj892cwbi2sdo1b14e] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_5j8dilbgvkty9yacy4315axjt] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_limitstypes_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_limitstypes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[limitid] [bigint] NOT NULL,
 CONSTRAINT [mpay_limitstypes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_merchantcategorycodes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_merchantcategorycodes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_merchantcategorycodes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_dh4bp57urpmkhuv1maxkw760u] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_messagetypegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_messagetypegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_messagetypegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_messagetypeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_messagetypeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_messagetypeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_messagetypes_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_messagetypes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[msgid] [bigint] NOT NULL,
 CONSTRAINT [mpay_messagetypes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mobileaccountgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mobileaccountgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_mobileaccountgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mobileaccountitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mobileaccountitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_mobileaccountitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpaymessages]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpaymessages](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[channelid] [nvarchar](1000) NULL,
	[messageid] [nvarchar](255) NOT NULL,
	[processingstamp] [datetime] NULL,
	[reasondesc] [nvarchar](4000) NULL,
	[reference] [nvarchar](1000) NULL,
	[requestcontent] [varchar](max) NULL,
	[requesttoken] [nvarchar](4000) NULL,
	[requestedid] [nvarchar](1000) NULL,
	[responsecontent] [varchar](max) NULL,
	[responsetoken] [nvarchar](4000) NULL,
	[sender] [nvarchar](100) NULL,
	[shopid] [nvarchar](1000) NULL,
	[walletid] [nvarchar](1000) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[bulkpaymentid] [bigint] NULL,
	[messagetypeid] [bigint] NULL,
	[processingstatusid] [bigint] NULL,
	[reasonid] [bigint] NULL,
	[refoperationid] [bigint] NULL,
	[reftrsansactionid] [bigint] NULL,
 CONSTRAINT [mpay_mpaymessages_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_cgedtava68q9v6pvdrcsw8ilu] UNIQUE NONCLUSTERED 
(
	[messageid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_fqo9xf3o4ah99ndgked9q4wuk] UNIQUE NONCLUSTERED 
(
	[reference] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpcleargrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpcleargrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_mpcleargrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearintegmsglogs]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearintegmsglogs](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[content] [varchar](max) NULL,
	[hint] [nvarchar](255) NULL,
	[integmsgtype] [nvarchar](255) NULL,
	[iscanceled] [bit] NOT NULL,
	[ispaymentinquiry] [bit] NULL,
	[isprocessed] [bit] NOT NULL,
	[messageid] [nvarchar](50) NULL,
	[mpclearmessageid] [nvarchar](250) NULL,
	[originalmessageid] [nvarchar](1000) NULL,
	[processingfaileddescription] [nvarchar](1000) NULL,
	[refsender] [nvarchar](255) NULL,
	[responsereceived] [bit] NOT NULL,
	[signingstamp] [datetime] NULL,
	[source] [nvarchar](1) NOT NULL,
	[switchcontent] [nvarchar](4000) NULL,
	[token] [nvarchar](4000) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[inwardtransactionid] [bigint] NULL,
	[reasonid] [bigint] NULL,
	[refmessageid] [bigint] NULL,
 CONSTRAINT [mpay_mpclearintegmsglogs_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearintegmsgtypegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearintegmsgtypegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_mpclearintegmsgtypegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearintegmsgtypeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearintegmsgtypeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_mpclearintegmsgtypeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearintegmsgtypes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearintegmsgtypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[isfinancial] [bit] NOT NULL,
	[isrequest] [bit] NOT NULL,
	[processor] [nvarchar](1000) NULL,
	[responsecode] [nvarchar](12) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_mpclearintegmsgtypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_h3rt3dioq8xgyvnmm3knl1r8x] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_suf2tm1svy72hyhrbfsos8vp3] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearintegmsgtypes_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearintegmsgtypes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[intgid] [bigint] NOT NULL,
 CONSTRAINT [mpay_mpclearintegmsgtypes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearintegrjctreasongrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearintegrjctreasongrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_mpclearintegrjctreasongrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearintegrjctreasonitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearintegrjctreasonitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_mpclearintegrjctreasonitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearintegrjctreasons]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearintegrjctreasons](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[mpclearcode] [nvarchar](12) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_mpclearintegrjctreasons_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_2a032ulpuuv0n9ypu1jbnn6uy] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_mto83qss6t5h48qb4w4ukr3k0] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_mpclearitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearmessages]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearmessages](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[content] [nvarchar](4000) NULL,
	[hint] [nvarchar](255) NULL,
	[integmsgtype] [nvarchar](255) NULL,
	[messageid] [nvarchar](50) NULL,
	[refsender] [nvarchar](255) NULL,
	[signingstamp] [datetime] NULL,
	[source] [nvarchar](1) NOT NULL,
	[token] [nvarchar](4000) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[reasonid] [bigint] NULL,
	[refmessageid] [bigint] NULL,
 CONSTRAINT [mpay_mpclearmessages_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearreasongrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearreasongrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_mpclearreasongrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearreasonitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearreasonitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_mpclearreasonitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearreasons]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearreasons](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_mpclearreasons_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_foxe2wy8smngl01710qaiqj0n] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_jmbu5o4inl7wf90xvieidmpaf] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearresponsecodegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearresponsecodegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_mpclearresponsecodegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearresponsecodeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearresponsecodeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_mpclearresponsecodeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearresponsecodes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearresponsecodes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[usage] [nvarchar](255) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_mpclearresponsecodes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_bmw50do260t0kvds340f3u26l] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_kcq8l4ncl6ld2ovtcjm6drnl5] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclearresponsecodes_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclearresponsecodes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[intgid] [bigint] NOT NULL,
 CONSTRAINT [mpay_mpclearresponsecodes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_mpclrintegrjctreasons_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_mpclrintegrjctreasons_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[intgid] [bigint] NOT NULL,
 CONSTRAINT [mpay_mpclrintegrjctreasons_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_networkmanactiontypes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_networkmanactiontypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_networkmanactiontypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_2sf0px5ncjp4oe6rmf5112u2j] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_sftpd6a6xj16hhen17qxu00kg] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_networkmanactiontypes_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_networkmanactiontypes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[intgid] [bigint] NOT NULL,
 CONSTRAINT [mpay_networkmanactiontypes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_networkmanagementgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_networkmanagementgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_networkmanagementgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_networkmanagementitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_networkmanagementitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_networkmanagementitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_networkmanagements]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_networkmanagements](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[description] [nvarchar](255) NULL,
	[isautologin] [bit] NOT NULL,
	[loginexpiration] [datetime] NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[operationtypeid] [bigint] NOT NULL,
	[reflastmsglogid] [bigint] NULL,
 CONSTRAINT [mpay_networkmanagements_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_networkmanintegmsgs]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_networkmanintegmsgs](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[narration] [nvarchar](4000) NULL,
	[refmessage] [nvarchar](40) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[actiontypeid] [bigint] NULL,
	[integmsgtypeid] [bigint] NOT NULL,
	[processrejectionid] [bigint] NULL,
	[refmsglogid] [bigint] NOT NULL,
	[reforiginalmsgid] [bigint] NULL,
	[responseid] [bigint] NULL,
 CONSTRAINT [mpay_networkmanintegmsgs_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationchannelgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationchannelgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_notificationchannelgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationchannelitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationchannelitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_notificationchannelitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationchannels_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationchannels_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[channelid] [bigint] NOT NULL,
 CONSTRAINT [mpay_notificationchannels_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_notificationgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_notificationitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notifications]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notifications](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[content] [nvarchar](4000) NOT NULL,
	[errordesc] [nvarchar](200) NULL,
	[extradata1] [nvarchar](1000) NULL,
	[extradata2] [nvarchar](1000) NULL,
	[extradata3] [nvarchar](1000) NULL,
	[receiver] [nvarchar](255) NULL,
	[success] [bit] NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refchannelid] [bigint] NOT NULL,
	[refmessageid] [bigint] NULL,
 CONSTRAINT [mpay_notifications_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationservicearggrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationservicearggrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_notificationservicearggrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationserviceargitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationserviceargitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_notificationserviceargitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationserviceargs]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationserviceargs](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[name] [nvarchar](1000) NOT NULL,
	[value] [nvarchar](1000) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refserviceid] [bigint] NULL,
 CONSTRAINT [mpay_notificationserviceargs_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_eqwv5t7ynsbwaidyhe5323j41] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[refserviceid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationservicegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationservicegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_notificationservicegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationserviceitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationserviceitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_notificationserviceitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationservices]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationservices](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[processor] [nvarchar](1000) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_notificationservices_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_ma5ry7y5g0fmqunqv70t39tub] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[processor] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationtemplategrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationtemplategrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_notificationtemplategrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationtemplateitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationtemplateitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_notificationtemplateitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationtemplates]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationtemplates](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[template] [nvarchar](4000) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[reflanguageid] [bigint] NOT NULL,
	[refoperationid] [bigint] NOT NULL,
	[reftypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_notificationtemplates_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_o1ctbgy4pu4geitdogg6ovjm7] UNIQUE NONCLUSTERED 
(
	[refoperationid] ASC,
	[reftypeid] ASC,
	[reflanguageid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationtypegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationtypegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_notificationtypegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationtypeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationtypeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_notificationtypeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationtypes]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationtypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_notificationtypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_47wi2h966dssevftk0ejaknwj] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_pq6wbcnoook7jxx3y3cr3mnqj] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_notificationtypes_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_notificationtypes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[typeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_notificationtypes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_processingstatuses_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_processingstatuses_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[sttsid] [bigint] NOT NULL,
 CONSTRAINT [mpay_processingstatuses_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_processingstatusgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_processingstatusgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_processingstatusgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_processingstatusitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_processingstatusitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_processingstatusitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_profilegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_profilegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_profilegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_profileitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_profileitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_profileitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_profiles]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_profiles](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[chargesschemeid] [bigint] NOT NULL,
	[commissionsschemeid] [bigint] NOT NULL,
	[currencyid] [bigint] NOT NULL,
	[limitsschemeid] [bigint] NOT NULL,
	[requesttypesschemeid] [bigint] NOT NULL,
	[taxschemeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_profiles_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_qyixvxm5gy93b6jreyq9wx8xa] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_pspdetailgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_pspdetailgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_pspdetailgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_pspdetailitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_pspdetailitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_pspdetailitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_pspdetails]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_pspdetails](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[pspnationalid] [nvarchar](20) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refclearingaccountid] [bigint] NULL,
	[refdifferencesaccountid] [bigint] NULL,
	[refpspid] [bigint] NULL,
 CONSTRAINT [mpay_pspdetails_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_pspgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_pspgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_pspgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_pspitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_pspitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_pspitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_reasongrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_reasongrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_reasongrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_reasonitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_reasonitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_reasonitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_reasons]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_reasons](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[mpclearcode] [nvarchar](12) NULL,
	[smsenabled] [bit] NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_reasons_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_leii2bkoy590iu2bq566jux6l] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_objgq1oo00r0ipjeap9h6uxa4] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_reasons_nls]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_reasons_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[rsnid] [bigint] NOT NULL,
 CONSTRAINT [mpay_reasons_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_reg_file_sttgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_reg_file_sttgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_reg_file_sttgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_reg_file_sttitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_reg_file_sttitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_reg_file_sttitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_reg_file_stts]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_reg_file_stts](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_reg_file_stts_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_cplxyi920aysrmge171n7yc99] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_jpgrs067kptgj2py1uqfg6w1t] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_reg_instrs_sttgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_reg_instrs_sttgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_reg_instrs_sttgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_reg_instrs_sttitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_reg_instrs_sttitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_reg_instrs_sttitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_reg_instrs_stts]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_reg_instrs_stts](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_reg_instrs_stts_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_registrationotpgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_registrationotpgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_registrationotpgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_registrationotpitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_registrationotpitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_registrationotpitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_registrationotps]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_registrationotps](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[mobile] [nvarchar](255) NOT NULL,
	[code] [nvarchar](255) NOT NULL,
	[requesttime] [datetime] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[messagetypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_registrationotps_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_requesttypesdetailgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_requesttypesdetailgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_requesttypesdetailgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_requesttypesdetailitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_requesttypesdetailitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_requesttypesdetailitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_requesttypesdetails]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_requesttypesdetails](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[isactive] [bit] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[msgtypeid] [bigint] NOT NULL,
	[refschemeid] [bigint] NULL,
 CONSTRAINT [mpay_requesttypesdetails_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_c9kq6fn8lq1c3kdpjse7be28o] UNIQUE NONCLUSTERED 
(
	[refschemeid] ASC,
	[msgtypeid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_requesttypesscheme]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_requesttypesscheme](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_requesttypesscheme_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_requesttypesschemegrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_requesttypesschemegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_requesttypesschemegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_requesttypesschemeitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_requesttypesschemeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_requesttypesschemeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_saibotp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_saibotp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[attemptsnum] [bigint] NOT NULL,
	[dob] [date] NULL,
	[idnum] [nvarchar](100) NOT NULL,
	[mobilenumber] [nvarchar](100) NOT NULL,
	[nationality] [nvarchar](100) NULL,
	[otp] [nvarchar](255) NOT NULL,
	[requestretrynumber] [bigint] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_saibotp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_saibotpgrp]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_saibotpgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_saibotpgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_saibotpitm]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_saibotpitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_saibotpitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_service_detail_rep]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_service_detail_rep](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[active] [nvarchar](250) NULL,
	[blocked] [nvarchar](250) NULL,
	[chargeduration] [nvarchar](250) NULL,
	[corporatename] [nvarchar](250) NULL,
	[createddate] [nvarchar](250) NULL,
	[createduser] [nvarchar](250) NULL,
	[deleteddate] [nvarchar](250) NULL,
	[deleteduser] [nvarchar](250) NULL,
	[extractstamp] [nvarchar](250) NULL,
	[lasttransactionamount] [nvarchar](250) NULL,
	[lasttransactiontype] [nvarchar](250) NULL,
	[lasttransactiondate] [nvarchar](250) NULL,
	[modifieddate] [nvarchar](250) NULL,
	[modifieduser] [nvarchar](250) NULL,
	[notificationchannel] [nvarchar](250) NULL,
	[notificationreceiver] [nvarchar](250) NULL,
	[paymenttype] [nvarchar](250) NULL,
	[pinlastchanges] [nvarchar](250) NULL,
	[profile] [nvarchar](250) NULL,
	[registered] [nvarchar](250) NULL,
	[retrycount] [nvarchar](250) NULL,
	[servicealias] [nvarchar](250) NULL,
	[servicebalance] [nvarchar](250) NULL,
	[servicebankname] [nvarchar](250) NULL,
	[servicecategory] [nvarchar](250) NULL,
	[servicedescription] [nvarchar](250) NULL,
	[serviceiban] [nvarchar](250) NULL,
	[servicempayaccount] [nvarchar](250) NULL,
	[servicename] [nvarchar](250) NULL,
	[servicetype] [nvarchar](250) NULL,
	[servierbanknumber] [nvarchar](250) NULL,
	[status] [nvarchar](250) NULL,
	[wallettype] [nvarchar](250) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_service_detail_rep_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_service_trans_rep]    Script Date: 9/6/2021 12:23:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_service_trans_rep](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[corporatename] [nvarchar](250) NULL,
	[extractstamp] [nvarchar](250) NULL,
	[otherpartyamount] [nvarchar](250) NULL,
	[otherpartymobileno] [nvarchar](250) NULL,
	[servicebalanceaftertrn] [nvarchar](250) NULL,
	[servicebalancebeforetrn] [nvarchar](250) NULL,
	[servicebankname] [nvarchar](250) NULL,
	[servicebanknumber] [nvarchar](250) NULL,
	[serviceiban] [nvarchar](250) NULL,
	[servicempayaccount] [nvarchar](250) NULL,
	[servicename] [nvarchar](250) NULL,
	[servicestatus] [nvarchar](250) NULL,
	[trnamount] [nvarchar](250) NULL,
	[trncharges] [nvarchar](250) NULL,
	[trndate] [nvarchar](250) NULL,
	[trndescription] [nvarchar](250) NULL,
	[trnnetamount] [nvarchar](250) NULL,
	[trnrefernceno] [nvarchar](250) NULL,
	[trnside] [nvarchar](250) NULL,
	[trntime] [nvarchar](250) NULL,
	[trntype] [nvarchar](250) NULL,
	[trnvat] [nvarchar](250) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_service_trans_rep_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_serviceaccountgrp]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_serviceaccountgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_serviceaccountgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_serviceaccountitm]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_serviceaccountitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_serviceaccountitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_servicecashin]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_servicecashin](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[amount] [numeric](21, 8) NOT NULL,
	[chargeamount] [numeric](21, 8) NULL,
	[comments] [nvarchar](255) NULL,
	[ischargeincluded] [bit] NULL,
	[reasondesc] [nvarchar](4000) NULL,
	[refserviceaccount] [nvarchar](50) NULL,
	[taxamount] [numeric](21, 8) NULL,
	[totalamount] [numeric](21, 8) NULL,
	[transamount] [numeric](21, 8) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
	[processingstatusid] [bigint] NULL,
	[reasonid] [bigint] NULL,
	[refcorporateserviceid] [bigint] NOT NULL,
	[reflastmsglogid] [bigint] NULL,
	[refmessageid] [bigint] NULL,
 CONSTRAINT [mpay_servicecashin_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_servicecashout]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_servicecashout](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[amount] [numeric](21, 8) NOT NULL,
	[chargeamount] [numeric](21, 8) NULL,
	[comments] [nvarchar](255) NULL,
	[customertype] [nvarchar](19) NULL,
	[ischargeincluded] [bit] NULL,
	[reasondesc] [nvarchar](4000) NULL,
	[refserviceaccount] [nvarchar](50) NULL,
	[taxamount] [numeric](21, 8) NULL,
	[totalamount] [numeric](21, 8) NULL,
	[transamount] [numeric](21, 8) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
	[processingstatusid] [bigint] NULL,
	[reasonid] [bigint] NULL,
	[refcorporateserviceid] [bigint] NOT NULL,
	[reflastmsglogid] [bigint] NULL,
	[refmessageid] [bigint] NULL,
 CONSTRAINT [mpay_servicecashout_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_serviceintegmessageatt]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_serviceintegmessageatt](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[attfile] [varbinary](max) NULL,
	[attachment_source] [nvarchar](255) NULL,
	[attachment_token] [nvarchar](255) NULL,
	[comments] [nvarchar](255) NULL,
	[contenttype] [nvarchar](255) NULL,
	[entityid] [nvarchar](255) NULL,
	[image_thumbnail] [varbinary](max) NULL,
	[image_type] [nvarchar](255) NULL,
	[name] [nvarchar](255) NULL,
	[original_micr] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[rev] [int] NULL,
	[attachment_size] [numeric](19, 2) NULL,
 CONSTRAINT [mpay_serviceintegmessageatt_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_serviceintegmessages]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_serviceintegmessages](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[reasoncode] [nvarchar](19) NULL,
	[reasondescription] [nvarchar](1000) NULL,
	[reqfield1] [nvarchar](1000) NULL,
	[reqfield2] [nvarchar](1000) NULL,
	[reqfield3] [nvarchar](1000) NULL,
	[reqfield4] [nvarchar](1000) NULL,
	[reqfield5] [nvarchar](1000) NULL,
	[requestcontent] [varchar](max) NOT NULL,
	[requestdate] [datetime] NOT NULL,
	[requestid] [nvarchar](255) NOT NULL,
	[requesttoken] [nvarchar](4000) NULL,
	[responsecontent] [varchar](max) NULL,
	[responsedate] [datetime] NULL,
	[responseid] [nvarchar](255) NULL,
	[responsetoken] [nvarchar](4000) NULL,
	[rspfield1] [nvarchar](1000) NULL,
	[rspfield2] [nvarchar](1000) NULL,
	[rspfield3] [nvarchar](1000) NULL,
	[rspfield4] [nvarchar](1000) NULL,
	[rspfield5] [nvarchar](1000) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refmessageid] [bigint] NULL,
	[refserviceid] [bigint] NOT NULL,
	[refstatusid] [bigint] NULL,
 CONSTRAINT [mpay_serviceintegmessages_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_serviceintegreasongrp]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_serviceintegreasongrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_serviceintegreasongrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_serviceintegreasonitm]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_serviceintegreasonitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_serviceintegreasonitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_serviceintegreasons]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_serviceintegreasons](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[processorcode] [nvarchar](12) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[mpaymappingreasonid] [bigint] NULL,
 CONSTRAINT [mpay_serviceintegreasons_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_9pcw4vcvmq1yyx6syxsbr6nqw] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[processorcode] ASC,
	[mpaymappingreasonid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_serviceintegreasons_nls]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_serviceintegreasons_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[rsnid] [bigint] NOT NULL,
 CONSTRAINT [mpay_serviceintegreasons_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_serviceintegsettings]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_serviceintegsettings](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[description] [nvarchar](1000) NULL,
	[key] [nvarchar](255) NOT NULL,
	[value] [nvarchar](1000) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refserviceid] [bigint] NOT NULL,
 CONSTRAINT [mpay_serviceintegsettings_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_servicescategories]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_servicescategories](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_servicescategories_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_ks1o2977lk1q0gwjaegtx2n8e] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_ofmyfw5y3md5qkg5p6ffrw2m0] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_servicescategories_nls]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_servicescategories_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[servicecategoryid] [bigint] NOT NULL,
 CONSTRAINT [mpay_servicescategories_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_servicetypes]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_servicetypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[securesecret] [nvarchar](255) NOT NULL,
	[vpcaccesscode] [nvarchar](255) NOT NULL,
	[vpcmerchant] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_servicetypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_4smltacvnvho55c7om5jhcoxn] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_k4yji6x8jl9fobq5lv0ue38hf] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_servicetypes_nls]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_servicetypes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[servicetypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_servicetypes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_sessionreport]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_sessionreport](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[reportcreationdate] [datetime] NULL,
	[sessionid] [bigint] NULL,
	[totalwalletbalance] [numeric](21, 8) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
	[sessionstatusid] [bigint] NULL,
 CONSTRAINT [mpay_sessionreport_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_sessionstatugrp]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_sessionstatugrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_sessionstatugrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_sessionstatuitm]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_sessionstatuitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_sessionstatuitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_sessionstatus]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_sessionstatus](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_sessionstatus_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_m70v8cptyof03je7u50l3vx33] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_tladigm0xg0ctip1p4ut0e0w] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_staticqrcode]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_staticqrcode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[generationtype] [nvarchar](20) NOT NULL,
	[url] [nvarchar](255) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[refcorporateid] [bigint] NOT NULL,
	[refdeviceid] [bigint] NULL,
	[refserviceid] [bigint] NOT NULL,
 CONSTRAINT [mpay_staticqrcode_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_staticqrcodegrp]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_staticqrcodegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_staticqrcodegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_staticqrcodeitm]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_staticqrcodeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_staticqrcodeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_sysconfigs]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_sysconfigs](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[configkey] [nvarchar](255) NOT NULL,
	[configvalue] [nvarchar](4000) NOT NULL,
	[hint] [nvarchar](255) NULL,
	[isdownloadable] [bit] NULL,
	[regex] [nvarchar](255) NULL,
	[section] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_sysconfigs_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_225un4y90xgd29asw4r9p3vaw] UNIQUE NONCLUSTERED 
(
	[section] ASC,
	[configkey] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_system_balance]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_system_balance](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[accountid] [numeric](19, 0) NULL,
	[jvamount] [numeric](19, 3) NULL,
	[isdebit] [bit] NULL,
	[txtime] [datetime] NULL,
	[selected] [bit] NULL,
	[txttime] [datetime] NULL,
	[jvid] [bigint] NULL,
 CONSTRAINT [mpay_system_balance_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_systemaccountscashout]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_systemaccountscashout](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[amount] [numeric](21, 8) NOT NULL,
	[comments] [nvarchar](255) NULL,
	[destinationservicebalance] [numeric](21, 8) NULL,
	[sourceservicebalance] [numeric](21, 8) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
	[destinationserviceid] [bigint] NULL,
	[processingstatusid] [bigint] NULL,
	[reasonid] [bigint] NULL,
	[reftransactionid] [bigint] NULL,
	[sourceserviceid] [bigint] NOT NULL,
 CONSTRAINT [mpay_systemaccountscashout_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_taxschemedetailgrp]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_taxschemedetailgrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_taxschemedetailgrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_taxschemedetailitm]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_taxschemedetailitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_taxschemedetailitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_taxschemedetails]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_taxschemedetails](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[description] [nvarchar](255) NULL,
	[isactive] [bit] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[msgtypeid] [bigint] NOT NULL,
	[refschemeid] [bigint] NULL,
 CONSTRAINT [mpay_taxschemedetails_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_f4vjnjk2fla8is36vqnkyc170] UNIQUE NONCLUSTERED 
(
	[refschemeid] ASC,
	[msgtypeid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_taxschemegrp]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_taxschemegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_taxschemegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_taxschemeitm]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_taxschemeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_taxschemeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_taxschemes]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_taxschemes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[isactive] [bit] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
 CONSTRAINT [mpay_taxschemes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_6n683xoo752p9rqb9rjqewwvn] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_taxslicegrp]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_taxslicegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_taxslicegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_taxsliceitm]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_taxsliceitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_taxsliceitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_taxslices]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_taxslices](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[maxamount] [numeric](21, 8) NULL,
	[minamount] [numeric](21, 8) NULL,
	[taxamount] [numeric](21, 8) NULL,
	[taxpercent] [numeric](21, 8) NULL,
	[taxtype] [nvarchar](1) NOT NULL,
	[txamountlimit] [numeric](21, 8) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[reftaxdetailsid] [bigint] NULL,
 CONSTRAINT [mpay_taxslices_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactionconfigs]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactionconfigs](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[description] [nvarchar](500) NOT NULL,
	[isreceiverbanked] [bit] NOT NULL,
	[issenderbanked] [bit] NOT NULL,
	[maxamount] [float] NOT NULL,
	[minamount] [float] NOT NULL,
	[reversable] [bit] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
	[messagetypeid] [bigint] NOT NULL,
	[receivertypeid] [bigint] NOT NULL,
	[refoperationid] [bigint] NOT NULL,
	[reftypeid] [bigint] NOT NULL,
	[sendertypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_transactionconfigs_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_2i2m6pkd1xa0m8pv4wie2usdj] UNIQUE NONCLUSTERED 
(
	[messagetypeid] ASC,
	[sendertypeid] ASC,
	[issenderbanked] ASC,
	[receivertypeid] ASC,
	[isreceiverbanked] ASC,
	[currencyid] ASC,
	[reftypeid] ASC,
	[refoperationid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactiondirectiongrp]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactiondirectiongrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_transactiondirectiongrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactiondirectionitm]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactiondirectionitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_transactiondirectionitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactiondirections_nls]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactiondirections_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[directionid] [bigint] NOT NULL,
 CONSTRAINT [mpay_transactiondirections_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactionsconfig]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactionsconfig](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[bankedunbanked] [nvarchar](500) NOT NULL,
	[description] [nvarchar](500) NOT NULL,
	[issender] [bit] NOT NULL,
	[maxamount] [float] NOT NULL,
	[minamount] [float] NOT NULL,
	[reversable] [bit] NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[clienttypeid] [bigint] NOT NULL,
	[messagetypeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_transactionsconfig_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_p3m51g7ehyckn8pe03ox17r2f] UNIQUE NONCLUSTERED 
(
	[clienttypeid] ASC,
	[issender] ASC,
	[bankedunbanked] ASC,
	[messagetypeid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactionsizegrp]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactionsizegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_transactionsizegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactionsizeitm]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactionsizeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_transactionsizeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactionsizes]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactionsizes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_transactionsizes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_cj6yksanc4lw015gw9qrov1iq] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_ejnohm12phqv2q11phsqn0igg] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactionsizes_nls]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactionsizes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[sizeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_transactionsizes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactionssummary]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactionssummary](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[reportcreationdate] [datetime] NULL,
	[totalamount] [numeric](21, 8) NULL,
	[totalcharge] [numeric](21, 8) NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NULL,
	[directionid] [bigint] NULL,
	[messagetypeid] [bigint] NULL,
	[sessionid] [bigint] NULL,
 CONSTRAINT [mpay_transactionssummary_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactiontypegrp]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactiontypegrp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[actionname] [nvarchar](255) NULL,
	[auditchangetype] [int] NULL,
	[audit_change_type_des] [nvarchar](255) NULL,
	[branch] [nvarchar](255) NULL,
	[changefield] [nvarchar](255) NULL,
	[changefielddescription] [nvarchar](255) NULL,
	[changefieldname] [nvarchar](255) NULL,
	[change_history_token] [nvarchar](255) NULL,
	[draft] [bit] NULL,
	[entityid] [nvarchar](255) NULL,
	[recordid] [nvarchar](255) NULL,
	[ref_value] [nvarchar](255) NULL,
	[security_level] [nvarchar](255) NULL,
	[user_group] [nvarchar](255) NULL,
	[userid] [nvarchar](255) NULL,
	[username] [nvarchar](255) NULL,
	[viewdescription] [nvarchar](255) NULL,
	[viewname] [nvarchar](255) NULL,
	[workstation] [nvarchar](255) NULL,
 CONSTRAINT [mpay_transactiontypegrp_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactiontypeitm]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactiontypeitm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[field] [nvarchar](255) NULL,
	[fielddescription] [nvarchar](255) NULL,
	[ip_address] [nvarchar](255) NULL,
	[newvalue] [varchar](max) NULL,
	[oldvalue] [varchar](max) NULL,
	[security_level] [nvarchar](255) NULL,
	[groupid] [bigint] NULL,
 CONSTRAINT [mpay_transactiontypeitm_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactiontypes]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactiontypes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[code] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NULL,
	[name] [nvarchar](255) NOT NULL,
	[mpclearprocessingcode] [nvarchar](10) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
 CONSTRAINT [mpay_transactiontypes_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_5jj2u1febqybuggskiqo7nxff] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_tg8vq8pdsek0k017gbck1lpy3] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_transactiontypes_nls]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_transactiontypes_nls](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[languagecode] [nvarchar](255) NULL,
	[translation] [nvarchar](255) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[typeid] [bigint] NOT NULL,
 CONSTRAINT [mpay_transactiontypes_nls_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mpay_trnscheckoutrecords]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mpay_trnscheckoutrecords](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[z_archive_on] [datetime] NULL,
	[z_archive_queued] [datetime] NULL,
	[z_archive_status] [nvarchar](255) NULL,
	[z_assigned_group] [bigint] NULL,
	[z_assigned_user] [bigint] NULL,
	[z_created_by] [nvarchar](255) NULL,
	[z_creation_date] [datetime] NULL,
	[z_deleted_by] [nvarchar](255) NULL,
	[z_deleted_flag] [bit] NULL,
	[z_deleted_on] [datetime] NULL,
	[z_editable] [bit] NULL,
	[z_locked_by] [nvarchar](255) NULL,
	[z_locked_until] [datetime] NULL,
	[z_org_id] [bigint] NULL,
	[z_tenant_id] [nvarchar](255) NULL,
	[z_updated_by] [nvarchar](255) NULL,
	[z_updating_date] [datetime] NULL,
	[z_workflow_id] [bigint] NULL,
	[z_ws_token] [nvarchar](255) NULL,
	[z_draft_status] [nvarchar](255) NULL,
	[checkoutid] [nvarchar](100) NOT NULL,
	[processingstatus] [nvarchar](50) NOT NULL,
	[sendermobile] [nvarchar](50) NOT NULL,
	[totalamount] [numeric](21, 8) NOT NULL,
	[z_draft_id] [bigint] NULL,
	[z_status_id] [bigint] NULL,
	[currencyid] [bigint] NOT NULL,
	[refmessageid] [bigint] NOT NULL,
 CONSTRAINT [mpay_trnscheckoutrecords_pkey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uk_15v0fgrjbrfqw8rj8id3rgwyd] UNIQUE NONCLUSTERED 
(
	[checkoutid] ASC,
	[z_tenant_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [BR_CONDITION_SCHEME_ID_IDX]    Script Date: 9/6/2021 12:23:32 PM ******/
CREATE NONCLUSTERED INDEX [BR_CONDITION_SCHEME_ID_IDX] ON [dbo].[JFW_BROLES_VIEWS]
(
	[CONDITION_SCHEME_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [BR_VIEW_ID_IDX]    Script Date: 9/6/2021 12:23:32 PM ******/
CREATE NONCLUSTERED INDEX [BR_VIEW_ID_IDX] ON [dbo].[JFW_BROLES_VIEWS]
(
	[VIEW_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [BRA_WORKFLOW_ACTION_IDX]    Script Date: 9/6/2021 12:23:32 PM ******/
CREATE NONCLUSTERED INDEX [BRA_WORKFLOW_ACTION_IDX] ON [dbo].[JFW_BUSINESS_ROLES_ACTIONS]
(
	[WORKFLOW_ACTION] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [BRA_WORKFLOW_STEP_IDX]    Script Date: 9/6/2021 12:23:32 PM ******/
CREATE NONCLUSTERED INDEX [BRA_WORKFLOW_STEP_IDX] ON [dbo].[JFW_BUSINESS_ROLES_ACTIONS]
(
	[WORKFLOW_STEP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [SV_VIEW_ID_IDX]    Script Date: 9/6/2021 12:23:32 PM ******/
CREATE NONCLUSTERED INDEX [SV_VIEW_ID_IDX] ON [dbo].[JFW_SCHM_VIEWS]
(
	[VIEW_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [SV_VIEWS_SCHM_ID_IDX]    Script Date: 9/6/2021 12:23:32 PM ******/
CREATE NONCLUSTERED INDEX [SV_VIEWS_SCHM_ID_IDX] ON [dbo].[JFW_SCHM_VIEWS]
(
	[VIEWS_SCHM_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [TNT_STATUS_BAR_ID_IDX]    Script Date: 9/6/2021 12:23:32 PM ******/
CREATE NONCLUSTERED INDEX [TNT_STATUS_BAR_ID_IDX] ON [dbo].[JFW_TENANT]
(
	[STATUS_BAR_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES]  WITH CHECK ADD  CONSTRAINT [FK41338ypmpyh2rtlwimbe0mssn] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES] CHECK CONSTRAINT [FK41338ypmpyh2rtlwimbe0mssn]
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES]  WITH CHECK ADD  CONSTRAINT [FK6slowtu9kh6cjnmw80inwqmng] FOREIGN KEY([JFW_CALENDAR_ID])
REFERENCES [dbo].[JFW_CALENDAR] ([ID])
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES] CHECK CONSTRAINT [FK6slowtu9kh6cjnmw80inwqmng]
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES]  WITH CHECK ADD  CONSTRAINT [FKdgjikftu4sgl45bivc0ulc7li] FOREIGN KEY([AUTH_SCHEM_ID])
REFERENCES [dbo].[JFW_AUTH_CHAIN_SCHM] ([ID])
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES] CHECK CONSTRAINT [FKdgjikftu4sgl45bivc0ulc7li]
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES]  WITH CHECK ADD  CONSTRAINT [FKf54cuqhyfith6fsgavqrr3bpc] FOREIGN KEY([WORKING_HOURS_SCHED_ID])
REFERENCES [dbo].[JFW_WORKING_HOURS] ([id])
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES] CHECK CONSTRAINT [FKf54cuqhyfith6fsgavqrr3bpc]
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES]  WITH CHECK ADD  CONSTRAINT [FKiljmil7ttcq8mf086pjrd3wv4] FOREIGN KEY([SECRET_QUESTION_POLICY_ID])
REFERENCES [dbo].[JFW_SECRET_QUESTION_POLICY] ([id])
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES] CHECK CONSTRAINT [FKiljmil7ttcq8mf086pjrd3wv4]
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES]  WITH CHECK ADD  CONSTRAINT [FKmhovhhpk26hnjk0v9uuxomygt] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES] CHECK CONSTRAINT [FKmhovhhpk26hnjk0v9uuxomygt]
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES]  WITH CHECK ADD  CONSTRAINT [FKo0ea0bicwpc96fkkwxeghqakd] FOREIGN KEY([AUTH_CHAIN_SCHM])
REFERENCES [dbo].[JFW_AUTH_CHAIN_SCHM] ([ID])
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES] CHECK CONSTRAINT [FKo0ea0bicwpc96fkkwxeghqakd]
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES]  WITH CHECK ADD  CONSTRAINT [FKs16miytcqfocftkv3ncrlgel2] FOREIGN KEY([otpScheme_ID])
REFERENCES [dbo].[JFW_OTP_SCHEME] ([id])
GO
ALTER TABLE [dbo].[JFW_ACCOUNT_POLICIES] CHECK CONSTRAINT [FKs16miytcqfocftkv3ncrlgel2]
GO
ALTER TABLE [dbo].[JFW_ACTION_LEVEL]  WITH CHECK ADD  CONSTRAINT [FK21yf7nh0haohljkvg426pt3iv] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_ACTION_LEVEL] CHECK CONSTRAINT [FK21yf7nh0haohljkvg426pt3iv]
GO
ALTER TABLE [dbo].[JFW_ACTION_LEVEL]  WITH CHECK ADD  CONSTRAINT [FKkeyunp37ettkb7iudl6bsnkxb] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_ACTION_LEVEL] CHECK CONSTRAINT [FKkeyunp37ettkb7iudl6bsnkxb]
GO
ALTER TABLE [dbo].[JFW_ATTACHMNT_DICTIONARIES]  WITH CHECK ADD  CONSTRAINT [FK1v5ge6fhtstjt1xlcnfbqorpo] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_ATTACHMNT_DICTIONARIES] CHECK CONSTRAINT [FK1v5ge6fhtstjt1xlcnfbqorpo]
GO
ALTER TABLE [dbo].[JFW_ATTACHMNT_DICTIONARIES]  WITH CHECK ADD  CONSTRAINT [FKsbtm3ccg5g6c48yvmawhepwnq] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_ATTACHMNT_DICTIONARIES] CHECK CONSTRAINT [FKsbtm3ccg5g6c48yvmawhepwnq]
GO
ALTER TABLE [dbo].[JFW_AUTH_CHAIN]  WITH CHECK ADD  CONSTRAINT [FKcudx87cbkpacfemrsjnhin8d9] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_AUTH_CHAIN] CHECK CONSTRAINT [FKcudx87cbkpacfemrsjnhin8d9]
GO
ALTER TABLE [dbo].[JFW_AUTH_CHAIN]  WITH CHECK ADD  CONSTRAINT [FKfb8muebekeq6v5gn86d6qls3a] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_AUTH_CHAIN] CHECK CONSTRAINT [FKfb8muebekeq6v5gn86d6qls3a]
GO
ALTER TABLE [dbo].[JFW_AUTH_CHAIN]  WITH CHECK ADD  CONSTRAINT [FKhtebok9aecdenlgyjvddbym3] FOREIGN KEY([chainScheme_ID])
REFERENCES [dbo].[JFW_AUTH_CHAIN_SCHM] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_AUTH_CHAIN] CHECK CONSTRAINT [FKhtebok9aecdenlgyjvddbym3]
GO
ALTER TABLE [dbo].[JFW_AUTH_CHAIN_SCHM]  WITH CHECK ADD  CONSTRAINT [FK8s8iv73cgeautm56k383tqgce] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_AUTH_CHAIN_SCHM] CHECK CONSTRAINT [FK8s8iv73cgeautm56k383tqgce]
GO
ALTER TABLE [dbo].[JFW_AUTH_CHAIN_SCHM]  WITH CHECK ADD  CONSTRAINT [FKk2cu5qmt4jgwd2f730lw0w2ud] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_AUTH_CHAIN_SCHM] CHECK CONSTRAINT [FKk2cu5qmt4jgwd2f730lw0w2ud]
GO
ALTER TABLE [dbo].[JFW_AUTHENTICATION_TOKEN]  WITH CHECK ADD  CONSTRAINT [FK6x0xtuwsa8wdyhlsghp1id3so] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_AUTHENTICATION_TOKEN] CHECK CONSTRAINT [FK6x0xtuwsa8wdyhlsghp1id3so]
GO
ALTER TABLE [dbo].[JFW_AUTHENTICATION_TOKEN]  WITH CHECK ADD  CONSTRAINT [FKa5uqwdw8ctl4ys8ojs9lcd8n2] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_AUTHENTICATION_TOKEN] CHECK CONSTRAINT [FKa5uqwdw8ctl4ys8ojs9lcd8n2]
GO
ALTER TABLE [dbo].[JFW_BROLES_GENERIC_AUTH]  WITH CHECK ADD  CONSTRAINT [FKhs2ld2nm8bpmxwonpfb0rkega] FOREIGN KEY([BROLE_ID])
REFERENCES [dbo].[JFW_BUSINESS_ROLES] ([ID])
GO
ALTER TABLE [dbo].[JFW_BROLES_GENERIC_AUTH] CHECK CONSTRAINT [FKhs2ld2nm8bpmxwonpfb0rkega]
GO
ALTER TABLE [dbo].[JFW_BROLES_GENERIC_AUTH]  WITH CHECK ADD  CONSTRAINT [FKxe2y3tv75l1np3eydq6adjn8] FOREIGN KEY([AUTHORITY_ID])
REFERENCES [dbo].[JFW_GENERIC_AUTHORITIES] ([id])
GO
ALTER TABLE [dbo].[JFW_BROLES_GENERIC_AUTH] CHECK CONSTRAINT [FKxe2y3tv75l1np3eydq6adjn8]
GO
ALTER TABLE [dbo].[JFW_BROLES_RPRTS_AUTH]  WITH CHECK ADD  CONSTRAINT [FK8m5864crgypehh9gvoodjant9] FOREIGN KEY([AUTHORITY_ID])
REFERENCES [dbo].[JFW_RPTS_AUTHORITIES] ([id])
GO
ALTER TABLE [dbo].[JFW_BROLES_RPRTS_AUTH] CHECK CONSTRAINT [FK8m5864crgypehh9gvoodjant9]
GO
ALTER TABLE [dbo].[JFW_BROLES_RPRTS_AUTH]  WITH CHECK ADD  CONSTRAINT [FKhsoti5dhon21p2mf65g2yiyt] FOREIGN KEY([BROLE_ID])
REFERENCES [dbo].[JFW_BUSINESS_ROLES] ([ID])
GO
ALTER TABLE [dbo].[JFW_BROLES_RPRTS_AUTH] CHECK CONSTRAINT [FKhsoti5dhon21p2mf65g2yiyt]
GO
ALTER TABLE [dbo].[JFW_BROLES_VIEWS]  WITH CHECK ADD  CONSTRAINT [FK1m1e358rbxo3t9kf77343miqp] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_BROLES_VIEWS] CHECK CONSTRAINT [FK1m1e358rbxo3t9kf77343miqp]
GO
ALTER TABLE [dbo].[JFW_BROLES_VIEWS]  WITH CHECK ADD  CONSTRAINT [FKb2rw4k69ohh2hu7dy35otsv2y] FOREIGN KEY([BUSINESS_ROLE_ID])
REFERENCES [dbo].[JFW_BUSINESS_ROLES] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_BROLES_VIEWS] CHECK CONSTRAINT [FKb2rw4k69ohh2hu7dy35otsv2y]
GO
ALTER TABLE [dbo].[JFW_BROLES_VIEWS]  WITH CHECK ADD  CONSTRAINT [FKl0vt7h0tj0lo1vaciti1f4wyq] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_BROLES_VIEWS] CHECK CONSTRAINT [FKl0vt7h0tj0lo1vaciti1f4wyq]
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES]  WITH CHECK ADD  CONSTRAINT [FK5l5f1vfuuldpyru89bsqrrbfc] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES] CHECK CONSTRAINT [FK5l5f1vfuuldpyru89bsqrrbfc]
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES]  WITH CHECK ADD  CONSTRAINT [FKr1m91mf5u5f66kok95su3v48x] FOREIGN KEY([ACTION_LEVEL])
REFERENCES [dbo].[JFW_ACTION_LEVEL] ([ID])
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES] CHECK CONSTRAINT [FKr1m91mf5u5f66kok95su3v48x]
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES]  WITH CHECK ADD  CONSTRAINT [FKvo4ftyd495fl9gkfa6ljj8r4] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES] CHECK CONSTRAINT [FKvo4ftyd495fl9gkfa6ljj8r4]
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES_ACTIONS]  WITH CHECK ADD  CONSTRAINT [FKagm3fivg5skvsf7mf71cjwa6i] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES_ACTIONS] CHECK CONSTRAINT [FKagm3fivg5skvsf7mf71cjwa6i]
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES_ACTIONS]  WITH CHECK ADD  CONSTRAINT [FKaw0xtu7rs25vkgmhb7t0xeydb] FOREIGN KEY([BUSINESS_ROLE_VIEW_ID])
REFERENCES [dbo].[JFW_BROLES_VIEWS] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES_ACTIONS] CHECK CONSTRAINT [FKaw0xtu7rs25vkgmhb7t0xeydb]
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES_ACTIONS]  WITH CHECK ADD  CONSTRAINT [FKcchemxe1tmy8uq9t5qldjbi73] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES_ACTIONS] CHECK CONSTRAINT [FKcchemxe1tmy8uq9t5qldjbi73]
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES_ACTIONS]  WITH CHECK ADD  CONSTRAINT [FKesywnmwe19dl60e9se41wvjr7] FOREIGN KEY([BUSINESS_ROLE_ID])
REFERENCES [dbo].[JFW_BUSINESS_ROLES] ([ID])
GO
ALTER TABLE [dbo].[JFW_BUSINESS_ROLES_ACTIONS] CHECK CONSTRAINT [FKesywnmwe19dl60e9se41wvjr7]
GO
ALTER TABLE [dbo].[JFW_CALENDAR]  WITH CHECK ADD  CONSTRAINT [FK5wdi476wgr6t77kja91ekfk3a] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_CALENDAR] CHECK CONSTRAINT [FK5wdi476wgr6t77kja91ekfk3a]
GO
ALTER TABLE [dbo].[JFW_CALENDAR]  WITH CHECK ADD  CONSTRAINT [FKdn7yjbsdl61i4xefql6esynka] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_CALENDAR] CHECK CONSTRAINT [FKdn7yjbsdl61i4xefql6esynka]
GO
ALTER TABLE [dbo].[JFW_CHANGE_ITEM]  WITH CHECK ADD  CONSTRAINT [FKsncdfm0f35l3olfaif8dlhd8j] FOREIGN KEY([groupid])
REFERENCES [dbo].[JFW_CHANGE_GROUP] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_CHANGE_ITEM] CHECK CONSTRAINT [FKsncdfm0f35l3olfaif8dlhd8j]
GO
ALTER TABLE [dbo].[JFW_COUNTRIES]  WITH CHECK ADD  CONSTRAINT [FKgdvtjcw92nq3a86dlccgr54o2] FOREIGN KEY([DEFAULT_CURRENCY])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[JFW_COUNTRIES] CHECK CONSTRAINT [FKgdvtjcw92nq3a86dlccgr54o2]
GO
ALTER TABLE [dbo].[JFW_COUNTRIES]  WITH CHECK ADD  CONSTRAINT [FKhy6jera2jrnhbq6qi6wq949sv] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_COUNTRIES] CHECK CONSTRAINT [FKhy6jera2jrnhbq6qi6wq949sv]
GO
ALTER TABLE [dbo].[JFW_COUNTRIES]  WITH CHECK ADD  CONSTRAINT [FKix2f3bpk3ney6r2rtm3acmwtl] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_COUNTRIES] CHECK CONSTRAINT [FKix2f3bpk3ney6r2rtm3acmwtl]
GO
ALTER TABLE [dbo].[JFW_COUNTRY_TRANSLATION]  WITH CHECK ADD  CONSTRAINT [FK5x7bsbmd6ux9wdgsk470vkxik] FOREIGN KEY([country_id])
REFERENCES [dbo].[JFW_COUNTRIES] ([id])
GO
ALTER TABLE [dbo].[JFW_COUNTRY_TRANSLATION] CHECK CONSTRAINT [FK5x7bsbmd6ux9wdgsk470vkxik]
GO
ALTER TABLE [dbo].[JFW_COUNTRY_TRANSLATION]  WITH CHECK ADD  CONSTRAINT [FK97prpx2o9betebt4y9n3jspet] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_COUNTRY_TRANSLATION] CHECK CONSTRAINT [FK97prpx2o9betebt4y9n3jspet]
GO
ALTER TABLE [dbo].[JFW_COUNTRY_TRANSLATION]  WITH CHECK ADD  CONSTRAINT [FKgn6ymaf8v5g48nr4c1l3jk2r2] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_COUNTRY_TRANSLATION] CHECK CONSTRAINT [FKgn6ymaf8v5g48nr4c1l3jk2r2]
GO
ALTER TABLE [dbo].[JFW_CURRENCIES]  WITH CHECK ADD  CONSTRAINT [FK8sv9tsabtw5xubbo3hwtftkeo] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_CURRENCIES] CHECK CONSTRAINT [FK8sv9tsabtw5xubbo3hwtftkeo]
GO
ALTER TABLE [dbo].[JFW_CURRENCIES]  WITH CHECK ADD  CONSTRAINT [FK9xrxhv62cg4dha072e5i67unx] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_CURRENCIES] CHECK CONSTRAINT [FK9xrxhv62cg4dha072e5i67unx]
GO
ALTER TABLE [dbo].[JFW_CUSTOM_REPORTS]  WITH CHECK ADD  CONSTRAINT [FKbn8nv27g4c0teue17t3fb113m] FOREIGN KEY([AUTHORITY_ID])
REFERENCES [dbo].[JFW_RPTS_AUTHORITIES] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_CUSTOM_REPORTS] CHECK CONSTRAINT [FKbn8nv27g4c0teue17t3fb113m]
GO
ALTER TABLE [dbo].[JFW_CUSTOM_REPORTS]  WITH CHECK ADD  CONSTRAINT [FKdb2ech2iigiifofaxc0m8sqwo] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_CUSTOM_REPORTS] CHECK CONSTRAINT [FKdb2ech2iigiifofaxc0m8sqwo]
GO
ALTER TABLE [dbo].[JFW_CUSTOM_REPORTS]  WITH CHECK ADD  CONSTRAINT [FKmuhhhcrl1jvyj8smkhpwsc0] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_CUSTOM_REPORTS] CHECK CONSTRAINT [FKmuhhhcrl1jvyj8smkhpwsc0]
GO
ALTER TABLE [dbo].[JFW_DRAFTS]  WITH CHECK ADD  CONSTRAINT [FKqgkt8l1u5gnkblinp0ajuo21w] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_DRAFTS] CHECK CONSTRAINT [FKqgkt8l1u5gnkblinp0ajuo21w]
GO
ALTER TABLE [dbo].[JFW_EXCEPTIONAL_WORKING_DAYS]  WITH CHECK ADD  CONSTRAINT [FK9667pg992h5f8ygl4qld8qph7] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_EXCEPTIONAL_WORKING_DAYS] CHECK CONSTRAINT [FK9667pg992h5f8ygl4qld8qph7]
GO
ALTER TABLE [dbo].[JFW_EXCEPTIONAL_WORKING_DAYS]  WITH CHECK ADD  CONSTRAINT [FKay8nn0hgvwwpk8lqoprxqn40] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_EXCEPTIONAL_WORKING_DAYS] CHECK CONSTRAINT [FKay8nn0hgvwwpk8lqoprxqn40]
GO
ALTER TABLE [dbo].[JFW_EXCEPTIONAL_WORKING_DAYS]  WITH CHECK ADD  CONSTRAINT [FKlp8coii1dnx2tai10k2s51pgi] FOREIGN KEY([CALENDAR_ID])
REFERENCES [dbo].[JFW_CALENDAR] ([ID])
GO
ALTER TABLE [dbo].[JFW_EXCEPTIONAL_WORKING_DAYS] CHECK CONSTRAINT [FKlp8coii1dnx2tai10k2s51pgi]
GO
ALTER TABLE [dbo].[JFW_EXCPTN_HNDLR]  WITH CHECK ADD  CONSTRAINT [FKkxw6mfpx1p7x1h78v2km5bkyu] FOREIGN KEY([EXCPTN_HNDLR_SCHME_ID])
REFERENCES [dbo].[JFW_EXCPTN_HNDLR_SCHEME] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_EXCPTN_HNDLR] CHECK CONSTRAINT [FKkxw6mfpx1p7x1h78v2km5bkyu]
GO
ALTER TABLE [dbo].[JFW_FLD_USR_LGN_LOG]  WITH CHECK ADD  CONSTRAINT [FKekglea1ic4sgv6cale7lylfxh] FOREIGN KEY([USER_ID])
REFERENCES [dbo].[JFW_USERS] ([id])
GO
ALTER TABLE [dbo].[JFW_FLD_USR_LGN_LOG] CHECK CONSTRAINT [FKekglea1ic4sgv6cale7lylfxh]
GO
ALTER TABLE [dbo].[JFW_FLD_USR_LGN_LOG]  WITH CHECK ADD  CONSTRAINT [FKhac7sla3f5le6qrht2ura4jxb] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_FLD_USR_LGN_LOG] CHECK CONSTRAINT [FKhac7sla3f5le6qrht2ura4jxb]
GO
ALTER TABLE [dbo].[JFW_FLD_USR_LGN_LOG]  WITH CHECK ADD  CONSTRAINT [FKjbskcwauqcvoxwiy7s6lwpiyl] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_FLD_USR_LGN_LOG] CHECK CONSTRAINT [FKjbskcwauqcvoxwiy7s6lwpiyl]
GO
ALTER TABLE [dbo].[JFW_GENERIC_AUTHORITIES]  WITH CHECK ADD  CONSTRAINT [FK131ou0jp602dssfkthj8edoqx] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_GENERIC_AUTHORITIES] CHECK CONSTRAINT [FK131ou0jp602dssfkthj8edoqx]
GO
ALTER TABLE [dbo].[JFW_GENERIC_AUTHORITIES]  WITH CHECK ADD  CONSTRAINT [FK198i4a4qscm02y4hotd0yr2hc] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_GENERIC_AUTHORITIES] CHECK CONSTRAINT [FK198i4a4qscm02y4hotd0yr2hc]
GO
ALTER TABLE [dbo].[JFW_GENERIC_AUTHORITIES]  WITH CHECK ADD  CONSTRAINT [FKq815giydcw1nsp005ma8dhyc5] FOREIGN KEY([userType_ID])
REFERENCES [dbo].[JFW_USER_TYPES] ([ID])
GO
ALTER TABLE [dbo].[JFW_GENERIC_AUTHORITIES] CHECK CONSTRAINT [FKq815giydcw1nsp005ma8dhyc5]
GO
ALTER TABLE [dbo].[JFW_GENERIC_CONFIGURATION]  WITH CHECK ADD  CONSTRAINT [FK4xc1i4exj3r8v117kjlny78t1] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_GENERIC_CONFIGURATION] CHECK CONSTRAINT [FK4xc1i4exj3r8v117kjlny78t1]
GO
ALTER TABLE [dbo].[JFW_GENERIC_CONFIGURATION]  WITH CHECK ADD  CONSTRAINT [FKa9g4m28ve2co2hnt91su50u9i] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_GENERIC_CONFIGURATION] CHECK CONSTRAINT [FKa9g4m28ve2co2hnt91su50u9i]
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_CAT]  WITH CHECK ADD  CONSTRAINT [FK3ivx6mm0uq1w0nbw1fv5bin0w] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_CAT] CHECK CONSTRAINT [FK3ivx6mm0uq1w0nbw1fv5bin0w]
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_CAT]  WITH CHECK ADD  CONSTRAINT [FKqq41n2incj4lxpaaqi1bgd4at] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_CAT] CHECK CONSTRAINT [FKqq41n2incj4lxpaaqi1bgd4at]
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_CAT_TRANS]  WITH CHECK ADD  CONSTRAINT [FKir3p9t5adcxt7y11ntp3v240o] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_CAT_TRANS] CHECK CONSTRAINT [FKir3p9t5adcxt7y11ntp3v240o]
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_CAT_TRANS]  WITH CHECK ADD  CONSTRAINT [FKoygh8eps223tk4jg61xxax5ue] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_CAT_TRANS] CHECK CONSTRAINT [FKoygh8eps223tk4jg61xxax5ue]
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_CAT_TRANS]  WITH CHECK ADD  CONSTRAINT [FKqenkiib0lrkqpmarmfa5y2p01] FOREIGN KEY([genericLookupCategory_id])
REFERENCES [dbo].[JFW_GENERIC_LKUP_CAT] ([id])
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_CAT_TRANS] CHECK CONSTRAINT [FKqenkiib0lrkqpmarmfa5y2p01]
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_TRANS]  WITH CHECK ADD  CONSTRAINT [FK32ylrvh9b9syv7lep07t0f6xh] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_TRANS] CHECK CONSTRAINT [FK32ylrvh9b9syv7lep07t0f6xh]
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_TRANS]  WITH CHECK ADD  CONSTRAINT [FKgf7elmrc3h6o15gg25oig7g2j] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_TRANS] CHECK CONSTRAINT [FKgf7elmrc3h6o15gg25oig7g2j]
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_TRANS]  WITH CHECK ADD  CONSTRAINT [FKpc65w8aepmrxamy1ula5vmth5] FOREIGN KEY([genericLookup_id])
REFERENCES [dbo].[JFW_GENERIC_LOOKUP] ([id])
GO
ALTER TABLE [dbo].[JFW_GENERIC_LKUP_TRANS] CHECK CONSTRAINT [FKpc65w8aepmrxamy1ula5vmth5]
GO
ALTER TABLE [dbo].[JFW_GENERIC_LOOKUP]  WITH CHECK ADD  CONSTRAINT [FK5l17ck84lejo49i6qse75g7h9] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_GENERIC_LOOKUP] CHECK CONSTRAINT [FK5l17ck84lejo49i6qse75g7h9]
GO
ALTER TABLE [dbo].[JFW_GENERIC_LOOKUP]  WITH CHECK ADD  CONSTRAINT [FK8jmtn14tgm54eghxaxkoi4w75] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_GENERIC_LOOKUP] CHECK CONSTRAINT [FK8jmtn14tgm54eghxaxkoi4w75]
GO
ALTER TABLE [dbo].[JFW_GENERIC_LOOKUP]  WITH CHECK ADD  CONSTRAINT [FKin7401immtohxvs73u0hw0c6t] FOREIGN KEY([genericLookupCategory_id])
REFERENCES [dbo].[JFW_GENERIC_LKUP_CAT] ([id])
GO
ALTER TABLE [dbo].[JFW_GENERIC_LOOKUP] CHECK CONSTRAINT [FKin7401immtohxvs73u0hw0c6t]
GO
ALTER TABLE [dbo].[JFW_GROUP_MEMBERS]  WITH CHECK ADD  CONSTRAINT [FKhhfqeu31yctq0ty3ng2goooyl] FOREIGN KEY([GROUP_ID])
REFERENCES [dbo].[JFW_GROUPS] ([id])
GO
ALTER TABLE [dbo].[JFW_GROUP_MEMBERS] CHECK CONSTRAINT [FKhhfqeu31yctq0ty3ng2goooyl]
GO
ALTER TABLE [dbo].[JFW_GROUP_MEMBERS]  WITH CHECK ADD  CONSTRAINT [FKo82xgwace9docadjo8qcn9s07] FOREIGN KEY([user_id])
REFERENCES [dbo].[JFW_USERS] ([id])
GO
ALTER TABLE [dbo].[JFW_GROUP_MEMBERS] CHECK CONSTRAINT [FKo82xgwace9docadjo8qcn9s07]
GO
ALTER TABLE [dbo].[JFW_GROUPS]  WITH CHECK ADD  CONSTRAINT [FK14orpjfrgdfq6ho4c6rvj8ols] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_GROUPS] CHECK CONSTRAINT [FK14orpjfrgdfq6ho4c6rvj8ols]
GO
ALTER TABLE [dbo].[JFW_GROUPS]  WITH CHECK ADD  CONSTRAINT [FKat8jgafjqkykhvjqj6yn2vd4a] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_GROUPS] CHECK CONSTRAINT [FKat8jgafjqkykhvjqj6yn2vd4a]
GO
ALTER TABLE [dbo].[JFW_GROUPS]  WITH CHECK ADD  CONSTRAINT [FKpy9d1desl6ffoki6mh1etpxrm] FOREIGN KEY([rootOrg_ID])
REFERENCES [dbo].[JFW_ORGS] ([ID])
GO
ALTER TABLE [dbo].[JFW_GROUPS] CHECK CONSTRAINT [FKpy9d1desl6ffoki6mh1etpxrm]
GO
ALTER TABLE [dbo].[JFW_GROUPS_BUSINESS_ROLES]  WITH CHECK ADD  CONSTRAINT [FKltjrc6nxf5ipyrbxb7gr1n0gm] FOREIGN KEY([GROUP_ID])
REFERENCES [dbo].[JFW_GROUPS] ([id])
GO
ALTER TABLE [dbo].[JFW_GROUPS_BUSINESS_ROLES] CHECK CONSTRAINT [FKltjrc6nxf5ipyrbxb7gr1n0gm]
GO
ALTER TABLE [dbo].[JFW_GROUPS_BUSINESS_ROLES]  WITH CHECK ADD  CONSTRAINT [FKpdbd6iho8wucsgbytrik23xxd] FOREIGN KEY([BROLE_ID])
REFERENCES [dbo].[JFW_BUSINESS_ROLES] ([ID])
GO
ALTER TABLE [dbo].[JFW_GROUPS_BUSINESS_ROLES] CHECK CONSTRAINT [FKpdbd6iho8wucsgbytrik23xxd]
GO
ALTER TABLE [dbo].[JFW_HOLIDAYS]  WITH CHECK ADD  CONSTRAINT [FK62ee60ytwu72vqktu29yojo3l] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_HOLIDAYS] CHECK CONSTRAINT [FK62ee60ytwu72vqktu29yojo3l]
GO
ALTER TABLE [dbo].[JFW_HOLIDAYS]  WITH CHECK ADD  CONSTRAINT [FKjx6kwqnnojesjcniabwo8ro5t] FOREIGN KEY([CALENDAR_ID])
REFERENCES [dbo].[JFW_CALENDAR] ([ID])
GO
ALTER TABLE [dbo].[JFW_HOLIDAYS] CHECK CONSTRAINT [FKjx6kwqnnojesjcniabwo8ro5t]
GO
ALTER TABLE [dbo].[JFW_HOLIDAYS]  WITH CHECK ADD  CONSTRAINT [FKndbhgu2u2g9cq6co2pvnye9hf] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_HOLIDAYS] CHECK CONSTRAINT [FKndbhgu2u2g9cq6co2pvnye9hf]
GO
ALTER TABLE [dbo].[JFW_LOGIN_TOKEN]  WITH CHECK ADD  CONSTRAINT [FK743woqcy2iapehdprfb571rxi] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_LOGIN_TOKEN] CHECK CONSTRAINT [FK743woqcy2iapehdprfb571rxi]
GO
ALTER TABLE [dbo].[JFW_LOGIN_TOKEN]  WITH CHECK ADD  CONSTRAINT [FK8bpvmhu3fpejodjavhxgyw6v] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_LOGIN_TOKEN] CHECK CONSTRAINT [FK8bpvmhu3fpejodjavhxgyw6v]
GO
ALTER TABLE [dbo].[JFW_MESSAGE_PRM]  WITH CHECK ADD  CONSTRAINT [FK123k881dnbf0ariwklfvpxl5t] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_MESSAGE_PRM] CHECK CONSTRAINT [FK123k881dnbf0ariwklfvpxl5t]
GO
ALTER TABLE [dbo].[JFW_MESSAGE_PRM]  WITH CHECK ADD  CONSTRAINT [FKajr09gtsbl84ts87ol505fpj6] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_MESSAGE_PRM] CHECK CONSTRAINT [FKajr09gtsbl84ts87ol505fpj6]
GO
ALTER TABLE [dbo].[JFW_MESSAGE_PRM]  WITH CHECK ADD  CONSTRAINT [FKfitm87wco1m2ynb0eyrfr15dg] FOREIGN KEY([MESSAGE_ID])
REFERENCES [dbo].[JFW_MESSAGES] ([ID])
GO
ALTER TABLE [dbo].[JFW_MESSAGE_PRM] CHECK CONSTRAINT [FKfitm87wco1m2ynb0eyrfr15dg]
GO
ALTER TABLE [dbo].[JFW_MESSAGES]  WITH CHECK ADD  CONSTRAINT [FKgq4mojh06xujb25vfx0xjwvig] FOREIGN KEY([TO_USER])
REFERENCES [dbo].[JFW_USERS] ([id])
GO
ALTER TABLE [dbo].[JFW_MESSAGES] CHECK CONSTRAINT [FKgq4mojh06xujb25vfx0xjwvig]
GO
ALTER TABLE [dbo].[JFW_MESSAGES]  WITH CHECK ADD  CONSTRAINT [FKh1prqmqyupt73r9wmk7jv8xwp] FOREIGN KEY([FROM_USER])
REFERENCES [dbo].[JFW_USERS] ([id])
GO
ALTER TABLE [dbo].[JFW_MESSAGES] CHECK CONSTRAINT [FKh1prqmqyupt73r9wmk7jv8xwp]
GO
ALTER TABLE [dbo].[JFW_MESSAGES]  WITH CHECK ADD  CONSTRAINT [FKhd68ssniyj3x3mr90o8eq3rg8] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_MESSAGES] CHECK CONSTRAINT [FKhd68ssniyj3x3mr90o8eq3rg8]
GO
ALTER TABLE [dbo].[JFW_MESSAGES]  WITH CHECK ADD  CONSTRAINT [FKlqp3nhxiiltugbdlkkg4xa7ig] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_MESSAGES] CHECK CONSTRAINT [FKlqp3nhxiiltugbdlkkg4xa7ig]
GO
ALTER TABLE [dbo].[JFW_NOTIFICATION_IM]  WITH CHECK ADD  CONSTRAINT [FK6rx5v0bv2o9ty8sh2hiua0prw] FOREIGN KEY([user_id])
REFERENCES [dbo].[JFW_USERS] ([id])
GO
ALTER TABLE [dbo].[JFW_NOTIFICATION_IM] CHECK CONSTRAINT [FK6rx5v0bv2o9ty8sh2hiua0prw]
GO
ALTER TABLE [dbo].[JFW_NOTIFY_RESULT_GROUP]  WITH CHECK ADD  CONSTRAINT [FKnoxh37nk4pcuqh1y4pe3fj6f9] FOREIGN KEY([NOTIF_SCHEME_ID])
REFERENCES [dbo].[JFW_NOTIFY_SCHEMES] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_NOTIFY_RESULT_GROUP] CHECK CONSTRAINT [FKnoxh37nk4pcuqh1y4pe3fj6f9]
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEME_GROUPS]  WITH CHECK ADD  CONSTRAINT [FKdwo5vj77k0sqacmce4crdk01b] FOREIGN KEY([GROUP_ID])
REFERENCES [dbo].[JFW_GROUPS] ([id])
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEME_GROUPS] CHECK CONSTRAINT [FKdwo5vj77k0sqacmce4crdk01b]
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEME_GROUPS]  WITH CHECK ADD  CONSTRAINT [FKeb8tkvwm9tr720rs6icqau8lq] FOREIGN KEY([SCHEME_ID])
REFERENCES [dbo].[JFW_NOTIFY_SCHEMES] ([id])
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEME_GROUPS] CHECK CONSTRAINT [FKeb8tkvwm9tr720rs6icqau8lq]
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEME_ROLES]  WITH CHECK ADD  CONSTRAINT [FK6axdr6xpfq63p3kn6kxwdfdos] FOREIGN KEY([SCHEME_ID])
REFERENCES [dbo].[JFW_NOTIFY_SCHEMES] ([id])
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEME_ROLES] CHECK CONSTRAINT [FK6axdr6xpfq63p3kn6kxwdfdos]
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEME_ROLES]  WITH CHECK ADD  CONSTRAINT [FKap32wlrncvrrb2eux9h7dlk4t] FOREIGN KEY([ROLE_ID])
REFERENCES [dbo].[JFW_BUSINESS_ROLES] ([ID])
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEME_ROLES] CHECK CONSTRAINT [FKap32wlrncvrrb2eux9h7dlk4t]
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEMES]  WITH CHECK ADD  CONSTRAINT [FK9rtfeaaqy4udetg92w78jsngw] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEMES] CHECK CONSTRAINT [FK9rtfeaaqy4udetg92w78jsngw]
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEMES]  WITH CHECK ADD  CONSTRAINT [FKm008ta2tf7ivs4a27opdtkshk] FOREIGN KEY([ORG_ID])
REFERENCES [dbo].[JFW_ORGS] ([ID])
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEMES] CHECK CONSTRAINT [FKm008ta2tf7ivs4a27opdtkshk]
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEMES]  WITH CHECK ADD  CONSTRAINT [FKpmpcjaktvwco3uit7jg8htmon] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEMES] CHECK CONSTRAINT [FKpmpcjaktvwco3uit7jg8htmon]
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEMES]  WITH CHECK ADD  CONSTRAINT [FKrrq7lofhkakp5q6o2a7lcqyh1] FOREIGN KEY([EMAIL_PROVIDER_ID])
REFERENCES [dbo].[JFW_EMAIL_PROVIDER_CONFIG] ([ID])
GO
ALTER TABLE [dbo].[JFW_NOTIFY_SCHEMES] CHECK CONSTRAINT [FKrrq7lofhkakp5q6o2a7lcqyh1]
GO
ALTER TABLE [dbo].[JFW_NOTIFY_WF_RESULT]  WITH CHECK ADD  CONSTRAINT [FK1cqavw383jxsb04wkndqvaym1] FOREIGN KEY([resultGroup_id])
REFERENCES [dbo].[JFW_NOTIFY_RESULT_GROUP] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_NOTIFY_WF_RESULT] CHECK CONSTRAINT [FK1cqavw383jxsb04wkndqvaym1]
GO
ALTER TABLE [dbo].[JFW_NOTIFY_WF_RESULT]  WITH CHECK ADD  CONSTRAINT [FKctbee31bgjgjf3l63206os7vg] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_NOTIFY_WF_RESULT] CHECK CONSTRAINT [FKctbee31bgjgjf3l63206os7vg]
GO
ALTER TABLE [dbo].[JFW_NOTIFY_WF_RESULT]  WITH CHECK ADD  CONSTRAINT [FKi5sqrnu8mhy3kvjt7lamntkas] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_NOTIFY_WF_RESULT] CHECK CONSTRAINT [FKi5sqrnu8mhy3kvjt7lamntkas]
GO
ALTER TABLE [dbo].[JFW_OLD_PASSWORDS]  WITH CHECK ADD  CONSTRAINT [FKi8utqp6mfjfk5v2mgd6f6lrq0] FOREIGN KEY([user_id])
REFERENCES [dbo].[JFW_USERS] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_OLD_PASSWORDS] CHECK CONSTRAINT [FKi8utqp6mfjfk5v2mgd6f6lrq0]
GO
ALTER TABLE [dbo].[JFW_OLD_PASSWORDS]  WITH CHECK ADD  CONSTRAINT [FKlr3l9agj8wuaptx7hw9ok4nef] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_OLD_PASSWORDS] CHECK CONSTRAINT [FKlr3l9agj8wuaptx7hw9ok4nef]
GO
ALTER TABLE [dbo].[JFW_OLD_PASSWORDS]  WITH CHECK ADD  CONSTRAINT [FKoy9mtv5pl22g34h4rh2s6ncd8] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_OLD_PASSWORDS] CHECK CONSTRAINT [FKoy9mtv5pl22g34h4rh2s6ncd8]
GO
ALTER TABLE [dbo].[JFW_ORG_COMM_CHANNELS]  WITH CHECK ADD  CONSTRAINT [FKown2sx4u7hhij7viu0gfv2fkv] FOREIGN KEY([ORG_ID])
REFERENCES [dbo].[JFW_ORGS] ([ID])
GO
ALTER TABLE [dbo].[JFW_ORG_COMM_CHANNELS] CHECK CONSTRAINT [FKown2sx4u7hhij7viu0gfv2fkv]
GO
ALTER TABLE [dbo].[JFW_ORG_PREFERENCES]  WITH CHECK ADD  CONSTRAINT [FKfq7hbsef2mqui71kv75s0c9tf] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_ORG_PREFERENCES] CHECK CONSTRAINT [FKfq7hbsef2mqui71kv75s0c9tf]
GO
ALTER TABLE [dbo].[JFW_ORG_PREFERENCES]  WITH CHECK ADD  CONSTRAINT [FKhg8r83xbfa53p6u5dr95ytr4v] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_ORG_PREFERENCES] CHECK CONSTRAINT [FKhg8r83xbfa53p6u5dr95ytr4v]
GO
ALTER TABLE [dbo].[JFW_ORG_PREFERENCES]  WITH CHECK ADD  CONSTRAINT [FKpotikr6jl8lvnd6hvarrw4t6f] FOREIGN KEY([ASSIGNED_ORG_ID])
REFERENCES [dbo].[JFW_ORGS] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_ORG_PREFERENCES] CHECK CONSTRAINT [FKpotikr6jl8lvnd6hvarrw4t6f]
GO
ALTER TABLE [dbo].[JFW_ORG_TYPES]  WITH CHECK ADD  CONSTRAINT [FK95wc882v3hw7il99tirvti6g9] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_ORG_TYPES] CHECK CONSTRAINT [FK95wc882v3hw7il99tirvti6g9]
GO
ALTER TABLE [dbo].[JFW_ORG_TYPES]  WITH CHECK ADD  CONSTRAINT [FKm0fqxhy6e0gmbt6hy5j0wru3] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_ORG_TYPES] CHECK CONSTRAINT [FKm0fqxhy6e0gmbt6hy5j0wru3]
GO
ALTER TABLE [dbo].[JFW_ORG_WORKFLOW]  WITH CHECK ADD  CONSTRAINT [FK2c3je90dnau6soycebso8pi20] FOREIGN KEY([ORG_WRKFLW_SCHME_ID])
REFERENCES [dbo].[JFW_ORG_WORKFLOW_SCHM] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_ORG_WORKFLOW] CHECK CONSTRAINT [FK2c3je90dnau6soycebso8pi20]
GO
ALTER TABLE [dbo].[JFW_ORGS]  WITH CHECK ADD  CONSTRAINT [FK14e4oa28fsug09m8alhhilq1c] FOREIGN KEY([TERM_CONDITION])
REFERENCES [dbo].[JFW_TERMS_CONDITIONS] ([ID])
GO
ALTER TABLE [dbo].[JFW_ORGS] CHECK CONSTRAINT [FK14e4oa28fsug09m8alhhilq1c]
GO
ALTER TABLE [dbo].[JFW_ORGS]  WITH CHECK ADD  CONSTRAINT [FK47p202wdhsgkrfgvfmqguvnhe] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_ORGS] CHECK CONSTRAINT [FK47p202wdhsgkrfgvfmqguvnhe]
GO
ALTER TABLE [dbo].[JFW_ORGS]  WITH CHECK ADD  CONSTRAINT [FK5cwm77ud4ctrxo15wtp2ly9hs] FOREIGN KEY([PARENT_ID])
REFERENCES [dbo].[JFW_ORGS] ([ID])
GO
ALTER TABLE [dbo].[JFW_ORGS] CHECK CONSTRAINT [FK5cwm77ud4ctrxo15wtp2ly9hs]
GO
ALTER TABLE [dbo].[JFW_ORGS]  WITH CHECK ADD  CONSTRAINT [FK8606yqdi0nllnapsm9lb89x6m] FOREIGN KEY([ORG_TYPE_CD])
REFERENCES [dbo].[JFW_ORG_TYPES] ([ID])
GO
ALTER TABLE [dbo].[JFW_ORGS] CHECK CONSTRAINT [FK8606yqdi0nllnapsm9lb89x6m]
GO
ALTER TABLE [dbo].[JFW_ORGS]  WITH CHECK ADD  CONSTRAINT [FKgganegilwr1kctj7077bh8ii] FOREIGN KEY([VIEWS_SCHME])
REFERENCES [dbo].[JFW_VIEWS_SCHM] ([ID])
GO
ALTER TABLE [dbo].[JFW_ORGS] CHECK CONSTRAINT [FKgganegilwr1kctj7077bh8ii]
GO
ALTER TABLE [dbo].[JFW_ORGS]  WITH CHECK ADD  CONSTRAINT [FKq2i8jh6ow4f38dbe5eag2edcm] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_ORGS] CHECK CONSTRAINT [FKq2i8jh6ow4f38dbe5eag2edcm]
GO
ALTER TABLE [dbo].[JFW_OS_CURRENTSTEP]  WITH CHECK ADD  CONSTRAINT [FKla9l0wgqlnhqimi5tnqnxsrv7] FOREIGN KEY([entry_id])
REFERENCES [dbo].[JFW_OS_WFENTRY] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_OS_CURRENTSTEP] CHECK CONSTRAINT [FKla9l0wgqlnhqimi5tnqnxsrv7]
GO
ALTER TABLE [dbo].[JFW_OS_HISTORYSTEP]  WITH CHECK ADD  CONSTRAINT [FKjjqyiv4rq3pp161qohn1wi9bc] FOREIGN KEY([entry_id])
REFERENCES [dbo].[JFW_OS_WFENTRY] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_OS_HISTORYSTEP] CHECK CONSTRAINT [FKjjqyiv4rq3pp161qohn1wi9bc]
GO
ALTER TABLE [dbo].[JFW_OTP_SCHEME]  WITH CHECK ADD  CONSTRAINT [FKq3vfjcaw6bhrd6n5h1j479er1] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_OTP_SCHEME] CHECK CONSTRAINT [FKq3vfjcaw6bhrd6n5h1j479er1]
GO
ALTER TABLE [dbo].[JFW_OTP_SCHEME]  WITH CHECK ADD  CONSTRAINT [FKqvquj43128truyj3r8sn8lmbj] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_OTP_SCHEME] CHECK CONSTRAINT [FKqvquj43128truyj3r8sn8lmbj]
GO
ALTER TABLE [dbo].[JFW_RPTS_AUTHORITIES]  WITH CHECK ADD  CONSTRAINT [FKfocd8omtlt4262f8gieaqdp2t] FOREIGN KEY([userType_ID])
REFERENCES [dbo].[JFW_USER_TYPES] ([ID])
GO
ALTER TABLE [dbo].[JFW_RPTS_AUTHORITIES] CHECK CONSTRAINT [FKfocd8omtlt4262f8gieaqdp2t]
GO
ALTER TABLE [dbo].[JFW_RPTS_AUTHORITIES]  WITH CHECK ADD  CONSTRAINT [FKphuf6pe8pp9ntdfblh2y5t7mx] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_RPTS_AUTHORITIES] CHECK CONSTRAINT [FKphuf6pe8pp9ntdfblh2y5t7mx]
GO
ALTER TABLE [dbo].[JFW_RPTS_AUTHORITIES]  WITH CHECK ADD  CONSTRAINT [FKqfmny8xxpqneoymf4mtmwm741] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_RPTS_AUTHORITIES] CHECK CONSTRAINT [FKqfmny8xxpqneoymf4mtmwm741]
GO
ALTER TABLE [dbo].[JFW_SCHM_VIEWS]  WITH CHECK ADD  CONSTRAINT [FK9mi3xe8uq18ri7lg1fumd8yco] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_SCHM_VIEWS] CHECK CONSTRAINT [FK9mi3xe8uq18ri7lg1fumd8yco]
GO
ALTER TABLE [dbo].[JFW_SCHM_VIEWS]  WITH CHECK ADD  CONSTRAINT [FKdsxgvw9gm3mlysy2iyqajd2tf] FOREIGN KEY([VIEWS_SCHM_ID])
REFERENCES [dbo].[JFW_VIEWS_SCHM] ([ID])
GO
ALTER TABLE [dbo].[JFW_SCHM_VIEWS] CHECK CONSTRAINT [FKdsxgvw9gm3mlysy2iyqajd2tf]
GO
ALTER TABLE [dbo].[JFW_SCHM_VIEWS]  WITH CHECK ADD  CONSTRAINT [FKsi2g4215dm0epjyp80dmalbgp] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_SCHM_VIEWS] CHECK CONSTRAINT [FKsi2g4215dm0epjyp80dmalbgp]
GO
ALTER TABLE [dbo].[JFW_SECRET_QUESTION_POLICY]  WITH CHECK ADD  CONSTRAINT [FK3ir9yvuefk7ku3w4g7yhr4vsw] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_SECRET_QUESTION_POLICY] CHECK CONSTRAINT [FK3ir9yvuefk7ku3w4g7yhr4vsw]
GO
ALTER TABLE [dbo].[JFW_SECRET_QUESTION_POLICY]  WITH CHECK ADD  CONSTRAINT [FKeh5hsrs478n97ogi4u7wmjw42] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_SECRET_QUESTION_POLICY] CHECK CONSTRAINT [FKeh5hsrs478n97ogi4u7wmjw42]
GO
ALTER TABLE [dbo].[JFW_SECRET_QUESTIONS]  WITH CHECK ADD  CONSTRAINT [FKg14leyydrrlc9s44y9qsnqxli] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_SECRET_QUESTIONS] CHECK CONSTRAINT [FKg14leyydrrlc9s44y9qsnqxli]
GO
ALTER TABLE [dbo].[JFW_SECRET_QUESTIONS]  WITH CHECK ADD  CONSTRAINT [FKqbjh00ymvhcwml0vfxy3ihg0t] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_SECRET_QUESTIONS] CHECK CONSTRAINT [FKqbjh00ymvhcwml0vfxy3ihg0t]
GO
ALTER TABLE [dbo].[JFW_SECURITY_ADT_CHNG_ITEM]  WITH CHECK ADD  CONSTRAINT [FKda7mhwcu97yqj3lt962nhujxi] FOREIGN KEY([groupid])
REFERENCES [dbo].[JFW_SECURITY_ADT_CHNG_GRP] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_SECURITY_ADT_CHNG_ITEM] CHECK CONSTRAINT [FKda7mhwcu97yqj3lt962nhujxi]
GO
ALTER TABLE [dbo].[JFW_SECURITY_ALERTS_USER]  WITH CHECK ADD  CONSTRAINT [FK4vkkcgygdhke4dln0b20lscxu] FOREIGN KEY([SCHEME_ID])
REFERENCES [dbo].[JFW_SECURITY_ALERTS] ([id])
GO
ALTER TABLE [dbo].[JFW_SECURITY_ALERTS_USER] CHECK CONSTRAINT [FK4vkkcgygdhke4dln0b20lscxu]
GO
ALTER TABLE [dbo].[JFW_SECURITY_ALERTS_USER]  WITH CHECK ADD  CONSTRAINT [FKhdoxt19yb60jil3ei3nec9h1m] FOREIGN KEY([USER_ID])
REFERENCES [dbo].[JFW_USERS] ([id])
GO
ALTER TABLE [dbo].[JFW_SECURITY_ALERTS_USER] CHECK CONSTRAINT [FKhdoxt19yb60jil3ei3nec9h1m]
GO
ALTER TABLE [dbo].[JFW_SECURITY_CHANGE_ITEM]  WITH CHECK ADD  CONSTRAINT [FK4tdo96wrp9v0s0mlg1i08kt3u] FOREIGN KEY([groupid])
REFERENCES [dbo].[JFW_SECURITY_CHANGE_GROUP] ([id])
GO
ALTER TABLE [dbo].[JFW_SECURITY_CHANGE_ITEM] CHECK CONSTRAINT [FK4tdo96wrp9v0s0mlg1i08kt3u]
GO
ALTER TABLE [dbo].[JFW_SESSION_INFORMATION]  WITH CHECK ADD  CONSTRAINT [FK5cs292mefwntatg0yrkcb8hqn] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_SESSION_INFORMATION] CHECK CONSTRAINT [FK5cs292mefwntatg0yrkcb8hqn]
GO
ALTER TABLE [dbo].[JFW_SESSION_INFORMATION]  WITH CHECK ADD  CONSTRAINT [FKhy98r3cqqv97fljkmb06bdyvc] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_SESSION_INFORMATION] CHECK CONSTRAINT [FKhy98r3cqqv97fljkmb06bdyvc]
GO
ALTER TABLE [dbo].[JFW_SUCC_USR_LGN_LOGS]  WITH CHECK ADD  CONSTRAINT [FKdt0jn3r2kihdg0o6ybptcrqy8] FOREIGN KEY([USER_ID])
REFERENCES [dbo].[JFW_USERS] ([id])
GO
ALTER TABLE [dbo].[JFW_SUCC_USR_LGN_LOGS] CHECK CONSTRAINT [FKdt0jn3r2kihdg0o6ybptcrqy8]
GO
ALTER TABLE [dbo].[JFW_SUCC_USR_LGN_LOGS]  WITH CHECK ADD  CONSTRAINT [FKo965jwwnm6n2gyh10cbnvo7tm] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_SUCC_USR_LGN_LOGS] CHECK CONSTRAINT [FKo965jwwnm6n2gyh10cbnvo7tm]
GO
ALTER TABLE [dbo].[JFW_SUCC_USR_LGN_LOGS]  WITH CHECK ADD  CONSTRAINT [FKohc90xksosagevrd5vy07jpow] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_SUCC_USR_LGN_LOGS] CHECK CONSTRAINT [FKohc90xksosagevrd5vy07jpow]
GO
ALTER TABLE [dbo].[JFW_SYS_NOTIFICATIONS]  WITH CHECK ADD  CONSTRAINT [FK2mhq43gfkf5bxiqs6thgfbm2a] FOREIGN KEY([EXECUTED_BY_ID])
REFERENCES [dbo].[JFW_USERS] ([id])
GO
ALTER TABLE [dbo].[JFW_SYS_NOTIFICATIONS] CHECK CONSTRAINT [FK2mhq43gfkf5bxiqs6thgfbm2a]
GO
ALTER TABLE [dbo].[JFW_SYS_NOTIFICATIONS]  WITH CHECK ADD  CONSTRAINT [FK9k68e4pcfn9ig3uk6rep6uoxu] FOREIGN KEY([SCHEME_ID])
REFERENCES [dbo].[JFW_NOTIFY_SCHEMES] ([id])
GO
ALTER TABLE [dbo].[JFW_SYS_NOTIFICATIONS] CHECK CONSTRAINT [FK9k68e4pcfn9ig3uk6rep6uoxu]
GO
ALTER TABLE [dbo].[JFW_SYS_NOTIFICATIONS]  WITH CHECK ADD  CONSTRAINT [FKa5vfhnxvm934fbf88qqjt5qid] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_SYS_NOTIFICATIONS] CHECK CONSTRAINT [FKa5vfhnxvm934fbf88qqjt5qid]
GO
ALTER TABLE [dbo].[JFW_SYS_NOTIFICATIONS]  WITH CHECK ADD  CONSTRAINT [FKdjg2i85t4kdbx0lf43rp6nsg] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_SYS_NOTIFICATIONS] CHECK CONSTRAINT [FKdjg2i85t4kdbx0lf43rp6nsg]
GO
ALTER TABLE [dbo].[JFW_TENANT]  WITH CHECK ADD  CONSTRAINT [FK8jnb8arq35ny4ynfae7iob1cm] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_TENANT] CHECK CONSTRAINT [FK8jnb8arq35ny4ynfae7iob1cm]
GO
ALTER TABLE [dbo].[JFW_TENANT]  WITH CHECK ADD  CONSTRAINT [FKfp98itds25pr72ynm2tx385vh] FOREIGN KEY([FAILED_LOGIN_SCHEME])
REFERENCES [dbo].[JFW_SECURITY_ALERTS] ([id])
GO
ALTER TABLE [dbo].[JFW_TENANT] CHECK CONSTRAINT [FKfp98itds25pr72ynm2tx385vh]
GO
ALTER TABLE [dbo].[JFW_TENANT]  WITH CHECK ADD  CONSTRAINT [FKliipgjakbfpymgbpmo2lp1lyu] FOREIGN KEY([VIEWS_SCHME])
REFERENCES [dbo].[JFW_VIEWS_SCHM] ([ID])
GO
ALTER TABLE [dbo].[JFW_TENANT] CHECK CONSTRAINT [FKliipgjakbfpymgbpmo2lp1lyu]
GO
ALTER TABLE [dbo].[JFW_TENANT]  WITH CHECK ADD  CONSTRAINT [FKluxyu8dtwcsxqxy0w8il0re44] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_TENANT] CHECK CONSTRAINT [FKluxyu8dtwcsxqxy0w8il0re44]
GO
ALTER TABLE [dbo].[JFW_TENANT]  WITH CHECK ADD  CONSTRAINT [FKt4wuoh6q4w9blmligc9ssln8u] FOREIGN KEY([SUPER_ORG_VIEWS_SCHME])
REFERENCES [dbo].[JFW_VIEWS_SCHM] ([ID])
GO
ALTER TABLE [dbo].[JFW_TENANT] CHECK CONSTRAINT [FKt4wuoh6q4w9blmligc9ssln8u]
GO
ALTER TABLE [dbo].[JFW_TERMS_CONDITIONS]  WITH CHECK ADD  CONSTRAINT [FK3tpwiukkittl9os359c1ufqhw] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_TERMS_CONDITIONS] CHECK CONSTRAINT [FK3tpwiukkittl9os359c1ufqhw]
GO
ALTER TABLE [dbo].[JFW_TERMS_CONDITIONS]  WITH CHECK ADD  CONSTRAINT [FKpqthii1gxnw0qxjx8ctda158f] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_TERMS_CONDITIONS] CHECK CONSTRAINT [FKpqthii1gxnw0qxjx8ctda158f]
GO
ALTER TABLE [dbo].[JFW_USER_NOTIFICATIONS]  WITH CHECK ADD  CONSTRAINT [FK6gqj7upmyuaqwwu8d435ycy4h] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_USER_NOTIFICATIONS] CHECK CONSTRAINT [FK6gqj7upmyuaqwwu8d435ycy4h]
GO
ALTER TABLE [dbo].[JFW_USER_NOTIFICATIONS]  WITH CHECK ADD  CONSTRAINT [FK9dgsjsp6d8c4gv3ngfkabg0sq] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_USER_NOTIFICATIONS] CHECK CONSTRAINT [FK9dgsjsp6d8c4gv3ngfkabg0sq]
GO
ALTER TABLE [dbo].[JFW_USER_NOTIFICATIONS]  WITH CHECK ADD  CONSTRAINT [FKp3ookpb247r0qiesyp012m9pv] FOREIGN KEY([SYS_NOTIF_ID])
REFERENCES [dbo].[JFW_SYS_NOTIFICATIONS] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[JFW_USER_NOTIFICATIONS] CHECK CONSTRAINT [FKp3ookpb247r0qiesyp012m9pv]
GO
ALTER TABLE [dbo].[JFW_USER_PREFS]  WITH CHECK ADD  CONSTRAINT [FK2w3wcknr9bdj8mj0e8rfobvex] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_USER_PREFS] CHECK CONSTRAINT [FK2w3wcknr9bdj8mj0e8rfobvex]
GO
ALTER TABLE [dbo].[JFW_USER_PREFS]  WITH CHECK ADD  CONSTRAINT [FK5w7xxgeyfeof1uvbr6x3g16p4] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_USER_PREFS] CHECK CONSTRAINT [FK5w7xxgeyfeof1uvbr6x3g16p4]
GO
ALTER TABLE [dbo].[JFW_USER_PROFILE]  WITH CHECK ADD  CONSTRAINT [FK91bqu00bxjfsg5op9v5qrctl9] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_USER_PROFILE] CHECK CONSTRAINT [FK91bqu00bxjfsg5op9v5qrctl9]
GO
ALTER TABLE [dbo].[JFW_USER_PROFILE]  WITH CHECK ADD  CONSTRAINT [FKm1kca6oqm26gyp91nmjm17aly] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_USER_PROFILE] CHECK CONSTRAINT [FKm1kca6oqm26gyp91nmjm17aly]
GO
ALTER TABLE [dbo].[JFW_USER_SECRET_ANSWERS]  WITH CHECK ADD  CONSTRAINT [FK2j5u4pg6h2jxko4q348sv25xl] FOREIGN KEY([QUESTION_ID])
REFERENCES [dbo].[JFW_SECRET_QUESTIONS] ([QUESTION_ID])
GO
ALTER TABLE [dbo].[JFW_USER_SECRET_ANSWERS] CHECK CONSTRAINT [FK2j5u4pg6h2jxko4q348sv25xl]
GO
ALTER TABLE [dbo].[JFW_USER_SECRET_ANSWERS]  WITH CHECK ADD  CONSTRAINT [FKa5g86mnhnqnvpc4s66r7uaxb9] FOREIGN KEY([USER_ID])
REFERENCES [dbo].[JFW_USERS] ([id])
GO
ALTER TABLE [dbo].[JFW_USER_SECRET_ANSWERS] CHECK CONSTRAINT [FKa5g86mnhnqnvpc4s66r7uaxb9]
GO
ALTER TABLE [dbo].[JFW_USER_SECRET_ANSWERS]  WITH CHECK ADD  CONSTRAINT [FKl5kd4jyaacqyx4wgl9i12w05c] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_USER_SECRET_ANSWERS] CHECK CONSTRAINT [FKl5kd4jyaacqyx4wgl9i12w05c]
GO
ALTER TABLE [dbo].[JFW_USER_SECRET_ANSWERS]  WITH CHECK ADD  CONSTRAINT [FKxonolxtkuc2speq2nbqni56c] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_USER_SECRET_ANSWERS] CHECK CONSTRAINT [FKxonolxtkuc2speq2nbqni56c]
GO
ALTER TABLE [dbo].[JFW_USER_STATE_PREF]  WITH CHECK ADD  CONSTRAINT [FKhyjxx5045d1k36smjikejpp83] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_USER_STATE_PREF] CHECK CONSTRAINT [FKhyjxx5045d1k36smjikejpp83]
GO
ALTER TABLE [dbo].[JFW_USER_STATE_PREF]  WITH CHECK ADD  CONSTRAINT [FKqw858060oh04fat32broiptp] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_USER_STATE_PREF] CHECK CONSTRAINT [FKqw858060oh04fat32broiptp]
GO
ALTER TABLE [dbo].[JFW_USER_TYPES]  WITH CHECK ADD  CONSTRAINT [FK72scqkto48adaoj0kj5qc4e17] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_USER_TYPES] CHECK CONSTRAINT [FK72scqkto48adaoj0kj5qc4e17]
GO
ALTER TABLE [dbo].[JFW_USER_TYPES]  WITH CHECK ADD  CONSTRAINT [FKc3isc1lvoevdhbmpcb1nnliyp] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_USER_TYPES] CHECK CONSTRAINT [FKc3isc1lvoevdhbmpcb1nnliyp]
GO
ALTER TABLE [dbo].[JFW_USERS]  WITH CHECK ADD  CONSTRAINT [FK6ogusc63b4drrm38vbswbsvrf] FOREIGN KEY([USER_TYPE_CD])
REFERENCES [dbo].[JFW_USER_TYPES] ([ID])
GO
ALTER TABLE [dbo].[JFW_USERS] CHECK CONSTRAINT [FK6ogusc63b4drrm38vbswbsvrf]
GO
ALTER TABLE [dbo].[JFW_USERS]  WITH CHECK ADD  CONSTRAINT [FKdnm7avhj9okmeubvnwtmo8fdw] FOREIGN KEY([USER_PROFILE_ID])
REFERENCES [dbo].[JFW_USER_PROFILE] ([id])
GO
ALTER TABLE [dbo].[JFW_USERS] CHECK CONSTRAINT [FKdnm7avhj9okmeubvnwtmo8fdw]
GO
ALTER TABLE [dbo].[JFW_USERS]  WITH CHECK ADD  CONSTRAINT [FKhgfhcjwarg96cu9nk4m4nfk6u] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_USERS] CHECK CONSTRAINT [FKhgfhcjwarg96cu9nk4m4nfk6u]
GO
ALTER TABLE [dbo].[JFW_USERS]  WITH CHECK ADD  CONSTRAINT [FKil4l895tmwrvw6ncii6bv5f6p] FOREIGN KEY([accountPolicy_id])
REFERENCES [dbo].[JFW_ACCOUNT_POLICIES] ([id])
GO
ALTER TABLE [dbo].[JFW_USERS] CHECK CONSTRAINT [FKil4l895tmwrvw6ncii6bv5f6p]
GO
ALTER TABLE [dbo].[JFW_USERS]  WITH CHECK ADD  CONSTRAINT [FKk68lr5cqf7d93b04oyffu69wy] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_USERS] CHECK CONSTRAINT [FKk68lr5cqf7d93b04oyffu69wy]
GO
ALTER TABLE [dbo].[JFW_USERS]  WITH CHECK ADD  CONSTRAINT [FKt9tyxw78chulilw6xttkmjavr] FOREIGN KEY([rootOrg_ID])
REFERENCES [dbo].[JFW_ORGS] ([ID])
GO
ALTER TABLE [dbo].[JFW_USERS] CHECK CONSTRAINT [FKt9tyxw78chulilw6xttkmjavr]
GO
ALTER TABLE [dbo].[JFW_USR_CUST_AUTH]  WITH CHECK ADD  CONSTRAINT [FK8l2ih73il19pr64w22ubbo9yo] FOREIGN KEY([AUTHORITY_NAME])
REFERENCES [dbo].[JFW_CUSTOM_AUTHORITIES] ([AUTHORITY_NAME])
GO
ALTER TABLE [dbo].[JFW_USR_CUST_AUTH] CHECK CONSTRAINT [FK8l2ih73il19pr64w22ubbo9yo]
GO
ALTER TABLE [dbo].[JFW_USR_CUST_AUTH]  WITH CHECK ADD  CONSTRAINT [FKo535t8t8cu0cv3jlm8mejkceh] FOREIGN KEY([user_id])
REFERENCES [dbo].[JFW_USERS] ([id])
GO
ALTER TABLE [dbo].[JFW_USR_CUST_AUTH] CHECK CONSTRAINT [FKo535t8t8cu0cv3jlm8mejkceh]
GO
ALTER TABLE [dbo].[JFW_WEEK_DAYS]  WITH CHECK ADD  CONSTRAINT [FKf2aalw1vqupjirowq5hpbvt3x] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_WEEK_DAYS] CHECK CONSTRAINT [FKf2aalw1vqupjirowq5hpbvt3x]
GO
ALTER TABLE [dbo].[JFW_WEEK_DAYS]  WITH CHECK ADD  CONSTRAINT [FKkavyrx1cahivoh2nsd8op5m51] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_WEEK_DAYS] CHECK CONSTRAINT [FKkavyrx1cahivoh2nsd8op5m51]
GO
ALTER TABLE [dbo].[JFW_WEEK_DAYS]  WITH CHECK ADD  CONSTRAINT [FKro82dp6ju0bt9raaxwq5hyci0] FOREIGN KEY([CALENDAR_ID])
REFERENCES [dbo].[JFW_CALENDAR] ([ID])
GO
ALTER TABLE [dbo].[JFW_WEEK_DAYS] CHECK CONSTRAINT [FKro82dp6ju0bt9raaxwq5hyci0]
GO
ALTER TABLE [dbo].[JFW_WORKING_HOURS]  WITH CHECK ADD  CONSTRAINT [FKd7dl13erg2itrfa1htagt6gri] FOREIGN KEY([Z_STATUS_ID])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[JFW_WORKING_HOURS] CHECK CONSTRAINT [FKd7dl13erg2itrfa1htagt6gri]
GO
ALTER TABLE [dbo].[JFW_WORKING_HOURS]  WITH CHECK ADD  CONSTRAINT [FKfy4pqw373uod0uhec0ei4qtw3] FOREIGN KEY([Z_DRAFT_ID])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[JFW_WORKING_HOURS] CHECK CONSTRAINT [FKfy4pqw373uod0uhec0ei4qtw3]
GO
ALTER TABLE [dbo].[mpay_accountitm]  WITH CHECK ADD  CONSTRAINT [fk_h8g06fmd774nay9p6nl7pt03c] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_accountgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_accountitm] CHECK CONSTRAINT [fk_h8g06fmd774nay9p6nl7pt03c]
GO
ALTER TABLE [dbo].[mpay_accounts]  WITH CHECK ADD  CONSTRAINT [fk_1xtl9ln7ejmm58s8jwyeft3wq] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_accounts] CHECK CONSTRAINT [fk_1xtl9ln7ejmm58s8jwyeft3wq]
GO
ALTER TABLE [dbo].[mpay_accounts]  WITH CHECK ADD  CONSTRAINT [fk_3y09vxrmv27bcusv3dfp20fi1] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_accounts] CHECK CONSTRAINT [fk_3y09vxrmv27bcusv3dfp20fi1]
GO
ALTER TABLE [dbo].[mpay_accounts]  WITH CHECK ADD  CONSTRAINT [fk_d3ely0mliik6y53t1qrexuhqu] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_accounts] CHECK CONSTRAINT [fk_d3ely0mliik6y53t1qrexuhqu]
GO
ALTER TABLE [dbo].[mpay_accounts]  WITH CHECK ADD  CONSTRAINT [fk_du2ld416vtwbii5pmu15ad0ij] FOREIGN KEY([accparentid])
REFERENCES [dbo].[mpay_accounts] ([id])
GO
ALTER TABLE [dbo].[mpay_accounts] CHECK CONSTRAINT [fk_du2ld416vtwbii5pmu15ad0ij]
GO
ALTER TABLE [dbo].[mpay_accounts]  WITH CHECK ADD  CONSTRAINT [fk_lcdeed0jk063wd5dlad3i4sdj] FOREIGN KEY([balancetypeid])
REFERENCES [dbo].[mpay_balancetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_accounts] CHECK CONSTRAINT [fk_lcdeed0jk063wd5dlad3i4sdj]
GO
ALTER TABLE [dbo].[mpay_accounts]  WITH CHECK ADD  CONSTRAINT [fk_pb8f5dv5b8numt24ssawndigp] FOREIGN KEY([accounttypeid])
REFERENCES [dbo].[mpay_accounttypes] ([id])
GO
ALTER TABLE [dbo].[mpay_accounts] CHECK CONSTRAINT [fk_pb8f5dv5b8numt24ssawndigp]
GO
ALTER TABLE [dbo].[mpay_accounttypeitm]  WITH CHECK ADD  CONSTRAINT [fk_979j124pw3qbp4ibxprwfq7aw] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_accounttypegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_accounttypeitm] CHECK CONSTRAINT [fk_979j124pw3qbp4ibxprwfq7aw]
GO
ALTER TABLE [dbo].[mpay_accounttypes]  WITH CHECK ADD  CONSTRAINT [fk_bq6jt8djnia12prksspge0jce] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_accounttypes] CHECK CONSTRAINT [fk_bq6jt8djnia12prksspge0jce]
GO
ALTER TABLE [dbo].[mpay_accounttypes]  WITH CHECK ADD  CONSTRAINT [fk_os1s9l94q7kagqaohxx7kcf6p] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_accounttypes] CHECK CONSTRAINT [fk_os1s9l94q7kagqaohxx7kcf6p]
GO
ALTER TABLE [dbo].[mpay_accounttypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_2kkx4aru0t122j1x4n3wvypsd] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_accounttypes_nls] CHECK CONSTRAINT [fk_2kkx4aru0t122j1x4n3wvypsd]
GO
ALTER TABLE [dbo].[mpay_accounttypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_cg8ya6tde23am4l6ek1r4t3f9] FOREIGN KEY([accid])
REFERENCES [dbo].[mpay_accounttypes] ([id])
GO
ALTER TABLE [dbo].[mpay_accounttypes_nls] CHECK CONSTRAINT [fk_cg8ya6tde23am4l6ek1r4t3f9]
GO
ALTER TABLE [dbo].[mpay_accounttypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_jryrnpxv3rpthvbu3dkcvmyhf] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_accounttypes_nls] CHECK CONSTRAINT [fk_jryrnpxv3rpthvbu3dkcvmyhf]
GO
ALTER TABLE [dbo].[mpay_amlcaseitm]  WITH CHECK ADD  CONSTRAINT [fk_kvy4ionrvbgailnbd9uceu43d] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_amlcasegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_amlcaseitm] CHECK CONSTRAINT [fk_kvy4ionrvbgailnbd9uceu43d]
GO
ALTER TABLE [dbo].[mpay_amlcases]  WITH CHECK ADD  CONSTRAINT [fk_bphcaixho3ign8xxnlkokp0uk] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_amlcases] CHECK CONSTRAINT [fk_bphcaixho3ign8xxnlkokp0uk]
GO
ALTER TABLE [dbo].[mpay_amlcases]  WITH CHECK ADD  CONSTRAINT [fk_oh2keu5hb9sardv1va8evb8v8] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_amlcases] CHECK CONSTRAINT [fk_oh2keu5hb9sardv1va8evb8v8]
GO
ALTER TABLE [dbo].[mpay_amlcases]  WITH CHECK ADD  CONSTRAINT [fk_rt6qc85kcdjcu2o38e8cn5u19] FOREIGN KEY([refcustomerid])
REFERENCES [dbo].[mpay_customers] ([id])
GO
ALTER TABLE [dbo].[mpay_amlcases] CHECK CONSTRAINT [fk_rt6qc85kcdjcu2o38e8cn5u19]
GO
ALTER TABLE [dbo].[mpay_amlhititm]  WITH CHECK ADD  CONSTRAINT [fk_bap032bio94njm7x3yrtndda1] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_amlhitgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_amlhititm] CHECK CONSTRAINT [fk_bap032bio94njm7x3yrtndda1]
GO
ALTER TABLE [dbo].[mpay_amlhits]  WITH CHECK ADD  CONSTRAINT [fk_dw6onv1fytgxdr6ilsax7c0xo] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_amlhits] CHECK CONSTRAINT [fk_dw6onv1fytgxdr6ilsax7c0xo]
GO
ALTER TABLE [dbo].[mpay_amlhits]  WITH CHECK ADD  CONSTRAINT [fk_f3t401j05af5tputlo7pthvxl] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_amlhits] CHECK CONSTRAINT [fk_f3t401j05af5tputlo7pthvxl]
GO
ALTER TABLE [dbo].[mpay_amlhits]  WITH CHECK ADD  CONSTRAINT [fk_ffg688w0lcfgts2nry781jqjy] FOREIGN KEY([refcaseid])
REFERENCES [dbo].[mpay_amlcases] ([id])
GO
ALTER TABLE [dbo].[mpay_amlhits] CHECK CONSTRAINT [fk_ffg688w0lcfgts2nry781jqjy]
GO
ALTER TABLE [dbo].[mpay_appschecksums]  WITH CHECK ADD  CONSTRAINT [fk_cf1ueo5bowo1aduw35mjutij8] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_appschecksums] CHECK CONSTRAINT [fk_cf1ueo5bowo1aduw35mjutij8]
GO
ALTER TABLE [dbo].[mpay_appschecksums]  WITH CHECK ADD  CONSTRAINT [fk_e0vacfvh0qye5tjad0ph9xkuf] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_appschecksums] CHECK CONSTRAINT [fk_e0vacfvh0qye5tjad0ph9xkuf]
GO
ALTER TABLE [dbo].[mpay_atm_voucheritm]  WITH CHECK ADD  CONSTRAINT [fk_5h6t4j5ytuqpty2djyvxxwjyb] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_atm_vouchergrp] ([id])
GO
ALTER TABLE [dbo].[mpay_atm_voucheritm] CHECK CONSTRAINT [fk_5h6t4j5ytuqpty2djyvxxwjyb]
GO
ALTER TABLE [dbo].[mpay_atm_vouchers]  WITH CHECK ADD  CONSTRAINT [fk_1l1jh53vywbsmqtw3gnchwja6] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_atm_vouchers] CHECK CONSTRAINT [fk_1l1jh53vywbsmqtw3gnchwja6]
GO
ALTER TABLE [dbo].[mpay_atm_vouchers]  WITH CHECK ADD  CONSTRAINT [fk_5eqh708i8uwxq3k4feayadjx3] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_atm_vouchers] CHECK CONSTRAINT [fk_5eqh708i8uwxq3k4feayadjx3]
GO
ALTER TABLE [dbo].[mpay_atm_vouchers]  WITH CHECK ADD  CONSTRAINT [fk_ffmk82080l8fjd2jj8sehth77] FOREIGN KEY([refmobileid])
REFERENCES [dbo].[mpay_customermobiles] ([id])
GO
ALTER TABLE [dbo].[mpay_atm_vouchers] CHECK CONSTRAINT [fk_ffmk82080l8fjd2jj8sehth77]
GO
ALTER TABLE [dbo].[mpay_atm_vouchers]  WITH CHECK ADD  CONSTRAINT [fk_gxytpsr3aiots7v2f32n7m4rc] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_atm_vouchers] CHECK CONSTRAINT [fk_gxytpsr3aiots7v2f32n7m4rc]
GO
ALTER TABLE [dbo].[mpay_atm_vouchers]  WITH CHECK ADD  CONSTRAINT [fk_pa735doxcc4990b7o04df2j3i] FOREIGN KEY([messagetypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_atm_vouchers] CHECK CONSTRAINT [fk_pa735doxcc4990b7o04df2j3i]
GO
ALTER TABLE [dbo].[mpay_attachmenttypeitm]  WITH CHECK ADD  CONSTRAINT [fk_t2pw9a4ipgxtyyjr45w01oaio] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_attachmenttypegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_attachmenttypeitm] CHECK CONSTRAINT [fk_t2pw9a4ipgxtyyjr45w01oaio]
GO
ALTER TABLE [dbo].[mpay_attachmenttypes]  WITH CHECK ADD  CONSTRAINT [fk_cksjjb634brjd87ue8vv9tv1t] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_attachmenttypes] CHECK CONSTRAINT [fk_cksjjb634brjd87ue8vv9tv1t]
GO
ALTER TABLE [dbo].[mpay_attachmenttypes]  WITH CHECK ADD  CONSTRAINT [fk_i3u0xa083yh06c40bqjk0dhsn] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_attachmenttypes] CHECK CONSTRAINT [fk_i3u0xa083yh06c40bqjk0dhsn]
GO
ALTER TABLE [dbo].[mpay_balancetypeitm]  WITH CHECK ADD  CONSTRAINT [fk_oj1b0fjun1p33edyy2xymgyjv] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_balancetypegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_balancetypeitm] CHECK CONSTRAINT [fk_oj1b0fjun1p33edyy2xymgyjv]
GO
ALTER TABLE [dbo].[mpay_balancetypes]  WITH CHECK ADD  CONSTRAINT [fk_e22lxrfme1jsllnll5ndtn8fu] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_balancetypes] CHECK CONSTRAINT [fk_e22lxrfme1jsllnll5ndtn8fu]
GO
ALTER TABLE [dbo].[mpay_balancetypes]  WITH CHECK ADD  CONSTRAINT [fk_nwiibbm1dfhqp8twshfykv0q] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_balancetypes] CHECK CONSTRAINT [fk_nwiibbm1dfhqp8twshfykv0q]
GO
ALTER TABLE [dbo].[mpay_balancetypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_c4drn7lm0imqetff44ikeildn] FOREIGN KEY([typeid])
REFERENCES [dbo].[mpay_balancetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_balancetypes_nls] CHECK CONSTRAINT [fk_c4drn7lm0imqetff44ikeildn]
GO
ALTER TABLE [dbo].[mpay_balancetypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_gk55yjowtjjpqyaegs3ckwggj] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_balancetypes_nls] CHECK CONSTRAINT [fk_gk55yjowtjjpqyaegs3ckwggj]
GO
ALTER TABLE [dbo].[mpay_balancetypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_j3y60xh8x4s8r2fm6gfy0c2b6] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_balancetypes_nls] CHECK CONSTRAINT [fk_j3y60xh8x4s8r2fm6gfy0c2b6]
GO
ALTER TABLE [dbo].[mpay_bankintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_57h11pc765paadk4yvw3q8w7b] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_bankintegmessages] CHECK CONSTRAINT [fk_57h11pc765paadk4yvw3q8w7b]
GO
ALTER TABLE [dbo].[mpay_bankintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_9dq6k26niinipifktui3ane5v] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegmessages] CHECK CONSTRAINT [fk_9dq6k26niinipifktui3ane5v]
GO
ALTER TABLE [dbo].[mpay_bankintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_d9hk6vsrud5jwfo85txvmhwq6] FOREIGN KEY([refstatusid])
REFERENCES [dbo].[mpay_bankintegstatuses] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegmessages] CHECK CONSTRAINT [fk_d9hk6vsrud5jwfo85txvmhwq6]
GO
ALTER TABLE [dbo].[mpay_bankintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_emigxrvyw6kb157c1urpnmytx] FOREIGN KEY([refbankid])
REFERENCES [dbo].[mpay_banks] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegmessages] CHECK CONSTRAINT [fk_emigxrvyw6kb157c1urpnmytx]
GO
ALTER TABLE [dbo].[mpay_bankintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_euy5a2e1ytfhv5ml8uop1krd5] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_bankintegmessages] CHECK CONSTRAINT [fk_euy5a2e1ytfhv5ml8uop1krd5]
GO
ALTER TABLE [dbo].[mpay_bankintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_qvcfvx9wi7w69xnt5irjk7rkr] FOREIGN KEY([reftransactionid])
REFERENCES [dbo].[mpay_transactions] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegmessages] CHECK CONSTRAINT [fk_qvcfvx9wi7w69xnt5irjk7rkr]
GO
ALTER TABLE [dbo].[mpay_bankintegreasonitm]  WITH CHECK ADD  CONSTRAINT [fk_o01w4ems6oitqb8xpmuskjnr2] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_bankintegreasongrp] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegreasonitm] CHECK CONSTRAINT [fk_o01w4ems6oitqb8xpmuskjnr2]
GO
ALTER TABLE [dbo].[mpay_bankintegreasons]  WITH CHECK ADD  CONSTRAINT [fk_gpr8vrqxhynv3gu0qny6w7ny5] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegreasons] CHECK CONSTRAINT [fk_gpr8vrqxhynv3gu0qny6w7ny5]
GO
ALTER TABLE [dbo].[mpay_bankintegreasons]  WITH CHECK ADD  CONSTRAINT [fk_mlrw95fpy9w7dlr4uobi1xi9n] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_bankintegreasons] CHECK CONSTRAINT [fk_mlrw95fpy9w7dlr4uobi1xi9n]
GO
ALTER TABLE [dbo].[mpay_bankintegreasons_nls]  WITH CHECK ADD  CONSTRAINT [fk_18g4sd8doutfkxjktalo2puym] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegreasons_nls] CHECK CONSTRAINT [fk_18g4sd8doutfkxjktalo2puym]
GO
ALTER TABLE [dbo].[mpay_bankintegreasons_nls]  WITH CHECK ADD  CONSTRAINT [fk_clbugcxcue11einn8mm4onb1h] FOREIGN KEY([rsnid])
REFERENCES [dbo].[mpay_bankintegreasons] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegreasons_nls] CHECK CONSTRAINT [fk_clbugcxcue11einn8mm4onb1h]
GO
ALTER TABLE [dbo].[mpay_bankintegreasons_nls]  WITH CHECK ADD  CONSTRAINT [fk_mie8k7rjil6sv355niq6mnco8] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_bankintegreasons_nls] CHECK CONSTRAINT [fk_mie8k7rjil6sv355niq6mnco8]
GO
ALTER TABLE [dbo].[mpay_bankintegsettings]  WITH CHECK ADD  CONSTRAINT [fk_hxnbd62sdjw3tt4fngnh8llxu] FOREIGN KEY([refbankid])
REFERENCES [dbo].[mpay_banks] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegsettings] CHECK CONSTRAINT [fk_hxnbd62sdjw3tt4fngnh8llxu]
GO
ALTER TABLE [dbo].[mpay_bankintegsettings]  WITH CHECK ADD  CONSTRAINT [fk_kk3vyre274hyi5lq7td0w7ow0] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegsettings] CHECK CONSTRAINT [fk_kk3vyre274hyi5lq7td0w7ow0]
GO
ALTER TABLE [dbo].[mpay_bankintegsettings]  WITH CHECK ADD  CONSTRAINT [fk_pje64lhe5wi2t0mouv9ro848g] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_bankintegsettings] CHECK CONSTRAINT [fk_pje64lhe5wi2t0mouv9ro848g]
GO
ALTER TABLE [dbo].[mpay_bankintegstatus_nls]  WITH CHECK ADD  CONSTRAINT [fk_jbjelm0hl86ahmd8fcrkyqfhq] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_bankintegstatus_nls] CHECK CONSTRAINT [fk_jbjelm0hl86ahmd8fcrkyqfhq]
GO
ALTER TABLE [dbo].[mpay_bankintegstatus_nls]  WITH CHECK ADD  CONSTRAINT [fk_t3yl1e5o1du8rkdr35vt7xsdw] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegstatus_nls] CHECK CONSTRAINT [fk_t3yl1e5o1du8rkdr35vt7xsdw]
GO
ALTER TABLE [dbo].[mpay_bankintegstatus_nls]  WITH CHECK ADD  CONSTRAINT [fk_tjkgblgvq3ia326ohfedabmy3] FOREIGN KEY([sttsid])
REFERENCES [dbo].[mpay_bankintegstatuses] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegstatus_nls] CHECK CONSTRAINT [fk_tjkgblgvq3ia326ohfedabmy3]
GO
ALTER TABLE [dbo].[mpay_bankintegstatuses]  WITH CHECK ADD  CONSTRAINT [fk_fr54b491h67ra7noq1el0heg9] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_bankintegstatuses] CHECK CONSTRAINT [fk_fr54b491h67ra7noq1el0heg9]
GO
ALTER TABLE [dbo].[mpay_bankintegstatuses]  WITH CHECK ADD  CONSTRAINT [fk_hpn1pmd0qg3utrmjymflshtf7] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegstatuses] CHECK CONSTRAINT [fk_hpn1pmd0qg3utrmjymflshtf7]
GO
ALTER TABLE [dbo].[mpay_bankintegstatusitm]  WITH CHECK ADD  CONSTRAINT [fk_jdwtv3odisk49l2ftgcb3q65x] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_bankintegstatusgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_bankintegstatusitm] CHECK CONSTRAINT [fk_jdwtv3odisk49l2ftgcb3q65x]
GO
ALTER TABLE [dbo].[mpay_bankitm]  WITH CHECK ADD  CONSTRAINT [fk_s4lchb5xgebysy3n7d0xwseb4] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_bankgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_bankitm] CHECK CONSTRAINT [fk_s4lchb5xgebysy3n7d0xwseb4]
GO
ALTER TABLE [dbo].[mpay_banks]  WITH CHECK ADD  CONSTRAINT [fk_akwlooyyuqwb8y542852u8txq] FOREIGN KEY([refchargeaccountid])
REFERENCES [dbo].[mpay_accounts] ([id])
GO
ALTER TABLE [dbo].[mpay_banks] CHECK CONSTRAINT [fk_akwlooyyuqwb8y542852u8txq]
GO
ALTER TABLE [dbo].[mpay_banks]  WITH CHECK ADD  CONSTRAINT [fk_cg2v5djrneddymr3ddb9wjtd1] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_banks] CHECK CONSTRAINT [fk_cg2v5djrneddymr3ddb9wjtd1]
GO
ALTER TABLE [dbo].[mpay_banks]  WITH CHECK ADD  CONSTRAINT [fk_qnoh2b8l5mp3c0oejf27a0nny] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_banks] CHECK CONSTRAINT [fk_qnoh2b8l5mp3c0oejf27a0nny]
GO
ALTER TABLE [dbo].[mpay_banksusers]  WITH CHECK ADD  CONSTRAINT [fk_3id280i3vqycucdpwxc49dext] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_banksusers] CHECK CONSTRAINT [fk_3id280i3vqycucdpwxc49dext]
GO
ALTER TABLE [dbo].[mpay_banksusers]  WITH CHECK ADD  CONSTRAINT [fk_op18rju9u070p9y60squrh8pr] FOREIGN KEY([bankid])
REFERENCES [dbo].[mpay_banks] ([id])
GO
ALTER TABLE [dbo].[mpay_banksusers] CHECK CONSTRAINT [fk_op18rju9u070p9y60squrh8pr]
GO
ALTER TABLE [dbo].[mpay_banksusers]  WITH CHECK ADD  CONSTRAINT [fk_v5woh2lp5u6lxiadxopvpp3q] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_banksusers] CHECK CONSTRAINT [fk_v5woh2lp5u6lxiadxopvpp3q]
GO
ALTER TABLE [dbo].[mpay_bulkpayment]  WITH CHECK ADD  CONSTRAINT [fk_8y8ant1v2ucobxfvk8upp8t6] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_bulkpayment] CHECK CONSTRAINT [fk_8y8ant1v2ucobxfvk8upp8t6]
GO
ALTER TABLE [dbo].[mpay_bulkpayment]  WITH CHECK ADD  CONSTRAINT [fk_ju250nctnrq77r1ltpxe1lk35] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_bulkpayment] CHECK CONSTRAINT [fk_ju250nctnrq77r1ltpxe1lk35]
GO
ALTER TABLE [dbo].[mpay_bulkregistration]  WITH CHECK ADD  CONSTRAINT [fk_6pj170qir7aypfgcm09cw1xxp] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_bulkregistration] CHECK CONSTRAINT [fk_6pj170qir7aypfgcm09cw1xxp]
GO
ALTER TABLE [dbo].[mpay_bulkregistration]  WITH CHECK ADD  CONSTRAINT [fk_skva5u83mexdm0hlggjs1953d] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_bulkregistration] CHECK CONSTRAINT [fk_skva5u83mexdm0hlggjs1953d]
GO
ALTER TABLE [dbo].[mpay_cashin]  WITH CHECK ADD  CONSTRAINT [fk_1n4tqtom76hl6pmgxudjr6aw8] FOREIGN KEY([bankid])
REFERENCES [dbo].[mpay_banks] ([id])
GO
ALTER TABLE [dbo].[mpay_cashin] CHECK CONSTRAINT [fk_1n4tqtom76hl6pmgxudjr6aw8]
GO
ALTER TABLE [dbo].[mpay_cashin]  WITH CHECK ADD  CONSTRAINT [fk_2k7vqmssu2hu3tmm84dge3tpp] FOREIGN KEY([refmessageid])
REFERENCES [dbo].[mpay_mpaymessages] ([id])
GO
ALTER TABLE [dbo].[mpay_cashin] CHECK CONSTRAINT [fk_2k7vqmssu2hu3tmm84dge3tpp]
GO
ALTER TABLE [dbo].[mpay_cashin]  WITH CHECK ADD  CONSTRAINT [fk_3bf5psdg8n734t27p27javfh] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_cashin] CHECK CONSTRAINT [fk_3bf5psdg8n734t27p27javfh]
GO
ALTER TABLE [dbo].[mpay_cashin]  WITH CHECK ADD  CONSTRAINT [fk_6iltvo5w05p8ps1wevx4nw96f] FOREIGN KEY([reflastmsglogid])
REFERENCES [dbo].[mpay_mpclearintegmsglogs] ([id])
GO
ALTER TABLE [dbo].[mpay_cashin] CHECK CONSTRAINT [fk_6iltvo5w05p8ps1wevx4nw96f]
GO
ALTER TABLE [dbo].[mpay_cashin]  WITH CHECK ADD  CONSTRAINT [fk_6npgs52uf5074hgdex2mavrye] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_cashin] CHECK CONSTRAINT [fk_6npgs52uf5074hgdex2mavrye]
GO
ALTER TABLE [dbo].[mpay_cashin]  WITH CHECK ADD  CONSTRAINT [fk_9c6varbsg7qs3rnhigwi5g445] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_cashin] CHECK CONSTRAINT [fk_9c6varbsg7qs3rnhigwi5g445]
GO
ALTER TABLE [dbo].[mpay_cashin]  WITH CHECK ADD  CONSTRAINT [fk_avaiyq1okjbiwlgj7p3wi4l0d] FOREIGN KEY([reasonid])
REFERENCES [dbo].[mpay_reasons] ([id])
GO
ALTER TABLE [dbo].[mpay_cashin] CHECK CONSTRAINT [fk_avaiyq1okjbiwlgj7p3wi4l0d]
GO
ALTER TABLE [dbo].[mpay_cashin]  WITH CHECK ADD  CONSTRAINT [fk_eq8kopss0qeuuwfi7aqfsoadr] FOREIGN KEY([processingstatusid])
REFERENCES [dbo].[mpay_processingstatuses] ([id])
GO
ALTER TABLE [dbo].[mpay_cashin] CHECK CONSTRAINT [fk_eq8kopss0qeuuwfi7aqfsoadr]
GO
ALTER TABLE [dbo].[mpay_cashin]  WITH CHECK ADD  CONSTRAINT [fk_m8mgxfftsfgk3vn2rjs1bkbyd] FOREIGN KEY([refcustmobid])
REFERENCES [dbo].[mpay_customermobiles] ([id])
GO
ALTER TABLE [dbo].[mpay_cashin] CHECK CONSTRAINT [fk_m8mgxfftsfgk3vn2rjs1bkbyd]
GO
ALTER TABLE [dbo].[mpay_cashout]  WITH CHECK ADD  CONSTRAINT [fk_21mvbpglbych5lygku4u20h1y] FOREIGN KEY([refcustmobid])
REFERENCES [dbo].[mpay_customermobiles] ([id])
GO
ALTER TABLE [dbo].[mpay_cashout] CHECK CONSTRAINT [fk_21mvbpglbych5lygku4u20h1y]
GO
ALTER TABLE [dbo].[mpay_cashout]  WITH CHECK ADD  CONSTRAINT [fk_9npt1meg1wm3tpuso4omsnqdk] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_cashout] CHECK CONSTRAINT [fk_9npt1meg1wm3tpuso4omsnqdk]
GO
ALTER TABLE [dbo].[mpay_cashout]  WITH CHECK ADD  CONSTRAINT [fk_b2tx482hnn0widlahr1s75n0i] FOREIGN KEY([processingstatusid])
REFERENCES [dbo].[mpay_processingstatuses] ([id])
GO
ALTER TABLE [dbo].[mpay_cashout] CHECK CONSTRAINT [fk_b2tx482hnn0widlahr1s75n0i]
GO
ALTER TABLE [dbo].[mpay_cashout]  WITH CHECK ADD  CONSTRAINT [fk_b6uuwk62cv6qlj6ikf2kytn6t] FOREIGN KEY([reasonid])
REFERENCES [dbo].[mpay_reasons] ([id])
GO
ALTER TABLE [dbo].[mpay_cashout] CHECK CONSTRAINT [fk_b6uuwk62cv6qlj6ikf2kytn6t]
GO
ALTER TABLE [dbo].[mpay_cashout]  WITH CHECK ADD  CONSTRAINT [fk_d3hufgmltah8mqcj7rxfbit1s] FOREIGN KEY([reflastmsglogid])
REFERENCES [dbo].[mpay_mpclearintegmsglogs] ([id])
GO
ALTER TABLE [dbo].[mpay_cashout] CHECK CONSTRAINT [fk_d3hufgmltah8mqcj7rxfbit1s]
GO
ALTER TABLE [dbo].[mpay_cashout]  WITH CHECK ADD  CONSTRAINT [fk_e2ih4udgf0l3cj9j8j2ehp08x] FOREIGN KEY([refmessageid])
REFERENCES [dbo].[mpay_mpaymessages] ([id])
GO
ALTER TABLE [dbo].[mpay_cashout] CHECK CONSTRAINT [fk_e2ih4udgf0l3cj9j8j2ehp08x]
GO
ALTER TABLE [dbo].[mpay_cashout]  WITH CHECK ADD  CONSTRAINT [fk_qmkwxjr3r9hw8lkck8vomjlyn] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_cashout] CHECK CONSTRAINT [fk_qmkwxjr3r9hw8lkck8vomjlyn]
GO
ALTER TABLE [dbo].[mpay_cashout]  WITH CHECK ADD  CONSTRAINT [fk_tl2d1r2wdlc73mok1msh13iah] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_cashout] CHECK CONSTRAINT [fk_tl2d1r2wdlc73mok1msh13iah]
GO
ALTER TABLE [dbo].[mpay_channeltype]  WITH CHECK ADD  CONSTRAINT [fk_3ylsudvdu0htteqehbupwxwsq] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_channeltype] CHECK CONSTRAINT [fk_3ylsudvdu0htteqehbupwxwsq]
GO
ALTER TABLE [dbo].[mpay_channeltype]  WITH CHECK ADD  CONSTRAINT [fk_9tvu0q90kl165l7iuv6ae9dud] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_channeltype] CHECK CONSTRAINT [fk_9tvu0q90kl165l7iuv6ae9dud]
GO
ALTER TABLE [dbo].[mpay_channeltype_nls]  WITH CHECK ADD  CONSTRAINT [fk_1d1hvdgjlo08viji5kdse1mp2] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_channeltype_nls] CHECK CONSTRAINT [fk_1d1hvdgjlo08viji5kdse1mp2]
GO
ALTER TABLE [dbo].[mpay_channeltype_nls]  WITH CHECK ADD  CONSTRAINT [fk_2306va4x7l1uo0yc5k74pclpx] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_channeltype_nls] CHECK CONSTRAINT [fk_2306va4x7l1uo0yc5k74pclpx]
GO
ALTER TABLE [dbo].[mpay_channeltype_nls]  WITH CHECK ADD  CONSTRAINT [fk_lbiuwm70xxe45savfhxxii936] FOREIGN KEY([channeltypeid])
REFERENCES [dbo].[mpay_channeltype] ([id])
GO
ALTER TABLE [dbo].[mpay_channeltype_nls] CHECK CONSTRAINT [fk_lbiuwm70xxe45savfhxxii936]
GO
ALTER TABLE [dbo].[mpay_channeltypeitm]  WITH CHECK ADD  CONSTRAINT [fk_99sctfbykrr8g32k3acmtw6pd] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_channeltypegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_channeltypeitm] CHECK CONSTRAINT [fk_99sctfbykrr8g32k3acmtw6pd]
GO
ALTER TABLE [dbo].[mpay_chargesschemedetailitm]  WITH CHECK ADD  CONSTRAINT [fk_8ef7a26n3psnrdxh6oj2x1qi0] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_chargesschemedetailgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_chargesschemedetailitm] CHECK CONSTRAINT [fk_8ef7a26n3psnrdxh6oj2x1qi0]
GO
ALTER TABLE [dbo].[mpay_chargesschemedetails]  WITH CHECK ADD  CONSTRAINT [fk_7cour562gw39xwwr13hslc5a2] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_chargesschemedetails] CHECK CONSTRAINT [fk_7cour562gw39xwwr13hslc5a2]
GO
ALTER TABLE [dbo].[mpay_chargesschemedetails]  WITH CHECK ADD  CONSTRAINT [fk_c0cmksjw7wjeb4usbnvedhhp1] FOREIGN KEY([msgtypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_chargesschemedetails] CHECK CONSTRAINT [fk_c0cmksjw7wjeb4usbnvedhhp1]
GO
ALTER TABLE [dbo].[mpay_chargesschemedetails]  WITH CHECK ADD  CONSTRAINT [fk_liihmgoffmqgwhklcyoaaim7r] FOREIGN KEY([refchargesschemeid])
REFERENCES [dbo].[mpay_chargesschemes] ([id])
GO
ALTER TABLE [dbo].[mpay_chargesschemedetails] CHECK CONSTRAINT [fk_liihmgoffmqgwhklcyoaaim7r]
GO
ALTER TABLE [dbo].[mpay_chargesschemedetails]  WITH CHECK ADD  CONSTRAINT [fk_tddxatl668q8yam6ypxoe0u4i] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_chargesschemedetails] CHECK CONSTRAINT [fk_tddxatl668q8yam6ypxoe0u4i]
GO
ALTER TABLE [dbo].[mpay_chargesschemeitm]  WITH CHECK ADD  CONSTRAINT [fk_el7owjyygyd6wpcn623gr4u95] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_chargesschemegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_chargesschemeitm] CHECK CONSTRAINT [fk_el7owjyygyd6wpcn623gr4u95]
GO
ALTER TABLE [dbo].[mpay_chargesschemes]  WITH CHECK ADD  CONSTRAINT [fk_dulik9lhbxdwmebna2n5pn885] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_chargesschemes] CHECK CONSTRAINT [fk_dulik9lhbxdwmebna2n5pn885]
GO
ALTER TABLE [dbo].[mpay_chargesschemes]  WITH CHECK ADD  CONSTRAINT [fk_f09l8j0qesl914wuvnaok0jov] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_chargesschemes] CHECK CONSTRAINT [fk_f09l8j0qesl914wuvnaok0jov]
GO
ALTER TABLE [dbo].[mpay_chargesschemes]  WITH CHECK ADD  CONSTRAINT [fk_fgncbj78g514ssj3fh878wtlp] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_chargesschemes] CHECK CONSTRAINT [fk_fgncbj78g514ssj3fh878wtlp]
GO
ALTER TABLE [dbo].[mpay_chargessliceitm]  WITH CHECK ADD  CONSTRAINT [fk_tlk1dq7w8musier9gigm8te84] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_chargesslicegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_chargessliceitm] CHECK CONSTRAINT [fk_tlk1dq7w8musier9gigm8te84]
GO
ALTER TABLE [dbo].[mpay_chargesslices]  WITH CHECK ADD  CONSTRAINT [fk_axj2enfn9rmxa8ciofb8fg1el] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_chargesslices] CHECK CONSTRAINT [fk_axj2enfn9rmxa8ciofb8fg1el]
GO
ALTER TABLE [dbo].[mpay_chargesslices]  WITH CHECK ADD  CONSTRAINT [fk_d181sk7vkp43r603w42lhd1vw] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_chargesslices] CHECK CONSTRAINT [fk_d181sk7vkp43r603w42lhd1vw]
GO
ALTER TABLE [dbo].[mpay_chargesslices]  WITH CHECK ADD  CONSTRAINT [fk_qc8pkxf9044iqr331nj0kt77r] FOREIGN KEY([refchargesdetailsid])
REFERENCES [dbo].[mpay_chargesschemedetails] ([id])
GO
ALTER TABLE [dbo].[mpay_chargesslices] CHECK CONSTRAINT [fk_qc8pkxf9044iqr331nj0kt77r]
GO
ALTER TABLE [dbo].[mpay_cities]  WITH CHECK ADD  CONSTRAINT [fk_dtyslsagal0dycjg84v2xy9gp] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_cities] CHECK CONSTRAINT [fk_dtyslsagal0dycjg84v2xy9gp]
GO
ALTER TABLE [dbo].[mpay_cities]  WITH CHECK ADD  CONSTRAINT [fk_mv4vdvin9gw27g2p6b34w1oxm] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_cities] CHECK CONSTRAINT [fk_mv4vdvin9gw27g2p6b34w1oxm]
GO
ALTER TABLE [dbo].[mpay_citiesnls]  WITH CHECK ADD  CONSTRAINT [fk_4w14lkabhqefsmn6oyjicb29t] FOREIGN KEY([cityid])
REFERENCES [dbo].[mpay_cities] ([id])
GO
ALTER TABLE [dbo].[mpay_citiesnls] CHECK CONSTRAINT [fk_4w14lkabhqefsmn6oyjicb29t]
GO
ALTER TABLE [dbo].[mpay_citiesnls]  WITH CHECK ADD  CONSTRAINT [fk_cp2cw3frt3yg3m7gc4juhht4i] FOREIGN KEY([clangid])
REFERENCES [dbo].[mpay_languages] ([id])
GO
ALTER TABLE [dbo].[mpay_citiesnls] CHECK CONSTRAINT [fk_cp2cw3frt3yg3m7gc4juhht4i]
GO
ALTER TABLE [dbo].[mpay_citiesnls]  WITH CHECK ADD  CONSTRAINT [fk_d1lk3by8gj1urfg2a302xe7dm] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_citiesnls] CHECK CONSTRAINT [fk_d1lk3by8gj1urfg2a302xe7dm]
GO
ALTER TABLE [dbo].[mpay_citiesnls]  WITH CHECK ADD  CONSTRAINT [fk_gb8bqeasp3p0cc0uat4t5xmt9] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_citiesnls] CHECK CONSTRAINT [fk_gb8bqeasp3p0cc0uat4t5xmt9]
GO
ALTER TABLE [dbo].[mpay_cityareas]  WITH CHECK ADD  CONSTRAINT [fk_3mcecnpcyccelcawil30krl5a] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_cityareas] CHECK CONSTRAINT [fk_3mcecnpcyccelcawil30krl5a]
GO
ALTER TABLE [dbo].[mpay_cityareas]  WITH CHECK ADD  CONSTRAINT [fk_lcrh6fdxrjv9t2iyr6upgt0lc] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_cityareas] CHECK CONSTRAINT [fk_lcrh6fdxrjv9t2iyr6upgt0lc]
GO
ALTER TABLE [dbo].[mpay_cityareas]  WITH CHECK ADD  CONSTRAINT [fk_mdfmim91xvob3misnefcad9jc] FOREIGN KEY([cityid])
REFERENCES [dbo].[mpay_cities] ([id])
GO
ALTER TABLE [dbo].[mpay_cityareas] CHECK CONSTRAINT [fk_mdfmim91xvob3misnefcad9jc]
GO
ALTER TABLE [dbo].[mpay_cityareas_nls]  WITH CHECK ADD  CONSTRAINT [fk_7tvpt3k0g9cmun81n6bvxha2] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_cityareas_nls] CHECK CONSTRAINT [fk_7tvpt3k0g9cmun81n6bvxha2]
GO
ALTER TABLE [dbo].[mpay_cityareas_nls]  WITH CHECK ADD  CONSTRAINT [fk_md04mwe7p3f5sx24m1ubpvvru] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_cityareas_nls] CHECK CONSTRAINT [fk_md04mwe7p3f5sx24m1ubpvvru]
GO
ALTER TABLE [dbo].[mpay_cityareas_nls]  WITH CHECK ADD  CONSTRAINT [fk_ne8m5nbvd2qdvwtp736vp76c4] FOREIGN KEY([arealanguageid])
REFERENCES [dbo].[mpay_languages] ([id])
GO
ALTER TABLE [dbo].[mpay_cityareas_nls] CHECK CONSTRAINT [fk_ne8m5nbvd2qdvwtp736vp76c4]
GO
ALTER TABLE [dbo].[mpay_cityareas_nls]  WITH CHECK ADD  CONSTRAINT [fk_sif9ec1rpt2b2wu0uwl9hduj] FOREIGN KEY([areaid])
REFERENCES [dbo].[mpay_cityareas] ([id])
GO
ALTER TABLE [dbo].[mpay_cityareas_nls] CHECK CONSTRAINT [fk_sif9ec1rpt2b2wu0uwl9hduj]
GO
ALTER TABLE [dbo].[mpay_clientscommissionitm]  WITH CHECK ADD  CONSTRAINT [fk_abstr1mlecalp80a92iv9v4ms] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_clientscommissiongrp] ([id])
GO
ALTER TABLE [dbo].[mpay_clientscommissionitm] CHECK CONSTRAINT [fk_abstr1mlecalp80a92iv9v4ms]
GO
ALTER TABLE [dbo].[mpay_clientscommissions]  WITH CHECK ADD  CONSTRAINT [fk_2mq9wetbuhblnysak929igr1c] FOREIGN KEY([accountid])
REFERENCES [dbo].[mpay_accounts] ([id])
GO
ALTER TABLE [dbo].[mpay_clientscommissions] CHECK CONSTRAINT [fk_2mq9wetbuhblnysak929igr1c]
GO
ALTER TABLE [dbo].[mpay_clientscommissions]  WITH CHECK ADD  CONSTRAINT [fk_8b0dax9rcgxb4mqwahaue4njd] FOREIGN KEY([messagetypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_clientscommissions] CHECK CONSTRAINT [fk_8b0dax9rcgxb4mqwahaue4njd]
GO
ALTER TABLE [dbo].[mpay_clientscommissions]  WITH CHECK ADD  CONSTRAINT [fk_rdmfxe3834uaivs2mx76fkgs4] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_clientscommissions] CHECK CONSTRAINT [fk_rdmfxe3834uaivs2mx76fkgs4]
GO
ALTER TABLE [dbo].[mpay_clientscommissions]  WITH CHECK ADD  CONSTRAINT [fk_sspmmw3326ym1a977vr9svjal] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_clientscommissions] CHECK CONSTRAINT [fk_sspmmw3326ym1a977vr9svjal]
GO
ALTER TABLE [dbo].[mpay_clientslimititm]  WITH CHECK ADD  CONSTRAINT [fk_j5q5mthi5knv19nf5fpgf6k3v] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_clientslimitgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_clientslimititm] CHECK CONSTRAINT [fk_j5q5mthi5knv19nf5fpgf6k3v]
GO
ALTER TABLE [dbo].[mpay_clientslimits]  WITH CHECK ADD  CONSTRAINT [fk_41iu35su4fyumwd8ypm9tc8oy] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_clientslimits] CHECK CONSTRAINT [fk_41iu35su4fyumwd8ypm9tc8oy]
GO
ALTER TABLE [dbo].[mpay_clientslimits]  WITH CHECK ADD  CONSTRAINT [fk_e0av8lv1ks5h472p6mxapkyip] FOREIGN KEY([accountid])
REFERENCES [dbo].[mpay_accounts] ([id])
GO
ALTER TABLE [dbo].[mpay_clientslimits] CHECK CONSTRAINT [fk_e0av8lv1ks5h472p6mxapkyip]
GO
ALTER TABLE [dbo].[mpay_clientslimits]  WITH CHECK ADD  CONSTRAINT [fk_esxvo1boyp047gd8vp59ef80q] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_clientslimits] CHECK CONSTRAINT [fk_esxvo1boyp047gd8vp59ef80q]
GO
ALTER TABLE [dbo].[mpay_clientslimits]  WITH CHECK ADD  CONSTRAINT [fk_mexh3fp9t7avegqsw30u9k201] FOREIGN KEY([messagetypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_clientslimits] CHECK CONSTRAINT [fk_mexh3fp9t7avegqsw30u9k201]
GO
ALTER TABLE [dbo].[mpay_clientsotpitm]  WITH CHECK ADD  CONSTRAINT [fk_lac6ebfpjoly9shd95au6u16g] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_clientsotpgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_clientsotpitm] CHECK CONSTRAINT [fk_lac6ebfpjoly9shd95au6u16g]
GO
ALTER TABLE [dbo].[mpay_clientsotps]  WITH CHECK ADD  CONSTRAINT [fk_90x7i7ki43h2wgml4nbovxq5f] FOREIGN KEY([sendermobileid])
REFERENCES [dbo].[mpay_customermobiles] ([id])
GO
ALTER TABLE [dbo].[mpay_clientsotps] CHECK CONSTRAINT [fk_90x7i7ki43h2wgml4nbovxq5f]
GO
ALTER TABLE [dbo].[mpay_clientsotps]  WITH CHECK ADD  CONSTRAINT [fk_9e2x78t7psbqyghvdfhkic4kb] FOREIGN KEY([messagetypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_clientsotps] CHECK CONSTRAINT [fk_9e2x78t7psbqyghvdfhkic4kb]
GO
ALTER TABLE [dbo].[mpay_clientsotps]  WITH CHECK ADD  CONSTRAINT [fk_h30jf5f42hc2ixpmjkhgk1hay] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_clientsotps] CHECK CONSTRAINT [fk_h30jf5f42hc2ixpmjkhgk1hay]
GO
ALTER TABLE [dbo].[mpay_clientsotps]  WITH CHECK ADD  CONSTRAINT [fk_hvmmwkt5f6kcofvf0ewarve7r] FOREIGN KEY([senderserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_clientsotps] CHECK CONSTRAINT [fk_hvmmwkt5f6kcofvf0ewarve7r]
GO
ALTER TABLE [dbo].[mpay_clientsotps]  WITH CHECK ADD  CONSTRAINT [fk_puysm7re6n1e91wpl6xogq1l7] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_clientsotps] CHECK CONSTRAINT [fk_puysm7re6n1e91wpl6xogq1l7]
GO
ALTER TABLE [dbo].[mpay_clientsotps]  WITH CHECK ADD  CONSTRAINT [fk_t0fh0v1u8txjboxmlu3vpiar7] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_clientsotps] CHECK CONSTRAINT [fk_t0fh0v1u8txjboxmlu3vpiar7]
GO
ALTER TABLE [dbo].[mpay_clienttype_nls]  WITH CHECK ADD  CONSTRAINT [fk_a6n2osy6ek85fu28b6bupj9c] FOREIGN KEY([clienttypeid])
REFERENCES [dbo].[mpay_clienttypes] ([id])
GO
ALTER TABLE [dbo].[mpay_clienttype_nls] CHECK CONSTRAINT [fk_a6n2osy6ek85fu28b6bupj9c]
GO
ALTER TABLE [dbo].[mpay_clienttype_nls]  WITH CHECK ADD  CONSTRAINT [fk_dt1ceyl426qakbc5k9odvto7x] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_clienttype_nls] CHECK CONSTRAINT [fk_dt1ceyl426qakbc5k9odvto7x]
GO
ALTER TABLE [dbo].[mpay_clienttype_nls]  WITH CHECK ADD  CONSTRAINT [fk_g8sn6t18uhitije065fufddaj] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_clienttype_nls] CHECK CONSTRAINT [fk_g8sn6t18uhitije065fufddaj]
GO
ALTER TABLE [dbo].[mpay_clienttypes]  WITH CHECK ADD  CONSTRAINT [fk_9p6gf9nd9qgm8671k4u8jf6vn] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_clienttypes] CHECK CONSTRAINT [fk_9p6gf9nd9qgm8671k4u8jf6vn]
GO
ALTER TABLE [dbo].[mpay_clienttypes]  WITH CHECK ADD  CONSTRAINT [fk_ilgtb75es3hwmnhkmegxn7f1r] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_clienttypes] CHECK CONSTRAINT [fk_ilgtb75es3hwmnhkmegxn7f1r]
GO
ALTER TABLE [dbo].[mpay_commissionitm]  WITH CHECK ADD  CONSTRAINT [fk_3iy6h439va0tpssxw9dv3hmax] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_commissiongrp] ([id])
GO
ALTER TABLE [dbo].[mpay_commissionitm] CHECK CONSTRAINT [fk_3iy6h439va0tpssxw9dv3hmax]
GO
ALTER TABLE [dbo].[mpay_commissionparameteritm]  WITH CHECK ADD  CONSTRAINT [fk_t614vajce03d6a4ysj53j1e2u] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_commissionparametergrp] ([id])
GO
ALTER TABLE [dbo].[mpay_commissionparameteritm] CHECK CONSTRAINT [fk_t614vajce03d6a4ysj53j1e2u]
GO
ALTER TABLE [dbo].[mpay_commissionparameters]  WITH CHECK ADD  CONSTRAINT [fk_dx96q3lahsbkrx0ebo7vhtqiq] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_commissionparameters] CHECK CONSTRAINT [fk_dx96q3lahsbkrx0ebo7vhtqiq]
GO
ALTER TABLE [dbo].[mpay_commissionparameters]  WITH CHECK ADD  CONSTRAINT [fk_n8d9ax26i4kr5vnvd6bo3nf58] FOREIGN KEY([refcommissionid])
REFERENCES [dbo].[mpay_commissions] ([id])
GO
ALTER TABLE [dbo].[mpay_commissionparameters] CHECK CONSTRAINT [fk_n8d9ax26i4kr5vnvd6bo3nf58]
GO
ALTER TABLE [dbo].[mpay_commissionparameters]  WITH CHECK ADD  CONSTRAINT [fk_tg0ey95cvc14xhqae0p9b8yah] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_commissionparameters] CHECK CONSTRAINT [fk_tg0ey95cvc14xhqae0p9b8yah]
GO
ALTER TABLE [dbo].[mpay_commissions]  WITH CHECK ADD  CONSTRAINT [fk_6xx0jjluthqpyqri1pd0fr4yx] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_commissions] CHECK CONSTRAINT [fk_6xx0jjluthqpyqri1pd0fr4yx]
GO
ALTER TABLE [dbo].[mpay_commissions]  WITH CHECK ADD  CONSTRAINT [fk_fa3f8mi8uwsbx1kojbshtwycf] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_commissions] CHECK CONSTRAINT [fk_fa3f8mi8uwsbx1kojbshtwycf]
GO
ALTER TABLE [dbo].[mpay_commissions]  WITH CHECK ADD  CONSTRAINT [fk_q87av04qjq4awth0e1ohugrh8] FOREIGN KEY([refcommissionschemeid])
REFERENCES [dbo].[mpay_commissionschemes] ([id])
GO
ALTER TABLE [dbo].[mpay_commissions] CHECK CONSTRAINT [fk_q87av04qjq4awth0e1ohugrh8]
GO
ALTER TABLE [dbo].[mpay_commissions]  WITH CHECK ADD  CONSTRAINT [fk_rcktn44fwyn1r144f7by2gu9f] FOREIGN KEY([msgtypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_commissions] CHECK CONSTRAINT [fk_rcktn44fwyn1r144f7by2gu9f]
GO
ALTER TABLE [dbo].[mpay_commissionschemeitm]  WITH CHECK ADD  CONSTRAINT [fk_p6re97w0ugdniw0uslhjnhnmn] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_commissionschemegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_commissionschemeitm] CHECK CONSTRAINT [fk_p6re97w0ugdniw0uslhjnhnmn]
GO
ALTER TABLE [dbo].[mpay_commissionschemes]  WITH CHECK ADD  CONSTRAINT [fk_k7qkcfjd6l2evh6bl0rcrbxku] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_commissionschemes] CHECK CONSTRAINT [fk_k7qkcfjd6l2evh6bl0rcrbxku]
GO
ALTER TABLE [dbo].[mpay_commissionschemes]  WITH CHECK ADD  CONSTRAINT [fk_ma25folkfvuew039qi9m8fto3] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_commissionschemes] CHECK CONSTRAINT [fk_ma25folkfvuew039qi9m8fto3]
GO
ALTER TABLE [dbo].[mpay_corpintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_2uif0vpjt0xea18vt0qmt758w] FOREIGN KEY([refmsglogid])
REFERENCES [dbo].[mpay_mpclearintegmsglogs] ([id])
GO
ALTER TABLE [dbo].[mpay_corpintegmessages] CHECK CONSTRAINT [fk_2uif0vpjt0xea18vt0qmt758w]
GO
ALTER TABLE [dbo].[mpay_corpintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_3ejf4m9gluqm2p5by31p0torh] FOREIGN KEY([actiontypeid])
REFERENCES [dbo].[mpay_custintegactiontypes] ([id])
GO
ALTER TABLE [dbo].[mpay_corpintegmessages] CHECK CONSTRAINT [fk_3ejf4m9gluqm2p5by31p0torh]
GO
ALTER TABLE [dbo].[mpay_corpintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_4ph96lwh3ph711hhed68onuxv] FOREIGN KEY([refcorporateid])
REFERENCES [dbo].[mpay_corporates] ([id])
GO
ALTER TABLE [dbo].[mpay_corpintegmessages] CHECK CONSTRAINT [fk_4ph96lwh3ph711hhed68onuxv]
GO
ALTER TABLE [dbo].[mpay_corpintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_6iw2bjgsmfgda468cnaqh7gs2] FOREIGN KEY([processrejectionid])
REFERENCES [dbo].[mpay_mpclearintegrjctreasons] ([id])
GO
ALTER TABLE [dbo].[mpay_corpintegmessages] CHECK CONSTRAINT [fk_6iw2bjgsmfgda468cnaqh7gs2]
GO
ALTER TABLE [dbo].[mpay_corpintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_9dmu1q0lmw1dhyjbn2psulqr2] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_corpintegmessages] CHECK CONSTRAINT [fk_9dmu1q0lmw1dhyjbn2psulqr2]
GO
ALTER TABLE [dbo].[mpay_corpintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_9eg81so11cog35teau1uivgbi] FOREIGN KEY([integmsgtypeid])
REFERENCES [dbo].[mpay_mpclearintegmsgtypes] ([id])
GO
ALTER TABLE [dbo].[mpay_corpintegmessages] CHECK CONSTRAINT [fk_9eg81so11cog35teau1uivgbi]
GO
ALTER TABLE [dbo].[mpay_corpintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_mwgy5hj5nrs7v2teak7hdl5i] FOREIGN KEY([responseid])
REFERENCES [dbo].[mpay_mpclearresponsecodes] ([id])
GO
ALTER TABLE [dbo].[mpay_corpintegmessages] CHECK CONSTRAINT [fk_mwgy5hj5nrs7v2teak7hdl5i]
GO
ALTER TABLE [dbo].[mpay_corpintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_qm4k78nut1xp2kquq6lnao6my] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_corpintegmessages] CHECK CONSTRAINT [fk_qm4k78nut1xp2kquq6lnao6my]
GO
ALTER TABLE [dbo].[mpay_corpintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_qqafaj2xmavc6at2l91052jdd] FOREIGN KEY([refserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_corpintegmessages] CHECK CONSTRAINT [fk_qqafaj2xmavc6at2l91052jdd]
GO
ALTER TABLE [dbo].[mpay_corpintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_sjddf2a6uvoqv62k4khpw37m5] FOREIGN KEY([reforiginalmsgid])
REFERENCES [dbo].[mpay_corpintegmessages] ([id])
GO
ALTER TABLE [dbo].[mpay_corpintegmessages] CHECK CONSTRAINT [fk_sjddf2a6uvoqv62k4khpw37m5]
GO
ALTER TABLE [dbo].[mpay_corplegalentities]  WITH CHECK ADD  CONSTRAINT [fk_d3iet9bqw2lkkw5e0sl0pgmh4] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_corplegalentities] CHECK CONSTRAINT [fk_d3iet9bqw2lkkw5e0sl0pgmh4]
GO
ALTER TABLE [dbo].[mpay_corplegalentities]  WITH CHECK ADD  CONSTRAINT [fk_q6i6wxppi3ggxl0jqiruhmnwu] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_corplegalentities] CHECK CONSTRAINT [fk_q6i6wxppi3ggxl0jqiruhmnwu]
GO
ALTER TABLE [dbo].[mpay_corplegalentities_nls]  WITH CHECK ADD  CONSTRAINT [fk_1m7drp1dymux04hpre2k051cv] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_corplegalentities_nls] CHECK CONSTRAINT [fk_1m7drp1dymux04hpre2k051cv]
GO
ALTER TABLE [dbo].[mpay_corplegalentities_nls]  WITH CHECK ADD  CONSTRAINT [fk_6aaffm8wsbipxarqkyp5asswt] FOREIGN KEY([legalentityid])
REFERENCES [dbo].[mpay_corplegalentities] ([id])
GO
ALTER TABLE [dbo].[mpay_corplegalentities_nls] CHECK CONSTRAINT [fk_6aaffm8wsbipxarqkyp5asswt]
GO
ALTER TABLE [dbo].[mpay_corplegalentities_nls]  WITH CHECK ADD  CONSTRAINT [fk_q74pms1o2xv7t6yxi25b7xlkq] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_corplegalentities_nls] CHECK CONSTRAINT [fk_q74pms1o2xv7t6yxi25b7xlkq]
GO
ALTER TABLE [dbo].[mpay_corpoarteservices]  WITH CHECK ADD  CONSTRAINT [fk_6vl72a8y66k5ssslb128uokmj] FOREIGN KEY([servicecategoryid])
REFERENCES [dbo].[mpay_servicescategories] ([id])
GO
ALTER TABLE [dbo].[mpay_corpoarteservices] CHECK CONSTRAINT [fk_6vl72a8y66k5ssslb128uokmj]
GO
ALTER TABLE [dbo].[mpay_corpoarteservices]  WITH CHECK ADD  CONSTRAINT [fk_cmxc8482prjmfm7v200o0ung4] FOREIGN KEY([refcorporateid])
REFERENCES [dbo].[mpay_corporates] ([id])
GO
ALTER TABLE [dbo].[mpay_corpoarteservices] CHECK CONSTRAINT [fk_cmxc8482prjmfm7v200o0ung4]
GO
ALTER TABLE [dbo].[mpay_corpoarteservices]  WITH CHECK ADD  CONSTRAINT [fk_f3q6e80hlthvibrf3glsuadtg] FOREIGN KEY([transactionsizeid])
REFERENCES [dbo].[mpay_transactionsizes] ([id])
GO
ALTER TABLE [dbo].[mpay_corpoarteservices] CHECK CONSTRAINT [fk_f3q6e80hlthvibrf3glsuadtg]
GO
ALTER TABLE [dbo].[mpay_corpoarteservices]  WITH CHECK ADD  CONSTRAINT [fk_ghtgh999edt92d265ryehtm2v] FOREIGN KEY([paymenttypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_corpoarteservices] CHECK CONSTRAINT [fk_ghtgh999edt92d265ryehtm2v]
GO
ALTER TABLE [dbo].[mpay_corpoarteservices]  WITH CHECK ADD  CONSTRAINT [fk_lhqdw5x48t1loij8pk1dk81pt] FOREIGN KEY([adminserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_corpoarteservices] CHECK CONSTRAINT [fk_lhqdw5x48t1loij8pk1dk81pt]
GO
ALTER TABLE [dbo].[mpay_corpoarteservices]  WITH CHECK ADD  CONSTRAINT [fk_mjf89wgh3hl4clu7ye7hd2l4c] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_corpoarteservices] CHECK CONSTRAINT [fk_mjf89wgh3hl4clu7ye7hd2l4c]
GO
ALTER TABLE [dbo].[mpay_corpoarteservices]  WITH CHECK ADD  CONSTRAINT [fk_mvkn5ixjwkaidxoqe3f9pox17] FOREIGN KEY([refprofileid])
REFERENCES [dbo].[mpay_profiles] ([id])
GO
ALTER TABLE [dbo].[mpay_corpoarteservices] CHECK CONSTRAINT [fk_mvkn5ixjwkaidxoqe3f9pox17]
GO
ALTER TABLE [dbo].[mpay_corpoarteservices]  WITH CHECK ADD  CONSTRAINT [fk_n4m3i1ie1ftrebqgxvh694nlx] FOREIGN KEY([cityid])
REFERENCES [dbo].[mpay_cities] ([id])
GO
ALTER TABLE [dbo].[mpay_corpoarteservices] CHECK CONSTRAINT [fk_n4m3i1ie1ftrebqgxvh694nlx]
GO
ALTER TABLE [dbo].[mpay_corpoarteservices]  WITH CHECK ADD  CONSTRAINT [fk_ouinmokxpmii3ft4atsehbtvg] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_corpoarteservices] CHECK CONSTRAINT [fk_ouinmokxpmii3ft4atsehbtvg]
GO
ALTER TABLE [dbo].[mpay_corpoarteservices]  WITH CHECK ADD  CONSTRAINT [fk_ps5wew4uf40cmiqp9u0tymtn3] FOREIGN KEY([refmerchantcategorycodeid])
REFERENCES [dbo].[mpay_merchantcategorycodes] ([id])
GO
ALTER TABLE [dbo].[mpay_corpoarteservices] CHECK CONSTRAINT [fk_ps5wew4uf40cmiqp9u0tymtn3]
GO
ALTER TABLE [dbo].[mpay_corpoarteservices]  WITH CHECK ADD  CONSTRAINT [fk_s7k6iricnwgp8p068uhiqbxcl] FOREIGN KEY([servicetypeid])
REFERENCES [dbo].[mpay_servicetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_corpoarteservices] CHECK CONSTRAINT [fk_s7k6iricnwgp8p068uhiqbxcl]
GO
ALTER TABLE [dbo].[mpay_corpoarteservices]  WITH CHECK ADD  CONSTRAINT [fk_s9jr23y2ldbtf77vhhu4pxeex] FOREIGN KEY([notificationchannelid])
REFERENCES [dbo].[mpay_notificationchannels] ([id])
GO
ALTER TABLE [dbo].[mpay_corpoarteservices] CHECK CONSTRAINT [fk_s9jr23y2ldbtf77vhhu4pxeex]
GO
ALTER TABLE [dbo].[mpay_corpoarteservices]  WITH CHECK ADD  CONSTRAINT [fk_swf3qlnbaukr9kpijgaqef2h3] FOREIGN KEY([bankid])
REFERENCES [dbo].[mpay_banks] ([id])
GO
ALTER TABLE [dbo].[mpay_corpoarteservices] CHECK CONSTRAINT [fk_swf3qlnbaukr9kpijgaqef2h3]
GO
ALTER TABLE [dbo].[mpay_corporatedeviceitm]  WITH CHECK ADD  CONSTRAINT [fk_kco13ly4hsnadqa4ygn1l2w7] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_corporatedevicegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_corporatedeviceitm] CHECK CONSTRAINT [fk_kco13ly4hsnadqa4ygn1l2w7]
GO
ALTER TABLE [dbo].[mpay_corporatedevices]  WITH CHECK ADD  CONSTRAINT [fk_49xckftsqft6bm5kff9ppmlrp] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_corporatedevices] CHECK CONSTRAINT [fk_49xckftsqft6bm5kff9ppmlrp]
GO
ALTER TABLE [dbo].[mpay_corporatedevices]  WITH CHECK ADD  CONSTRAINT [fk_ca7d4wuvoq8yyse9mm5yxy8xh] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_corporatedevices] CHECK CONSTRAINT [fk_ca7d4wuvoq8yyse9mm5yxy8xh]
GO
ALTER TABLE [dbo].[mpay_corporatedevices]  WITH CHECK ADD  CONSTRAINT [fk_s71edqaj6h47ny553txpqv4ke] FOREIGN KEY([refcorporateserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_corporatedevices] CHECK CONSTRAINT [fk_s71edqaj6h47ny553txpqv4ke]
GO
ALTER TABLE [dbo].[mpay_corporatedevicetokens]  WITH CHECK ADD  CONSTRAINT [fk_14ipa6w6w5wqwq3slset656i2] FOREIGN KEY([corporatedeviceid])
REFERENCES [dbo].[mpay_corporatedevices] ([id])
GO
ALTER TABLE [dbo].[mpay_corporatedevicetokens] CHECK CONSTRAINT [fk_14ipa6w6w5wqwq3slset656i2]
GO
ALTER TABLE [dbo].[mpay_corporatedevicetokens]  WITH CHECK ADD  CONSTRAINT [fk_1s94c10te8n48nbviudtgex96] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_corporatedevicetokens] CHECK CONSTRAINT [fk_1s94c10te8n48nbviudtgex96]
GO
ALTER TABLE [dbo].[mpay_corporatedevicetokens]  WITH CHECK ADD  CONSTRAINT [fk_f2bgli5yrpquffj0bnma588rq] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_corporatedevicetokens] CHECK CONSTRAINT [fk_f2bgli5yrpquffj0bnma588rq]
GO
ALTER TABLE [dbo].[mpay_corporatedocumentitm]  WITH CHECK ADD  CONSTRAINT [fk_dm6xdmnko3933p4k6x88hnsc0] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_corporatedocumentgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_corporatedocumentitm] CHECK CONSTRAINT [fk_dm6xdmnko3933p4k6x88hnsc0]
GO
ALTER TABLE [dbo].[mpay_corporatedocuments]  WITH CHECK ADD  CONSTRAINT [fk_2a8oymnd3xungpa5npuuy4p4v] FOREIGN KEY([corporateid])
REFERENCES [dbo].[mpay_corporates] ([id])
GO
ALTER TABLE [dbo].[mpay_corporatedocuments] CHECK CONSTRAINT [fk_2a8oymnd3xungpa5npuuy4p4v]
GO
ALTER TABLE [dbo].[mpay_corporatedocuments]  WITH CHECK ADD  CONSTRAINT [fk_aio6hd2djt1oyiuev1tpeaxdl] FOREIGN KEY([documenttypeid])
REFERENCES [dbo].[mpay_attachmenttypes] ([id])
GO
ALTER TABLE [dbo].[mpay_corporatedocuments] CHECK CONSTRAINT [fk_aio6hd2djt1oyiuev1tpeaxdl]
GO
ALTER TABLE [dbo].[mpay_corporatedocuments]  WITH CHECK ADD  CONSTRAINT [fk_cu49aaf4nyffgorlhtktmer5c] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_corporatedocuments] CHECK CONSTRAINT [fk_cu49aaf4nyffgorlhtktmer5c]
GO
ALTER TABLE [dbo].[mpay_corporatedocuments]  WITH CHECK ADD  CONSTRAINT [fk_kf7lm7sem4qfgmstrtf83veem] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_corporatedocuments] CHECK CONSTRAINT [fk_kf7lm7sem4qfgmstrtf83veem]
GO
ALTER TABLE [dbo].[mpay_corporateitm]  WITH CHECK ADD  CONSTRAINT [fk_mnjkpexd13vgu728nto7dk03m] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_corporategrp] ([id])
GO
ALTER TABLE [dbo].[mpay_corporateitm] CHECK CONSTRAINT [fk_mnjkpexd13vgu728nto7dk03m]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_10ka8jim326ym1xvuucdf5byt] FOREIGN KEY([servicetypeid])
REFERENCES [dbo].[mpay_servicetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_10ka8jim326ym1xvuucdf5byt]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_14b2l42ct3eucenvoq9y1kr3l] FOREIGN KEY([paymenttypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_14b2l42ct3eucenvoq9y1kr3l]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_1f9g572moao8o0stwvny2u0ga] FOREIGN KEY([refchargeaccountid])
REFERENCES [dbo].[mpay_accounts] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_1f9g572moao8o0stwvny2u0ga]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_5canmy3bky9v0ptvjluxc5fe6] FOREIGN KEY([refmerchantcategorycodeid])
REFERENCES [dbo].[mpay_merchantcategorycodes] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_5canmy3bky9v0ptvjluxc5fe6]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_5te6gtpbb8oueu0umk50iafgh] FOREIGN KEY([cityid])
REFERENCES [dbo].[mpay_cities] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_5te6gtpbb8oueu0umk50iafgh]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_6ny6yqr2201w9qdysdsoqcf3o] FOREIGN KEY([idtypeid])
REFERENCES [dbo].[mpay_idtypes] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_6ny6yqr2201w9qdysdsoqcf3o]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_6xpslle5m8i6h9cuhjo87t2pe] FOREIGN KEY([reflastmsglogid])
REFERENCES [dbo].[mpay_mpclearintegmsglogs] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_6xpslle5m8i6h9cuhjo87t2pe]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_7b9wgkljrrd4woxq1ex2h93tp] FOREIGN KEY([servicecategoryid])
REFERENCES [dbo].[mpay_servicescategories] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_7b9wgkljrrd4woxq1ex2h93tp]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_7n7u2gih6dp5nu2pa2nfxqv5t] FOREIGN KEY([legalentityid])
REFERENCES [dbo].[mpay_corplegalentities] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_7n7u2gih6dp5nu2pa2nfxqv5t]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_7q11cspm54xw5tw808cho82mr] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_7q11cspm54xw5tw808cho82mr]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_9pr91cl7j6owigxmg0t8gkpdn] FOREIGN KEY([refcountryid])
REFERENCES [dbo].[JFW_COUNTRIES] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_9pr91cl7j6owigxmg0t8gkpdn]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_ak9bqtaehw18b5fy5byjx9fp4] FOREIGN KEY([preflangid])
REFERENCES [dbo].[mpay_languages] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_ak9bqtaehw18b5fy5byjx9fp4]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_cctc5vodsdxkclfs4i1ccnbh9] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_cctc5vodsdxkclfs4i1ccnbh9]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_d487pvvl99yebvcemtklhlek7] FOREIGN KEY([refprofileid])
REFERENCES [dbo].[mpay_profiles] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_d487pvvl99yebvcemtklhlek7]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_d7vnqjvt3f08c69qr88hk29ku] FOREIGN KEY([bankid])
REFERENCES [dbo].[mpay_banks] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_d7vnqjvt3f08c69qr88hk29ku]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_ek4bf290yf39dnvtgr3jcuihf] FOREIGN KEY([transactionsizeid])
REFERENCES [dbo].[mpay_transactionsizes] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_ek4bf290yf39dnvtgr3jcuihf]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_f81x45pfcghnitmkkwgwau887] FOREIGN KEY([instid])
REFERENCES [dbo].[mpay_intg_corp_reg_instrs] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_f81x45pfcghnitmkkwgwau887]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_gu2pdslwhq4gc6v3mlqm9l4y2] FOREIGN KEY([nationalityid])
REFERENCES [dbo].[JFW_COUNTRIES] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_gu2pdslwhq4gc6v3mlqm9l4y2]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_iijfobdlda6frj4ovirv3lddk] FOREIGN KEY([refaccountid])
REFERENCES [dbo].[mpay_accounts] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_iijfobdlda6frj4ovirv3lddk]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_irkn32b18ubr3hrrm4o00khyv] FOREIGN KEY([clienttypeid])
REFERENCES [dbo].[mpay_clienttypes] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_irkn32b18ubr3hrrm4o00khyv]
GO
ALTER TABLE [dbo].[mpay_corporates]  WITH CHECK ADD  CONSTRAINT [fk_qbnt7x0csor44rloyxn2kisks] FOREIGN KEY([servicenotificationchannelid])
REFERENCES [dbo].[mpay_notificationchannels] ([id])
GO
ALTER TABLE [dbo].[mpay_corporates] CHECK CONSTRAINT [fk_qbnt7x0csor44rloyxn2kisks]
GO
ALTER TABLE [dbo].[mpay_corporatestenants]  WITH CHECK ADD  CONSTRAINT [fk_1t1omsm5elhyhpju2019kvxvt] FOREIGN KEY([corporates_id])
REFERENCES [dbo].[mpay_corporates] ([id])
GO
ALTER TABLE [dbo].[mpay_corporatestenants] CHECK CONSTRAINT [fk_1t1omsm5elhyhpju2019kvxvt]
GO
ALTER TABLE [dbo].[mpay_corporatestenants]  WITH CHECK ADD  CONSTRAINT [fk_9k06g4f8kfn4a4dy48lgy6qli] FOREIGN KEY([tenants_id])
REFERENCES [dbo].[JFW_TENANT] ([ID])
GO
ALTER TABLE [dbo].[mpay_corporatestenants] CHECK CONSTRAINT [fk_9k06g4f8kfn4a4dy48lgy6qli]
GO
ALTER TABLE [dbo].[mpay_custintegactiontypes]  WITH CHECK ADD  CONSTRAINT [fk_b5p7nbtxlbwg20gke112en1ww] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_custintegactiontypes] CHECK CONSTRAINT [fk_b5p7nbtxlbwg20gke112en1ww]
GO
ALTER TABLE [dbo].[mpay_custintegactiontypes]  WITH CHECK ADD  CONSTRAINT [fk_otkcdaqtc1k9nvjr8dqdaosne] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_custintegactiontypes] CHECK CONSTRAINT [fk_otkcdaqtc1k9nvjr8dqdaosne]
GO
ALTER TABLE [dbo].[mpay_custintegactiontypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_gtthgm24hb4f19r88lpsq1k0a] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_custintegactiontypes_nls] CHECK CONSTRAINT [fk_gtthgm24hb4f19r88lpsq1k0a]
GO
ALTER TABLE [dbo].[mpay_custintegactiontypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_i817kdjokyf0yj2y02apgw1m5] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_custintegactiontypes_nls] CHECK CONSTRAINT [fk_i817kdjokyf0yj2y02apgw1m5]
GO
ALTER TABLE [dbo].[mpay_custintegactiontypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_m59r9iddox19htnoev9cdsr0] FOREIGN KEY([intgid])
REFERENCES [dbo].[mpay_custintegactiontypes] ([id])
GO
ALTER TABLE [dbo].[mpay_custintegactiontypes_nls] CHECK CONSTRAINT [fk_m59r9iddox19htnoev9cdsr0]
GO
ALTER TABLE [dbo].[mpay_custintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_33mmijwuj0irg5nnwjw3q0sxp] FOREIGN KEY([refmsglogid])
REFERENCES [dbo].[mpay_mpclearintegmsglogs] ([id])
GO
ALTER TABLE [dbo].[mpay_custintegmessages] CHECK CONSTRAINT [fk_33mmijwuj0irg5nnwjw3q0sxp]
GO
ALTER TABLE [dbo].[mpay_custintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_3dh7sf0n0qqfs8qaui4rj94qo] FOREIGN KEY([responseid])
REFERENCES [dbo].[mpay_mpclearresponsecodes] ([id])
GO
ALTER TABLE [dbo].[mpay_custintegmessages] CHECK CONSTRAINT [fk_3dh7sf0n0qqfs8qaui4rj94qo]
GO
ALTER TABLE [dbo].[mpay_custintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_6vdc2uj8v1q7ov0f1s44dpxpj] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_custintegmessages] CHECK CONSTRAINT [fk_6vdc2uj8v1q7ov0f1s44dpxpj]
GO
ALTER TABLE [dbo].[mpay_custintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_ayst9ge8hhwmkxdmfkdtjb5no] FOREIGN KEY([processrejectionid])
REFERENCES [dbo].[mpay_mpclearintegrjctreasons] ([id])
GO
ALTER TABLE [dbo].[mpay_custintegmessages] CHECK CONSTRAINT [fk_ayst9ge8hhwmkxdmfkdtjb5no]
GO
ALTER TABLE [dbo].[mpay_custintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_b5vaxdqidoncp4b9ly1jga1m7] FOREIGN KEY([refmobileid])
REFERENCES [dbo].[mpay_customermobiles] ([id])
GO
ALTER TABLE [dbo].[mpay_custintegmessages] CHECK CONSTRAINT [fk_b5vaxdqidoncp4b9ly1jga1m7]
GO
ALTER TABLE [dbo].[mpay_custintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_cn174jdv0dxdboqqycwdabug1] FOREIGN KEY([refcustomerid])
REFERENCES [dbo].[mpay_customers] ([id])
GO
ALTER TABLE [dbo].[mpay_custintegmessages] CHECK CONSTRAINT [fk_cn174jdv0dxdboqqycwdabug1]
GO
ALTER TABLE [dbo].[mpay_custintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_e81t3gg1c96o1vd64rp391004] FOREIGN KEY([integmsgtypeid])
REFERENCES [dbo].[mpay_mpclearintegmsgtypes] ([id])
GO
ALTER TABLE [dbo].[mpay_custintegmessages] CHECK CONSTRAINT [fk_e81t3gg1c96o1vd64rp391004]
GO
ALTER TABLE [dbo].[mpay_custintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_ey7r2yicvqe8phnubucaoqnqo] FOREIGN KEY([reforiginalmsgid])
REFERENCES [dbo].[mpay_custintegmessages] ([id])
GO
ALTER TABLE [dbo].[mpay_custintegmessages] CHECK CONSTRAINT [fk_ey7r2yicvqe8phnubucaoqnqo]
GO
ALTER TABLE [dbo].[mpay_custintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_flb7m0si1fbii4o85qjgxagn9] FOREIGN KEY([actiontypeid])
REFERENCES [dbo].[mpay_custintegactiontypes] ([id])
GO
ALTER TABLE [dbo].[mpay_custintegmessages] CHECK CONSTRAINT [fk_flb7m0si1fbii4o85qjgxagn9]
GO
ALTER TABLE [dbo].[mpay_custintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_l88ou72e95jbqinkhfa7j1htd] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_custintegmessages] CHECK CONSTRAINT [fk_l88ou72e95jbqinkhfa7j1htd]
GO
ALTER TABLE [dbo].[mpay_customerdeviceitm]  WITH CHECK ADD  CONSTRAINT [fk_pelgfkyx9s8wnq90r482yk2mf] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_customerdevicegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_customerdeviceitm] CHECK CONSTRAINT [fk_pelgfkyx9s8wnq90r482yk2mf]
GO
ALTER TABLE [dbo].[mpay_customerdevices]  WITH CHECK ADD  CONSTRAINT [fk_25pgngp9v5t7wlltodcwrnud] FOREIGN KEY([refcustomermobileid])
REFERENCES [dbo].[mpay_customermobiles] ([id])
GO
ALTER TABLE [dbo].[mpay_customerdevices] CHECK CONSTRAINT [fk_25pgngp9v5t7wlltodcwrnud]
GO
ALTER TABLE [dbo].[mpay_customerdevices]  WITH CHECK ADD  CONSTRAINT [fk_2f0ea0l88h2un4u7ksvbk1qhq] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_customerdevices] CHECK CONSTRAINT [fk_2f0ea0l88h2un4u7ksvbk1qhq]
GO
ALTER TABLE [dbo].[mpay_customerdevices]  WITH CHECK ADD  CONSTRAINT [fk_6sfi15q56wt5m93l60jupfn45] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_customerdevices] CHECK CONSTRAINT [fk_6sfi15q56wt5m93l60jupfn45]
GO
ALTER TABLE [dbo].[mpay_customerdevicetokens]  WITH CHECK ADD  CONSTRAINT [fk_f3qvlg8y5nl3mloporxuhakwl] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_customerdevicetokens] CHECK CONSTRAINT [fk_f3qvlg8y5nl3mloporxuhakwl]
GO
ALTER TABLE [dbo].[mpay_customerdevicetokens]  WITH CHECK ADD  CONSTRAINT [fk_f6dgdj6c1m14swfy9ljgb8u62] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_customerdevicetokens] CHECK CONSTRAINT [fk_f6dgdj6c1m14swfy9ljgb8u62]
GO
ALTER TABLE [dbo].[mpay_customerdevicetokens]  WITH CHECK ADD  CONSTRAINT [fk_qe0xmbx82ed311869h7xtjmr4] FOREIGN KEY([customerdeviceid])
REFERENCES [dbo].[mpay_customerdevices] ([id])
GO
ALTER TABLE [dbo].[mpay_customerdevicetokens] CHECK CONSTRAINT [fk_qe0xmbx82ed311869h7xtjmr4]
GO
ALTER TABLE [dbo].[mpay_customerdocumentitm]  WITH CHECK ADD  CONSTRAINT [fk_glt0g1v611d52whe7yan2dith] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_customerdocumentgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_customerdocumentitm] CHECK CONSTRAINT [fk_glt0g1v611d52whe7yan2dith]
GO
ALTER TABLE [dbo].[mpay_customerdocuments]  WITH CHECK ADD  CONSTRAINT [fk_546s3yxtw1vdxi64qwrhb4k6w] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_customerdocuments] CHECK CONSTRAINT [fk_546s3yxtw1vdxi64qwrhb4k6w]
GO
ALTER TABLE [dbo].[mpay_customerdocuments]  WITH CHECK ADD  CONSTRAINT [fk_h93r4uca7b4b8egayloyi5d8x] FOREIGN KEY([customerid])
REFERENCES [dbo].[mpay_customers] ([id])
GO
ALTER TABLE [dbo].[mpay_customerdocuments] CHECK CONSTRAINT [fk_h93r4uca7b4b8egayloyi5d8x]
GO
ALTER TABLE [dbo].[mpay_customerdocuments]  WITH CHECK ADD  CONSTRAINT [fk_hhgc5ckapmn9i367s8dam4kw0] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_customerdocuments] CHECK CONSTRAINT [fk_hhgc5ckapmn9i367s8dam4kw0]
GO
ALTER TABLE [dbo].[mpay_customerdocuments]  WITH CHECK ADD  CONSTRAINT [fk_j7ni947ergg98oqdqh3ypm07x] FOREIGN KEY([documenttypeid])
REFERENCES [dbo].[mpay_attachmenttypes] ([id])
GO
ALTER TABLE [dbo].[mpay_customerdocuments] CHECK CONSTRAINT [fk_j7ni947ergg98oqdqh3ypm07x]
GO
ALTER TABLE [dbo].[mpay_customeritm]  WITH CHECK ADD  CONSTRAINT [fk_fpq8lt2aoxi5776iudb2a9j2r] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_customergrp] ([id])
GO
ALTER TABLE [dbo].[mpay_customeritm] CHECK CONSTRAINT [fk_fpq8lt2aoxi5776iudb2a9j2r]
GO
ALTER TABLE [dbo].[mpay_customermobileitm]  WITH CHECK ADD  CONSTRAINT [fk_kpijgcbri9eaepa0x7xccrwob] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_customermobilegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_customermobileitm] CHECK CONSTRAINT [fk_kpijgcbri9eaepa0x7xccrwob]
GO
ALTER TABLE [dbo].[mpay_customermobiles]  WITH CHECK ADD  CONSTRAINT [fk_8oddvmvoy4gqpge56712v9r4o] FOREIGN KEY([channeltypeid])
REFERENCES [dbo].[mpay_channeltype] ([id])
GO
ALTER TABLE [dbo].[mpay_customermobiles] CHECK CONSTRAINT [fk_8oddvmvoy4gqpge56712v9r4o]
GO
ALTER TABLE [dbo].[mpay_customermobiles]  WITH CHECK ADD  CONSTRAINT [fk_8rthfwks5iwe59o3i1xh2j9j] FOREIGN KEY([transactionsizeid])
REFERENCES [dbo].[mpay_transactionsizes] ([id])
GO
ALTER TABLE [dbo].[mpay_customermobiles] CHECK CONSTRAINT [fk_8rthfwks5iwe59o3i1xh2j9j]
GO
ALTER TABLE [dbo].[mpay_customermobiles]  WITH CHECK ADD  CONSTRAINT [fk_b7dx7nsxa1g31y41mcfdi898t] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_customermobiles] CHECK CONSTRAINT [fk_b7dx7nsxa1g31y41mcfdi898t]
GO
ALTER TABLE [dbo].[mpay_customermobiles]  WITH CHECK ADD  CONSTRAINT [fk_fgt6qfrsfflrolsqsl0g86cox] FOREIGN KEY([refprofileid])
REFERENCES [dbo].[mpay_profiles] ([id])
GO
ALTER TABLE [dbo].[mpay_customermobiles] CHECK CONSTRAINT [fk_fgt6qfrsfflrolsqsl0g86cox]
GO
ALTER TABLE [dbo].[mpay_customermobiles]  WITH CHECK ADD  CONSTRAINT [fk_fk87jrxs4sbsjlikphug33fvx] FOREIGN KEY([bankid])
REFERENCES [dbo].[mpay_banks] ([id])
GO
ALTER TABLE [dbo].[mpay_customermobiles] CHECK CONSTRAINT [fk_fk87jrxs4sbsjlikphug33fvx]
GO
ALTER TABLE [dbo].[mpay_customermobiles]  WITH CHECK ADD  CONSTRAINT [fk_kbhgylwg3u7aqqy2ghm96ftue] FOREIGN KEY([referralid])
REFERENCES [dbo].[mpay_customermobiles] ([id])
GO
ALTER TABLE [dbo].[mpay_customermobiles] CHECK CONSTRAINT [fk_kbhgylwg3u7aqqy2ghm96ftue]
GO
ALTER TABLE [dbo].[mpay_customermobiles]  WITH CHECK ADD  CONSTRAINT [fk_oqprkiiwxs27prh5khv70cnk7] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_customermobiles] CHECK CONSTRAINT [fk_oqprkiiwxs27prh5khv70cnk7]
GO
ALTER TABLE [dbo].[mpay_customermobiles]  WITH CHECK ADD  CONSTRAINT [fk_s3ihvhoe1bjcacem14yfue8dp] FOREIGN KEY([refcustomerid])
REFERENCES [dbo].[mpay_customers] ([id])
GO
ALTER TABLE [dbo].[mpay_customermobiles] CHECK CONSTRAINT [fk_s3ihvhoe1bjcacem14yfue8dp]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_1f2hmg038r2jtd1htvqjo6w9d] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_1f2hmg038r2jtd1htvqjo6w9d]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_2kmns97yrs4le0tiiwpgtpu6c] FOREIGN KEY([reflastmsglogid])
REFERENCES [dbo].[mpay_mpclearintegmsglogs] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_2kmns97yrs4le0tiiwpgtpu6c]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_7lhiu1239sc1o4a0podc2l00i] FOREIGN KEY([clienttypeid])
REFERENCES [dbo].[mpay_clienttypes] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_7lhiu1239sc1o4a0podc2l00i]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_80f9ei5whq95yn7tqlfrhsvh3] FOREIGN KEY([nationalityid])
REFERENCES [dbo].[JFW_COUNTRIES] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_80f9ei5whq95yn7tqlfrhsvh3]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_8l4nb75j3su71l6kgdr3m4v5i] FOREIGN KEY([cityid])
REFERENCES [dbo].[mpay_cities] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_8l4nb75j3su71l6kgdr3m4v5i]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_9fgqs4dsw75qjsaskp8nc67o9] FOREIGN KEY([bankid])
REFERENCES [dbo].[mpay_banks] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_9fgqs4dsw75qjsaskp8nc67o9]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_bd472no6og9jpbkkp1p30i1ro] FOREIGN KEY([idtypeid])
REFERENCES [dbo].[mpay_idtypes] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_bd472no6og9jpbkkp1p30i1ro]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_dkhewjwqgtjixuv51p68c1pwh] FOREIGN KEY([refprofileid])
REFERENCES [dbo].[mpay_profiles] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_dkhewjwqgtjixuv51p68c1pwh]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_ffax2dndihxec6uk51rw9et7l] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_ffax2dndihxec6uk51rw9et7l]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_j0f3t9f3lepfi2abcj36uf8b9] FOREIGN KEY([bulkregistrationid])
REFERENCES [dbo].[mpay_bulkregistration] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_j0f3t9f3lepfi2abcj36uf8b9]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_lokq49by0u3niw65oa5dltisf] FOREIGN KEY([ekycstatus])
REFERENCES [dbo].[MPAY_EKYCSTATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_lokq49by0u3niw65oa5dltisf]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_ltc3m93er0impfqcrbn16hjsd] FOREIGN KEY([instid])
REFERENCES [dbo].[mpay_intg_cust_reg_instrs] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_ltc3m93er0impfqcrbn16hjsd]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_n7l7c4hokvcjqolbu0a117by5] FOREIGN KEY([transactionsizeid])
REFERENCES [dbo].[mpay_transactionsizes] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_n7l7c4hokvcjqolbu0a117by5]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_ogqgkqj4dua1yuqvuqvpgcvxa] FOREIGN KEY([areaid])
REFERENCES [dbo].[mpay_cityareas] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_ogqgkqj4dua1yuqvuqvpgcvxa]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_rlk1j6ir3dk73u1a9i4ua343v] FOREIGN KEY([beneficiaryidtypeid])
REFERENCES [dbo].[mpay_idtypes] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_rlk1j6ir3dk73u1a9i4ua343v]
GO
ALTER TABLE [dbo].[mpay_customers]  WITH CHECK ADD  CONSTRAINT [fk_tlxpyd64d19wuww4lwtpewb8c] FOREIGN KEY([preflangid])
REFERENCES [dbo].[mpay_languages] ([id])
GO
ALTER TABLE [dbo].[mpay_customers] CHECK CONSTRAINT [fk_tlxpyd64d19wuww4lwtpewb8c]
GO
ALTER TABLE [dbo].[mpay_endpointoperations]  WITH CHECK ADD  CONSTRAINT [fk_7m26g93g7tdu9tbgtwhi3vu7r] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_endpointoperations] CHECK CONSTRAINT [fk_7m26g93g7tdu9tbgtwhi3vu7r]
GO
ALTER TABLE [dbo].[mpay_endpointoperations]  WITH CHECK ADD  CONSTRAINT [fk_g3wa1236twcdtu38mle7ha83t] FOREIGN KEY([messagetypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_endpointoperations] CHECK CONSTRAINT [fk_g3wa1236twcdtu38mle7ha83t]
GO
ALTER TABLE [dbo].[mpay_endpointoperations]  WITH CHECK ADD  CONSTRAINT [fk_pt6osvmf6hfhxiotyd2xkvwd5] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_endpointoperations] CHECK CONSTRAINT [fk_pt6osvmf6hfhxiotyd2xkvwd5]
GO
ALTER TABLE [dbo].[mpay_epaystatuscodeitm]  WITH CHECK ADD  CONSTRAINT [fk_238b2j8j8nklp29t52014df3m] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_epaystatuscodegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_epaystatuscodeitm] CHECK CONSTRAINT [fk_238b2j8j8nklp29t52014df3m]
GO
ALTER TABLE [dbo].[mpay_epaystatuscodes]  WITH CHECK ADD  CONSTRAINT [fk_3x45i2swbqvbqhntrdb0gbra0] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_epaystatuscodes] CHECK CONSTRAINT [fk_3x45i2swbqvbqhntrdb0gbra0]
GO
ALTER TABLE [dbo].[mpay_epaystatuscodes]  WITH CHECK ADD  CONSTRAINT [fk_klvdccwkylnpgaoaskxqawha5] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_epaystatuscodes] CHECK CONSTRAINT [fk_klvdccwkylnpgaoaskxqawha5]
GO
ALTER TABLE [dbo].[mpay_epaystatuscodes_nls]  WITH CHECK ADD  CONSTRAINT [fk_env35b2gfrmfic3n2srmoe8e] FOREIGN KEY([sttsid])
REFERENCES [dbo].[mpay_epaystatuscodes] ([id])
GO
ALTER TABLE [dbo].[mpay_epaystatuscodes_nls] CHECK CONSTRAINT [fk_env35b2gfrmfic3n2srmoe8e]
GO
ALTER TABLE [dbo].[mpay_epaystatuscodes_nls]  WITH CHECK ADD  CONSTRAINT [fk_o8vk10g8vif03m22kkh6ae0eg] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_epaystatuscodes_nls] CHECK CONSTRAINT [fk_o8vk10g8vif03m22kkh6ae0eg]
GO
ALTER TABLE [dbo].[mpay_epaystatuscodes_nls]  WITH CHECK ADD  CONSTRAINT [fk_ogmvbkouhqmvl4lrwq6y5j4qu] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_epaystatuscodes_nls] CHECK CONSTRAINT [fk_ogmvbkouhqmvl4lrwq6y5j4qu]
GO
ALTER TABLE [dbo].[mpay_externalintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_6alpxptqvb0u6yg4b13drxnve] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_externalintegmessages] CHECK CONSTRAINT [fk_6alpxptqvb0u6yg4b13drxnve]
GO
ALTER TABLE [dbo].[mpay_externalintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_audlr1u4l3phcv6jr6ya8ch2x] FOREIGN KEY([refstatusid])
REFERENCES [dbo].[mpay_processingstatuses] ([id])
GO
ALTER TABLE [dbo].[mpay_externalintegmessages] CHECK CONSTRAINT [fk_audlr1u4l3phcv6jr6ya8ch2x]
GO
ALTER TABLE [dbo].[mpay_externalintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_ns7r5ryk1g9vvd8vsy38m8lwm] FOREIGN KEY([refserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_externalintegmessages] CHECK CONSTRAINT [fk_ns7r5ryk1g9vvd8vsy38m8lwm]
GO
ALTER TABLE [dbo].[mpay_externalintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_s5uyefcsxaivluhgywdsn3u0d] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_externalintegmessages] CHECK CONSTRAINT [fk_s5uyefcsxaivluhgywdsn3u0d]
GO
ALTER TABLE [dbo].[mpay_externalintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_t7g9npc027d8chfyh8vv348mt] FOREIGN KEY([refmessageid])
REFERENCES [dbo].[mpay_mpaymessages] ([id])
GO
ALTER TABLE [dbo].[mpay_externalintegmessages] CHECK CONSTRAINT [fk_t7g9npc027d8chfyh8vv348mt]
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_5h55qq77ofshfr7b3qtkxuy0y] FOREIGN KEY([reforiginalmsgid])
REFERENCES [dbo].[mpay_financialintegmsgs] ([id])
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs] CHECK CONSTRAINT [fk_5h55qq77ofshfr7b3qtkxuy0y]
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_6y6elfol2lh87221hi945tgli] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs] CHECK CONSTRAINT [fk_6y6elfol2lh87221hi945tgli]
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_8rybpkxa0b8vrf9p4apm3we8q] FOREIGN KEY([processrejectionid])
REFERENCES [dbo].[mpay_mpclearintegrjctreasons] ([id])
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs] CHECK CONSTRAINT [fk_8rybpkxa0b8vrf9p4apm3we8q]
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_b162ppxy0fry6xi7itd5tnk3x] FOREIGN KEY([integmsgtypeid])
REFERENCES [dbo].[mpay_mpclearintegmsgtypes] ([id])
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs] CHECK CONSTRAINT [fk_b162ppxy0fry6xi7itd5tnk3x]
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_e07bwgwmyfghfcofjn7pufi6] FOREIGN KEY([paymenttypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs] CHECK CONSTRAINT [fk_e07bwgwmyfghfcofjn7pufi6]
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_kuuqcf89c4f06i58o8ltfjtho] FOREIGN KEY([refmsglogid])
REFERENCES [dbo].[mpay_mpclearintegmsglogs] ([id])
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs] CHECK CONSTRAINT [fk_kuuqcf89c4f06i58o8ltfjtho]
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_lkc4aneuy304xypmpao0ha9y7] FOREIGN KEY([responseid])
REFERENCES [dbo].[mpay_mpclearresponsecodes] ([id])
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs] CHECK CONSTRAINT [fk_lkc4aneuy304xypmpao0ha9y7]
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_mboasm8iyd5do9kqyplyn6f59] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs] CHECK CONSTRAINT [fk_mboasm8iyd5do9kqyplyn6f59]
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_rx8cw1v6coa0vxhrlc8it7hpr] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_financialintegmsgs] CHECK CONSTRAINT [fk_rx8cw1v6coa0vxhrlc8it7hpr]
GO
ALTER TABLE [dbo].[mpay_idtypes]  WITH CHECK ADD  CONSTRAINT [fk_3rf3yysxqtlbda0opr6fnxlcx] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_idtypes] CHECK CONSTRAINT [fk_3rf3yysxqtlbda0opr6fnxlcx]
GO
ALTER TABLE [dbo].[mpay_idtypes]  WITH CHECK ADD  CONSTRAINT [fk_7et4blpglh9g53yjgyymf2otv] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_idtypes] CHECK CONSTRAINT [fk_7et4blpglh9g53yjgyymf2otv]
GO
ALTER TABLE [dbo].[mpay_idtypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_ai5a9kvvl343l3dkkbpdjg2ma] FOREIGN KEY([idtypeid])
REFERENCES [dbo].[mpay_idtypes] ([id])
GO
ALTER TABLE [dbo].[mpay_idtypes_nls] CHECK CONSTRAINT [fk_ai5a9kvvl343l3dkkbpdjg2ma]
GO
ALTER TABLE [dbo].[mpay_idtypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_jt75cfnbfx9hvaghwf1ualbhv] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_idtypes_nls] CHECK CONSTRAINT [fk_jt75cfnbfx9hvaghwf1ualbhv]
GO
ALTER TABLE [dbo].[mpay_idtypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_llwpaeoce2q1pd1m0qxjye4il] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_idtypes_nls] CHECK CONSTRAINT [fk_llwpaeoce2q1pd1m0qxjye4il]
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instritm]  WITH CHECK ADD  CONSTRAINT [fk_7sk8qxpcf4p93db0vheeeqqu9] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_intg_corp_reg_instrgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instritm] CHECK CONSTRAINT [fk_7sk8qxpcf4p93db0vheeeqqu9]
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs]  WITH CHECK ADD  CONSTRAINT [fk_9654gk61bsg6b1tc1u8v9tu4y] FOREIGN KEY([reginstrprocessingsttsid])
REFERENCES [dbo].[mpay_reg_instrs_stts] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs] CHECK CONSTRAINT [fk_9654gk61bsg6b1tc1u8v9tu4y]
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs]  WITH CHECK ADD  CONSTRAINT [fk_9tm9o0794bf8difgx8k6c2x4k] FOREIGN KEY([reasonid])
REFERENCES [dbo].[mpay_reasons] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs] CHECK CONSTRAINT [fk_9tm9o0794bf8difgx8k6c2x4k]
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs]  WITH CHECK ADD  CONSTRAINT [fk_bwil7k4vigab1kdqiywfa8qr6] FOREIGN KEY([refregfileid])
REFERENCES [dbo].[mpay_intg_reg_files] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs] CHECK CONSTRAINT [fk_bwil7k4vigab1kdqiywfa8qr6]
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs]  WITH CHECK ADD  CONSTRAINT [fk_c0s4tg5aqa0jk9waw1g8buygm] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs] CHECK CONSTRAINT [fk_c0s4tg5aqa0jk9waw1g8buygm]
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs]  WITH CHECK ADD  CONSTRAINT [fk_e3gcnbu8omk8e4levva458vmj] FOREIGN KEY([servicetypeid])
REFERENCES [dbo].[mpay_servicetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs] CHECK CONSTRAINT [fk_e3gcnbu8omk8e4levva458vmj]
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs]  WITH CHECK ADD  CONSTRAINT [fk_fkuvx7ti740nw1n5qvmcxdyv5] FOREIGN KEY([servicecategoryid])
REFERENCES [dbo].[mpay_servicescategories] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs] CHECK CONSTRAINT [fk_fkuvx7ti740nw1n5qvmcxdyv5]
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs]  WITH CHECK ADD  CONSTRAINT [fk_r1rcwgrvx74nr77lfspuic3qn] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_corp_reg_instrs] CHECK CONSTRAINT [fk_r1rcwgrvx74nr77lfspuic3qn]
GO
ALTER TABLE [dbo].[mpay_intg_cust_reg_instritm]  WITH CHECK ADD  CONSTRAINT [fk_hfh178aauq8ks5pmigk0w0umf] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_intg_cust_reg_instrgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_cust_reg_instritm] CHECK CONSTRAINT [fk_hfh178aauq8ks5pmigk0w0umf]
GO
ALTER TABLE [dbo].[mpay_intg_cust_reg_instrs]  WITH CHECK ADD  CONSTRAINT [fk_77mttkuohkkfoo6gnl115wwaq] FOREIGN KEY([refregfileid])
REFERENCES [dbo].[mpay_intg_reg_files] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_cust_reg_instrs] CHECK CONSTRAINT [fk_77mttkuohkkfoo6gnl115wwaq]
GO
ALTER TABLE [dbo].[mpay_intg_cust_reg_instrs]  WITH CHECK ADD  CONSTRAINT [fk_788jlf63cv2smxurgii1t8thh] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_cust_reg_instrs] CHECK CONSTRAINT [fk_788jlf63cv2smxurgii1t8thh]
GO
ALTER TABLE [dbo].[mpay_intg_cust_reg_instrs]  WITH CHECK ADD  CONSTRAINT [fk_9n4dhc4jfe9cioxrktkv0ie07] FOREIGN KEY([reginstrprocessingsttsid])
REFERENCES [dbo].[mpay_reg_instrs_stts] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_cust_reg_instrs] CHECK CONSTRAINT [fk_9n4dhc4jfe9cioxrktkv0ie07]
GO
ALTER TABLE [dbo].[mpay_intg_cust_reg_instrs]  WITH CHECK ADD  CONSTRAINT [fk_jyhk96doowcx8pidshyf44ns3] FOREIGN KEY([reasonid])
REFERENCES [dbo].[mpay_reasons] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_cust_reg_instrs] CHECK CONSTRAINT [fk_jyhk96doowcx8pidshyf44ns3]
GO
ALTER TABLE [dbo].[mpay_intg_cust_reg_instrs]  WITH CHECK ADD  CONSTRAINT [fk_qb16361nhbrm9qfuhanny3f6x] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_intg_cust_reg_instrs] CHECK CONSTRAINT [fk_qb16361nhbrm9qfuhanny3f6x]
GO
ALTER TABLE [dbo].[mpay_intg_reg_fileitm]  WITH CHECK ADD  CONSTRAINT [fk_9as4lti51793bo0g440nwbi3j] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_intg_reg_filegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_reg_fileitm] CHECK CONSTRAINT [fk_9as4lti51793bo0g440nwbi3j]
GO
ALTER TABLE [dbo].[mpay_intg_reg_files]  WITH CHECK ADD  CONSTRAINT [fk_78wkw76u21qnmayxk8c08q8r3] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_intg_reg_files] CHECK CONSTRAINT [fk_78wkw76u21qnmayxk8c08q8r3]
GO
ALTER TABLE [dbo].[mpay_intg_reg_files]  WITH CHECK ADD  CONSTRAINT [fk_en14ia2lrx0153fnbi27aol37] FOREIGN KEY([regfileprocessingsttsid])
REFERENCES [dbo].[mpay_reg_file_stts] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_reg_files] CHECK CONSTRAINT [fk_en14ia2lrx0153fnbi27aol37]
GO
ALTER TABLE [dbo].[mpay_intg_reg_files]  WITH CHECK ADD  CONSTRAINT [fk_nath5dr9p7q4ybvvagguydsr0] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_intg_reg_files] CHECK CONSTRAINT [fk_nath5dr9p7q4ybvvagguydsr0]
GO
ALTER TABLE [dbo].[mpay_jvdetails]  WITH CHECK ADD  CONSTRAINT [fk_7fw2lmahost19mo4v5bo61o8c] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_jvdetails] CHECK CONSTRAINT [fk_7fw2lmahost19mo4v5bo61o8c]
GO
ALTER TABLE [dbo].[mpay_jvdetails]  WITH CHECK ADD  CONSTRAINT [fk_fcof6j7ht8gequm4gg2nsqoog] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_jvdetails] CHECK CONSTRAINT [fk_fcof6j7ht8gequm4gg2nsqoog]
GO
ALTER TABLE [dbo].[mpay_jvdetails]  WITH CHECK ADD  CONSTRAINT [fk_jwwx4ygdhufi72y9y2ivbmg8v] FOREIGN KEY([jvtypeid])
REFERENCES [dbo].[mpay_jvtypes] ([id])
GO
ALTER TABLE [dbo].[mpay_jvdetails] CHECK CONSTRAINT [fk_jwwx4ygdhufi72y9y2ivbmg8v]
GO
ALTER TABLE [dbo].[mpay_jvdetails]  WITH CHECK ADD  CONSTRAINT [fk_lvft0mhboffxnrndgb1vskmpc] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_jvdetails] CHECK CONSTRAINT [fk_lvft0mhboffxnrndgb1vskmpc]
GO
ALTER TABLE [dbo].[mpay_jvdetails]  WITH CHECK ADD  CONSTRAINT [fk_mh73aj2mqqbuv7fni99xhjf9c] FOREIGN KEY([refaccountid])
REFERENCES [dbo].[mpay_accounts] ([id])
GO
ALTER TABLE [dbo].[mpay_jvdetails] CHECK CONSTRAINT [fk_mh73aj2mqqbuv7fni99xhjf9c]
GO
ALTER TABLE [dbo].[mpay_jvdetails]  WITH CHECK ADD  CONSTRAINT [fk_q1lt7xh5uw09h67xi5t514wrp] FOREIGN KEY([reftrsansactionid])
REFERENCES [dbo].[mpay_transactions] ([id])
GO
ALTER TABLE [dbo].[mpay_jvdetails] CHECK CONSTRAINT [fk_q1lt7xh5uw09h67xi5t514wrp]
GO
ALTER TABLE [dbo].[mpay_jvtypeitm]  WITH CHECK ADD  CONSTRAINT [fk_9nkur403ef8i19pt4deelgqj5] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_jvtypegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_jvtypeitm] CHECK CONSTRAINT [fk_9nkur403ef8i19pt4deelgqj5]
GO
ALTER TABLE [dbo].[mpay_jvtypes]  WITH CHECK ADD  CONSTRAINT [fk_3t1q730504pvcjfccxym9dqeh] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_jvtypes] CHECK CONSTRAINT [fk_3t1q730504pvcjfccxym9dqeh]
GO
ALTER TABLE [dbo].[mpay_jvtypes]  WITH CHECK ADD  CONSTRAINT [fk_85kn4uiwfyra69i8tjbefr62g] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_jvtypes] CHECK CONSTRAINT [fk_85kn4uiwfyra69i8tjbefr62g]
GO
ALTER TABLE [dbo].[mpay_jvtypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_epx3dmsgeygh9iuogy51pyuwc] FOREIGN KEY([jvtid])
REFERENCES [dbo].[mpay_jvtypes] ([id])
GO
ALTER TABLE [dbo].[mpay_jvtypes_nls] CHECK CONSTRAINT [fk_epx3dmsgeygh9iuogy51pyuwc]
GO
ALTER TABLE [dbo].[mpay_jvtypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_hr4dv04hj2313we0pihe52hql] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_jvtypes_nls] CHECK CONSTRAINT [fk_hr4dv04hj2313we0pihe52hql]
GO
ALTER TABLE [dbo].[mpay_jvtypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_mi87wcbp3c33u6bthpf26n2de] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_jvtypes_nls] CHECK CONSTRAINT [fk_mi87wcbp3c33u6bthpf26n2de]
GO
ALTER TABLE [dbo].[mpay_languages]  WITH CHECK ADD  CONSTRAINT [fk_5nwlxefq37vgce2h6xjylq7ri] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_languages] CHECK CONSTRAINT [fk_5nwlxefq37vgce2h6xjylq7ri]
GO
ALTER TABLE [dbo].[mpay_languages]  WITH CHECK ADD  CONSTRAINT [fk_8ohw82nasg5cksd79nu2al6dt] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_languages] CHECK CONSTRAINT [fk_8ohw82nasg5cksd79nu2al6dt]
GO
ALTER TABLE [dbo].[mpay_languages_nls]  WITH CHECK ADD  CONSTRAINT [fk_93f5kqijeqj52hv6scjeeecd6] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_languages_nls] CHECK CONSTRAINT [fk_93f5kqijeqj52hv6scjeeecd6]
GO
ALTER TABLE [dbo].[mpay_languages_nls]  WITH CHECK ADD  CONSTRAINT [fk_fdmyjt6cdvys0ua1i0h0fy389] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_languages_nls] CHECK CONSTRAINT [fk_fdmyjt6cdvys0ua1i0h0fy389]
GO
ALTER TABLE [dbo].[mpay_languages_nls]  WITH CHECK ADD  CONSTRAINT [fk_sl28in9jse4sp2myj3gqg1mae] FOREIGN KEY([langid])
REFERENCES [dbo].[mpay_languages] ([id])
GO
ALTER TABLE [dbo].[mpay_languages_nls] CHECK CONSTRAINT [fk_sl28in9jse4sp2myj3gqg1mae]
GO
ALTER TABLE [dbo].[mpay_limititm]  WITH CHECK ADD  CONSTRAINT [fk_216lv5qxfgvj49q07i5v0758n] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_limitgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_limititm] CHECK CONSTRAINT [fk_216lv5qxfgvj49q07i5v0758n]
GO
ALTER TABLE [dbo].[mpay_limits]  WITH CHECK ADD  CONSTRAINT [fk_6pnhkk6uw0fvb1qtiextuvjxi] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_limits] CHECK CONSTRAINT [fk_6pnhkk6uw0fvb1qtiextuvjxi]
GO
ALTER TABLE [dbo].[mpay_limits]  WITH CHECK ADD  CONSTRAINT [fk_l3t796octs9g1fhapuui3af7i] FOREIGN KEY([refschemeid])
REFERENCES [dbo].[mpay_limitsschemes] ([id])
GO
ALTER TABLE [dbo].[mpay_limits] CHECK CONSTRAINT [fk_l3t796octs9g1fhapuui3af7i]
GO
ALTER TABLE [dbo].[mpay_limits]  WITH CHECK ADD  CONSTRAINT [fk_mca0amvhtl4oubfgltufhcjcj] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_limits] CHECK CONSTRAINT [fk_mca0amvhtl4oubfgltufhcjcj]
GO
ALTER TABLE [dbo].[mpay_limits]  WITH CHECK ADD  CONSTRAINT [fk_p9060c0ftxiegtnnqgo8q55yg] FOREIGN KEY([reftypeid])
REFERENCES [dbo].[mpay_limitstypes] ([id])
GO
ALTER TABLE [dbo].[mpay_limits] CHECK CONSTRAINT [fk_p9060c0ftxiegtnnqgo8q55yg]
GO
ALTER TABLE [dbo].[mpay_limitsdetailitm]  WITH CHECK ADD  CONSTRAINT [fk_3r2d1rjm2nnobeltjwe0p2jsq] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_limitsdetailgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_limitsdetailitm] CHECK CONSTRAINT [fk_3r2d1rjm2nnobeltjwe0p2jsq]
GO
ALTER TABLE [dbo].[mpay_limitsdetails]  WITH CHECK ADD  CONSTRAINT [fk_3pq9i2qxxtjj3y28njoc5d1yc] FOREIGN KEY([msgtypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_limitsdetails] CHECK CONSTRAINT [fk_3pq9i2qxxtjj3y28njoc5d1yc]
GO
ALTER TABLE [dbo].[mpay_limitsdetails]  WITH CHECK ADD  CONSTRAINT [fk_a0tklk8nkkgf0v0mg8gneqe1c] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_limitsdetails] CHECK CONSTRAINT [fk_a0tklk8nkkgf0v0mg8gneqe1c]
GO
ALTER TABLE [dbo].[mpay_limitsdetails]  WITH CHECK ADD  CONSTRAINT [fk_asc8yoxv2s7aqb392xoi8eli6] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_limitsdetails] CHECK CONSTRAINT [fk_asc8yoxv2s7aqb392xoi8eli6]
GO
ALTER TABLE [dbo].[mpay_limitsdetails]  WITH CHECK ADD  CONSTRAINT [fk_q7iauyaqvj1tgsqq6piivk3lk] FOREIGN KEY([reflimitid])
REFERENCES [dbo].[mpay_limits] ([id])
GO
ALTER TABLE [dbo].[mpay_limitsdetails] CHECK CONSTRAINT [fk_q7iauyaqvj1tgsqq6piivk3lk]
GO
ALTER TABLE [dbo].[mpay_limitsschemeitm]  WITH CHECK ADD  CONSTRAINT [fk_pwbcn9nsvhlst3ugxj8koivc2] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_limitsschemegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_limitsschemeitm] CHECK CONSTRAINT [fk_pwbcn9nsvhlst3ugxj8koivc2]
GO
ALTER TABLE [dbo].[mpay_limitsschemes]  WITH CHECK ADD  CONSTRAINT [fk_2831a8hr19s9utjegkcbtsplo] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_limitsschemes] CHECK CONSTRAINT [fk_2831a8hr19s9utjegkcbtsplo]
GO
ALTER TABLE [dbo].[mpay_limitsschemes]  WITH CHECK ADD  CONSTRAINT [fk_8ew87uu769e518gllio2qugp4] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_limitsschemes] CHECK CONSTRAINT [fk_8ew87uu769e518gllio2qugp4]
GO
ALTER TABLE [dbo].[mpay_limitsschemes]  WITH CHECK ADD  CONSTRAINT [fk_ggtdsdtiicw0ih92w116q0dvf] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_limitsschemes] CHECK CONSTRAINT [fk_ggtdsdtiicw0ih92w116q0dvf]
GO
ALTER TABLE [dbo].[mpay_limitstypeitm]  WITH CHECK ADD  CONSTRAINT [fk_1sowtiqjivptdp7gcdgqbl9jf] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_limitstypegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_limitstypeitm] CHECK CONSTRAINT [fk_1sowtiqjivptdp7gcdgqbl9jf]
GO
ALTER TABLE [dbo].[mpay_limitstypes]  WITH CHECK ADD  CONSTRAINT [fk_2c9jls3w6u8wj95hpxsfbgn7d] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_limitstypes] CHECK CONSTRAINT [fk_2c9jls3w6u8wj95hpxsfbgn7d]
GO
ALTER TABLE [dbo].[mpay_limitstypes]  WITH CHECK ADD  CONSTRAINT [fk_okigc68pl6eeg7k33hh8ubqm3] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_limitstypes] CHECK CONSTRAINT [fk_okigc68pl6eeg7k33hh8ubqm3]
GO
ALTER TABLE [dbo].[mpay_limitstypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_5sngwdjgr2bc8hgaxewdhmbse] FOREIGN KEY([limitid])
REFERENCES [dbo].[mpay_limitstypes] ([id])
GO
ALTER TABLE [dbo].[mpay_limitstypes_nls] CHECK CONSTRAINT [fk_5sngwdjgr2bc8hgaxewdhmbse]
GO
ALTER TABLE [dbo].[mpay_limitstypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_82dbf0jwb8o2go3d0fmttl65g] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_limitstypes_nls] CHECK CONSTRAINT [fk_82dbf0jwb8o2go3d0fmttl65g]
GO
ALTER TABLE [dbo].[mpay_limitstypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_psytcudlhg7ynq7mb76px9rs2] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_limitstypes_nls] CHECK CONSTRAINT [fk_psytcudlhg7ynq7mb76px9rs2]
GO
ALTER TABLE [dbo].[mpay_merchantcategorycodes]  WITH CHECK ADD  CONSTRAINT [fk_2xblyjbro8r4trevxh2yfcj0c] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_merchantcategorycodes] CHECK CONSTRAINT [fk_2xblyjbro8r4trevxh2yfcj0c]
GO
ALTER TABLE [dbo].[mpay_merchantcategorycodes]  WITH CHECK ADD  CONSTRAINT [fk_ff5arw5ltm4tso1774wx7p0a] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_merchantcategorycodes] CHECK CONSTRAINT [fk_ff5arw5ltm4tso1774wx7p0a]
GO
ALTER TABLE [dbo].[mpay_messagetypeitm]  WITH CHECK ADD  CONSTRAINT [fk_8xtk112kwa3ql4f5uwn26s8ir] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_messagetypegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_messagetypeitm] CHECK CONSTRAINT [fk_8xtk112kwa3ql4f5uwn26s8ir]
GO
ALTER TABLE [dbo].[mpay_messagetypes]  WITH CHECK ADD  CONSTRAINT [fk_7cg75sqhatx5tn8f7ydfsxblr] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_messagetypes] CHECK CONSTRAINT [fk_7cg75sqhatx5tn8f7ydfsxblr]
GO
ALTER TABLE [dbo].[mpay_messagetypes]  WITH CHECK ADD  CONSTRAINT [fk_f468dk3njc9fof77qekj6ijqw] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_messagetypes] CHECK CONSTRAINT [fk_f468dk3njc9fof77qekj6ijqw]
GO
ALTER TABLE [dbo].[mpay_messagetypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_4a5q4u4dpqn40ncpgxcd79bv7] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_messagetypes_nls] CHECK CONSTRAINT [fk_4a5q4u4dpqn40ncpgxcd79bv7]
GO
ALTER TABLE [dbo].[mpay_messagetypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_f2apipsrqr37fdyba4hwn1533] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_messagetypes_nls] CHECK CONSTRAINT [fk_f2apipsrqr37fdyba4hwn1533]
GO
ALTER TABLE [dbo].[mpay_messagetypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_l0xlnarmexi2ph5pw4y1o3nih] FOREIGN KEY([msgid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_messagetypes_nls] CHECK CONSTRAINT [fk_l0xlnarmexi2ph5pw4y1o3nih]
GO
ALTER TABLE [dbo].[mpay_mobileaccountitm]  WITH CHECK ADD  CONSTRAINT [fk_o58bb2901cilgqbog0381u2kj] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_mobileaccountgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_mobileaccountitm] CHECK CONSTRAINT [fk_o58bb2901cilgqbog0381u2kj]
GO
ALTER TABLE [dbo].[mpay_mobileaccounts]  WITH CHECK ADD  CONSTRAINT [fk_5wihdj23xp08pi98jpa9qcgsn] FOREIGN KEY([refaccountid])
REFERENCES [dbo].[mpay_accounts] ([id])
GO
ALTER TABLE [dbo].[mpay_mobileaccounts] CHECK CONSTRAINT [fk_5wihdj23xp08pi98jpa9qcgsn]
GO
ALTER TABLE [dbo].[mpay_mobileaccounts]  WITH CHECK ADD  CONSTRAINT [fk_6gnoq6foqmc50mkk5rinij5w4] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_mobileaccounts] CHECK CONSTRAINT [fk_6gnoq6foqmc50mkk5rinij5w4]
GO
ALTER TABLE [dbo].[mpay_mobileaccounts]  WITH CHECK ADD  CONSTRAINT [fk_7bjp4qx6m9f2fb4jjkfib77c9] FOREIGN KEY([transactionsizeid])
REFERENCES [dbo].[mpay_transactionsizes] ([id])
GO
ALTER TABLE [dbo].[mpay_mobileaccounts] CHECK CONSTRAINT [fk_7bjp4qx6m9f2fb4jjkfib77c9]
GO
ALTER TABLE [dbo].[mpay_mobileaccounts]  WITH CHECK ADD  CONSTRAINT [fk_cplakto8y0ttkq5ouacc85vx4] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_mobileaccounts] CHECK CONSTRAINT [fk_cplakto8y0ttkq5ouacc85vx4]
GO
ALTER TABLE [dbo].[mpay_mobileaccounts]  WITH CHECK ADD  CONSTRAINT [fk_f961cgi68jvyyc099oexsrf7w] FOREIGN KEY([refprofileid])
REFERENCES [dbo].[mpay_profiles] ([id])
GO
ALTER TABLE [dbo].[mpay_mobileaccounts] CHECK CONSTRAINT [fk_f961cgi68jvyyc099oexsrf7w]
GO
ALTER TABLE [dbo].[mpay_mobileaccounts]  WITH CHECK ADD  CONSTRAINT [fk_gmc0gfnfqhc68e7oiu7hpkv2b] FOREIGN KEY([bankid])
REFERENCES [dbo].[mpay_banks] ([id])
GO
ALTER TABLE [dbo].[mpay_mobileaccounts] CHECK CONSTRAINT [fk_gmc0gfnfqhc68e7oiu7hpkv2b]
GO
ALTER TABLE [dbo].[mpay_mobileaccounts]  WITH CHECK ADD  CONSTRAINT [fk_jw8o7ikmt02sffa868of3ek1y] FOREIGN KEY([newprofileid])
REFERENCES [dbo].[mpay_profiles] ([id])
GO
ALTER TABLE [dbo].[mpay_mobileaccounts] CHECK CONSTRAINT [fk_jw8o7ikmt02sffa868of3ek1y]
GO
ALTER TABLE [dbo].[mpay_mobileaccounts]  WITH CHECK ADD  CONSTRAINT [fk_lj11y060mjrfku3vc4bacvj55] FOREIGN KEY([mobileid])
REFERENCES [dbo].[mpay_customermobiles] ([id])
GO
ALTER TABLE [dbo].[mpay_mobileaccounts] CHECK CONSTRAINT [fk_lj11y060mjrfku3vc4bacvj55]
GO
ALTER TABLE [dbo].[mpay_mpaymessages]  WITH CHECK ADD  CONSTRAINT [fk_17q8v7kxq2ducb4f2ijq81y5q] FOREIGN KEY([reasonid])
REFERENCES [dbo].[mpay_reasons] ([id])
GO
ALTER TABLE [dbo].[mpay_mpaymessages] CHECK CONSTRAINT [fk_17q8v7kxq2ducb4f2ijq81y5q]
GO
ALTER TABLE [dbo].[mpay_mpaymessages]  WITH CHECK ADD  CONSTRAINT [fk_2bl8lfgmn5ykjbnpcdhnk2y14] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_mpaymessages] CHECK CONSTRAINT [fk_2bl8lfgmn5ykjbnpcdhnk2y14]
GO
ALTER TABLE [dbo].[mpay_mpaymessages]  WITH CHECK ADD  CONSTRAINT [fk_4glmd2ivk2avckj2uteax9ukg] FOREIGN KEY([processingstatusid])
REFERENCES [dbo].[mpay_processingstatuses] ([id])
GO
ALTER TABLE [dbo].[mpay_mpaymessages] CHECK CONSTRAINT [fk_4glmd2ivk2avckj2uteax9ukg]
GO
ALTER TABLE [dbo].[mpay_mpaymessages]  WITH CHECK ADD  CONSTRAINT [fk_aajnny3hvfdyhocqtt1lehe8x] FOREIGN KEY([refoperationid])
REFERENCES [dbo].[mpay_endpointoperations] ([id])
GO
ALTER TABLE [dbo].[mpay_mpaymessages] CHECK CONSTRAINT [fk_aajnny3hvfdyhocqtt1lehe8x]
GO
ALTER TABLE [dbo].[mpay_mpaymessages]  WITH CHECK ADD  CONSTRAINT [fk_c99xxs1jvoyrvtfc39ytc6bgb] FOREIGN KEY([reftrsansactionid])
REFERENCES [dbo].[mpay_transactions] ([id])
GO
ALTER TABLE [dbo].[mpay_mpaymessages] CHECK CONSTRAINT [fk_c99xxs1jvoyrvtfc39ytc6bgb]
GO
ALTER TABLE [dbo].[mpay_mpaymessages]  WITH CHECK ADD  CONSTRAINT [fk_gk42e7gbw1ixyd29v9nu4np7w] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_mpaymessages] CHECK CONSTRAINT [fk_gk42e7gbw1ixyd29v9nu4np7w]
GO
ALTER TABLE [dbo].[mpay_mpaymessages]  WITH CHECK ADD  CONSTRAINT [fk_h7ttepq0idrxmv96jlfnqlxlp] FOREIGN KEY([bulkpaymentid])
REFERENCES [dbo].[mpay_bulkpayment] ([id])
GO
ALTER TABLE [dbo].[mpay_mpaymessages] CHECK CONSTRAINT [fk_h7ttepq0idrxmv96jlfnqlxlp]
GO
ALTER TABLE [dbo].[mpay_mpaymessages]  WITH CHECK ADD  CONSTRAINT [fk_kxqwd7a00vi55cuxcamhxkr2i] FOREIGN KEY([messagetypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_mpaymessages] CHECK CONSTRAINT [fk_kxqwd7a00vi55cuxcamhxkr2i]
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsglogs]  WITH CHECK ADD  CONSTRAINT [fk_chfuy4t6rq6vhh0vdwtt0n7l0] FOREIGN KEY([inwardtransactionid])
REFERENCES [dbo].[mpay_transactions] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsglogs] CHECK CONSTRAINT [fk_chfuy4t6rq6vhh0vdwtt0n7l0]
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsglogs]  WITH CHECK ADD  CONSTRAINT [fk_fl5trp932y4uc0pyj0utaqq60] FOREIGN KEY([reasonid])
REFERENCES [dbo].[mpay_mpclearintegrjctreasons] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsglogs] CHECK CONSTRAINT [fk_fl5trp932y4uc0pyj0utaqq60]
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsglogs]  WITH CHECK ADD  CONSTRAINT [fk_gwa00v8tnfqrvcox05x2u4gds] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsglogs] CHECK CONSTRAINT [fk_gwa00v8tnfqrvcox05x2u4gds]
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsglogs]  WITH CHECK ADD  CONSTRAINT [fk_rhkdolj8c84txmr8kqkbkm9l3] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsglogs] CHECK CONSTRAINT [fk_rhkdolj8c84txmr8kqkbkm9l3]
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsglogs]  WITH CHECK ADD  CONSTRAINT [fk_s4tc2jggcg1yetrfetrm550si] FOREIGN KEY([refmessageid])
REFERENCES [dbo].[mpay_mpaymessages] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsglogs] CHECK CONSTRAINT [fk_s4tc2jggcg1yetrfetrm550si]
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsgtypeitm]  WITH CHECK ADD  CONSTRAINT [fk_bgmrknltpferdy517ig66jrqr] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_mpclearintegmsgtypegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsgtypeitm] CHECK CONSTRAINT [fk_bgmrknltpferdy517ig66jrqr]
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsgtypes]  WITH CHECK ADD  CONSTRAINT [fk_7axid7nyw6jcu6w7sncgbfvxh] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsgtypes] CHECK CONSTRAINT [fk_7axid7nyw6jcu6w7sncgbfvxh]
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsgtypes]  WITH CHECK ADD  CONSTRAINT [fk_jv8x6p7sy1hxm3yd8liwbrt3q] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsgtypes] CHECK CONSTRAINT [fk_jv8x6p7sy1hxm3yd8liwbrt3q]
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsgtypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_1en2v50yvlcwdlvkm2587eegm] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsgtypes_nls] CHECK CONSTRAINT [fk_1en2v50yvlcwdlvkm2587eegm]
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsgtypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_520x5eve97kfuqgydckdv01ct] FOREIGN KEY([intgid])
REFERENCES [dbo].[mpay_mpclearintegmsgtypes] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsgtypes_nls] CHECK CONSTRAINT [fk_520x5eve97kfuqgydckdv01ct]
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsgtypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_fy922mdlf21ds8rnhlbnehh1a] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearintegmsgtypes_nls] CHECK CONSTRAINT [fk_fy922mdlf21ds8rnhlbnehh1a]
GO
ALTER TABLE [dbo].[mpay_mpclearintegrjctreasonitm]  WITH CHECK ADD  CONSTRAINT [fk_p10auxoloiof7i1gk4rkann8n] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_mpclearintegrjctreasongrp] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearintegrjctreasonitm] CHECK CONSTRAINT [fk_p10auxoloiof7i1gk4rkann8n]
GO
ALTER TABLE [dbo].[mpay_mpclearintegrjctreasons]  WITH CHECK ADD  CONSTRAINT [fk_idfcrl9y83fjd54j85pupwlsh] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_mpclearintegrjctreasons] CHECK CONSTRAINT [fk_idfcrl9y83fjd54j85pupwlsh]
GO
ALTER TABLE [dbo].[mpay_mpclearintegrjctreasons]  WITH CHECK ADD  CONSTRAINT [fk_ieeapnapwny0teo2v9pf99e1r] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearintegrjctreasons] CHECK CONSTRAINT [fk_ieeapnapwny0teo2v9pf99e1r]
GO
ALTER TABLE [dbo].[mpay_mpclearitm]  WITH CHECK ADD  CONSTRAINT [fk_m8u6ik9aq3cdmj49tjlo7itn8] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_mpcleargrp] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearitm] CHECK CONSTRAINT [fk_m8u6ik9aq3cdmj49tjlo7itn8]
GO
ALTER TABLE [dbo].[mpay_mpclearmessages]  WITH CHECK ADD  CONSTRAINT [fk_aj9mo2xei880e3pnnhr6g04ka] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_mpclearmessages] CHECK CONSTRAINT [fk_aj9mo2xei880e3pnnhr6g04ka]
GO
ALTER TABLE [dbo].[mpay_mpclearmessages]  WITH CHECK ADD  CONSTRAINT [fk_cmbkeewxhdtsbjwvoysvf26gj] FOREIGN KEY([reasonid])
REFERENCES [dbo].[mpay_mpclearintegrjctreasons] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearmessages] CHECK CONSTRAINT [fk_cmbkeewxhdtsbjwvoysvf26gj]
GO
ALTER TABLE [dbo].[mpay_mpclearmessages]  WITH CHECK ADD  CONSTRAINT [fk_d5qfv89n7rchx15sccshf8373] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearmessages] CHECK CONSTRAINT [fk_d5qfv89n7rchx15sccshf8373]
GO
ALTER TABLE [dbo].[mpay_mpclearmessages]  WITH CHECK ADD  CONSTRAINT [fk_lnrp093shykcsff6txdjq8kh9] FOREIGN KEY([refmessageid])
REFERENCES [dbo].[mpay_mpaymessages] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearmessages] CHECK CONSTRAINT [fk_lnrp093shykcsff6txdjq8kh9]
GO
ALTER TABLE [dbo].[mpay_mpclearreasonitm]  WITH CHECK ADD  CONSTRAINT [fk_pdw3f61txxbqdec31og5ps9ae] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_mpclearreasongrp] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearreasonitm] CHECK CONSTRAINT [fk_pdw3f61txxbqdec31og5ps9ae]
GO
ALTER TABLE [dbo].[mpay_mpclearreasons]  WITH CHECK ADD  CONSTRAINT [fk_5l7ab7baqcxu0s6d0m5myele1] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearreasons] CHECK CONSTRAINT [fk_5l7ab7baqcxu0s6d0m5myele1]
GO
ALTER TABLE [dbo].[mpay_mpclearreasons]  WITH CHECK ADD  CONSTRAINT [fk_mlu5609w00r4g7wfxlxfo4eeu] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_mpclearreasons] CHECK CONSTRAINT [fk_mlu5609w00r4g7wfxlxfo4eeu]
GO
ALTER TABLE [dbo].[mpay_mpclearresponsecodeitm]  WITH CHECK ADD  CONSTRAINT [fk_lf2g044pqpqlrqdio8de9kh25] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_mpclearresponsecodegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearresponsecodeitm] CHECK CONSTRAINT [fk_lf2g044pqpqlrqdio8de9kh25]
GO
ALTER TABLE [dbo].[mpay_mpclearresponsecodes]  WITH CHECK ADD  CONSTRAINT [fk_3cf45v9w0b11ma9cyhe1aqbr4] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_mpclearresponsecodes] CHECK CONSTRAINT [fk_3cf45v9w0b11ma9cyhe1aqbr4]
GO
ALTER TABLE [dbo].[mpay_mpclearresponsecodes]  WITH CHECK ADD  CONSTRAINT [fk_lq8hd25lnythqr44ox31vyx2g] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearresponsecodes] CHECK CONSTRAINT [fk_lq8hd25lnythqr44ox31vyx2g]
GO
ALTER TABLE [dbo].[mpay_mpclearresponsecodes_nls]  WITH CHECK ADD  CONSTRAINT [fk_9j6xd29ak73dflvoql9hvy165] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearresponsecodes_nls] CHECK CONSTRAINT [fk_9j6xd29ak73dflvoql9hvy165]
GO
ALTER TABLE [dbo].[mpay_mpclearresponsecodes_nls]  WITH CHECK ADD  CONSTRAINT [fk_bcougl1ixts0ogw7n4n29eqdm] FOREIGN KEY([intgid])
REFERENCES [dbo].[mpay_mpclearresponsecodes] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclearresponsecodes_nls] CHECK CONSTRAINT [fk_bcougl1ixts0ogw7n4n29eqdm]
GO
ALTER TABLE [dbo].[mpay_mpclearresponsecodes_nls]  WITH CHECK ADD  CONSTRAINT [fk_lu54u7sbfcx58g2pcatw2rf35] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_mpclearresponsecodes_nls] CHECK CONSTRAINT [fk_lu54u7sbfcx58g2pcatw2rf35]
GO
ALTER TABLE [dbo].[mpay_mpclrintegrjctreasons_nls]  WITH CHECK ADD  CONSTRAINT [fk_2l77pnlp12l9kh37wygjk2gq6] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclrintegrjctreasons_nls] CHECK CONSTRAINT [fk_2l77pnlp12l9kh37wygjk2gq6]
GO
ALTER TABLE [dbo].[mpay_mpclrintegrjctreasons_nls]  WITH CHECK ADD  CONSTRAINT [fk_hagmvqlnhf01jdh4pxa1e7072] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_mpclrintegrjctreasons_nls] CHECK CONSTRAINT [fk_hagmvqlnhf01jdh4pxa1e7072]
GO
ALTER TABLE [dbo].[mpay_mpclrintegrjctreasons_nls]  WITH CHECK ADD  CONSTRAINT [fk_q8iwep4t8v3luv9hxxth7dao7] FOREIGN KEY([intgid])
REFERENCES [dbo].[mpay_mpclearintegrjctreasons] ([id])
GO
ALTER TABLE [dbo].[mpay_mpclrintegrjctreasons_nls] CHECK CONSTRAINT [fk_q8iwep4t8v3luv9hxxth7dao7]
GO
ALTER TABLE [dbo].[mpay_networkmanactiontypes]  WITH CHECK ADD  CONSTRAINT [fk_503ehmkuhbj0wg4pj9drlm9e7] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanactiontypes] CHECK CONSTRAINT [fk_503ehmkuhbj0wg4pj9drlm9e7]
GO
ALTER TABLE [dbo].[mpay_networkmanactiontypes]  WITH CHECK ADD  CONSTRAINT [fk_9fwykxf4mk0425bmv52849nlm] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_networkmanactiontypes] CHECK CONSTRAINT [fk_9fwykxf4mk0425bmv52849nlm]
GO
ALTER TABLE [dbo].[mpay_networkmanactiontypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_28v02nn5fjx46mmpltf3opxoq] FOREIGN KEY([intgid])
REFERENCES [dbo].[mpay_networkmanactiontypes] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanactiontypes_nls] CHECK CONSTRAINT [fk_28v02nn5fjx46mmpltf3opxoq]
GO
ALTER TABLE [dbo].[mpay_networkmanactiontypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_b5np7u80hbsua2t18prs5b6a1] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanactiontypes_nls] CHECK CONSTRAINT [fk_b5np7u80hbsua2t18prs5b6a1]
GO
ALTER TABLE [dbo].[mpay_networkmanactiontypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_fxtd7tfemdf7huno2liw2cgdo] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_networkmanactiontypes_nls] CHECK CONSTRAINT [fk_fxtd7tfemdf7huno2liw2cgdo]
GO
ALTER TABLE [dbo].[mpay_networkmanagementitm]  WITH CHECK ADD  CONSTRAINT [fk_b9f2tbi8b4gth9jdce4vsdhco] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_networkmanagementgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanagementitm] CHECK CONSTRAINT [fk_b9f2tbi8b4gth9jdce4vsdhco]
GO
ALTER TABLE [dbo].[mpay_networkmanagements]  WITH CHECK ADD  CONSTRAINT [fk_8axmg0r3qc961o2nqp34dexxd] FOREIGN KEY([reflastmsglogid])
REFERENCES [dbo].[mpay_mpclearintegmsglogs] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanagements] CHECK CONSTRAINT [fk_8axmg0r3qc961o2nqp34dexxd]
GO
ALTER TABLE [dbo].[mpay_networkmanagements]  WITH CHECK ADD  CONSTRAINT [fk_bgv8ahku2e2e830j4yuo2gres] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_networkmanagements] CHECK CONSTRAINT [fk_bgv8ahku2e2e830j4yuo2gres]
GO
ALTER TABLE [dbo].[mpay_networkmanagements]  WITH CHECK ADD  CONSTRAINT [fk_i102k84ssgc224113cp3ir5dx] FOREIGN KEY([operationtypeid])
REFERENCES [dbo].[mpay_networkmanactiontypes] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanagements] CHECK CONSTRAINT [fk_i102k84ssgc224113cp3ir5dx]
GO
ALTER TABLE [dbo].[mpay_networkmanagements]  WITH CHECK ADD  CONSTRAINT [fk_kdftq0xkwcjuvof46gm2boxn4] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanagements] CHECK CONSTRAINT [fk_kdftq0xkwcjuvof46gm2boxn4]
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_1lnyluaxyq77qtqbeoc3u67ci] FOREIGN KEY([actiontypeid])
REFERENCES [dbo].[mpay_networkmanactiontypes] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs] CHECK CONSTRAINT [fk_1lnyluaxyq77qtqbeoc3u67ci]
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_1tdl62wlbiiusi4rfft76c0vi] FOREIGN KEY([refmsglogid])
REFERENCES [dbo].[mpay_mpclearintegmsglogs] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs] CHECK CONSTRAINT [fk_1tdl62wlbiiusi4rfft76c0vi]
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_5n6kxupwrk8jaup0m6yl5en9l] FOREIGN KEY([responseid])
REFERENCES [dbo].[mpay_mpclearresponsecodes] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs] CHECK CONSTRAINT [fk_5n6kxupwrk8jaup0m6yl5en9l]
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_9jrfqq8kq93a43xr7ge794wmb] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs] CHECK CONSTRAINT [fk_9jrfqq8kq93a43xr7ge794wmb]
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_l2yp7uexqf9er4x2ul4ltc3ah] FOREIGN KEY([integmsgtypeid])
REFERENCES [dbo].[mpay_mpclearintegmsgtypes] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs] CHECK CONSTRAINT [fk_l2yp7uexqf9er4x2ul4ltc3ah]
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_lroypyn50ixty00y2m24x9m5f] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs] CHECK CONSTRAINT [fk_lroypyn50ixty00y2m24x9m5f]
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_m5qyeo5fvjalpjbtick9outh8] FOREIGN KEY([processrejectionid])
REFERENCES [dbo].[mpay_mpclearintegrjctreasons] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs] CHECK CONSTRAINT [fk_m5qyeo5fvjalpjbtick9outh8]
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs]  WITH CHECK ADD  CONSTRAINT [fk_p1mbfb6uar0pvgtunmmqxq3mi] FOREIGN KEY([reforiginalmsgid])
REFERENCES [dbo].[mpay_networkmanintegmsgs] ([id])
GO
ALTER TABLE [dbo].[mpay_networkmanintegmsgs] CHECK CONSTRAINT [fk_p1mbfb6uar0pvgtunmmqxq3mi]
GO
ALTER TABLE [dbo].[mpay_notificationchannelitm]  WITH CHECK ADD  CONSTRAINT [fk_eijcauxp3vwsbatc2b47ykhl6] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_notificationchannelgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationchannelitm] CHECK CONSTRAINT [fk_eijcauxp3vwsbatc2b47ykhl6]
GO
ALTER TABLE [dbo].[mpay_notificationchannels]  WITH CHECK ADD  CONSTRAINT [fk_g2sk4oalnway5u7fco9vx0rkf] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_notificationchannels] CHECK CONSTRAINT [fk_g2sk4oalnway5u7fco9vx0rkf]
GO
ALTER TABLE [dbo].[mpay_notificationchannels]  WITH CHECK ADD  CONSTRAINT [fk_nbea1fbgovvgqc3yy6wcrx7yc] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationchannels] CHECK CONSTRAINT [fk_nbea1fbgovvgqc3yy6wcrx7yc]
GO
ALTER TABLE [dbo].[mpay_notificationchannels_nls]  WITH CHECK ADD  CONSTRAINT [fk_a1mrlodf4mahxefdrh6oywndm] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_notificationchannels_nls] CHECK CONSTRAINT [fk_a1mrlodf4mahxefdrh6oywndm]
GO
ALTER TABLE [dbo].[mpay_notificationchannels_nls]  WITH CHECK ADD  CONSTRAINT [fk_lokq49by0u3niw65ot1dltisf] FOREIGN KEY([channelid])
REFERENCES [dbo].[mpay_notificationchannels] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationchannels_nls] CHECK CONSTRAINT [fk_lokq49by0u3niw65ot1dltisf]
GO
ALTER TABLE [dbo].[mpay_notificationchannels_nls]  WITH CHECK ADD  CONSTRAINT [fk_tkqbwjldiufunhlu74tb4maw1] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationchannels_nls] CHECK CONSTRAINT [fk_tkqbwjldiufunhlu74tb4maw1]
GO
ALTER TABLE [dbo].[mpay_notificationitm]  WITH CHECK ADD  CONSTRAINT [fk_irlcojeluvscdn3g7x0sf7l1k] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_notificationgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationitm] CHECK CONSTRAINT [fk_irlcojeluvscdn3g7x0sf7l1k]
GO
ALTER TABLE [dbo].[mpay_notifications]  WITH CHECK ADD  CONSTRAINT [fk_44nhfuhfkmjrqey69dlx2k1le] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_notifications] CHECK CONSTRAINT [fk_44nhfuhfkmjrqey69dlx2k1le]
GO
ALTER TABLE [dbo].[mpay_notifications]  WITH CHECK ADD  CONSTRAINT [fk_broinku57t6ht8vqqjwd0oajy] FOREIGN KEY([refchannelid])
REFERENCES [dbo].[mpay_notificationchannels] ([id])
GO
ALTER TABLE [dbo].[mpay_notifications] CHECK CONSTRAINT [fk_broinku57t6ht8vqqjwd0oajy]
GO
ALTER TABLE [dbo].[mpay_notifications]  WITH CHECK ADD  CONSTRAINT [fk_g0x3h4aljyrhu2hrsgegd9vp2] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_notifications] CHECK CONSTRAINT [fk_g0x3h4aljyrhu2hrsgegd9vp2]
GO
ALTER TABLE [dbo].[mpay_notifications]  WITH CHECK ADD  CONSTRAINT [fk_irldf6tk2fq0de1ja0unyi7a5] FOREIGN KEY([refmessageid])
REFERENCES [dbo].[mpay_mpaymessages] ([id])
GO
ALTER TABLE [dbo].[mpay_notifications] CHECK CONSTRAINT [fk_irldf6tk2fq0de1ja0unyi7a5]
GO
ALTER TABLE [dbo].[mpay_notificationserviceargitm]  WITH CHECK ADD  CONSTRAINT [fk_i0h5pbsatmoe0r37c5clrobjc] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_notificationservicearggrp] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationserviceargitm] CHECK CONSTRAINT [fk_i0h5pbsatmoe0r37c5clrobjc]
GO
ALTER TABLE [dbo].[mpay_notificationserviceargs]  WITH CHECK ADD  CONSTRAINT [fk_50qv1c8504en30ccwyu83l7cf] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_notificationserviceargs] CHECK CONSTRAINT [fk_50qv1c8504en30ccwyu83l7cf]
GO
ALTER TABLE [dbo].[mpay_notificationserviceargs]  WITH CHECK ADD  CONSTRAINT [fk_htu26ma62knl5v5ant1cm91yb] FOREIGN KEY([refserviceid])
REFERENCES [dbo].[mpay_notificationservices] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationserviceargs] CHECK CONSTRAINT [fk_htu26ma62knl5v5ant1cm91yb]
GO
ALTER TABLE [dbo].[mpay_notificationserviceargs]  WITH CHECK ADD  CONSTRAINT [fk_td6rc89m440gtwijjyoqi0gtn] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationserviceargs] CHECK CONSTRAINT [fk_td6rc89m440gtwijjyoqi0gtn]
GO
ALTER TABLE [dbo].[mpay_notificationserviceitm]  WITH CHECK ADD  CONSTRAINT [fk_apc727qbxv8jgf9ew4bgq4kwr] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_notificationservicegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationserviceitm] CHECK CONSTRAINT [fk_apc727qbxv8jgf9ew4bgq4kwr]
GO
ALTER TABLE [dbo].[mpay_notificationservices]  WITH CHECK ADD  CONSTRAINT [fk_5swhk477yq5cnt3n3jyp0w73r] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_notificationservices] CHECK CONSTRAINT [fk_5swhk477yq5cnt3n3jyp0w73r]
GO
ALTER TABLE [dbo].[mpay_notificationservices]  WITH CHECK ADD  CONSTRAINT [fk_8xs3yub2p0ibk31cknm3jhbyy] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationservices] CHECK CONSTRAINT [fk_8xs3yub2p0ibk31cknm3jhbyy]
GO
ALTER TABLE [dbo].[mpay_notificationtemplateitm]  WITH CHECK ADD  CONSTRAINT [fk_cl3vkcunqifokdh88spajiw53] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_notificationtemplategrp] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationtemplateitm] CHECK CONSTRAINT [fk_cl3vkcunqifokdh88spajiw53]
GO
ALTER TABLE [dbo].[mpay_notificationtemplates]  WITH NOCHECK ADD  CONSTRAINT [fk_1aopyie1ptjma5b2lwe4uc4af] FOREIGN KEY([refoperationid])
REFERENCES [dbo].[mpay_endpointoperations] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationtemplates] CHECK CONSTRAINT [fk_1aopyie1ptjma5b2lwe4uc4af]
GO
ALTER TABLE [dbo].[mpay_notificationtemplates]  WITH NOCHECK ADD  CONSTRAINT [fk_6n8atcjcc1ogwbhqewk9ti83w] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationtemplates] CHECK CONSTRAINT [fk_6n8atcjcc1ogwbhqewk9ti83w]
GO
ALTER TABLE [dbo].[mpay_notificationtemplates]  WITH NOCHECK ADD  CONSTRAINT [fk_8li5wbd3royv1kxpx6dhleu8a] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_notificationtemplates] CHECK CONSTRAINT [fk_8li5wbd3royv1kxpx6dhleu8a]
GO
ALTER TABLE [dbo].[mpay_notificationtemplates]  WITH NOCHECK ADD  CONSTRAINT [fk_aynitsnlwlx8jqo3mptmqnscm] FOREIGN KEY([reflanguageid])
REFERENCES [dbo].[mpay_languages] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationtemplates] CHECK CONSTRAINT [fk_aynitsnlwlx8jqo3mptmqnscm]
GO
ALTER TABLE [dbo].[mpay_notificationtemplates]  WITH NOCHECK ADD  CONSTRAINT [fk_omgabh77u0udj2h6wq11ucd9o] FOREIGN KEY([reftypeid])
REFERENCES [dbo].[mpay_notificationtypes] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationtemplates] CHECK CONSTRAINT [fk_omgabh77u0udj2h6wq11ucd9o]
GO
ALTER TABLE [dbo].[mpay_notificationtypeitm]  WITH CHECK ADD  CONSTRAINT [fk_fc10jxx1nx993v8j8hg5ih7qm] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_notificationtypegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationtypeitm] CHECK CONSTRAINT [fk_fc10jxx1nx993v8j8hg5ih7qm]
GO
ALTER TABLE [dbo].[mpay_notificationtypes]  WITH CHECK ADD  CONSTRAINT [fk_1sabv5e7h2s0u8uv91kcmm5wt] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_notificationtypes] CHECK CONSTRAINT [fk_1sabv5e7h2s0u8uv91kcmm5wt]
GO
ALTER TABLE [dbo].[mpay_notificationtypes]  WITH CHECK ADD  CONSTRAINT [fk_r9u2os6467ny1y26fc727xo3f] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationtypes] CHECK CONSTRAINT [fk_r9u2os6467ny1y26fc727xo3f]
GO
ALTER TABLE [dbo].[mpay_notificationtypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_69ek85ccy6jhxv30v5dpnw8u3] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationtypes_nls] CHECK CONSTRAINT [fk_69ek85ccy6jhxv30v5dpnw8u3]
GO
ALTER TABLE [dbo].[mpay_notificationtypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_jeoqxhl64hbbwepw2obr1eua1] FOREIGN KEY([typeid])
REFERENCES [dbo].[mpay_notificationtypes] ([id])
GO
ALTER TABLE [dbo].[mpay_notificationtypes_nls] CHECK CONSTRAINT [fk_jeoqxhl64hbbwepw2obr1eua1]
GO
ALTER TABLE [dbo].[mpay_notificationtypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_pveweoyci50r68kmw74x72f9f] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_notificationtypes_nls] CHECK CONSTRAINT [fk_pveweoyci50r68kmw74x72f9f]
GO
ALTER TABLE [dbo].[mpay_processingstatuses]  WITH CHECK ADD  CONSTRAINT [fk_eh092w5i9agly1qx8o0tbw2i3] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_processingstatuses] CHECK CONSTRAINT [fk_eh092w5i9agly1qx8o0tbw2i3]
GO
ALTER TABLE [dbo].[mpay_processingstatuses]  WITH CHECK ADD  CONSTRAINT [fk_plb9kd3b11b6mka34vfu2fc8n] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_processingstatuses] CHECK CONSTRAINT [fk_plb9kd3b11b6mka34vfu2fc8n]
GO
ALTER TABLE [dbo].[mpay_processingstatuses_nls]  WITH CHECK ADD  CONSTRAINT [fk_5wd4uho7wjiyk8cm8jnus161r] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_processingstatuses_nls] CHECK CONSTRAINT [fk_5wd4uho7wjiyk8cm8jnus161r]
GO
ALTER TABLE [dbo].[mpay_processingstatuses_nls]  WITH CHECK ADD  CONSTRAINT [fk_bcclxvbn2nwvpvfs6aix4pv5t] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_processingstatuses_nls] CHECK CONSTRAINT [fk_bcclxvbn2nwvpvfs6aix4pv5t]
GO
ALTER TABLE [dbo].[mpay_processingstatuses_nls]  WITH CHECK ADD  CONSTRAINT [fk_jg8e033d01infs2q5fdcp02lp] FOREIGN KEY([sttsid])
REFERENCES [dbo].[mpay_processingstatuses] ([id])
GO
ALTER TABLE [dbo].[mpay_processingstatuses_nls] CHECK CONSTRAINT [fk_jg8e033d01infs2q5fdcp02lp]
GO
ALTER TABLE [dbo].[mpay_processingstatusitm]  WITH CHECK ADD  CONSTRAINT [fk_2fawfffxm1c6vlrmktygvo4gp] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_processingstatusgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_processingstatusitm] CHECK CONSTRAINT [fk_2fawfffxm1c6vlrmktygvo4gp]
GO
ALTER TABLE [dbo].[mpay_profileitm]  WITH CHECK ADD  CONSTRAINT [fk_on3g5dlxf2wam1fh3emys10yx] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_profilegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_profileitm] CHECK CONSTRAINT [fk_on3g5dlxf2wam1fh3emys10yx]
GO
ALTER TABLE [dbo].[mpay_profiles]  WITH CHECK ADD  CONSTRAINT [fk_1w2orm3vyp16y9verx0vdxk4i] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_profiles] CHECK CONSTRAINT [fk_1w2orm3vyp16y9verx0vdxk4i]
GO
ALTER TABLE [dbo].[mpay_profiles]  WITH CHECK ADD  CONSTRAINT [fk_58gfc8cmg8tkp79kw307neyip] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_profiles] CHECK CONSTRAINT [fk_58gfc8cmg8tkp79kw307neyip]
GO
ALTER TABLE [dbo].[mpay_profiles]  WITH CHECK ADD  CONSTRAINT [fk_6tbyhwlxu8tkokhlje9gbudnk] FOREIGN KEY([commissionsschemeid])
REFERENCES [dbo].[mpay_commissionschemes] ([id])
GO
ALTER TABLE [dbo].[mpay_profiles] CHECK CONSTRAINT [fk_6tbyhwlxu8tkokhlje9gbudnk]
GO
ALTER TABLE [dbo].[mpay_profiles]  WITH CHECK ADD  CONSTRAINT [fk_80sw5o33r9b61uuibmdvqmkwm] FOREIGN KEY([requesttypesschemeid])
REFERENCES [dbo].[mpay_requesttypesscheme] ([id])
GO
ALTER TABLE [dbo].[mpay_profiles] CHECK CONSTRAINT [fk_80sw5o33r9b61uuibmdvqmkwm]
GO
ALTER TABLE [dbo].[mpay_profiles]  WITH CHECK ADD  CONSTRAINT [fk_irhtnh0jrbjfumomngrmu6xej] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_profiles] CHECK CONSTRAINT [fk_irhtnh0jrbjfumomngrmu6xej]
GO
ALTER TABLE [dbo].[mpay_profiles]  WITH CHECK ADD  CONSTRAINT [fk_j9o7a74mjyrmgma1gliarmyep] FOREIGN KEY([limitsschemeid])
REFERENCES [dbo].[mpay_limitsschemes] ([id])
GO
ALTER TABLE [dbo].[mpay_profiles] CHECK CONSTRAINT [fk_j9o7a74mjyrmgma1gliarmyep]
GO
ALTER TABLE [dbo].[mpay_profiles]  WITH CHECK ADD  CONSTRAINT [fk_q0swsdyquxvd487pp8hg4mmlk] FOREIGN KEY([taxschemeid])
REFERENCES [dbo].[mpay_taxschemes] ([id])
GO
ALTER TABLE [dbo].[mpay_profiles] CHECK CONSTRAINT [fk_q0swsdyquxvd487pp8hg4mmlk]
GO
ALTER TABLE [dbo].[mpay_profiles]  WITH CHECK ADD  CONSTRAINT [fk_tfufbjqydxhk6ggujs8ect0k2] FOREIGN KEY([chargesschemeid])
REFERENCES [dbo].[mpay_chargesschemes] ([id])
GO
ALTER TABLE [dbo].[mpay_profiles] CHECK CONSTRAINT [fk_tfufbjqydxhk6ggujs8ect0k2]
GO
ALTER TABLE [dbo].[mpay_pspdetailitm]  WITH CHECK ADD  CONSTRAINT [fk_pvrucad1ktcp0ahhsuny2l3ko] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_pspdetailgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_pspdetailitm] CHECK CONSTRAINT [fk_pvrucad1ktcp0ahhsuny2l3ko]
GO
ALTER TABLE [dbo].[mpay_pspdetails]  WITH CHECK ADD  CONSTRAINT [fk_6bt11xn4mddn6ldgruy4t1u9q] FOREIGN KEY([refpspid])
REFERENCES [dbo].[mpay_corporates] ([id])
GO
ALTER TABLE [dbo].[mpay_pspdetails] CHECK CONSTRAINT [fk_6bt11xn4mddn6ldgruy4t1u9q]
GO
ALTER TABLE [dbo].[mpay_pspdetails]  WITH CHECK ADD  CONSTRAINT [fk_frqpq41gq6dusrua6i0vi4fum] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_pspdetails] CHECK CONSTRAINT [fk_frqpq41gq6dusrua6i0vi4fum]
GO
ALTER TABLE [dbo].[mpay_pspdetails]  WITH CHECK ADD  CONSTRAINT [fk_gwxcwxb75g6hya1tc4ytonjp3] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_pspdetails] CHECK CONSTRAINT [fk_gwxcwxb75g6hya1tc4ytonjp3]
GO
ALTER TABLE [dbo].[mpay_pspdetails]  WITH CHECK ADD  CONSTRAINT [fk_q3t09ira2byce522i73xy3htc] FOREIGN KEY([refdifferencesaccountid])
REFERENCES [dbo].[mpay_accounts] ([id])
GO
ALTER TABLE [dbo].[mpay_pspdetails] CHECK CONSTRAINT [fk_q3t09ira2byce522i73xy3htc]
GO
ALTER TABLE [dbo].[mpay_pspdetails]  WITH CHECK ADD  CONSTRAINT [fk_svcg3fejgnhtjxlvwrn7shl6b] FOREIGN KEY([refclearingaccountid])
REFERENCES [dbo].[mpay_accounts] ([id])
GO
ALTER TABLE [dbo].[mpay_pspdetails] CHECK CONSTRAINT [fk_svcg3fejgnhtjxlvwrn7shl6b]
GO
ALTER TABLE [dbo].[mpay_pspitm]  WITH CHECK ADD  CONSTRAINT [fk_i10erqxc48p25moexbw8g6osb] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_pspgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_pspitm] CHECK CONSTRAINT [fk_i10erqxc48p25moexbw8g6osb]
GO
ALTER TABLE [dbo].[mpay_reasonitm]  WITH CHECK ADD  CONSTRAINT [fk_h8evv8qm7lfpcv1yrn2k3m1y1] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_reasongrp] ([id])
GO
ALTER TABLE [dbo].[mpay_reasonitm] CHECK CONSTRAINT [fk_h8evv8qm7lfpcv1yrn2k3m1y1]
GO
ALTER TABLE [dbo].[mpay_reasons]  WITH CHECK ADD  CONSTRAINT [fk_7y155r8o3226wamwgrdhsdhd] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_reasons] CHECK CONSTRAINT [fk_7y155r8o3226wamwgrdhsdhd]
GO
ALTER TABLE [dbo].[mpay_reasons]  WITH CHECK ADD  CONSTRAINT [fk_p6y3535faus80ju6lk6pfnxh5] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_reasons] CHECK CONSTRAINT [fk_p6y3535faus80ju6lk6pfnxh5]
GO
ALTER TABLE [dbo].[mpay_reasons_nls]  WITH CHECK ADD  CONSTRAINT [fk_9x3l9cc4v4s74lasy5cit0r79] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_reasons_nls] CHECK CONSTRAINT [fk_9x3l9cc4v4s74lasy5cit0r79]
GO
ALTER TABLE [dbo].[mpay_reasons_nls]  WITH CHECK ADD  CONSTRAINT [fk_gabgu0o6ytw0yrfkprfm936ji] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_reasons_nls] CHECK CONSTRAINT [fk_gabgu0o6ytw0yrfkprfm936ji]
GO
ALTER TABLE [dbo].[mpay_reasons_nls]  WITH CHECK ADD  CONSTRAINT [fk_j9in0w0vfodvuomr67ibkruyp] FOREIGN KEY([rsnid])
REFERENCES [dbo].[mpay_reasons] ([id])
GO
ALTER TABLE [dbo].[mpay_reasons_nls] CHECK CONSTRAINT [fk_j9in0w0vfodvuomr67ibkruyp]
GO
ALTER TABLE [dbo].[mpay_reg_file_sttitm]  WITH CHECK ADD  CONSTRAINT [fk_d3scvvx9xk83wtd8ia57hk4u9] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_reg_file_sttgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_reg_file_sttitm] CHECK CONSTRAINT [fk_d3scvvx9xk83wtd8ia57hk4u9]
GO
ALTER TABLE [dbo].[mpay_reg_file_stts]  WITH CHECK ADD  CONSTRAINT [fk_ckw5sikw7br27osfducho5uby] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_reg_file_stts] CHECK CONSTRAINT [fk_ckw5sikw7br27osfducho5uby]
GO
ALTER TABLE [dbo].[mpay_reg_file_stts]  WITH CHECK ADD  CONSTRAINT [fk_nevqvt5ey5nyxhj3kuhrfgs6h] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_reg_file_stts] CHECK CONSTRAINT [fk_nevqvt5ey5nyxhj3kuhrfgs6h]
GO
ALTER TABLE [dbo].[mpay_reg_instrs_sttitm]  WITH CHECK ADD  CONSTRAINT [fk_ob9375vyaam656ry7r4jox06r] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_reg_instrs_sttgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_reg_instrs_sttitm] CHECK CONSTRAINT [fk_ob9375vyaam656ry7r4jox06r]
GO
ALTER TABLE [dbo].[mpay_reg_instrs_stts]  WITH CHECK ADD  CONSTRAINT [fk_ra8whwotw8odclc5gmp0n16pw] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_reg_instrs_stts] CHECK CONSTRAINT [fk_ra8whwotw8odclc5gmp0n16pw]
GO
ALTER TABLE [dbo].[mpay_reg_instrs_stts]  WITH CHECK ADD  CONSTRAINT [fk_t089ysgto42slhcewi3oyeguc] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_reg_instrs_stts] CHECK CONSTRAINT [fk_t089ysgto42slhcewi3oyeguc]
GO
ALTER TABLE [dbo].[mpay_registrationotpitm]  WITH CHECK ADD  CONSTRAINT [fk_fvic61tph41mk3ekjx375ieqf] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_registrationotpgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_registrationotpitm] CHECK CONSTRAINT [fk_fvic61tph41mk3ekjx375ieqf]
GO
ALTER TABLE [dbo].[mpay_registrationotps]  WITH CHECK ADD  CONSTRAINT [fk_10gv49kygw32fcvps9jeua279] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_registrationotps] CHECK CONSTRAINT [fk_10gv49kygw32fcvps9jeua279]
GO
ALTER TABLE [dbo].[mpay_registrationotps]  WITH CHECK ADD  CONSTRAINT [fk_1yolqw42quh45pvj61p7pcwnl] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_registrationotps] CHECK CONSTRAINT [fk_1yolqw42quh45pvj61p7pcwnl]
GO
ALTER TABLE [dbo].[mpay_registrationotps]  WITH CHECK ADD  CONSTRAINT [fk_970gsdvfffb59nks5metf5ye7] FOREIGN KEY([messagetypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_registrationotps] CHECK CONSTRAINT [fk_970gsdvfffb59nks5metf5ye7]
GO
ALTER TABLE [dbo].[mpay_requesttypesdetailitm]  WITH CHECK ADD  CONSTRAINT [fk_6bfymw1n4u5asq6sgstwpxdk2] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_requesttypesdetailgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_requesttypesdetailitm] CHECK CONSTRAINT [fk_6bfymw1n4u5asq6sgstwpxdk2]
GO
ALTER TABLE [dbo].[mpay_requesttypesdetails]  WITH CHECK ADD  CONSTRAINT [fk_7x5arxkxfhrsl0eb6h2du42vg] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_requesttypesdetails] CHECK CONSTRAINT [fk_7x5arxkxfhrsl0eb6h2du42vg]
GO
ALTER TABLE [dbo].[mpay_requesttypesdetails]  WITH CHECK ADD  CONSTRAINT [fk_ht5yq571b0h7351xqkulitqny] FOREIGN KEY([refschemeid])
REFERENCES [dbo].[mpay_requesttypesscheme] ([id])
GO
ALTER TABLE [dbo].[mpay_requesttypesdetails] CHECK CONSTRAINT [fk_ht5yq571b0h7351xqkulitqny]
GO
ALTER TABLE [dbo].[mpay_requesttypesdetails]  WITH CHECK ADD  CONSTRAINT [fk_ptyr3657w92ff1yik1asi8he5] FOREIGN KEY([msgtypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_requesttypesdetails] CHECK CONSTRAINT [fk_ptyr3657w92ff1yik1asi8he5]
GO
ALTER TABLE [dbo].[mpay_requesttypesdetails]  WITH CHECK ADD  CONSTRAINT [fk_s9ibkd68vukhvs94dsu0bdjyn] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_requesttypesdetails] CHECK CONSTRAINT [fk_s9ibkd68vukhvs94dsu0bdjyn]
GO
ALTER TABLE [dbo].[mpay_requesttypesscheme]  WITH CHECK ADD  CONSTRAINT [fk_27pg58vc3qx8sy23u5gsm7rd9] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_requesttypesscheme] CHECK CONSTRAINT [fk_27pg58vc3qx8sy23u5gsm7rd9]
GO
ALTER TABLE [dbo].[mpay_requesttypesscheme]  WITH CHECK ADD  CONSTRAINT [fk_obsl2460lq3w1hxnf7mqw414s] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_requesttypesscheme] CHECK CONSTRAINT [fk_obsl2460lq3w1hxnf7mqw414s]
GO
ALTER TABLE [dbo].[mpay_requesttypesschemeitm]  WITH CHECK ADD  CONSTRAINT [fk_qogcvatpk9scnhqyvi29js6wx] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_requesttypesschemegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_requesttypesschemeitm] CHECK CONSTRAINT [fk_qogcvatpk9scnhqyvi29js6wx]
GO
ALTER TABLE [dbo].[mpay_saibotp]  WITH CHECK ADD  CONSTRAINT [fk_5odexmkchi14f6iktggeak2ss] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_saibotp] CHECK CONSTRAINT [fk_5odexmkchi14f6iktggeak2ss]
GO
ALTER TABLE [dbo].[mpay_saibotp]  WITH CHECK ADD  CONSTRAINT [fk_b3kms9gdukhevy4seri8xosv1] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_saibotp] CHECK CONSTRAINT [fk_b3kms9gdukhevy4seri8xosv1]
GO
ALTER TABLE [dbo].[mpay_saibotpitm]  WITH CHECK ADD  CONSTRAINT [fk_ms9odu3qs7x6gajrqq968rd8e] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_saibotpgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_saibotpitm] CHECK CONSTRAINT [fk_ms9odu3qs7x6gajrqq968rd8e]
GO
ALTER TABLE [dbo].[mpay_service_detail_rep]  WITH CHECK ADD  CONSTRAINT [fk_hm691lcov7sop53fod4dt8uto] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_service_detail_rep] CHECK CONSTRAINT [fk_hm691lcov7sop53fod4dt8uto]
GO
ALTER TABLE [dbo].[mpay_service_detail_rep]  WITH CHECK ADD  CONSTRAINT [fk_lpowe34y9rqphp1fxnjhw5wmy] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_service_detail_rep] CHECK CONSTRAINT [fk_lpowe34y9rqphp1fxnjhw5wmy]
GO
ALTER TABLE [dbo].[mpay_service_trans_rep]  WITH CHECK ADD  CONSTRAINT [fk_hlja1fel7k45ev6ud9hymo8dx] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_service_trans_rep] CHECK CONSTRAINT [fk_hlja1fel7k45ev6ud9hymo8dx]
GO
ALTER TABLE [dbo].[mpay_service_trans_rep]  WITH CHECK ADD  CONSTRAINT [fk_k16v8p96iubys5eagv6w7vgqe] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_service_trans_rep] CHECK CONSTRAINT [fk_k16v8p96iubys5eagv6w7vgqe]
GO
ALTER TABLE [dbo].[mpay_serviceaccountitm]  WITH CHECK ADD  CONSTRAINT [fk_i8hfl70svmiger0gw74hc8fi5] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_serviceaccountgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceaccountitm] CHECK CONSTRAINT [fk_i8hfl70svmiger0gw74hc8fi5]
GO
ALTER TABLE [dbo].[mpay_serviceaccounts]  WITH CHECK ADD  CONSTRAINT [fk_42whj5hk0d5q3oqb21lqpa2uw] FOREIGN KEY([transactionsizeid])
REFERENCES [dbo].[mpay_transactionsizes] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceaccounts] CHECK CONSTRAINT [fk_42whj5hk0d5q3oqb21lqpa2uw]
GO
ALTER TABLE [dbo].[mpay_serviceaccounts]  WITH CHECK ADD  CONSTRAINT [fk_455co74i38rff3jbue31yx0e0] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceaccounts] CHECK CONSTRAINT [fk_455co74i38rff3jbue31yx0e0]
GO
ALTER TABLE [dbo].[mpay_serviceaccounts]  WITH CHECK ADD  CONSTRAINT [fk_cuamhcbv53uwoae5nvdh4a5hf] FOREIGN KEY([newprofileid])
REFERENCES [dbo].[mpay_profiles] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceaccounts] CHECK CONSTRAINT [fk_cuamhcbv53uwoae5nvdh4a5hf]
GO
ALTER TABLE [dbo].[mpay_serviceaccounts]  WITH CHECK ADD  CONSTRAINT [fk_ilbcyyhf0n3nbg60paxj4pgo1] FOREIGN KEY([serviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceaccounts] CHECK CONSTRAINT [fk_ilbcyyhf0n3nbg60paxj4pgo1]
GO
ALTER TABLE [dbo].[mpay_serviceaccounts]  WITH CHECK ADD  CONSTRAINT [fk_jofvgwf0taj6932721f2b071k] FOREIGN KEY([refprofileid])
REFERENCES [dbo].[mpay_profiles] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceaccounts] CHECK CONSTRAINT [fk_jofvgwf0taj6932721f2b071k]
GO
ALTER TABLE [dbo].[mpay_serviceaccounts]  WITH CHECK ADD  CONSTRAINT [fk_k0vwfmur6eelopehb47cuk7tn] FOREIGN KEY([refaccountid])
REFERENCES [dbo].[mpay_accounts] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceaccounts] CHECK CONSTRAINT [fk_k0vwfmur6eelopehb47cuk7tn]
GO
ALTER TABLE [dbo].[mpay_serviceaccounts]  WITH CHECK ADD  CONSTRAINT [fk_ked420sebqvjl14mdel8tcyod] FOREIGN KEY([bankid])
REFERENCES [dbo].[mpay_banks] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceaccounts] CHECK CONSTRAINT [fk_ked420sebqvjl14mdel8tcyod]
GO
ALTER TABLE [dbo].[mpay_serviceaccounts]  WITH CHECK ADD  CONSTRAINT [fk_npgyxn0ukuuwd00blpgojvffp] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_serviceaccounts] CHECK CONSTRAINT [fk_npgyxn0ukuuwd00blpgojvffp]
GO
ALTER TABLE [dbo].[mpay_servicecashin]  WITH CHECK ADD  CONSTRAINT [fk_3qckyxgpy25jy61ethody8ju3] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_servicecashin] CHECK CONSTRAINT [fk_3qckyxgpy25jy61ethody8ju3]
GO
ALTER TABLE [dbo].[mpay_servicecashin]  WITH CHECK ADD  CONSTRAINT [fk_9eilm3ssr1tyhl806y89kcs8m] FOREIGN KEY([reflastmsglogid])
REFERENCES [dbo].[mpay_mpclearintegmsglogs] ([id])
GO
ALTER TABLE [dbo].[mpay_servicecashin] CHECK CONSTRAINT [fk_9eilm3ssr1tyhl806y89kcs8m]
GO
ALTER TABLE [dbo].[mpay_servicecashin]  WITH CHECK ADD  CONSTRAINT [fk_9ye2erfcqui6y0yk2xokk8f1t] FOREIGN KEY([refcorporateserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_servicecashin] CHECK CONSTRAINT [fk_9ye2erfcqui6y0yk2xokk8f1t]
GO
ALTER TABLE [dbo].[mpay_servicecashin]  WITH CHECK ADD  CONSTRAINT [fk_ajc1fpki38oy6l0sj033kjpur] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_servicecashin] CHECK CONSTRAINT [fk_ajc1fpki38oy6l0sj033kjpur]
GO
ALTER TABLE [dbo].[mpay_servicecashin]  WITH CHECK ADD  CONSTRAINT [fk_cfcp5kv0ekvwe21fhe3l96e7e] FOREIGN KEY([reasonid])
REFERENCES [dbo].[mpay_reasons] ([id])
GO
ALTER TABLE [dbo].[mpay_servicecashin] CHECK CONSTRAINT [fk_cfcp5kv0ekvwe21fhe3l96e7e]
GO
ALTER TABLE [dbo].[mpay_servicecashin]  WITH CHECK ADD  CONSTRAINT [fk_en5xnjygbtiepi0t3apg2j0mt] FOREIGN KEY([refmessageid])
REFERENCES [dbo].[mpay_mpaymessages] ([id])
GO
ALTER TABLE [dbo].[mpay_servicecashin] CHECK CONSTRAINT [fk_en5xnjygbtiepi0t3apg2j0mt]
GO
ALTER TABLE [dbo].[mpay_servicecashin]  WITH CHECK ADD  CONSTRAINT [fk_kqtbh10x7n463gieqf4m0tr4y] FOREIGN KEY([processingstatusid])
REFERENCES [dbo].[mpay_processingstatuses] ([id])
GO
ALTER TABLE [dbo].[mpay_servicecashin] CHECK CONSTRAINT [fk_kqtbh10x7n463gieqf4m0tr4y]
GO
ALTER TABLE [dbo].[mpay_servicecashin]  WITH CHECK ADD  CONSTRAINT [fk_phpn77tvububh8iap8fxgbi8l] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_servicecashin] CHECK CONSTRAINT [fk_phpn77tvububh8iap8fxgbi8l]
GO
ALTER TABLE [dbo].[mpay_servicecashout]  WITH CHECK ADD  CONSTRAINT [fk_4noqei38oywiqs2gn16g2gv98] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_servicecashout] CHECK CONSTRAINT [fk_4noqei38oywiqs2gn16g2gv98]
GO
ALTER TABLE [dbo].[mpay_servicecashout]  WITH CHECK ADD  CONSTRAINT [fk_650x3dy2sdpq855yv9e4c3d3n] FOREIGN KEY([refmessageid])
REFERENCES [dbo].[mpay_mpaymessages] ([id])
GO
ALTER TABLE [dbo].[mpay_servicecashout] CHECK CONSTRAINT [fk_650x3dy2sdpq855yv9e4c3d3n]
GO
ALTER TABLE [dbo].[mpay_servicecashout]  WITH CHECK ADD  CONSTRAINT [fk_h420fs8o0k2122u8cvtakhrdb] FOREIGN KEY([processingstatusid])
REFERENCES [dbo].[mpay_processingstatuses] ([id])
GO
ALTER TABLE [dbo].[mpay_servicecashout] CHECK CONSTRAINT [fk_h420fs8o0k2122u8cvtakhrdb]
GO
ALTER TABLE [dbo].[mpay_servicecashout]  WITH CHECK ADD  CONSTRAINT [fk_halpa7eo44s7h3q572pfgev6y] FOREIGN KEY([refcorporateserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_servicecashout] CHECK CONSTRAINT [fk_halpa7eo44s7h3q572pfgev6y]
GO
ALTER TABLE [dbo].[mpay_servicecashout]  WITH CHECK ADD  CONSTRAINT [fk_itsltltqugvyvn6vuvi8aevec] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_servicecashout] CHECK CONSTRAINT [fk_itsltltqugvyvn6vuvi8aevec]
GO
ALTER TABLE [dbo].[mpay_servicecashout]  WITH CHECK ADD  CONSTRAINT [fk_nqufvppe26melc403hxkcvbsy] FOREIGN KEY([reasonid])
REFERENCES [dbo].[mpay_reasons] ([id])
GO
ALTER TABLE [dbo].[mpay_servicecashout] CHECK CONSTRAINT [fk_nqufvppe26melc403hxkcvbsy]
GO
ALTER TABLE [dbo].[mpay_servicecashout]  WITH CHECK ADD  CONSTRAINT [fk_pl7fgniyam5iaf2f87dy6bwdl] FOREIGN KEY([reflastmsglogid])
REFERENCES [dbo].[mpay_mpclearintegmsglogs] ([id])
GO
ALTER TABLE [dbo].[mpay_servicecashout] CHECK CONSTRAINT [fk_pl7fgniyam5iaf2f87dy6bwdl]
GO
ALTER TABLE [dbo].[mpay_servicecashout]  WITH CHECK ADD  CONSTRAINT [fk_tipww0iiq0o5fh0x1np9jhkbl] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_servicecashout] CHECK CONSTRAINT [fk_tipww0iiq0o5fh0x1np9jhkbl]
GO
ALTER TABLE [dbo].[mpay_serviceintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_27jrmg2fvv658oie75oge29dq] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceintegmessages] CHECK CONSTRAINT [fk_27jrmg2fvv658oie75oge29dq]
GO
ALTER TABLE [dbo].[mpay_serviceintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_2x4nugqbra2adfvsygaarmwju] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_serviceintegmessages] CHECK CONSTRAINT [fk_2x4nugqbra2adfvsygaarmwju]
GO
ALTER TABLE [dbo].[mpay_serviceintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_2yvvlbe5jhsxeiw6e9854iiv] FOREIGN KEY([refstatusid])
REFERENCES [dbo].[mpay_processingstatuses] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceintegmessages] CHECK CONSTRAINT [fk_2yvvlbe5jhsxeiw6e9854iiv]
GO
ALTER TABLE [dbo].[mpay_serviceintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_eqwo2rfgcd57wu61gpvynj5j3] FOREIGN KEY([refserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceintegmessages] CHECK CONSTRAINT [fk_eqwo2rfgcd57wu61gpvynj5j3]
GO
ALTER TABLE [dbo].[mpay_serviceintegmessages]  WITH CHECK ADD  CONSTRAINT [fk_fobr93d0wf96vlrxx17xkhcxx] FOREIGN KEY([refmessageid])
REFERENCES [dbo].[mpay_mpaymessages] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceintegmessages] CHECK CONSTRAINT [fk_fobr93d0wf96vlrxx17xkhcxx]
GO
ALTER TABLE [dbo].[mpay_serviceintegreasonitm]  WITH CHECK ADD  CONSTRAINT [fk_ryw5vdfpa5nn6fb516j4wp0nl] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_serviceintegreasongrp] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceintegreasonitm] CHECK CONSTRAINT [fk_ryw5vdfpa5nn6fb516j4wp0nl]
GO
ALTER TABLE [dbo].[mpay_serviceintegreasons]  WITH CHECK ADD  CONSTRAINT [fk_j3tx7ljt6s44lihjvb89c0c7d] FOREIGN KEY([mpaymappingreasonid])
REFERENCES [dbo].[mpay_reasons] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceintegreasons] CHECK CONSTRAINT [fk_j3tx7ljt6s44lihjvb89c0c7d]
GO
ALTER TABLE [dbo].[mpay_serviceintegreasons]  WITH CHECK ADD  CONSTRAINT [fk_mw3vgdve5qt2tgty0f938sfwu] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceintegreasons] CHECK CONSTRAINT [fk_mw3vgdve5qt2tgty0f938sfwu]
GO
ALTER TABLE [dbo].[mpay_serviceintegreasons]  WITH CHECK ADD  CONSTRAINT [fk_qoeqklor3faks2q9srlaijl2s] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_serviceintegreasons] CHECK CONSTRAINT [fk_qoeqklor3faks2q9srlaijl2s]
GO
ALTER TABLE [dbo].[mpay_serviceintegreasons_nls]  WITH CHECK ADD  CONSTRAINT [fk_9g4fpmn51qdro9w6ribyjw1pa] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceintegreasons_nls] CHECK CONSTRAINT [fk_9g4fpmn51qdro9w6ribyjw1pa]
GO
ALTER TABLE [dbo].[mpay_serviceintegreasons_nls]  WITH CHECK ADD  CONSTRAINT [fk_lnulhefj7cfhd93j3j7cyit9y] FOREIGN KEY([rsnid])
REFERENCES [dbo].[mpay_serviceintegreasons] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceintegreasons_nls] CHECK CONSTRAINT [fk_lnulhefj7cfhd93j3j7cyit9y]
GO
ALTER TABLE [dbo].[mpay_serviceintegreasons_nls]  WITH CHECK ADD  CONSTRAINT [fk_osfl2nsnnko0hodmm8d78tgs] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_serviceintegreasons_nls] CHECK CONSTRAINT [fk_osfl2nsnnko0hodmm8d78tgs]
GO
ALTER TABLE [dbo].[mpay_serviceintegsettings]  WITH CHECK ADD  CONSTRAINT [fk_cywgvn3fwu346xu50xj0u5u0l] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceintegsettings] CHECK CONSTRAINT [fk_cywgvn3fwu346xu50xj0u5u0l]
GO
ALTER TABLE [dbo].[mpay_serviceintegsettings]  WITH CHECK ADD  CONSTRAINT [fk_nk7obm8hsk40mre698oksoyh3] FOREIGN KEY([refserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_serviceintegsettings] CHECK CONSTRAINT [fk_nk7obm8hsk40mre698oksoyh3]
GO
ALTER TABLE [dbo].[mpay_serviceintegsettings]  WITH CHECK ADD  CONSTRAINT [fk_pex1r6auvtusbgtwi6s5m4lo2] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_serviceintegsettings] CHECK CONSTRAINT [fk_pex1r6auvtusbgtwi6s5m4lo2]
GO
ALTER TABLE [dbo].[mpay_servicescategories]  WITH CHECK ADD  CONSTRAINT [fk_4n1fmgam437h3myrxysi7v6ku] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_servicescategories] CHECK CONSTRAINT [fk_4n1fmgam437h3myrxysi7v6ku]
GO
ALTER TABLE [dbo].[mpay_servicescategories]  WITH CHECK ADD  CONSTRAINT [fk_qolcww8aad87h86pr813a96hy] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_servicescategories] CHECK CONSTRAINT [fk_qolcww8aad87h86pr813a96hy]
GO
ALTER TABLE [dbo].[mpay_servicescategories_nls]  WITH CHECK ADD  CONSTRAINT [fk_1b8huxd1poh5ky53n7q280n4a] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_servicescategories_nls] CHECK CONSTRAINT [fk_1b8huxd1poh5ky53n7q280n4a]
GO
ALTER TABLE [dbo].[mpay_servicescategories_nls]  WITH CHECK ADD  CONSTRAINT [fk_7bql1iktxljjwf26ujx4n7gfq] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_servicescategories_nls] CHECK CONSTRAINT [fk_7bql1iktxljjwf26ujx4n7gfq]
GO
ALTER TABLE [dbo].[mpay_servicescategories_nls]  WITH CHECK ADD  CONSTRAINT [fk_jf88iqfnkddm53k2ynr04mn5q] FOREIGN KEY([servicecategoryid])
REFERENCES [dbo].[mpay_servicescategories] ([id])
GO
ALTER TABLE [dbo].[mpay_servicescategories_nls] CHECK CONSTRAINT [fk_jf88iqfnkddm53k2ynr04mn5q]
GO
ALTER TABLE [dbo].[mpay_servicetypes]  WITH CHECK ADD  CONSTRAINT [fk_eghiggbefhryws9n7m3b3j65h] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_servicetypes] CHECK CONSTRAINT [fk_eghiggbefhryws9n7m3b3j65h]
GO
ALTER TABLE [dbo].[mpay_servicetypes]  WITH CHECK ADD  CONSTRAINT [fk_qwl0gmmdu41fgrnexf3x8rkxg] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_servicetypes] CHECK CONSTRAINT [fk_qwl0gmmdu41fgrnexf3x8rkxg]
GO
ALTER TABLE [dbo].[mpay_servicetypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_dodkbk3gqsevgwbv3uml18ukt] FOREIGN KEY([servicetypeid])
REFERENCES [dbo].[mpay_servicetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_servicetypes_nls] CHECK CONSTRAINT [fk_dodkbk3gqsevgwbv3uml18ukt]
GO
ALTER TABLE [dbo].[mpay_servicetypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_jaa9bgqiyjaew1uml18cgb8be] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_servicetypes_nls] CHECK CONSTRAINT [fk_jaa9bgqiyjaew1uml18cgb8be]
GO
ALTER TABLE [dbo].[mpay_servicetypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_o6vrp86uanjhrnxr1ei62hulp] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_servicetypes_nls] CHECK CONSTRAINT [fk_o6vrp86uanjhrnxr1ei62hulp]
GO
ALTER TABLE [dbo].[mpay_sessionreport]  WITH CHECK ADD  CONSTRAINT [fk_3y6sqcqdkui6e0v4dkmgonoj7] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_sessionreport] CHECK CONSTRAINT [fk_3y6sqcqdkui6e0v4dkmgonoj7]
GO
ALTER TABLE [dbo].[mpay_sessionreport]  WITH CHECK ADD  CONSTRAINT [fk_430ydcaqpte9v8rng4wbwurtd] FOREIGN KEY([sessionstatusid])
REFERENCES [dbo].[mpay_sessionstatus] ([id])
GO
ALTER TABLE [dbo].[mpay_sessionreport] CHECK CONSTRAINT [fk_430ydcaqpte9v8rng4wbwurtd]
GO
ALTER TABLE [dbo].[mpay_sessionreport]  WITH CHECK ADD  CONSTRAINT [fk_cm2t125u9x9m0u1kc3tjoo6h1] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_sessionreport] CHECK CONSTRAINT [fk_cm2t125u9x9m0u1kc3tjoo6h1]
GO
ALTER TABLE [dbo].[mpay_sessionreport]  WITH CHECK ADD  CONSTRAINT [fk_hrhypskbi288i07kgn997t75j] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_sessionreport] CHECK CONSTRAINT [fk_hrhypskbi288i07kgn997t75j]
GO
ALTER TABLE [dbo].[mpay_sessionstatuitm]  WITH CHECK ADD  CONSTRAINT [fk_61iy037sybbkd6duwj5rl68ij] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_sessionstatugrp] ([id])
GO
ALTER TABLE [dbo].[mpay_sessionstatuitm] CHECK CONSTRAINT [fk_61iy037sybbkd6duwj5rl68ij]
GO
ALTER TABLE [dbo].[mpay_sessionstatus]  WITH CHECK ADD  CONSTRAINT [fk_ehmw1eh6cpb2jyno24ylotpyl] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_sessionstatus] CHECK CONSTRAINT [fk_ehmw1eh6cpb2jyno24ylotpyl]
GO
ALTER TABLE [dbo].[mpay_sessionstatus]  WITH CHECK ADD  CONSTRAINT [fk_ijlfdb5dtq9peymwdttorehpt] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_sessionstatus] CHECK CONSTRAINT [fk_ijlfdb5dtq9peymwdttorehpt]
GO
ALTER TABLE [dbo].[mpay_staticqrcode]  WITH CHECK ADD  CONSTRAINT [fk_5a2x9atoltyymwu71ch6ay8p4] FOREIGN KEY([refserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_staticqrcode] CHECK CONSTRAINT [fk_5a2x9atoltyymwu71ch6ay8p4]
GO
ALTER TABLE [dbo].[mpay_staticqrcode]  WITH CHECK ADD  CONSTRAINT [fk_8qxbbuh4gx8asgtv23i8p9sig] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_staticqrcode] CHECK CONSTRAINT [fk_8qxbbuh4gx8asgtv23i8p9sig]
GO
ALTER TABLE [dbo].[mpay_staticqrcode]  WITH CHECK ADD  CONSTRAINT [fk_e4fkc3vc9adepamev4h158rs2] FOREIGN KEY([refcorporateid])
REFERENCES [dbo].[mpay_corporates] ([id])
GO
ALTER TABLE [dbo].[mpay_staticqrcode] CHECK CONSTRAINT [fk_e4fkc3vc9adepamev4h158rs2]
GO
ALTER TABLE [dbo].[mpay_staticqrcode]  WITH CHECK ADD  CONSTRAINT [fk_r82nigf0af7gek61vspi6quvh] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_staticqrcode] CHECK CONSTRAINT [fk_r82nigf0af7gek61vspi6quvh]
GO
ALTER TABLE [dbo].[mpay_staticqrcode]  WITH CHECK ADD  CONSTRAINT [fk_t8chdcqoygomc08dot6ngw8as] FOREIGN KEY([refdeviceid])
REFERENCES [dbo].[mpay_corporatedevices] ([id])
GO
ALTER TABLE [dbo].[mpay_staticqrcode] CHECK CONSTRAINT [fk_t8chdcqoygomc08dot6ngw8as]
GO
ALTER TABLE [dbo].[mpay_staticqrcodeitm]  WITH CHECK ADD  CONSTRAINT [fk_skrp0yrxi3u2yfo91y8okvsrh] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_staticqrcodegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_staticqrcodeitm] CHECK CONSTRAINT [fk_skrp0yrxi3u2yfo91y8okvsrh]
GO
ALTER TABLE [dbo].[mpay_sysconfigs]  WITH CHECK ADD  CONSTRAINT [fk_5hohtt9dr7lmvux5o6kg7bamv] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_sysconfigs] CHECK CONSTRAINT [fk_5hohtt9dr7lmvux5o6kg7bamv]
GO
ALTER TABLE [dbo].[mpay_sysconfigs]  WITH CHECK ADD  CONSTRAINT [fk_65m9h4pgygk8pu5qqcif75c8y] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_sysconfigs] CHECK CONSTRAINT [fk_65m9h4pgygk8pu5qqcif75c8y]
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout]  WITH CHECK ADD  CONSTRAINT [fk_2bqrx9xod2l2r8px2ectxiiwx] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout] CHECK CONSTRAINT [fk_2bqrx9xod2l2r8px2ectxiiwx]
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout]  WITH CHECK ADD  CONSTRAINT [fk_2kfxcw8awhwgcmj6rx5571fci] FOREIGN KEY([reftransactionid])
REFERENCES [dbo].[mpay_transactions] ([id])
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout] CHECK CONSTRAINT [fk_2kfxcw8awhwgcmj6rx5571fci]
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout]  WITH CHECK ADD  CONSTRAINT [fk_3ywb99s7l6dc4y22axpn69ngf] FOREIGN KEY([sourceserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout] CHECK CONSTRAINT [fk_3ywb99s7l6dc4y22axpn69ngf]
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout]  WITH CHECK ADD  CONSTRAINT [fk_93oqjddl7dfpaxk9v7jm7seif] FOREIGN KEY([reasonid])
REFERENCES [dbo].[mpay_reasons] ([id])
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout] CHECK CONSTRAINT [fk_93oqjddl7dfpaxk9v7jm7seif]
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout]  WITH CHECK ADD  CONSTRAINT [fk_bxcqkvx5ipixxacfayrv23w9j] FOREIGN KEY([destinationserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout] CHECK CONSTRAINT [fk_bxcqkvx5ipixxacfayrv23w9j]
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout]  WITH CHECK ADD  CONSTRAINT [fk_ianrxhx5sbd0kon4hjsc3xwl7] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout] CHECK CONSTRAINT [fk_ianrxhx5sbd0kon4hjsc3xwl7]
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout]  WITH CHECK ADD  CONSTRAINT [fk_jj5ojg6hqsixnh9os1oirrmqf] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout] CHECK CONSTRAINT [fk_jj5ojg6hqsixnh9os1oirrmqf]
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout]  WITH CHECK ADD  CONSTRAINT [fk_tk6b4keft1dy2ix1e3tr8njou] FOREIGN KEY([processingstatusid])
REFERENCES [dbo].[mpay_processingstatuses] ([id])
GO
ALTER TABLE [dbo].[mpay_systemaccountscashout] CHECK CONSTRAINT [fk_tk6b4keft1dy2ix1e3tr8njou]
GO
ALTER TABLE [dbo].[mpay_taxschemedetailitm]  WITH CHECK ADD  CONSTRAINT [fk_hgnsjlfmjr7t8ieivjsf6d46e] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_taxschemedetailgrp] ([id])
GO
ALTER TABLE [dbo].[mpay_taxschemedetailitm] CHECK CONSTRAINT [fk_hgnsjlfmjr7t8ieivjsf6d46e]
GO
ALTER TABLE [dbo].[mpay_taxschemedetails]  WITH CHECK ADD  CONSTRAINT [fk_5en8vd9obaj0ikwyowi5uyfih] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_taxschemedetails] CHECK CONSTRAINT [fk_5en8vd9obaj0ikwyowi5uyfih]
GO
ALTER TABLE [dbo].[mpay_taxschemedetails]  WITH CHECK ADD  CONSTRAINT [fk_aqw6exrekjwf7mgxw81wry5sb] FOREIGN KEY([msgtypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_taxschemedetails] CHECK CONSTRAINT [fk_aqw6exrekjwf7mgxw81wry5sb]
GO
ALTER TABLE [dbo].[mpay_taxschemedetails]  WITH CHECK ADD  CONSTRAINT [fk_c1ssgk12612m7x5e2hsv5qjrq] FOREIGN KEY([refschemeid])
REFERENCES [dbo].[mpay_taxschemes] ([id])
GO
ALTER TABLE [dbo].[mpay_taxschemedetails] CHECK CONSTRAINT [fk_c1ssgk12612m7x5e2hsv5qjrq]
GO
ALTER TABLE [dbo].[mpay_taxschemedetails]  WITH CHECK ADD  CONSTRAINT [fk_g5gr8bicugg2a3bcinwnchhiq] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_taxschemedetails] CHECK CONSTRAINT [fk_g5gr8bicugg2a3bcinwnchhiq]
GO
ALTER TABLE [dbo].[mpay_taxschemeitm]  WITH CHECK ADD  CONSTRAINT [fk_khupw3ie1li3fgp2wgpur0ibm] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_taxschemegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_taxschemeitm] CHECK CONSTRAINT [fk_khupw3ie1li3fgp2wgpur0ibm]
GO
ALTER TABLE [dbo].[mpay_taxschemes]  WITH CHECK ADD  CONSTRAINT [fk_5tsm219lmqqums79d5kvf5gew] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_taxschemes] CHECK CONSTRAINT [fk_5tsm219lmqqums79d5kvf5gew]
GO
ALTER TABLE [dbo].[mpay_taxschemes]  WITH CHECK ADD  CONSTRAINT [fk_9syxgp893eomw26elcg9bigkb] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_taxschemes] CHECK CONSTRAINT [fk_9syxgp893eomw26elcg9bigkb]
GO
ALTER TABLE [dbo].[mpay_taxschemes]  WITH CHECK ADD  CONSTRAINT [fk_k82css4fiv6ifq370ouawed5g] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_taxschemes] CHECK CONSTRAINT [fk_k82css4fiv6ifq370ouawed5g]
GO
ALTER TABLE [dbo].[mpay_taxsliceitm]  WITH CHECK ADD  CONSTRAINT [fk_g4frovxs6pc4ouqf66p46jgk2] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_taxslicegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_taxsliceitm] CHECK CONSTRAINT [fk_g4frovxs6pc4ouqf66p46jgk2]
GO
ALTER TABLE [dbo].[mpay_taxslices]  WITH CHECK ADD  CONSTRAINT [fk_9uwmtwuwaobu2du9b1u48lswd] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_taxslices] CHECK CONSTRAINT [fk_9uwmtwuwaobu2du9b1u48lswd]
GO
ALTER TABLE [dbo].[mpay_taxslices]  WITH CHECK ADD  CONSTRAINT [fk_fdabbethyf2nmrh4uasqyvd5] FOREIGN KEY([reftaxdetailsid])
REFERENCES [dbo].[mpay_taxschemedetails] ([id])
GO
ALTER TABLE [dbo].[mpay_taxslices] CHECK CONSTRAINT [fk_fdabbethyf2nmrh4uasqyvd5]
GO
ALTER TABLE [dbo].[mpay_taxslices]  WITH CHECK ADD  CONSTRAINT [fk_jy559mvv1looltkar592b8mwm] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_taxslices] CHECK CONSTRAINT [fk_jy559mvv1looltkar592b8mwm]
GO
ALTER TABLE [dbo].[mpay_transactionconfigs]  WITH CHECK ADD  CONSTRAINT [fk_4p4rrirf2pql5ba048q88ybg5] FOREIGN KEY([receivertypeid])
REFERENCES [dbo].[mpay_clienttypes] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionconfigs] CHECK CONSTRAINT [fk_4p4rrirf2pql5ba048q88ybg5]
GO
ALTER TABLE [dbo].[mpay_transactionconfigs]  WITH CHECK ADD  CONSTRAINT [fk_8b9ja0aje8o4yql2ygqtcim1j] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionconfigs] CHECK CONSTRAINT [fk_8b9ja0aje8o4yql2ygqtcim1j]
GO
ALTER TABLE [dbo].[mpay_transactionconfigs]  WITH CHECK ADD  CONSTRAINT [fk_ct42a0hq28g3d60ih6ihud28y] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_transactionconfigs] CHECK CONSTRAINT [fk_ct42a0hq28g3d60ih6ihud28y]
GO
ALTER TABLE [dbo].[mpay_transactionconfigs]  WITH CHECK ADD  CONSTRAINT [fk_e3aax99hxcuc1on1s6qwf6269] FOREIGN KEY([refoperationid])
REFERENCES [dbo].[mpay_endpointoperations] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionconfigs] CHECK CONSTRAINT [fk_e3aax99hxcuc1on1s6qwf6269]
GO
ALTER TABLE [dbo].[mpay_transactionconfigs]  WITH CHECK ADD  CONSTRAINT [fk_mw1jat66dkmtid646su6iboxc] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_transactionconfigs] CHECK CONSTRAINT [fk_mw1jat66dkmtid646su6iboxc]
GO
ALTER TABLE [dbo].[mpay_transactionconfigs]  WITH CHECK ADD  CONSTRAINT [fk_phtph32yv8gyin18b1mu3f2w7] FOREIGN KEY([messagetypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionconfigs] CHECK CONSTRAINT [fk_phtph32yv8gyin18b1mu3f2w7]
GO
ALTER TABLE [dbo].[mpay_transactionconfigs]  WITH CHECK ADD  CONSTRAINT [fk_qj61kcwsjono5xggu15eiy210] FOREIGN KEY([sendertypeid])
REFERENCES [dbo].[mpay_clienttypes] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionconfigs] CHECK CONSTRAINT [fk_qj61kcwsjono5xggu15eiy210]
GO
ALTER TABLE [dbo].[mpay_transactionconfigs]  WITH CHECK ADD  CONSTRAINT [fk_rb7mpiial7gxh8016obai5wfd] FOREIGN KEY([reftypeid])
REFERENCES [dbo].[mpay_transactiontypes] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionconfigs] CHECK CONSTRAINT [fk_rb7mpiial7gxh8016obai5wfd]
GO
ALTER TABLE [dbo].[mpay_transactiondirectionitm]  WITH CHECK ADD  CONSTRAINT [fk_jaoa1uhq3omi5ifu0ursw2rl9] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_transactiondirectiongrp] ([id])
GO
ALTER TABLE [dbo].[mpay_transactiondirectionitm] CHECK CONSTRAINT [fk_jaoa1uhq3omi5ifu0ursw2rl9]
GO
ALTER TABLE [dbo].[mpay_transactiondirections]  WITH CHECK ADD  CONSTRAINT [fk_bnyu6qx1ud656gt4mxh28cn2d] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_transactiondirections] CHECK CONSTRAINT [fk_bnyu6qx1ud656gt4mxh28cn2d]
GO
ALTER TABLE [dbo].[mpay_transactiondirections]  WITH CHECK ADD  CONSTRAINT [fk_tqorbqcg06t08at4a70grj6cu] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_transactiondirections] CHECK CONSTRAINT [fk_tqorbqcg06t08at4a70grj6cu]
GO
ALTER TABLE [dbo].[mpay_transactiondirections_nls]  WITH CHECK ADD  CONSTRAINT [fk_4q3iwrk8uumlknmqsg73c82dc] FOREIGN KEY([directionid])
REFERENCES [dbo].[mpay_transactiondirections] ([id])
GO
ALTER TABLE [dbo].[mpay_transactiondirections_nls] CHECK CONSTRAINT [fk_4q3iwrk8uumlknmqsg73c82dc]
GO
ALTER TABLE [dbo].[mpay_transactiondirections_nls]  WITH CHECK ADD  CONSTRAINT [fk_h0viuq34j58ji9jbblhb1ippo] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_transactiondirections_nls] CHECK CONSTRAINT [fk_h0viuq34j58ji9jbblhb1ippo]
GO
ALTER TABLE [dbo].[mpay_transactiondirections_nls]  WITH CHECK ADD  CONSTRAINT [fk_shhr16qtbqs1p1a44wqun4j5u] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_transactiondirections_nls] CHECK CONSTRAINT [fk_shhr16qtbqs1p1a44wqun4j5u]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_39e1lqdpnqahjul0v3dxwonv2] FOREIGN KEY([receiverserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_39e1lqdpnqahjul0v3dxwonv2]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_4deyvpvu78aourl8y87oldpb7] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_4deyvpvu78aourl8y87oldpb7]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_4g0cvygbltsuawbtkdjvskl7g] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_4g0cvygbltsuawbtkdjvskl7g]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_6cgcalmkluja9dq7fxfo9pca7] FOREIGN KEY([sendermobileaccountid])
REFERENCES [dbo].[mpay_mobileaccounts] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_6cgcalmkluja9dq7fxfo9pca7]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_8ekl2doij9v8whwahdxbcjloj] FOREIGN KEY([processingstatusid])
REFERENCES [dbo].[mpay_processingstatuses] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_8ekl2doij9v8whwahdxbcjloj]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_cwpmmxar3xwgnl31j2owc8brh] FOREIGN KEY([originaltransactionid])
REFERENCES [dbo].[mpay_transactions] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_cwpmmxar3xwgnl31j2owc8brh]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_dikj4td52ka4ydtntcclw9cam] FOREIGN KEY([receiverbankid])
REFERENCES [dbo].[mpay_banks] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_dikj4td52ka4ydtntcclw9cam]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_ex3uucko3bcn9cjtbtm9xjmde] FOREIGN KEY([receivermobileid])
REFERENCES [dbo].[mpay_customermobiles] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_ex3uucko3bcn9cjtbtm9xjmde]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_g92vdr852r1bmkn6drsx0b5yc] FOREIGN KEY([receiverserviceaccountid])
REFERENCES [dbo].[mpay_serviceaccounts] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_g92vdr852r1bmkn6drsx0b5yc]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_geyws1j3w7myxc0ms750r8n0u] FOREIGN KEY([inwardmessageid])
REFERENCES [dbo].[mpay_mpclearintegmsglogs] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_geyws1j3w7myxc0ms750r8n0u]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_gt4ior8almi8i0jx9q5gwmqwe] FOREIGN KEY([refmessageid])
REFERENCES [dbo].[mpay_mpaymessages] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_gt4ior8almi8i0jx9q5gwmqwe]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_i46o2c8bj206vpj6jbbufamfi] FOREIGN KEY([directionid])
REFERENCES [dbo].[mpay_transactiondirections] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_i46o2c8bj206vpj6jbbufamfi]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_ik3cmq43t4yvlhf1m1evu97dg] FOREIGN KEY([reasonid])
REFERENCES [dbo].[mpay_reasons] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_ik3cmq43t4yvlhf1m1evu97dg]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_jqowqr9fjavpeauqhnwffmfug] FOREIGN KEY([reftypeid])
REFERENCES [dbo].[mpay_transactiontypes] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_jqowqr9fjavpeauqhnwffmfug]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_n5jdtkocsdhwows34rt8kcxs0] FOREIGN KEY([senderbankid])
REFERENCES [dbo].[mpay_banks] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_n5jdtkocsdhwows34rt8kcxs0]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_no2urkc81mgp9ox55x20t5jir] FOREIGN KEY([senderserviceaccountid])
REFERENCES [dbo].[mpay_serviceaccounts] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_no2urkc81mgp9ox55x20t5jir]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_nqefk17wyuvqf4ctouj7172p3] FOREIGN KEY([senderserviceid])
REFERENCES [dbo].[mpay_corpoarteservices] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_nqefk17wyuvqf4ctouj7172p3]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_o29q9ib60qt56vijqjhiibdsa] FOREIGN KEY([sendermobileid])
REFERENCES [dbo].[mpay_customermobiles] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_o29q9ib60qt56vijqjhiibdsa]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_s8d8v6omhatxrnfu4rc688a7p] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_s8d8v6omhatxrnfu4rc688a7p]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_t8llu1c74ndrbijx74i2m1lda] FOREIGN KEY([receivermobileaccountid])
REFERENCES [dbo].[mpay_mobileaccounts] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_t8llu1c74ndrbijx74i2m1lda]
GO
ALTER TABLE [dbo].[mpay_transactions]  WITH CHECK ADD  CONSTRAINT [fk_ygih4lcx3uwlg8w92lbmb6m3] FOREIGN KEY([refoperationid])
REFERENCES [dbo].[mpay_endpointoperations] ([id])
GO
ALTER TABLE [dbo].[mpay_transactions] CHECK CONSTRAINT [fk_ygih4lcx3uwlg8w92lbmb6m3]
GO
ALTER TABLE [dbo].[mpay_transactionsconfig]  WITH CHECK ADD  CONSTRAINT [fk_43eelwrwta3iy816clln97a8i] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionsconfig] CHECK CONSTRAINT [fk_43eelwrwta3iy816clln97a8i]
GO
ALTER TABLE [dbo].[mpay_transactionsconfig]  WITH CHECK ADD  CONSTRAINT [fk_a2dagmwaehmhge5borpvk76fb] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_transactionsconfig] CHECK CONSTRAINT [fk_a2dagmwaehmhge5borpvk76fb]
GO
ALTER TABLE [dbo].[mpay_transactionsconfig]  WITH CHECK ADD  CONSTRAINT [fk_kfbrb4geokfpqleq32vnwsqfl] FOREIGN KEY([clienttypeid])
REFERENCES [dbo].[mpay_clienttypes] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionsconfig] CHECK CONSTRAINT [fk_kfbrb4geokfpqleq32vnwsqfl]
GO
ALTER TABLE [dbo].[mpay_transactionsconfig]  WITH CHECK ADD  CONSTRAINT [fk_o011v00xk3t8vqd0r1p14017g] FOREIGN KEY([messagetypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionsconfig] CHECK CONSTRAINT [fk_o011v00xk3t8vqd0r1p14017g]
GO
ALTER TABLE [dbo].[mpay_transactionsizeitm]  WITH CHECK ADD  CONSTRAINT [fk_k21o1y1uxd2lptbsdugufuedg] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_transactionsizegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionsizeitm] CHECK CONSTRAINT [fk_k21o1y1uxd2lptbsdugufuedg]
GO
ALTER TABLE [dbo].[mpay_transactionsizes]  WITH CHECK ADD  CONSTRAINT [fk_52i96w7mag2d1f6r5sm4pqasj] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionsizes] CHECK CONSTRAINT [fk_52i96w7mag2d1f6r5sm4pqasj]
GO
ALTER TABLE [dbo].[mpay_transactionsizes]  WITH CHECK ADD  CONSTRAINT [fk_8axnlgnx7lrqa5n2g6459098a] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_transactionsizes] CHECK CONSTRAINT [fk_8axnlgnx7lrqa5n2g6459098a]
GO
ALTER TABLE [dbo].[mpay_transactionsizes_nls]  WITH CHECK ADD  CONSTRAINT [fk_fgbgrvgin33l4bp2rm3iroa91] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_transactionsizes_nls] CHECK CONSTRAINT [fk_fgbgrvgin33l4bp2rm3iroa91]
GO
ALTER TABLE [dbo].[mpay_transactionsizes_nls]  WITH CHECK ADD  CONSTRAINT [fk_qt2v6kfv6d32g1bok2kyowc8s] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionsizes_nls] CHECK CONSTRAINT [fk_qt2v6kfv6d32g1bok2kyowc8s]
GO
ALTER TABLE [dbo].[mpay_transactionsizes_nls]  WITH CHECK ADD  CONSTRAINT [fk_rmw86t5yt377pvrph6bd237qg] FOREIGN KEY([sizeid])
REFERENCES [dbo].[mpay_transactionsizes] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionsizes_nls] CHECK CONSTRAINT [fk_rmw86t5yt377pvrph6bd237qg]
GO
ALTER TABLE [dbo].[mpay_transactionssummary]  WITH CHECK ADD  CONSTRAINT [fk_4jb5fe61c5ca59x9vs16cb6hi] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionssummary] CHECK CONSTRAINT [fk_4jb5fe61c5ca59x9vs16cb6hi]
GO
ALTER TABLE [dbo].[mpay_transactionssummary]  WITH CHECK ADD  CONSTRAINT [fk_a9d095msjt2pek8xcbenhmhli] FOREIGN KEY([sessionid])
REFERENCES [dbo].[mpay_sessionreport] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionssummary] CHECK CONSTRAINT [fk_a9d095msjt2pek8xcbenhmhli]
GO
ALTER TABLE [dbo].[mpay_transactionssummary]  WITH CHECK ADD  CONSTRAINT [fk_ke3nn0anwkgx7e6geomx8ln23] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_transactionssummary] CHECK CONSTRAINT [fk_ke3nn0anwkgx7e6geomx8ln23]
GO
ALTER TABLE [dbo].[mpay_transactionssummary]  WITH CHECK ADD  CONSTRAINT [fk_o2oanjlfg9obkm525n769id3i] FOREIGN KEY([messagetypeid])
REFERENCES [dbo].[mpay_messagetypes] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionssummary] CHECK CONSTRAINT [fk_o2oanjlfg9obkm525n769id3i]
GO
ALTER TABLE [dbo].[mpay_transactionssummary]  WITH CHECK ADD  CONSTRAINT [fk_pnyvb3hslojo3y84lfp614xx3] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_transactionssummary] CHECK CONSTRAINT [fk_pnyvb3hslojo3y84lfp614xx3]
GO
ALTER TABLE [dbo].[mpay_transactionssummary]  WITH CHECK ADD  CONSTRAINT [fk_qp8dx20ibn95duyjlsa42ufha] FOREIGN KEY([directionid])
REFERENCES [dbo].[mpay_transactiondirections] ([id])
GO
ALTER TABLE [dbo].[mpay_transactionssummary] CHECK CONSTRAINT [fk_qp8dx20ibn95duyjlsa42ufha]
GO
ALTER TABLE [dbo].[mpay_transactiontypeitm]  WITH CHECK ADD  CONSTRAINT [fk_6u8vgm24rf9i62dyb2f8y56m6] FOREIGN KEY([groupid])
REFERENCES [dbo].[mpay_transactiontypegrp] ([id])
GO
ALTER TABLE [dbo].[mpay_transactiontypeitm] CHECK CONSTRAINT [fk_6u8vgm24rf9i62dyb2f8y56m6]
GO
ALTER TABLE [dbo].[mpay_transactiontypes]  WITH CHECK ADD  CONSTRAINT [fk_b1ifskhw1wubvwq447a7uiein] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_transactiontypes] CHECK CONSTRAINT [fk_b1ifskhw1wubvwq447a7uiein]
GO
ALTER TABLE [dbo].[mpay_transactiontypes]  WITH CHECK ADD  CONSTRAINT [fk_n0gwfyd70tuxsduu9mqn4m6ob] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_transactiontypes] CHECK CONSTRAINT [fk_n0gwfyd70tuxsduu9mqn4m6ob]
GO
ALTER TABLE [dbo].[mpay_transactiontypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_4t9866fs71gj20pkjh9ay7pq3] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_transactiontypes_nls] CHECK CONSTRAINT [fk_4t9866fs71gj20pkjh9ay7pq3]
GO
ALTER TABLE [dbo].[mpay_transactiontypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_eqpcm3bo6uate761vwi6yjxjd] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_transactiontypes_nls] CHECK CONSTRAINT [fk_eqpcm3bo6uate761vwi6yjxjd]
GO
ALTER TABLE [dbo].[mpay_transactiontypes_nls]  WITH CHECK ADD  CONSTRAINT [fk_orsxwkcu4nipiq919nb3cmd89] FOREIGN KEY([typeid])
REFERENCES [dbo].[mpay_transactiontypes] ([id])
GO
ALTER TABLE [dbo].[mpay_transactiontypes_nls] CHECK CONSTRAINT [fk_orsxwkcu4nipiq919nb3cmd89]
GO
ALTER TABLE [dbo].[mpay_trnscheckoutrecords]  WITH CHECK ADD  CONSTRAINT [fk_1xwg33uv5ra6usalcuesr5gvy] FOREIGN KEY([refmessageid])
REFERENCES [dbo].[mpay_mpaymessages] ([id])
GO
ALTER TABLE [dbo].[mpay_trnscheckoutrecords] CHECK CONSTRAINT [fk_1xwg33uv5ra6usalcuesr5gvy]
GO
ALTER TABLE [dbo].[mpay_trnscheckoutrecords]  WITH CHECK ADD  CONSTRAINT [fk_5odfw4ibulev17jya3v02xyfn] FOREIGN KEY([z_draft_id])
REFERENCES [dbo].[JFW_DRAFTS] ([ID])
GO
ALTER TABLE [dbo].[mpay_trnscheckoutrecords] CHECK CONSTRAINT [fk_5odfw4ibulev17jya3v02xyfn]
GO
ALTER TABLE [dbo].[mpay_trnscheckoutrecords]  WITH CHECK ADD  CONSTRAINT [fk_cxcd9yn1ge24mete4b1c6gyr7] FOREIGN KEY([currencyid])
REFERENCES [dbo].[JFW_CURRENCIES] ([ID])
GO
ALTER TABLE [dbo].[mpay_trnscheckoutrecords] CHECK CONSTRAINT [fk_cxcd9yn1ge24mete4b1c6gyr7]
GO
ALTER TABLE [dbo].[mpay_trnscheckoutrecords]  WITH CHECK ADD  CONSTRAINT [fk_s8pwiqxva4k96307ah5nhtd5q] FOREIGN KEY([z_status_id])
REFERENCES [dbo].[JFW_WF_STATUS] ([id])
GO
ALTER TABLE [dbo].[mpay_trnscheckoutrecords] CHECK CONSTRAINT [fk_s8pwiqxva4k96307ah5nhtd5q]
GO
/****** Object:  StoredProcedure [dbo].[CALCULATE_LIMITS]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[CALCULATE_LIMITS] (
    @mobileId     bigint,
    @typeId       bigint,
    @dailyCount    numeric OUT,
    @dailyAmount  numeric(21, 8) out ,
    @weeklyCount  numeric OUT,
    @weeklyAmount numeric(21, 8) out ,
    @monthlyCount numeric OUT,
    @monthlyAmount numeric(21, 8) out ,
    @yearlyCount  numeric OUT,
    @yearlyAmount numeric(21, 8) out )

AS BEGIN
    DECLARE @mobileNumber VARCHAR(4000)
    SELECT @mobileNumber = m.MOBILENUMBER  FROM MPAY_CUSTOMERMOBILES m WHERE ID = @mobileId;

--DAILY AMOUNT
    BEGIN
        SELECT @dailyAmount = SUM(TRANSACTIONS.TOTALAMOUNT) FROM MPAY_TRANSACTIONS TRANSACTIONS, MPAY_ENDPOINTOPERATIONS OPERATIONS, MPAY_MPAYMESSAGES MESSAGES
        WHERE TRANSACTIONS.REFMESSAGEID = MESSAGES.ID
          AND MESSAGES.REFOPERATIONID = OPERATIONS.ID
          AND OPERATIONS.MESSAGETYPEID = @typeId
          AND TRANSACTIONS.SENDERMOBILEID = @mobileId
          AND CONVERT(DATE,TRANSACTIONS.TRANSDATE) = CONVERT(DATE,getdate())
          AND TRANSACTIONS.PROCESSINGSTATUSID IN (1,2,4);
        IF @dailyAmount IS NULL BEGIN
            SET @dailyAmount = 0;
        END
    END

--DAILY COUNT
    SELECT @dailyCount = COUNT(MESSAGES.ID)  FROM MPAY_MPAYMESSAGES MESSAGES, MPAY_ENDPOINTOPERATIONS OPERATIONS
    WHERE MESSAGES.REFOPERATIONID = OPERATIONS.ID
      AND OPERATIONS.MESSAGETYPEID = @typeId
      AND MESSAGES.SENDER = @mobileNumber
      AND CONVERT(DATE,MESSAGES.PROCESSINGSTAMP) = CONVERT(DATE,getdate())
      AND MESSAGES.PROCESSINGSTATUSID IN (1,2,4);

--WEEKLY AMOUNT
    BEGIN
        SELECT @weeklyAmount=SUM(TRANSACTIONS.TOTALAMOUNT)  FROM MPAY_TRANSACTIONS TRANSACTIONS, MPAY_ENDPOINTOPERATIONS OPERATIONS, MPAY_MPAYMESSAGES MESSAGES
        WHERE TRANSACTIONS.REFMESSAGEID = MESSAGES.ID
          AND MESSAGES.REFOPERATIONID = OPERATIONS.ID
          AND OPERATIONS.MESSAGETYPEID = @typeId
          AND TRANSACTIONS.SENDERMOBILEID = @mobileId
          AND YEAR( TRANSACTIONS.TRANSDATE) =  YEAR(getdate())
          AND convert(NUMERIC,DATEPART( wk, TRANSACTIONS.TRANSDATE))=convert(NUMERIC,DATEPART( wk, getdate()))
          AND convert(NUMERIC,CONVERT(VARCHAR,TRANSACTIONS.TRANSDATE))=convert(NUMERIC,DATEPART( wk, getdate()))
          AND TRANSACTIONS.PROCESSINGSTATUSID IN (1,2,4);
        IF @weeklyAmount IS NULL BEGIN
            set @weeklyAmount = 0;
        END ;
    END;

--WEEKLY COUNT
    SELECT @weeklyCount=COUNT(MESSAGES.ID)  FROM MPAY_MPAYMESSAGES MESSAGES, MPAY_ENDPOINTOPERATIONS OPERATIONS
    WHERE MESSAGES.REFOPERATIONID = OPERATIONS.ID
      AND OPERATIONS.MESSAGETYPEID = @typeId
      AND MESSAGES.SENDER = @mobileNumber
      AND YEAR( MESSAGES.PROCESSINGSTAMP) =  YEAR(getdate())
      AND convert(NUMERIC,DATEPART( wk, MESSAGES.PROCESSINGSTAMP))=convert(NUMERIC,DATEPART( wk, getdate()))
      AND MESSAGES.PROCESSINGSTATUSID IN (1,2,4);

--MONTHLY AMOUNT
    BEGIN
        SELECT @monthlyAmount=SUM(TRANSACTIONS.TOTALAMOUNT)  FROM MPAY_TRANSACTIONS TRANSACTIONS, MPAY_ENDPOINTOPERATIONS OPERATIONS, MPAY_MPAYMESSAGES MESSAGES
        WHERE TRANSACTIONS.REFMESSAGEID = MESSAGES.ID
          AND MESSAGES.REFOPERATIONID = OPERATIONS.ID
          AND OPERATIONS.MESSAGETYPEID = @typeId
          AND TRANSACTIONS.SENDERMOBILEID = @mobileId
          AND YEAR( TRANSACTIONS.TRANSDATE) =  YEAR(getdate())
          AND MONTH( TRANSACTIONS.TRANSDATE) =  MONTH(getdate())
          AND TRANSACTIONS.PROCESSINGSTATUSID IN (1,2,4);
        IF @monthlyAmount IS NULL BEGIN
            set @monthlyAmount = 0;
        END ;
    END;

--MONTHLY COUNT
    SELECT @monthlyCount=COUNT(MESSAGES.ID) FROM MPAY_MPAYMESSAGES MESSAGES, MPAY_ENDPOINTOPERATIONS OPERATIONS
    WHERE MESSAGES.REFOPERATIONID = OPERATIONS.ID
      AND OPERATIONS.MESSAGETYPEID = @typeId
      AND MESSAGES.SENDER = @mobileNumber
      AND YEAR( MESSAGES.PROCESSINGSTAMP) =  YEAR(getdate())
      AND MONTH( MESSAGES.PROCESSINGSTAMP) =  MONTH(getdate())
      AND MESSAGES.PROCESSINGSTATUSID IN (1,2,4);

--YEARLY AMOUNT
    BEGIN
        SELECT @yearlyAmount =SUM(TRANSACTIONS.TOTALAMOUNT)  FROM MPAY_TRANSACTIONS TRANSACTIONS, MPAY_ENDPOINTOPERATIONS OPERATIONS, MPAY_MPAYMESSAGES MESSAGES
        WHERE TRANSACTIONS.REFMESSAGEID = MESSAGES.ID
          AND MESSAGES.REFOPERATIONID = OPERATIONS.ID
          AND OPERATIONS.MESSAGETYPEID = @typeId
          AND TRANSACTIONS.SENDERMOBILEID = @mobileId
          AND MONTH( TRANSACTIONS.TRANSDATE) =  MONTH(getdate())
          AND TRANSACTIONS.PROCESSINGSTATUSID IN (1,2,4);
        IF @yearlyAmount IS NULL BEGIN
            set @yearlyAmount = 0;
        END ;
    END;

--YEARLY COUNT
    SELECT @yearlyCount=COUNT(MESSAGES.ID)  FROM MPAY_MPAYMESSAGES MESSAGES, MPAY_ENDPOINTOPERATIONS OPERATIONS
    WHERE MESSAGES.REFOPERATIONID = OPERATIONS.ID
      AND OPERATIONS.MESSAGETYPEID = @typeId
      AND MESSAGES.SENDER = @mobileNumber
      AND YEAR( MESSAGES.PROCESSINGSTAMP) =  YEAR(getdate())
      AND MESSAGES.PROCESSINGSTATUSID IN (1,2,4);

END
GO
/****** Object:  StoredProcedure [dbo].[Create_Limit]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create   PROCEDURE [dbo].[Create_Limit](
    @pACCOUNTID bigint,
    @pMESSAGETYPEID bigint,
    @pDATE datetime2
)
AS
BEGIN
    BEGIN TRY
        INSERT INTO MPAY_CLIENTSLIMITS (
            ACCOUNTID,
            MESSAGETYPEID,
            YEAR,
            DAILYDATE,
            DAILYCOUNT,
            DAILYAMOUNT,
            WEEKNUMBER,
            WEEKLYCOUNT,
            WEEKLYAMOUNT,
            MONTH,
            MONTHLYCOUNT,
            MONTHLYAMOUNT,
            YEARLYCOUNT,
            YEARLYAMOUNT)
        VALUES (
                   @pACCOUNTID,
                   @pMESSAGETYPEID,
                   YEAR(@pDATE),
                   CONVERT(DATE,@pDATE),
                   0,
                   0,
                   convert(NUMERIC,DATEPART( wk, @pDATE)),
                   0,
                   0,
                   MONTH(@pDATE),
                   0,
                   0,
                   0,
                   0);
    END TRY
    BEGIN CATCH
    end catch
END
GO
/****** Object:  StoredProcedure [dbo].[GET_ACCOUNT_LIMITS]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[GET_ACCOUNT_LIMITS] (
    @pACCOUNTID bigint,
    @pMESSAGETYPEID bigint,
    @rDAILYDATE  datetime2 OUT,
    @rWEEKNUMBER  bigint OUT,
    @rMONTH  bigint OUT,
    @rYEAR  bigint OUT,
    @rDAILYCOUNT  bigint OUT,
    @rDAILYAMOUNT  numeric(19, 2) OUT,
    @rWEEKLYCOUNT  bigint OUT,
    @rWEEKLYAMOUNT  numeric(19, 2) OUT,
    @rMONTHLYCOUNT  bigint OUT,
    @rMONTHLYAMOUNT  numeric(19, 2) OUT,
    @rYEARLYCOUNT  bigint OUT,
    @rYEARLYAMOUNT  numeric(19, 2) OUT)
AS
BEGIN
    SELECT @rDAILYDATE=DAILYDATE, @rWEEKNUMBER=WEEKNUMBER, @rMONTH=MONTH, @rYEAR=YEAR,
           @rDAILYCOUNT=DAILYCOUNT, @rDAILYAMOUNT=DAILYAMOUNT, @rWEEKLYCOUNT=WEEKLYCOUNT,
           @rWEEKLYAMOUNT=WEEKLYAMOUNT, @rMONTHLYCOUNT=MONTHLYCOUNT, @rMONTHLYAMOUNT=MONTHLYAMOUNT, @rYEARLYCOUNT=YEARLYCOUNT, @rYEARLYAMOUNT=YEARLYAMOUNT

    FROM MPAY_CLIENTSLIMITS
    WHERE  ACCOUNTID = @pACCOUNTID AND MESSAGETYPEID = @pMESSAGETYPEID;
    IF @@ROWCOUNT = 0
        BEGIN
            set @rDAILYCOUNT = 0;
            set @rDAILYAMOUNT = 0;
            set @rWEEKLYCOUNT= 0;
            set @rWEEKLYAMOUNT = 0;
            set @rMONTHLYCOUNT= 0;
            set @rMONTHLYAMOUNT = 0;
            set @rYEARLYCOUNT = 0;
            set @rYEARLYAMOUNT = 0;
        END

    IF @rDAILYCOUNT IS NULL BEGIN
        set  @rDAILYCOUNT = 0;
    END

    IF @rDAILYAMOUNT IS NULL BEGIN
        set @rDAILYAMOUNT = 0;
    END ;

    IF @rWEEKLYCOUNT IS NULL BEGIN
        set @rWEEKLYCOUNT = 0;
    END

    IF @rWEEKLYAMOUNT IS NULL BEGIN
        set @rWEEKLYAMOUNT = 0;
    END

    IF @rMONTHLYCOUNT IS NULL BEGIN
        set @rMONTHLYCOUNT = 0;
    END

    IF @rMONTHLYAMOUNT IS NULL BEGIN
        set @rMONTHLYAMOUNT = 0;
    END

    IF @rYEARLYCOUNT IS NULL BEGIN
        set @rYEARLYCOUNT = 0;
    END

    IF @rYEARLYAMOUNT IS NULL BEGIN
        set  @rYEARLYAMOUNT = 0;
    END

    IF @rMONTHLYAMOUNT IS NULL BEGIN
        set @rMONTHLYAMOUNT = 0;
    END

    IF @rDAILYDATE IS NULL BEGIN
        set  @rDAILYDATE = getdate();
    END

    IF @rMONTH IS NULL BEGIN
        set @rMONTH = 0;
    END

    IF @rWEEKNUMBER IS NULL BEGIN
        set @rWEEKNUMBER = 0;
    END

    IF @rYEAR IS NULL BEGIN
        set @rYEAR = 0;
    END

END
GO
/****** Object:  StoredProcedure [dbo].[Get_total_balance]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[Get_total_balance](
    @balance numeric(21, 8) out )
AS
BEGIN
    SELECT @balance=SUM(accounts.balance)  FROM MPAY_ACCOUNTS  accounts
    WHERE accounts.LEVEL1 is NULL
       OR accounts.LEVEL1=0;
    IF @balance IS NULL BEGIN
        set @balance = 0;
    END;
END
GO
/****** Object:  StoredProcedure [dbo].[GetNewAccountNumber]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[GetNewAccountNumber](
    @rNewSeqValue numeric output
)
    AS
BEGIN
set @rNewSeqValue = NEXT VALUE FOR ACCOUNT_NUMBER_SEQ;
END
GO
/****** Object:  StoredProcedure [dbo].[GetNewReferenceNumber]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[GetNewReferenceNumber](
    @rNewSeqValue numeric output
)
AS
BEGIN
set @rNewSeqValue = NEXT VALUE FOR REFERENCE_NUMBER_SEQ;
END
GO
/****** Object:  StoredProcedure [dbo].[GetNewSeqVal_MessageId]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    PROCEDURE [dbo].[GetNewSeqVal_MessageId](
    @rNewSeqValue numeric output
)
    AS
BEGIN
    set @rNewSeqValue = NEXT VALUE FOR MPAY_MESSAGEID_SEQ;
END
GO
/****** Object:  StoredProcedure [dbo].[PERSIST_MESSAGE]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[PERSIST_MESSAGE](
    @rMSG_ID                  numeric OUT,
    @pZ_CREATED_BY                 varchar(255),
    @pZ_CREATION_DATE               datetime2,
    @pZ_ORG_ID                   bigint,
    @pZ_TENANT_ID             varchar(255),
    @pZ_UPDATED_BY                   varchar(255),
    @pZ_UPDATING_DATE             datetime2,
    @pMESSAGEID             varchar(255),
    @pPROCESSINGSTAMP             datetime2,
    @pREFERENCE             varchar(1000),
    @pREQUESTTOKEN             varchar(4000),
    @pSENDER             varchar(100),
    @pMESSAGETYPEID             bigint,
    @pPROCESSINGSTATUSID             bigint,
    @pREASONID             bigint,
    @pREFOPERATIONID             bigint,
    @rROWCOUNT                    numeric OUT)
AS
BEGIN
    SELECT NEXT VALUE FOR MPAY_MPAYMESSAGES_SEQ AS rMSG_ID;

    SET IDENTITY_INSERT MPAY_MPAYMESSAGES ON;
    INSERT INTO MPAY_MPAYMESSAGES
    (ID, Z_CREATED_BY, Z_CREATION_DATE, Z_ORG_ID, Z_TENANT_ID, Z_UPDATED_BY, Z_UPDATING_DATE, MESSAGEID, PROCESSINGSTAMP, REFERENCE,REQUESTTOKEN, SENDER, MESSAGETYPEID, PROCESSINGSTATUSID, REASONID,REFOPERATIONID)
    VALUES
    (@rMSG_ID,@pZ_CREATED_BY, @pZ_CREATION_DATE, @pZ_ORG_ID, @pZ_TENANT_ID,@pZ_UPDATED_BY, @pZ_UPDATING_DATE, @pMESSAGEID, @pPROCESSINGSTAMP, @pREFERENCE,@pREQUESTTOKEN, @pSENDER, @pMESSAGETYPEID, @pPROCESSINGSTATUSID, @pREASONID,@pREFOPERATIONID);
    SET IDENTITY_INSERT MPAY_MPAYMESSAGES OFF;
    set @rROWCOUNT =@@ROWCOUNT;
END
GO
/****** Object:  StoredProcedure [dbo].[PostJournalVoucher]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create   PROCEDURE [dbo].[PostJournalVoucher] (
    @accountId           bigint,
    @amount              numeric(21, 8),
    @jvId                bigint,
    @vRowsCount           numeric OUT,
    @vBalanceAfter  numeric(21, 8) OUT,
    @vBalanceBefore  numeric(21, 8) OUT)
AS
BEGIN
    DECLARE @vType FLOAT;
    DECLARE @voucherId FLOAT ;

    SET NOCOUNT ON;

    SELECT @vType = BalanceTypeID, @vBalanceBefore = Balance FROM MPAY_Accounts WHERE ID = @accountId;
    SET @vBalanceAfter = @vBalanceBefore + @amount;
    IF @vType = 1 BEGIN --Debit
    UPDATE MPAY_Accounts SET balance = @vBalanceAfter
    WHERE id = @accountId AND @vBalanceAfter >= 0;
    END

    ELSE BEGIN IF @vType = 2 BEGIN --Credit
    UPDATE MPAY_Accounts SET balance = @vBalanceAfter
    WHERE id = @accountId AND @vBalanceAfter <= 0;
    END

    ELSE BEGIN
        UPDATE MPAY_Accounts SET balance = @vBalanceAfter
        WHERE id = @accountId;
    END

    END
    SET @vRowsCount = @@rowcount;
    IF @vRowsCount = 0 BEGIN
        UPDATE MPAY_JVDETAILS
        SET BALANCEBEFORE = @vBalanceBefore,
            BALANCEAFTER = @vBalanceBefore
        WHERE ID = @jvId;
    END
    ELSE BEGIN
        UPDATE MPAY_JVDETAILS
        SET BALANCEBEFORE = @vBalanceBefore,
            BALANCEAFTER = @vBalanceAfter,
            ISPOSTED = 1
        WHERE ID = @jvId;
    END
END
GO
/****** Object:  StoredProcedure [dbo].[Transaction_Summary_Report]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create   PROCEDURE [dbo].[Transaction_Summary_Report](
    @sessId varchar(1000),
    @c_dbTransaction CURSOR VARYING OUTPUT
)

AS
BEGIN
    SET NOCOUNT ON;
    SET @c_dbTransaction = CURSOR  FOR

        SELECT DISTINCT
            (SELECT getdate())CREATIONDATE  ,SESSIONID,SUM(TOTALAMOUNT) TOTALAMOUNT ,SUM(totalcharge) TOTALCHARGE,
            (select txdirection.id from mpay_transactiondirections txDirection where txDirection.id=transactions.DIRECTIONID) DIRECTION,
            (select id from mpay_messagetypes where id=(select messagetypeid from mpay_endpointoperations where id = transactions.refoperationid)) MESSAGETYPE

        FROM
            MPAY_TRANSACTIONS transactions
        WHERE
                PROCESSINGSTATUSID = 1 AND SESSIONID=@sessId
        GROUP BY REFOPERATIONID,DIRECTIONID,SESSIONID;

END
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_CLIENTS_LIMITS]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create   PROCEDURE [dbo].[UPDATE_CLIENTS_LIMITS]
    (
        @pACCOUNTID VARCHAR(4000) ,
        @pMESSAGETYPEID VARCHAR(4000) ,
        @pAMOUNT DECIMAL(19,4) ,
        @pCOUNT VARCHAR(4000) ,
        @pDATE DATETIME ,
        @rROWCOUNT FLOAT OUT)
    AS
    BEGIN
        DECLARE @rID VARCHAR(4000) ;
        DECLARE @vCount FLOAT;


        EXEC Create_Limit @pACCOUNTID, @pMESSAGETYPEID, @pDATE;
        declare @vYEAR bigint;
        declare @vDAILYDATE datetime2;
        declare @vWEEKNUMBER bigint;
        declare @vMONTH bigint;
        declare @vDAILYCOUNT bigint;
        declare @vDAILYAMOUNT numeric(19, 2);
        declare @vWEEKLYCOUNT bigint;
        declare @vWEEKLYAMOUNT numeric(19, 2);
        declare @vMONTHLYCOUNT bigint;
        declare @vMONTHLYAMOUNT numeric(19, 2)
        declare @vYEARLYCOUNT bigint;
        declare @vYEARLYAMOUNT numeric(19, 2);
        BEGIN
            SELECT @vYEAR = YEAR, @vDAILYDATE = DAILYDATE, @vWEEKNUMBER = WEEKNUMBER, @vMONTH = MONTH, @vDAILYCOUNT = DAILYCOUNT,
                   @vDAILYAMOUNT = DAILYAMOUNT, @vWEEKLYCOUNT = WEEKLYCOUNT, @vWEEKLYAMOUNT = WEEKLYAMOUNT, @vMONTHLYCOUNT = MONTHLYCOUNT,
                   @vMONTHLYAMOUNT = MONTHLYAMOUNT, @vYEARLYCOUNT = YEARLYCOUNT, @vYEARLYAMOUNT = YEARLYAMOUNT
            FROM MPAY_CLIENTSLIMITS with (updlock) WHERE ACCOUNTID = @pACCOUNTID AND MESSAGETYPEID = @pMESSAGETYPEID ;
            IF @vYEAR <> YEAR(@pDATE) BEGIN
                SET @vYEAR = YEAR(@pDATE);
                SET @vDAILYDATE = CONVERT(DATE,@pDATE);
                SET @vWEEKNUMBER = convert(NUMERIC,DATEPART( wk, @pDATE));
                SET @vMONTH = MONTH(@pDATE);
                SET @vDAILYCOUNT = @pCOUNT;
                SET @vWEEKLYCOUNT = @pCOUNT;
                SET @vMONTHLYCOUNT = @pCOUNT;
                SET @vYEARLYCOUNT = @pCOUNT;
                SET @vDAILYAMOUNT = @pAMOUNT;
                SET @vWEEKLYAMOUNT = @pAMOUNT;
                SET @vMONTHLYAMOUNT = @pAMOUNT;
                SET @vYEARLYAMOUNT = @pAMOUNT;
            END
            ELSE BEGIN
                SET @vYEARLYCOUNT = @vYEARLYCOUNT + @pCOUNT;
                SET @vYEARLYAMOUNT = @vYEARLYAMOUNT + @pAMOUNT;
                IF @vDAILYDATE <> CONVERT(DATE,@pDATE) BEGIN
                    SET @vDAILYDATE = CONVERT(DATE,@pDATE);
                    SET @vDAILYCOUNT = @pCOUNT;
                    SET @vDAILYAMOUNT = @pAMOUNT;
                END
                ELSE BEGIN
                    SET @vDAILYCOUNT = @vDAILYCOUNT + @pCOUNT;
                    SET @vDAILYAMOUNT = @vDAILYAMOUNT + @pAMOUNT;
                END
                IF @vWEEKNUMBER <> convert(NUMERIC,DATEPART( wk, @pDATE)) BEGIN
                    SET @vWEEKNUMBER = convert(NUMERIC,DATEPART( wk, @pDATE));
                    SET @vWEEKLYCOUNT = @pCOUNT;
                    SET @vWEEKLYAMOUNT = @pAMOUNT;
                END
                ELSE BEGIN
                    SET @vWEEKLYCOUNT = @vWEEKLYCOUNT + @pCOUNT;
                    SET @vWEEKLYAMOUNT = @vWEEKLYAMOUNT + @pAMOUNT;
                END
                IF @vMONTH <> MONTH(@pDATE) BEGIN
                    SET @vMONTH = MONTH(@pDATE);
                    SET @vMONTHLYCOUNT = @pCOUNT;
                    SET @vMONTHLYAMOUNT = @pAMOUNT;
                END
                ELSE BEGIN
                    SET @vMONTHLYCOUNT = @vMONTHLYCOUNT + @pCOUNT;
                    SET @vMONTHLYAMOUNT = @vMONTHLYAMOUNT + @pAMOUNT;
                END
            END
            IF @vDAILYCOUNT < 0 BEGIN
                SET @vDAILYCOUNT = 0;
            END
            IF @vDAILYAMOUNT < 0 BEGIN
                SET @vDAILYAMOUNT = 0;
            END
            IF @vWEEKLYCOUNT < 0 BEGIN
                SET @vWEEKLYCOUNT = 0;
            END
            IF @vWEEKLYAMOUNT < 0 BEGIN
                SET @vWEEKLYAMOUNT = 0;
            END
            IF @vMONTHLYCOUNT < 0 BEGIN
                SET @vMONTHLYCOUNT = 0;
            END
            IF @vMONTHLYAMOUNT < 0 BEGIN
                SET @vMONTHLYAMOUNT = 0;
            END
            IF @vYEARLYCOUNT < 0 BEGIN
                SET @vYEARLYCOUNT = 0;
            END
            IF @vYEARLYAMOUNT < 0 BEGIN
                SET @vYEARLYAMOUNT = 0;
            END
            UPDATE MPAY_CLIENTSLIMITS
            SET
                YEAR = @vYEAR,
                DAILYDATE = @vDAILYDATE,
                DAILYCOUNT = @vDAILYCOUNT,
                DAILYAMOUNT = @vDAILYAMOUNT,
                WEEKNUMBER = @vWEEKNUMBER,
                WEEKLYCOUNT = @vWEEKLYCOUNT,
                WEEKLYAMOUNT = @vWEEKLYAMOUNT,
                MONTH = @vMONTH,
                MONTHLYCOUNT = @vMONTHLYCOUNT,
                MONTHLYAMOUNT = @vMONTHLYAMOUNT,
                YEARLYCOUNT = @vYEARLYCOUNT,
                YEARLYAMOUNT = @vYEARLYAMOUNT
            WHERE ACCOUNTID = @pACCOUNTID AND  MESSAGETYPEID = @pMESSAGETYPEID;
        END;
        SET @rROWCOUNT = @@ROWCOUNT;
    END
GO
/****** Object:  StoredProcedure [dbo].[UpdateBalances]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[UpdateBalances] (
    @accountId bigint,
    @amount numeric(21, 8),
    @vRowsCount int OUT,
    @vBalanceBefore numeric(21, 8) OUT,
    @vBalanceAfter numeric(21, 8) OUT
)

    AS BEGIN DECLARE @vType FLOAT;

SELECT  @vType = BalanceTypeID,@vBalanceBefore = Balance FROM MPAY_Accounts with (updlock) WHERE ID = @accountId;

SET @vBalanceAfter = @vBalanceBefore + @amount;

IF @vType = 1  BEGIN--Debit
UPDATE MPAY_Accounts SET balance = @vBalanceAfter,ACCOUNTUPDATEDATE =getdate() WHERE id = @accountId  AND @vBalanceAfter >= 0;
set @vRowsCount =@@ROWCOUNT;
END

ELSE IF @vType = 2 BEGIN
UPDATE MPAY_Accounts SET balance = @vBalanceAfter ,ACCOUNTUPDATEDATE =getdate() WHERE id = @accountId  AND @vBalanceAfter <= 0;
set @vRowsCount =@@ROWCOUNT;
END

ELSE
BEGIN
UPDATE MPAY_Accounts SET balance = @vBalanceAfter ,ACCOUNTUPDATEDATE =getdate() WHERE id = @accountId;
set @vRowsCount =@@ROWCOUNT;
END


END
GO
/****** Object:  StoredProcedure [dbo].[UpdateRegFileRecordsCount]    Script Date: 9/6/2021 12:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[UpdateRegFileRecordsCount] (
    @fileid        NUMERIC,
    @vrowscount    numeric  OUT ,
    @vRecordsCount  numeric OUT)
AS

BEGIN

update MPAY_Intg_Reg_Files set REGFILEPROCESSEDRECORDS = (REGFILEPROCESSEDRECORDS + 1) where ID=@fileid;
SET @vrowscount =@@rowcount;
COMMIT;
SELECT @vRecordsCount=REGFILEPROCESSEDRECORDS FROM MPAY_Intg_Reg_Files where ID=@fileid;
END
GO
USE [master]
GO
ALTER DATABASE [mpayjfw] SET  READ_WRITE 
GO
